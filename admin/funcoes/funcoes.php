<?php
	/*
	function formatarTexto($texto, $alternativo = false){

		$retorno = isSet($texto) ? $texto : $alternativo;
		$retorno = htmlspecialchars($retorno, ENT_QUOTES);
			
		return $retorno;
	}
	*/
	// Troca apenas os caracteres ', ", <, > e &

	function removeCaracterSpecial($string){

		$from = '��������������������������';
		$to   = 'AAAAEEIOOOUUCaaaaeeiooouuc';

		$string = strtr($string, $from, $to); //subtitui os acentos

		$patterns[0] = '/ /'; 
		
		$replacements[0] = "";
		
		$string = preg_replace($patterns,$replacements,$string); //remover espa�o em branco
		
		$string = preg_replace("/[_\W]/", "", $string); // remover os caracteres especiais

		return $string;

	}

	function formatarNomeArquivo($nomeArquivo, $substituto = ''){

		$nomeArquivo = preg_replace('/[^A-Za-z0-9_.]/', $substituto, $nomeArquivo);

		return $nomeArquivo;

	}

	function formatarTexto($texto){

		$testo_formatado = str_replace("<br />", "", $texto);

		$testo_formatado = htmlspecialchars($testo_formatado);

		$testo_formatado = str_replace("'", "&#039;", $testo_formatado);		

		$testo_formatado = nl2br($testo_formatado);

		return $testo_formatado;

	}

	function redimensionarImagem($imagemAtual, $imagemFundo = "") {


		preg_match_all('/[A-Za-z0-9._]+\.(jpg|jpeg|gif|png)/i', $imagemAtual, $matches);
		$matches = $matches[1][0];

		// Resample
		switch ($matches) {

		  default: return false; break; //N�o consegui redimensionar, usando tamanho normal
		  
		  case "jpg": //caem no mesmo caso...
		  case "JPG": //caem no mesmo caso...
		  case "jpeg":
			
			// Get new dimensions
			list($larguraOriginalImagem, $alturaOriginalImagem) = getimagesize($imagemAtual);

			list($larguraImagemFundo, $alturaImagemFundo) = getimagesize($imagemFundo);

			$larguraImagem = $larguraOriginalImagem;
			$alturaImagem = $alturaOriginalImagem;
			 
			$scale = min($larguraImagemFundo/$larguraOriginalImagem, $alturaImagemFundo/$alturaOriginalImagem); 

			if ($scale < 1)
			{
				$larguraImagem = floor($scale * $larguraOriginalImagem);
				$alturaImagem = floor($scale * $alturaOriginalImagem);

			} 
	 

			$image_p = imagecreatetruecolor($larguraImagem, $alturaImagem);
			$image = imagecreatefromjpeg($imagemAtual);

			imagecopyresampled($image_p, $image, 0, 0, 0, 0, $larguraImagem, $alturaImagem, $larguraOriginalImagem, $alturaOriginalImagem);

			imagejpeg($image_p, $imagemAtual, 100);


			$idImagemFundo = ImageCreateFromJpeg($imagemFundo);

			$image = imagecreatefromjpeg($imagemAtual);

			$dest_x = ( $larguraImagemFundo / 2 ) - ( $larguraImagem / 2 );
			$dest_y = ( $alturaImagemFundo / 2 ) - ( $alturaImagem / 2);
	 
			ImageCopyMerge($idImagemFundo, $image, $dest_x, $dest_y, 0, 0, $larguraImagem, $alturaImagem, 100);

			imagejpeg($idImagemFundo, $imagemAtual, 100);


		  break;
		  
		  case "gif":
			// Get new dimensions
			list($width, $height) = getimagesize($filename);
			$new_width  = $newW;
			$new_height = $newH;
			
			$image_p = imagecreatetruecolor($new_width, $new_height);
			$image = imagecreatefromgif($filename);
			imagecolortransparent($image,imagecolortransparent($image));
			imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
			imagegif($image_p, $filename, 100);
		  break;
		  
		  case "png":

			list($larguraOriginalImagem, $alturaOriginalImagem) = getimagesize($imagemAtual);

			list($larguraImagemFundo, $alturaImagemFundo) = getimagesize($imagemFundo);

			$larguraImagem = $larguraOriginalImagem;
			$alturaImagem = $alturaOriginalImagem;
			 
			$scale = min($larguraImagemFundo/$larguraOriginalImagem, $alturaImagemFundo/$alturaOriginalImagem); 

			if ($scale < 1)
			{
				$larguraImagem = floor($scale * $larguraOriginalImagem);
				$alturaImagem = floor($scale * $alturaOriginalImagem);

			} 
	 

			$image_p = imagecreatetruecolor($larguraImagem, $alturaImagem);
			$image = imagecreatefrompng($imagemAtual);

			imagecopyresampled($image_p, $image, 0, 0, 0, 0, $larguraImagem, $alturaImagem, $larguraOriginalImagem, $alturaOriginalImagem);

			imagejpeg($image_p, $imagemAtual, 100);


			$idImagemFundo = ImageCreateFromJpeg($imagemFundo);

			$image = ImageCreateFromJpeg($imagemAtual);

			$dest_x = ( $larguraImagemFundo / 2 ) - ( $larguraImagem / 2 );
			$dest_y = ( $alturaImagemFundo / 2 ) - ( $alturaImagem / 2);
	 
			ImageCopyMerge($idImagemFundo, $image, $dest_x, $dest_y, 0, 0, $larguraImagem, $alturaImagem, 100);

			imagejpeg($idImagemFundo, $imagemAtual, 100);

		  break;
		}
		
		return true; //Consegui redimensionar.
	}

	function setIf($variable, $if_false = '') {
		return (isSet($variable))? $variable: $if_false;
	}

	function converteValor($price) {
		$price = preg_replace("/[^0-9\.]/", "", str_replace(',','.',$price));
		if (substr($price,-3,1)=='.') {
			$sents = '.'.substr($price,-2);
			$price = substr($price,0,strlen($price)-3);
		} elseif (substr($price,-2,1)=='.') {
			$sents = '.'.substr($price,-1);
			$price = substr($price,0,strlen($price)-2);
		} else {
			$sents = '.00';
		}
		$price = preg_replace("/[^0-9]/", "", $price);
		return number_format($price.$sents,2,'.','');
	}

	function verificar_email($email){
		$mail_correcto = 0;
		//verifico umas coisas
		if ((strlen($email) >= 6) && (substr_count($email,"@") == 1) && (substr($email,0,1) != "@") && (substr($email,strlen($email)-1,1) != "@")){
			if ((!strstr($email,"'")) && (!strstr($email,"\"")) && (!strstr($email,"\\")) && (!strstr($email,"\$")) && (!strstr($email," "))) {
				//vejo se tem caracter .
				if (substr_count($email,".")>= 1){
					//obtenho a termina��o do dominio
					$term_dom = substr(strrchr ($email, '.'),1);
					//verifico que a termina��o do dominio seja correcta
					if (strlen($term_dom)>1 && strlen($term_dom)<5 && (!strstr($term_dom,"@")) ){
						//verifico que o de antes do dominio seja correcto
						$antes_dom = substr($email,0,strlen($email) - strlen($term_dom) - 1);
						$caracter_ult = substr($antes_dom,strlen($antes_dom)-1,1);
						if ($caracter_ult != "@" && $caracter_ult != "."){
						   $mail_correcto = 1;
						}
					}
				}
			}
		}

		if ($mail_correcto)
		   return 1;
		else
		   return 0;

	}
	 
	function converteData($data) {

		$data = explode("/", $data);
		$data = $data[2].'-'.$data[1].'-'.$data[0];
		
		return $data;
	}

	function validaCPF($cpf)
	{	// Verifiva se o n�mero digitado cont�m todos os digitos
		$cpf = str_pad(ereg_replace('[^0-9]', '', $cpf), 11, '0', STR_PAD_LEFT);
		
		// Verifica se nenhuma das sequ�ncias abaixo foi digitada, caso seja, retorna falso
		if (strlen($cpf) != 11 || $cpf == '00000000000' || $cpf == '11111111111' || $cpf == '22222222222' || $cpf == '33333333333' || $cpf == '44444444444' || $cpf == '55555555555' || $cpf == '66666666666' || $cpf == '77777777777' || $cpf == '88888888888' || $cpf == '99999999999')
		{
		return false;
		}
		else
		{   // Calcula os n�meros para verificar se o CPF � verdadeiro
			for ($t = 9; $t < 11; $t++) {
				for ($d = 0, $c = 0; $c < $t; $c++) {
					$d += $cpf{$c} * (($t + 1) - $c);
				}

				$d = ((10 * $d) % 11) % 10;

				if ($cpf{$c} != $d) {
					return false;
				}
			}

			return true;
		}
	}


	function anti_injection($input){

		$input = str_replace('%3c', '<', $input);
		$input = str_replace('%3e', '>', $input);
		$input = str_replace('%22', "'", $input);

		$myFilter = new InputFilter(array(), array(), 0, 0, 1);
		
		$input = $myFilter->process($input);

		#$input = preg_replace(sql_regcase("/(from|update|select|insert|delete|where|drop table|show tables|#|\*|--|\\\\)/"),"",$input);
		$input = trim($input);//limpa espa�os vazio
		$input = htmlspecialchars($input, ENT_QUOTES);
		$input = strip_tags($input);//tira tags html e php
		$input = addslashes($input);//Adiciona barras invertidas a uma string

		return $input;
	}
	
	
	
	function decode_utf8($string)
    {
        $accented = array(
            '�', '�', '�', '�', '�', '�', '�', 'A', 'A',
            '�', 'C', 'C', '�',
            'D', '�',
            '�', '�', '�', '�', '�', '�', '�', 'a', 'a',
            '�', 'c', 'c', '�',
            'd', 'd',
            '�', '�', '�', '�', 'E', 'E',
            'G',
            '�', '�', '�', '�', 'I',
            'L', 'L', 'L',
            '�', '�', '�', '�', 'e', 'e',
            'g',
            '�', '�', '�', '�', 'i',
            'l', 'l', 'l',
            '�', 'N', 'N',
            '�', '�', '�', '�', '�', '�', 'O',
            'R', 'R',
            'S', 'S', '�',
            '�', 'n', 'n',
            '�', '�', '�', '�', '�', 'o',
            'r', 'r',
            's', 's', '�',
            'T', 'T',
            '�', '�', '�', 'U', '�', 'U', 'U',
            '�', '�',
            'Z', 'Z', '�',
            't', 't',
            '�', '�', '�', 'u', '�', 'u', 'u',
            '�', '�',
            'z', 'z', '�',
            '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?',
            '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?',
            '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?',
            '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?'
            );

        $replace = array(
            'A', 'A', 'A', 'A', 'A', 'A', 'AE', 'A', 'A',
            'C', 'C', 'C', 'CE',
            'D', 'D',
            'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'a', 'a',
            'c', 'c', 'c', 'ce',
            'd', 'd',
            'E', 'E', 'E', 'E', 'E', 'E',
            'G',
            'I', 'I', 'I', 'I', 'I',
            'L', 'L', 'L',
            'e', 'e', 'e', 'e', 'e', 'e',
            'g',
            'i', 'i', 'i', 'i', 'i',
            'l', 'l', 'l',
            'N', 'N', 'N',
            'O', 'O', 'O', 'O', 'O', 'O', 'O',
            'R', 'R',
            'S', 'S', 'S',
            'n', 'n', 'n',
            'o', 'o', 'o', 'o', 'o', 'o',
            'r', 'r',
            's', 's', 's',
            'T', 'T',
            'U', 'U', 'U', 'U', 'U', 'U', 'U',
            'Y', 'Y',
            'Z', 'Z', 'Z',
            't', 't',
            'u', 'u', 'u', 'u', 'u', 'u', 'u',
            'y', 'y',
            'z', 'z', 'z',
            'A', 'B', 'B', 'r', 'A', 'E', 'E', 'X', '3', 'N', 'N', 'K', 'N', 'M', 'H', 'O', 'N', 'P',
            'a', 'b', 'b', 'r', 'a', 'e', 'e', 'x', '3', 'n', 'n', 'k', 'n', 'm', 'h', 'o', 'p',
            'C', 'T', 'Y', 'O', 'X', 'U', 'u', 'W', 'W', 'b', 'b', 'b', 'E', 'O', 'R',
            'c', 't', 'y', 'o', 'x', 'u', 'u', 'w', 'w', 'b', 'b', 'b', 'e', 'o', 'r'
            );

        return str_replace($accented, $replace, $string);
    }
	
	function trataTxt($var) {
 $var = strtolower($var);
 $var = ereg_replace("[����]","a",$var);
 $var = ereg_replace("[���]","e",$var);
 $var = ereg_replace("[�����]","o",$var);
 $var = ereg_replace("[���]","u",$var);
 $var = str_replace("�","c",$var);
 return $var;
 }
 
 
 function humanDate($sqlDate) {              //Retorna data no formato D/M/A
    $data = explode('-', $sqlDate);
    return implode('/', array_reverse($data));
  }


   function sqlDate($humanDate) {              //Retorna data no formato A-M-D
    $data = explode('/', $humanDate);
    return implode('-', array_reverse($data));
  }
  
  
  function limpaTexto($texto) {
    $texto = str_replace('"',"'",$texto);
	$texto = str_replace('&lt;','<', $texto);
	$texto = str_replace('&gt;','>', $texto);
    return $texto;
  }
  
  function limpaTextoCompleto($texto) {
    $texto = str_replace('"',"'",$texto);
	$texto = str_replace('&lt;','<', $texto);
	$texto = str_replace('&gt;','>', $texto);
	$texto = str_replace('<p>','', $texto);
	$texto = str_replace('</p>','', $texto);
    return $texto;
  }
  
  

?>