	{literal}
	
	<script type="text/javascript">
	
	// We use a document ready jquery function.
	jQuery(document).ready(function(){
	
	jQuery("#listagemDados").jqGrid({
		
		{/literal}
		url:'index.php?secao=usuario&opcao=listarDados&nd='+new Date().getTime(),
		{literal}
				
		datatype: "json",
	
		colNames:['Nome', 'E-mail'],
		
		colModel:[
			{name:'nome',index:'nome', width:300},
			{name:'email',index:'email', width:300},
		],
		
		pager: jQuery('#paginacao'),
		
		rowNum:20,
		
		rowList:[20,50,100],
		
		imgpath: 'imgs',
		
		sortname: 'nome',
		
		viewrecords: true,
		
		sortorder: "asc",
		
		multiselect: true, 
		
		caption: "Listagem de usu�rios"
	});
	
	jQuery("#btn_alterar").click( 
		function(){ 
					
			var id = jQuery("#listagemDados").getGridParam('selarrrow'); 

			if(id.length == 1){

				{/literal}			
				window.location = 'index.php?secao=usuario&opcao=alterar&idUsuario='+ id;
				{literal}						

			}else{
			
				if(id.length == 0)
					alert("Selecione o usu�rio que deseja alterar.");
				else 
					alert("Selecione apenas um usu�rio.");
			}				
		}
	
	); 
	
	jQuery("#btn_excluir").click( 
		function(){ 
		
			var listaIdUsuarioExcluir; 
			
			listaIdUsuarioExcluir = jQuery("#listagemDados").getGridParam('selarrrow'); 
			
			if (listaIdUsuarioExcluir != '') { 
				
				if(confirm("Tem certeza que gostaria de excluir estes itens?")){
				
					{/literal}	
					window.location = 'index.php?secao=usuario&opcao=excluir&listaIdUsuarioExcluir='+ listaIdUsuarioExcluir;
					{literal}	
				} 
				
			} 
			else { 
				alert("Selecione os usu�rios que deseja excluir.");
			} 						
			
		}
	
	);
	
	});

	</script>	
	{/literal}
    <td class="mainContentArea">	
	<br />		
	<table class="searchTable" border="0" cellpadding="0" cellspacing="0" width="">
    <tbody>
	<tr> 	 
		{if $podeIncluir}
		<td align="center" width="130">
		<a href="index.php?secao=usuario&opcao=inserir" class="plainLink"> <img src="imgs/adicionar.png" border="0" width="48" height="48" /> <BR />Incluir novo usu&aacute;rio</a>
		</td>
		{/if}
		
		{if $podeAlterar}
		<td align="center" width="112">
		<a href="#" class="plainLink" id="btn_alterar"> <img src="imgs/alterar.png" border="0" width="48" height="48" /> <BR />Alterar usu&aacute;rio</a>
		</td>
		{/if}		
		{if $podeExcluir}
		<td align="center" width="112">	
		<a href="#" class="plainLink" id="btn_excluir"> <img src="imgs/remover.png" border="0" width="48" height="48" /> <BR />
		Excluir usu&aacute;rio(s)</a>  
		</td>		
		{/if}		
	</tr>
	<tr>
		<td align="center"></td>		
	</tr>
	</tbody>
	</table> 	
	<bR />	
	<table id="listagemDados" class="scroll" cellpadding="0" cellspacing="0"></table>

<!-- pager definition. class scroll tels that we want to use the same theme as grid -->
	<div id="paginacao" class="scroll" style="text-align:center;"></div>
				
	</td>