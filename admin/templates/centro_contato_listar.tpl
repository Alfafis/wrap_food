	{literal}
	
	<script type="text/javascript">
	 
	jQuery(document).ready(function(){
	
	jQuery("#listagemDados").jqGrid({
		
		{/literal}
		url:'index.php?secao=contato&opcao=listar&nd='+new Date().getTime(),
		{literal}
				
		datatype: "json",
	
		colNames:['C&oacute;digo', 'Nome', 'E-mail', 'Telefone', 'Cidade', 'Estado', 'Data Envio'],
		
		colModel:[
			{name:'id',index:'id', width:60},
			{name:'nome',index:'nome', width:250},
			{name:'email',index:'email', width:200},
			{name:'telefone',index:'telefone', width:100},
			{name:'cidade',index:'cidade', width:100},
			{name:'estado',index:'estado', width:100},
			{name:'data_envio',index:'data_envio', width:140}
		],
		
		pager: jQuery('#paginacao'),
		
		rowNum:100,
		
		rowList:[100,200,300],
		
		imgpath: 'imgs',
		
		sortname: 'data_envio',
		
		viewrecords: true,
		
		sortorder: "asc",
		
		multiselect: true, 
		
		caption: "Listagem de Contatos"
	});
	
	
	jQuery("#btn_excluir").click( 
		function(){ 
		
			var listaIdForum; 
			
			listaIdForum = jQuery("#listagemDados").getGridParam('selarrrow'); 
			
			if (listaIdForum != '') { 
				
				if(confirm("Tem certeza que gostaria de excluir este(s) iten(s)?")){
				
					{/literal}	
					window.location = 'index.php?secao=contato&opcao=excluir&listaIdForum='+ listaIdForum;
					{literal}	
				} 
				
			} 
			else { 
				alert("Selecione o(s) contato(s) que deseja excluir.");
			} 						
			
		}
		
	);
	
	jQuery("#btn_visualizar").click( 
		function(){ 
		
			var listaIdForum; 
			
			listaIdForum = jQuery("#listagemDados").getGridParam('selarrrow'); 
			
			if (listaIdForum != '') { 
				
					{/literal}	
					    window.open('index.php?secao=contato&opcao=visualizar&listaIdForum='+ listaIdForum,'page','toolbar=no,location=yes,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=600');  
					//window.location = 'index.php?secao=contato&opcao=visualizar&listaIdForum='+ listaIdForum;
					{literal}
					
				
			} 
			else { 
				alert("Selecione o contato que deseja visualizar.");
			} 						
			
		}
	);
	
	});

	</script>	
	{/literal}
	<td class="mainContentArea">	
	<br />		
	<table class="searchTable" border="0" cellpadding="0" cellspacing="0" width="">
    <tbody>
	<tr> 	 
		
		{if $podeExcluir}
		<td align="center" width="112">	
		<a href="#" class="plainLink" id="btn_excluir"> <img src="imgs/remover.png" border="0" width="48" height="48" /><br />Excluir contato(s)</a>  
		</td>		
		{/if}
        
		<td align="center" width="130">	
		<a href="#" id="btn_visualizar" class="plainLink"> <img src="imgs/visualizar.png" border="0" width="48" height="48" /> <BR />
		Visualizar contato</a>  
		</td>		
          
	</tr>
	<tr>
		<td align="center"></td>		
	</tr>
	</tbody>
	</table>  	
	<br />	
	<table id="listagemDados" class="scroll" cellpadding="0" cellspacing="0"></table>
<!-- pager definition. class scroll tels that we want to use the same theme as grid -->
	<div id="paginacao" class="scroll" style="text-align:center;"></div>				
	</td>