{literal}
<script>

	function validarFormulario(){
	
		if(document.form_usuario.nome.value == ""){
			alert("Preencha o campo Nome.");
			document.form_usuario.nome.focus();
			return false;
		} 
						
		if(document.form_usuario.email.value != ""){
		
			if(validarEmail(document.form_usuario.email.value) == false){
				alert("Informe um E-mail v�lido.");
				document.form_usuario.email.focus();
				return false;
			}
		}

	
		if(document.form_usuario.login.value == ""){
			alert("Preencha o campo Login.");
			document.form_usuario.login.focus();
			return false;
		}
		
	
		if(document.form_usuario.senha.value == ""){
			alert("Preencha o campo Senha.");
			document.form_usuario.senha.focus();
			return false;
		}			
			
		document.form_usuario.submit();
	}
	
</script>
{/literal}


<td class="mainContentArea">

<form name="form_usuario" method="post" action="index.php?secao=usuario&opcao=salvar" enctype="multipart/form-data">
 
<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
<tbody>
<tr>
	<td colspan="2" class="buttonRow">
	<input name="save" value="Salvar" onclick="validarFormulario();" class="normalButton" type="button">
	<input name="cancel" value="Cancelar" onClick="window.location='index.php?secao=usuario&opcao=listar'" class="normalButton" type="button">
	</td>
</tr>

<tr>
    <td colspan="2">&nbsp;</td>
</tr>

<tr>
    <td colspan="2" class="mainHeader">Inserindo usu&aacute;rio</td>
</tr>

<tr>
	<td colspan="2">&nbsp;</td>
</tr>

<tr>
    <td valign="top" width="50%">
	<table border="0" cellpadding="2" cellspacing="0" width="100%">
	<tbody>
	<tr>
		<td class="labelCell" align="right" width="15%">Nome:</td>
		<td class="contentCell"><input name="nome" id="nome" size="45" value="" class="inputBox" type="text"></td>
    </tr> 
	 
	<tr>
		<td class="labelCell" align="right">E-mail:</td>
		<td class="contentCell"><input name="email" size="45" value="" class="inputBox" type="text"></td>
    </tr> 
	
   
	<tr>
		<td class="labelCell" align="right">Login:</td>
		<td class="contentCell"><input name="login" id="login" size="21" maxlength="20" value="" class="inputBox" type="text"></td>
    </tr> 
	
	
	<tr>
		<td class="labelCell" align="right">Senha:</td>
		<td class="contentCell"><input name="senha" id="senha" size="11" maxlength="10" value="" class="inputBox" type="password"></td>
    </tr> 
	
	<tr>	
		<td>&nbsp;</td>
	</tr>
	
		
	<tr>
		<td class="labelCell" align="right" valign="top">Permiss&otilde;es:</td>
		<td class="contentCell">

		{foreach from=$listaModuloUsuario item=modulo}
		
		&bull; {$modulo[1]}<BR />
		  
		{html_checkboxes name="listaIdModuloHasFuncionalidadeModulo" options=$modulo[2] separator="<br />"}
		
		<BR />

		{/foreach}
		
		</td>
    </tr> 
	
	</tbody>
	</table>
    </td>
	
    <td align="left" valign="top" width="50%">&nbsp;</td>
</tr>
<tr>
    <td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" class="buttonRow">
	<input name="save" value="Salvar" onclick="validarFormulario();" class="normalButton" type="button">
	<input name="cancel" value="Cancelar" onClick="window.location='index.php?secao=usuario&opcao=listar'" class="normalButton" type="button">

	</td>
</tr>
</tbody>
</table>

</form>
</td>


<script>

document.getElementById('nome').focus();

</script>