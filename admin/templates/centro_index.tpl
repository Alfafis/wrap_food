{if $bannerPrincipal} 
<div class="swiper-container">
    <div class="swiper-wrapper">
      {foreach from=$bannerPrincipal item=banner}
      <div class="swiper-slide"><img class="full-image" src="banner/{$banner.arquivo}" alt="{$banner.nome}"/></div>
      {/foreach}
    </div>
    <!-- Add Pagination -->
    <div class="swiper-pagination"></div>
</div> 
{else}
<br /><br />
{/if}


<div id="container_swip">
    <h3 class="text-center">Escolha o Produto</h3>
	
    {foreach from=$listaPao item=pao}
    <div class="list no-padding">
      <div class="item item-menu" ng-repeat="category in categories" onclick="location.href='?secao=produtos&pao={$pao.id}'">
        <img src="fotos/{$pao.foto}" alt=""/>

        <div class="overlay">
          <span class="pull-left light">
            {$pao.nome}
          </span>
        </div>
      </div>
    </div>
    {/foreach}
    
    

</div><!-- fim cotainer sw -->
{literal}
<!-- Swiper JS -->
<script src="js/swiper.min.js"></script>

<!-- Initialize Swiper -->
<script>
var swiper = new Swiper('.swiper-container', {
	
  loop: true,
  autoplay: {
    delay: 2000,
  },
  pagination: {
    el: '.swiper-pagination',
  },
});
</script>
{/literal}