	{literal}
	
	<script type="text/javascript">
	 
	jQuery(document).ready(function(){
	
	jQuery("#listagemDados").jqGrid({
		
		{/literal}
		url:'index.php?secao=produtos&opcao=listar&nd='+new Date().getTime(),
		{literal}
				
		datatype: "json",
	
		colNames:['C&oacute;digo', 'Nome', 'Pre&ccedil;o', 'Tipo', 'Preparo', 'Ativo', 'Data Cadastro'],
		
		colModel:[
			{name:'id',index:'id', width:60},
			{name:'nome',index:'nome', width:150},
			{name:'preco',index:'preco', width:80},
			{name:'categoria',index:'categoria', width:80},
			{name:'tempo',index:'tempo', width:100},
			{name:'ativo',index:'ativo', width:80},
			{name:'data_cadastro',index:'data_cadastro', width:140}
		],
		
		pager: jQuery('#paginacao'),
		
		rowNum:100,
		
		rowList:[100,200,300],
		
		imgpath: 'imgs',
		
		sortname: 'categoria, preco, nome',
		
		viewrecords: true,
		
		sortorder: "asc",
		
		multiselect: true, 
		
		caption: "Listagem de produtos"
	});
	
	jQuery("#btn_alterar").click( 
		function(){ 
					
			var id = jQuery("#listagemDados").getGridParam('selarrrow'); 

			if(id.length == 1){

				{/literal}			
				window.location = 'index.php?secao=produtos&opcao=alterar&idProduto='+ id;
				{literal}						

			}else{
			
				if(id.length == 0)
					alert("Selecione o produtos que deseja alterar.");
				else 
					alert("Selecione apenas um forum.");
			}				
		}
	
	); 
	
	jQuery("#btn_excluir").click( 
		function(){ 
		
			var listaIdForum; 
			
			listaIdForum = jQuery("#listagemDados").getGridParam('selarrrow'); 

			if (listaIdForum != '') { 
				
				if(confirm("Tem certeza que gostaria de excluir este(s) iten(s)?")){
				
					{/literal}	
					window.location = 'index.php?secao=produtos&opcao=excluir&listaIdForum='+ listaIdForum;
					{literal}	
				} 
				
			} 
			else { 
				alert("Selecione o(s) produtos(s) que deseja excluir.");
			} 						
			
		}
		
	);
	
	jQuery("#btn_aprovar").click( 
		function(){ 
		
			var listaIdForum; 
			
			listaIdForum = jQuery("#listagemDados").getGridParam('selarrrow'); 
			
			if (listaIdForum != '') { 
				
				if(confirm("Tem certeza que gostaria de aprovar este(s) iten(s)?")){
				
					{/literal}	
					window.location = 'index.php?secao=produtos&opcao=aprovar&listaIdForum='+ listaIdForum;
					{literal}	
				} 
				
			} 
			else { 
				alert("Selecione a(s) produtos(s) que deseja aprovar.");
			} 						
			
		}
	);
	
	jQuery("#btn_visualizar").click( 
		function(){ 
		
			var listaIdForum; 
			
			listaIdForum = jQuery("#listagemDados").getGridParam('selarrrow'); 
			
			if (listaIdForum != '') { 
				
					{/literal}	
					    //window.open('index.php?secao=produtos&opcao=visualizar&listaIdForum='+ listaIdForum,'page','toolbar=no,location=yes,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=600');  
					window.location = 'index.php?secao=benner&opcao=visualizar&listaIdForum='+ listaIdForum;
					{literal}
					
				
			} 
			else { 
				alert("Selecione a(s) postagem(s) que deseja visualizar.");
			} 						
			
		}
	);
	
	});
	
	var nome = ""; 
	
	function pesquisar(){ 
	
		nome = jQuery("#nome").val(); 
		 
		jQuery("#listagemDados").setGridParam({url:'index.php?secao=produtos&opcao=listar&nome='+nome+'&nd='+new Date().getTime(),page:1}).trigger("reloadGrid"); 

	} 

	</script>	
	{/literal}
	<td class="mainContentArea">	
	<br />		
	<table class="searchTable" border="0" cellpadding="0" cellspacing="0" width="">
    <tbody>
	<tr> 	 
		{if $podeIncluir}
		<td align="center" width="112">
		<a href="index.php?secao=produtos&opcao=inserir" class="plainLink"> <img src="imgs/adicionar.png" border="0" width="48" height="48" /><br />Cadastrar produto </a>
		</td>
		{/if}	
		{if $podeAlterar}
		<td align="center" width="112">
		<a href="#" class="plainLink" id="btn_alterar"> <img src="imgs/alterar.png" border="0" width="48" height="48" /><br />Alterar produto</a>
		</td>
		{/if}
		{if $podeExcluir}
		<td align="center" width="112">	
		<a href="#" class="plainLink" id="btn_excluir"> <img src="imgs/remover.png" border="0" width="48" height="48" /><br />Excluir produto(s)</a>  
		</td>		
		{/if}
        
        {if $podeAprovar}
		<td align="center" width="112">	
		<a href="#" class="plainLink" id="btn_aprovar"> <img src="imgs/aceitar.png" border="0" width="48" height="48" /> <BR />
		Aprovar produto(s)</a>  
		</td>		
		{/if}
        
        {if $podeReprovar}
		<td align="center" width="130">	
		<a href="#" class="plainLink" id="btn_reprovar"> <img src="imgs/application_remove.png" border="0" width="48" height="48" /> <BR />
		Reprovar produto(s)</a>  
		</td>		
		{/if}
        
        {if $podeVisualizar}
		<td align="center" width="130">	
		<a href="#" id="btn_visualizar" class="plainLink"> <img src="imgs/visualizar.png" border="0" width="48" height="48" /> <BR />
		Visualizar produto(s)</a>  
		</td>		
		{/if}
          
	</tr>
	<tr>
		<td align="center"></td>		
	</tr>
	</tbody>
	</table>  	
	<br />	
	 
	<div style="padding-left:20px">	
		<div style="font-size:12px">Pesquisar por:</div> 
		
		<div>  		 
		Nome<br />  
		<input name="nome" size="40" value="{$nome}" class="inputBox" type="text" id="nome" onKeyPress="return pesquisarUsandoTeclaEnter(event)">
		
		<button onclick="pesquisar()" id="submitButton" style="margin-left:10px;">Pesquisar</button> 
		</div> 
		
		
	</div>	 
	
	<BR />
    
	<table id="listagemDados" class="scroll" cellpadding="0" cellspacing="0"></table>
<!-- pager definition. class scroll tels that we want to use the same theme as grid -->
	<div id="paginacao" class="scroll" style="text-align:center;"></div>				
	</td>