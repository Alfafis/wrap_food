{literal}
<script>
	function validarFormulario()
	{		 		
				
		if(document.form_marca.data_envio.value == ""){
			alert("Marque a data de envio.");
			document.form_marca.data_envio.focus();
			return false;
		} 
				
		document.form_marca.submit();
	}
	
	$(function() {

		$('#data_envio').datepicker({
			changeMonth: true,
			changeYear: true
		});

	}); 
</script>
{/literal}
<td class="mainContentArea">
    <form name="form_marca" method="post" action="index.php?secao=pedido&opcao=marcar_envio_salvar" enctype="multipart/form-data">
        <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
            <tbody>
                <tr>
                    <td colspan="2" class="buttonRow">
                    <input name="save" value="" onclick="validarFormulario();" class="normalButtonSalvar" type="button">
                    <input name="cancel" value="" onClick="window.location='index.php?secao=pedido'" class="normalButtonCancelar" type="button">
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="mainHeader">Cadastro de entrega do pedido {$idPedido} </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top" width="70%">
                        <table border="0" cellpadding="2" cellspacing="0" width="100%">
                            <tbody>	
                            	<input type="hidden" name="idPedido" value="{$idPedido}" />
                            	                                
                                <tr>
                                    <td class="labelCell" align="left" width="10%">Data do envio:</td>
                                    <td class="contentCell" align="left">
                                    <input name="data_envio" type="text" class="inputBox" id="data_envio" value="{$data_envio}" />
                                    </td>
                                </tr>
                                                               
                                
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2" class="buttonRow">
                    <input name="save" value="" onclick="validarFormulario();" class="normalButtonSalvar" type="button">
                    <input name="cancel" value="" onClick="window.location='index.php?secao=pedido'" class="normalButtonCancelar" type="button">                     
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</td>
