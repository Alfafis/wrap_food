<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<center>
<a href="#" onclick="document.imprimeEXE.submit();">Gerar excel</a>
</center>
<BR />
{$botoes_controle_paginas}

<div id="impressao">
 <h1 style="text-align:left;font-size:13px;line-height:normal;"><center>Relatório de freela(s) do mês de {if $mesc == "01"}Janeiro{/if}
    {if $mes == "02"}Fevereiro{/if}
    {if $mes == "03"}Mar&ccedil;o{/if}
    {if $mes == "04"}Abril{/if}
    {if $mes == "05"}Maio{/if}
    {if $mes == "06"}Junho{/if}
    {if $mes == "07"}Julho{/if}
    {if $mes == "08"}Agosto{/if}
    {if $mes == "09"}Setembro{/if}
    {if $mes == "10"}Outubro{/if}
    {if $mes == "11"}Novembro{/if}
    {if $mes == "12"}Dezembro{/if}</center>
 Nome:&nbsp;<span style="color:#F00">{$nomeFuncionario}</span>
 <br />Cargo:&nbsp;<span style="color:#F00">{$descricao_m}</span>
 </h1>
 
 
 <table cellpadding="0" cellspacing="0">
	<tr>
        <td width="150px" class="td_c" align="center"><strong>Dia</strong></td>
        <td width="150px" class="td_c" align="center"><strong>Hora Entrada</strong></td>
        <td width="150px" class="td_c" align="center"><strong>Hora Saída</strong></td>
        <td width="150px" class="td_c" align="center"><strong>Tipo</strong></td>
        <td width="150px" class="td_c" align="center"><strong>Tempo trabalhado</strong></td>
    </tr>
    {foreach from=$listaDados item=dados}
    <tr style="color:{if $dados[4] == 5}#090{/if}{if $dados[4] == 6}#f00{/if}">
        <td class="td_c" align="center">{$dados[0]}</td>
        <td class="td_c" align="center">{$dados[1]}</td>
        <td class="td_c" align="center">{if $dados[4] != 5 && $dados[4] != 6}{$dados[2]}{/if}</td>
        <td class="td_c" align="center">
        {if $dados[4] == 1}Horas serviço{/if}
        {if $dados[4] == 2}Almoço{/if}
        {if $dados[4] == 3}Lanche{/if}
        {if $dados[4] == 4}Freelancer{/if}
        {if $dados[4] == 5}Adição Freelancer{/if}
        {if $dados[4] == 6}Retirada Freelancer{/if}
        </td>
        <td class="td_c" align="center">{if $dados[4] != 5 && $dados[4] != 6}{if $dados[2] != ""}{if $dados[2] != ""}{$dados[3]}{/if}{/if}{else}{$dados[1]}{/if}</td>
    </tr>    
    {/foreach}
    <tr>
        <td class="td_c" colspan="4"><strong>Total Tempo</strong></td>
        <td class="td_c" align="center">{$total}</td>
    </tr>    
    
    
</table>

</div>  
  
<form target="_blank" name="imprimeEXE" action="excel/gerar_excel.php" method="post">
	<input type="hidden" name="nome_relatorio" value="{$mes}_{$ano}_{$nomeFuncionario}">
    <input type="hidden" name="conteudoexe">
</form>
				
{literal}
<script language="javascript">
	document.imprimeEXE.conteudoexe.value = document.getElementById('impressao').innerHTML;
</script>
{/literal}
  