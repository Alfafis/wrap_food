{literal}
<script>
	function validarFormulario()
	{		 		
		if(document.form_marca.nome.value == ""){
			alert("Preencha o campo Nome.");
			document.form_marca.nome.focus();
			return false;
		}
							
		document.form_marca.submit();
	}
	 
</script>
{/literal}
<td class="mainContentArea">
    <form name="form_marca" method="post" action="index.php?secao=banner&opcao=salvarAlteracao" enctype="multipart/form-data">
    <input type="hidden" name="id" value="{$banner.id}" />
        <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
            <tbody>
                <tr>
                    <td colspan="2" class="buttonRow">
                    <input name="save" value="" onclick="validarFormulario();" class="normalButtonSalvar" type="button">
                    <input name="cancel" value="" onClick="window.location='index.php?secao=banner'" class="normalButtonCancelar" type="button">
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="mainHeader">Altera&ccedil;&atilde;o de Banner </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top" width="70%">
                        <table border="0" cellpadding="2" cellspacing="0" width="100%">
                            <tbody>	
                            	
                                <tr>
                                    <td class="labelCell" align="right">T&iacute;tulo:</td>
                                    <td class="contentCell"><input type="text" name="nome" size="50" value="{$banner.nome}" class="inputBox" id="nome"></td>
                                </tr>
                                
                                
                                <tr>
                                    <td class="labelCell" align="right">Posi&ccedil;&atilde;o:</td>
                                    <td class="contentCell"><select name="posicao" class="inputBox" id="posicao">
                                    <option value="1" {if $banner.posicao == 1}selected="selected"{/if}>1</option>
                                    <option value="2" {if $banner.posicao == 2}selected="selected"{/if}>2</option>
                                    <option value="3" {if $banner.posicao == 3}selected="selected"{/if}>3</option>
                                    <option value="4" {if $banner.posicao == 4}selected="selected"{/if}>4</option>
                                    <option value="5" {if $banner.posicao == 5}selected="selected"{/if}>5</option>
                                    <option value="6" {if $banner.posicao == 6}selected="selected"{/if}>6</option>
                                    </select></td>
                                </tr>
                                <tr>
                                    <td class="labelCell" align="right">Arquivo:</td>
                                    <td class="contentCell">
                                    {if $banner.arquivo}
                                    <img src="../banner/{$banner.arquivo}" width="300px" style="border:solid 1px #CCCCCC;" />
                                    <br />
                                    {/if}
                                    <input type="file" name="arquivo" size="50" value="" class="inputBox" id="arquivo">
                                    <br />Dimens&otilde;es:<br /> 
                                    800px X 400px<br />
                                    </td>
                                </tr>
                                
                                
                                
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2" class="buttonRow">
                    <input name="save" value="" onclick="validarFormulario();" class="normalButtonSalvar" type="button">
                    <input name="cancel" value="" onClick="window.location='index.php?secao=banner'" class="normalButtonCancelar" type="button">                     
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</td>
<script>
	document.getElementById('nome').focus();
</script>
 