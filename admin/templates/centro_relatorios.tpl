	{literal}
	<script src="js/sorttable.js" type="text/javascript"></script>
	<script type="text/javascript">
	 
	jQuery(document).ready(function(){
	
	
	jQuery("#btn_pedidos").click( 
		function(){ 
			window.location = 'index.php?secao=relatorio&opcao=pedidos';
		}
	
	); 
	
	
	jQuery("#btn_produtos").click( 
		function(){ 
			window.location = 'index.php?secao=relatorio&opcao=produtos';
		}
	
	); 
	
	
	});
	
	
	</script>	
	{/literal}
	<td class="mainContentArea">	
	<br />		
	<table class="searchTable" border="0" cellpadding="0" cellspacing="0" width="">
    <tbody>
	<tr> 	 
		
		
        {if $podeVisualizar}
		<td align="center" width="130">	
		<a href="#" id="btn_pedidos" class="plainLink"> <img src="imgs/visualizar.png" border="0" width="48" height="48" /> <BR />
		Relat&oacute;rio Pedidos</a>  
		</td>
        <td align="center" width="130">	
		<a href="#" id="btn_produtos" class="plainLink"> <img src="imgs/visualizar.png" border="0" width="48" height="48" /> <BR />
		Relat&oacute;rio Produtos</a>  
		</td>		
		{/if}
        
          
	</tr>
	<tr>
		<td align="center"></td>		
	</tr>
	</tbody>
	</table>  	
	<br />	
	
    {if $opcao != ""}
    {if $listaRelatorio}
    <center>    
    <a href="#" onclick="document.imprimeEXE.submit();"><img src="imgs/excel.jpg" width="100" /><br />Gerar excel</a>
    <BR />
    {$botoes_controle_paginas}
    </center>
    <BR /><BR />
    {/if}
        {if $opcao == "pedidos"}
        {if $listaRelatorio}
        <div id="impressao">
         <h1 style="text-align:left;font-size:13px;line-height:normal;"><center>Relatório de Pedidos</h1>
         
         
         <table cellpadding="0" cellspacing="0" class="sortable">
            <tr style="background-color:#CCC;cursor:pointer;">
                <td class="td_c" align="center"><strong>Pedido</strong></td>
                <td class="td_c" align="center"><strong>Cliente</strong></td>
                <td class="td_c" align="center"><strong>Data_Compra</strong></td>
                <td class="td_c" align="center"><strong>Valor_Compra</strong></td>
                <td class="td_c" align="center"><strong>Status</strong></td>
                <td class="td_c" align="center"><strong>Desconto_Cupom</strong></td>
                <td class="td_c" align="center"><strong>Data_Pagamento</strong></td>
                <td class="td_c" align="center"><strong>Valor_Pagamento</strong></td>
                <td class="td_c" align="center"><strong>Tipo_Pagamento</strong></td>
                <td class="td_c" align="center"><strong>Hora_Agendamento</strong></td>
                <td class="td_c" align="center"><strong>Data_Pronto</strong></td>
            </tr>
            {foreach from=$listaRelatorio item=dados}
            <tr style="background-color:{$dados.cor}">
                <td class="td_c" align="center">{$dados.id}</td>
                <td class="td_c" align="center">{$dados.cliente}</td>
                <td class="td_c" align="center">{$dados.data_compra}</td>
                <td class="td_c" align="center">R$ {$dados.valorTotal|replace:".":","}</td>
                <td class="td_c" align="center">{$dados.status}</td>
                <td class="td_c" align="center">{$dados.desconto}%</td>
                <td class="td_c" align="center">{$dados.data_pagamento}</td>
                <td class="td_c" align="center">R$ {$dados.valor|replace:".":","}</td>
                <td class="td_c" align="center">{$dados.id_tipo_pagamento}</td>
                <td class="td_c" align="center">{$dados.agendado}</td>
                <td class="td_c" align="center">{$dados.data_pronto}</td>
            </tr>    
            {/foreach}
            
        </table>
        
        </div> 
        <br /><br />
        {else}
        <p style="text-align:center;font-size:12px;">Sem dados a serem exibidos.</p>
        {/if}
        
        {/if}<!-- fim if se for pedidos --> 
        
        
        
        {if $opcao == "produtos"}
        {if $listaRelatorio}
        <div id="impressao">
         <h1 style="text-align:left;font-size:13px;line-height:normal;"><center>Relatório de Produtos</h1>
         
         
         <table cellpadding="0" cellspacing="0" class="sortable">
            <tr style="background-color:#CCC;cursor:pointer;">
                <td class="td_c" align="center"><strong>Tipo</strong></td>
                <td class="td_c" align="center"><strong>Produto</strong></td>
                <td class="td_c" align="center"><strong>Qtde</strong></td>
                <td class="td_c" align="center"><strong>Valor</strong></td>
                <td class="td_c" align="center"><strong>Valor_Adicionais</strong></td>
                
                <td class="td_c" align="center"><strong>Pedido</strong></td>
                <td class="td_c" align="center"><strong>Cliente</strong></td>
                <td class="td_c" align="center"><strong>Data_Compra</strong></td>
                <td class="td_c" align="center"><strong>Valor_Compra</strong></td>
                <td class="td_c" align="center"><strong>Status</strong></td>
                <td class="td_c" align="center"><strong>Desconto_Cupom</strong></td>
                <td class="td_c" align="center"><strong>Data_Pagamento</strong></td>
                <td class="td_c" align="center"><strong>Valor_Pagamento</strong></td>
                <td class="td_c" align="center"><strong>Tipo_Pagamento</strong></td>
                <td class="td_c" align="center"><strong>Hora_Agendamento</strong></td>
                <td class="td_c" align="center"><strong>Data_Pronto</strong></td>
            </tr>
            {foreach from=$listaRelatorio item=dados}
            <tr style="background-color:{$dados.cor}">
            	<td class="td_c" align="center">{$dados.pao}</td>
                <td class="td_c" align="center">{$dados.produto}</td>
                <td class="td_c" align="center">{$dados.qtde}</td>
                <td class="td_c" align="center">R$ {$dados.preco_prod|replace:".":","}</td>
                <td class="td_c" align="center">R$ {if $dados.id_pao != 3}{$dados.preco_suco|replace:".":","}{else}{$dados.preco_adicionais|replace:".":","}{/if}</td>
                
                <td class="td_c" align="center">{$dados.pedido}</td>
                <td class="td_c" align="center">{$dados.cliente}</td>
                <td class="td_c" align="center">{$dados.data_compra}</td>
                <td class="td_c" align="center">R$ {$dados.valorTotal|replace:".":","}</td>
                <td class="td_c" align="center">{$dados.status}</td>
                <td class="td_c" align="center">{$dados.desconto}%</td>
                <td class="td_c" align="center">{$dados.data_pagamento}</td>
                <td class="td_c" align="center">R$ {$dados.valor|replace:".":","}</td>
                <td class="td_c" align="center">{$dados.id_tipo_pagamento}</td>
                <td class="td_c" align="center">{$dados.agendado}</td>
                <td class="td_c" align="center">{$dados.data_pronto}</td>
            </tr>    
            {/foreach}
            
        </table>
        
        </div> 
        <br /><br />
        {else}
        <p style="text-align:center;font-size:12px;">Sem dados a serem exibidos.</p>
        {/if}
        
        {/if}<!-- fim if se for pedidos --> 
    
    {else}
    
    	<p style="text-align:center;font-size:12px;">Escolha um dos relat&oacute;rios.</p>
    
    {/if}
      
    <form target="_blank" name="imprimeEXE" action="excel/gerar_excel.php" method="post">
        <input type="hidden" name="conteudoexe">
    </form>
                    
    {literal}
    <script language="javascript">
        document.imprimeEXE.conteudoexe.value = document.getElementById('impressao').innerHTML;
    </script>
    {/literal}
      
   		
	</td>