<html>
	<head>
	
	<title>.:: WRAP BRASIL ::.</title>
	<meta charset="utf-8">
	<link rel="icon" href="favicon.ico" type="image/x-icon" />
	
	<link rel="stylesheet" type="text/css" href="css/main.css" />		
	<link rel="stylesheet" type="text/css" href="css/print.css" media="print" />	
	<link rel="stylesheet" type="text/css" href="css/jquery.jdMenu.css" />	
	<link rel="stylesheet" type="text/css" href="css/ui/ui.all.css" />	
	<link rel="stylesheet" type="text/css" href="css/ui/demos.css" />	
	<link rel="stylesheet" type="text/css" href="css/jquery.autocomplete.css" />	
	<link rel="stylesheet" type="text/css" href="css/grid.css" media="screen" />	
	<link rel="stylesheet" type="text/css" href="css/jquery.tabs.css" media="print, projection, screen" />	
	<link rel="stylesheet" type="text/css" href="css/main.css" />	
	<link type="text/css" href="lytebox.css" rel="stylesheet" />
	{literal}
	
	<style type="text/css">
	#progreso {
	  background: url(imgs/textarea.png) no-repeat;
	  background-position: -300px 0px;
	  width: 300px;
	  height: 14px;
	  text-align: center;
	  color: #000000;
	  font-size: 8pt;
	  font-family: Arial;
	  text-transform: uppercase;
	}
	
	</style>
	
	<script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript">
	
	
	jQuery(function($){
	   $("#cep").change(function(){
		  var cep_code = $(this).val();
		  if( cep_code.length <= 0 ) return;
		  $.get("http://apps.widenet.com.br/busca-cep/api/cep.json", { code: cep_code },
			 function(result){
				if( result.status!=1 ){
				   alert(result.message || "Houve um erro desconhecido");
				   return;
				}
				$("input#cep").val( result.code );
				$("input#estado").val( result.state );
				$("input#cidade").val( result.city );
				$("input#bairro").val( result.district );
				$("input#endereco").val( result.address );
				$("input#estado").val( result.state );
				$("input#numero").focus();
			 });
	   });
	});
	</script>
    
	<script type="text/javascript" src="lytebox.js"></script>
	
	<script type="text/javascript" src="js/jdMenu/jquery.dimensions.js"></script>
	<script type="text/javascript" src="js/jdMenu/jquery.positionBy.js"></script>
	<script type="text/javascript" src="js/jdMenu/jquery.bgiframe.js"></script>
	<script type="text/javascript" src="js/jdMenu/jquery.jdMenu.js"></script>
	 
	<script type='text/javascript' src='js/autocomplete/jquery.bgiframe.min.js'></script>
	<script type='text/javascript' src='js/autocomplete/jquery.dimensions.js'></script>
	<script type='text/javascript' src='js/autocomplete/jquery.ajaxQueue.js'></script>
	<script type='text/javascript' src='js/autocomplete/thickbox-compressed.js'></script>
	<script type='text/javascript' src='js/autocomplete/jquery.autocomplete.js'></script>	
	<script type='text/javascript' src='js/autocomplete/localdata.js'></script>
	
	<script type="text/javascript" src="js/jquery.jqGrid.js"></script>
		 
	<script type="text/javascript" src="js/ui/ui.core.js"></script>
	<script type="text/javascript" src="js/ui/ui.datepicker.js"></script>
	<script type="text/javascript" src="js/ui/ui.datepicker-pt-BR.js"></script>

	<script type="text/javascript" src="js/jTabs/jquery.history_remote.pack.js"></script>
	<script type="text/javascript" src="js/jTabs/jquery.tabs.pack.js"></script>
	
    <script type="text/javascript" src="ckeditor/ckeditor.js"></script>
    
    <script src="../js/mascaras.js"></script>
     
	<script type="text/javascript">
	
	function pesquisarUsandoTeclaEnter(objEvent) {
	
		var tecla;  
						
		if (navigator.appName == 'Microsoft Internet Explorer') {  
			tecla = objEvent.keyCode;  
		} else if (navigator.appName == 'Netscape') {  
			tecla = objEvent.which;   
		}  
		
		if (tecla == 13){
			pesquisar();			
		}
	}
	  
	</script>
    
  <!-- TinyMCE -->
<script type="text/javascript" src="tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
	tinyMCE.init({
		// General options
		mode : "textareas",
		theme : "advanced",
		skin : "o2k7",
		plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,inlinepopups,autosave",

		// Theme options
		
		theme_advanced_buttons1 : "save,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,image,code,|,forecolor,backcolor",
		
		//theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
		//theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
		//theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
		//theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,

		// Example word content CSS (should be your site CSS) this one removes paragraph margins
		content_css : "css/word.css",

		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js",

		// Replace values for the template plugin
		template_replace_values : {
			username : "Some User",
			staffid : "991234"
		}
	});
</script>
<!-- /TinyMCE -->  
    
	{/literal}

</head>

<body style="background-color:#EFF4F8">
	 
<TABLE class="headerTable invisivel" cellSpacing=0 cellPadding=0 border=0 style="border: 1px solid #333;">
<tbody>
<tr>
    <td class="headerLeft" align="center">
      <IMG src="imgs/logo.png" style="margin:10px;" width="100px" border=0>
    </td>
    <td class="headerRight">
    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="invisivel">
    <tbody>
	<tr>
    	<td class="headerRightTop">
        <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
		<tr>
        
            <td class="headerButtons" nowrap="nowrap"><a href="sair.php" class="tabsOff"><img src="imgs/btn_sair.png" border="0">	</a></td>
            <td class="headerButtonsEnd"><img src="imgs/spacer.gif" alt="" height="1" width="1" border="0"></td>
		</tr>
		</tbody>
		</table>
        </td>
	</tr>
	<tr>
		<td class="headerRightBottom"> 
				<span class="headerTextBold">Bem-vindo {$usuarioLogado.nome}</span>   
		</td>
	</tr>
    </tbody>
	</table>
    </td>
</tr>
</tbody>
</table>


<div style="" class="invisivel">
	<ul class="jd_menu">
	
		{foreach from=$listaModulo item=modulo}
		<li>
	
		{if $modulo[2] neq ""}
		
		<a href="index.php?secao=modulo&idModulo={$modulo[0]}">{$modulo[1]} &raquo;</a>
				
		<ul>
			{foreach from=$modulo[2] item=subModulo}
			<li><a href="index.php?secao=modulo&idSubModulo={$subModulo[0]}">{$subModulo[1]}</a></li>
			{/foreach}	
		</ul>
		
		{else}
		
		<a href="index.php?secao=modulo&idModulo={$modulo[0]}">{$modulo[1]}</a>
		
		{/if}
		
		</li>
		{/foreach}

			
	</ul>	 	
</div>



<table class="mainContentTable" border="0" cellpadding="0" cellspacing="0">
<tbody>
<tr valign="top" style="background-color:#F9F9F9">
		
    <!-- Inicio Conteudo -->
	{include file=$templateCentro}
    <!-- Fim Conteudo -->
		
</tr>
</tbody>
</table>


<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0 style="border: 1px solid #333;" class="invisivel">
<TBODY>

<TR>
	<TD>
    <TABLE class="footerTable invisivel" cellSpacing=0 cellPadding=0 border=0>
    <TBODY>
    <TR vAlign=top>
    	<TD class=footerRight>
		<div class="footerText" align="center">
	Copyright © 2018 - WRAP BRASIL - Todos os direitos reservados.			<br>
	</div>
		
		</TD>
	</TR>
	</TBODY>
	</TABLE>
    
	</TD>
</TR>
</TBODY>
</TABLE>

</body>
</html>