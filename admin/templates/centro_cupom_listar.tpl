	{literal}
	
	<script type="text/javascript">
	 
	jQuery(document).ready(function(){
	
	jQuery("#listagemDados").jqGrid({
		
		{/literal}
		url:'index.php?secao=cupom&opcao=listar&nd='+new Date().getTime(),
		{literal}
				
		datatype: "json",
	
		colNames:['C&oacute;digo', 'Cliente', 'Desconto', 'Data Cadastro'],
		
		colModel:[
			{name:'id',index:'id', width:60},
			{name:'cliente',index:'cliente', width:200},
			{name:'desconto',index:'desconto', width:120},
			{name:'data_cadastro',index:'data_cadastro', width:150}
		],
		
		pager: jQuery('#paginacao'),
		
		rowNum:100,
		
		rowList:[100,200,300],
		
		imgpath: 'imgs',
		
		sortname: 'id',
		
		viewrecords: true,
		
		sortorder: "desc",
		
		multiselect: true, 
		
		caption: "Listagem de cupoms"
	});
	
	jQuery("#btn_alterar").click( 
		function(){ 
					
			var id = jQuery("#listagemDados").getGridParam('selarrrow'); 

			if(id.length == 1){

				{/literal}			
				window.location = 'index.php?secao=cupom&opcao=alterar&idForum='+ id;
				{literal}						

			}else{
			
				if(id.length == 0)
					alert("Selecione o cupom que deseja alterar.");
				else 
					alert("Selecione apenas uma cupom.");
			}				
		}
	
	); 
	
	jQuery("#btn_excluir").click( 
		function(){ 
		
			var listaIdForum; 
			
			listaIdForum = jQuery("#listagemDados").getGridParam('selarrrow'); 
			
			if (listaIdForum != '') { 
				
				if(confirm("Tem certeza que gostaria de excluir este(s) iten(s)?")){
				
					{/literal}	
					window.location = 'index.php?secao=cupom&opcao=excluir&listaIdForum='+ listaIdForum;
					{literal}	
				} 
				
			} 
			else { 
				alert("Selecione o(s) cupom(es) que deseja excluir.");
			} 						
			
		}
		
	);
	
	jQuery("#btn_aprovar").click( 
		function(){ 
					
			var id = jQuery("#listagemDados").getGridParam('selarrrow'); 

			if(id.length == 1){

				{/literal}			
				window.location = 'index.php?secao=cupom&opcao=subcol&idForum='+ id;
				{literal}						

			}else{
			
				if(id.length == 0)
					alert("Selecione o cupom que deseja tornar sub-cupom.");
				else 
					alert("Selecione apenas uma cupom.");
			}				
		}
	
	); 
	
	
	jQuery("#btn_visualizar").click( 
		function(){ 
		
			var listaIdForum; 
			
			id = jQuery("#listagemDados").getGridParam('selarrrow'); 
			
			if(id.length == 1){

				{/literal}			
				window.location = 'index.php?secao=cupom&opcao=visualizar&idForum='+ id;
				{literal}						

			}else{
			
				if(id.length == 0)
					alert("Selecione o cupom que deseja visualizar.");
				else 
					alert("Selecione apenas uma cupom.");
			}	
			
			
		}
	);
	
	jQuery("#btn_envio").click( 
		function(){ 
		
			var listaIdForum; 
			
			id = jQuery("#listagemDados").getGridParam('selarrrow'); 
			
			if(id.length == 1){

				{/literal}			
				window.location = 'index.php?secao=cupom&opcao=marcar_envio&idForum='+ id;
				{literal}						

			}else{
			
				if(id.length == 0)
					alert("Selecione o cupom que deseja marcar envio.");
				else 
					alert("Selecione apenas uma cupom.");
			}	
			
			
		}
	);
	
	});
	
	
	</script>	
	{/literal}
	<td class="mainContentArea">	
	<br />		
	<table class="searchTable" border="0" cellpadding="0" cellspacing="0" width="">
    <tbody>
	<tr> 	 
		<!--
		{if $podeAlterar}
		<td align="center" width="112">
		<a href="#" class="plainLink" id="btn_alterar"> <img src="imgs/alterar.png" border="0" width="48" height="48" /><br />Alterar cole&ccedil;&atilde;o</a>
		</td>
		{/if}
		{if $podeExcluir}
		<td align="center" width="112">	
		<a href="#" class="plainLink" id="btn_excluir"> <img src="imgs/remover.png" border="0" width="48" height="48" /><br />Excluir cole&ccedil;&atilde;o(s)</a>  
		</td>		
		{/if}
        
        {if $podeAprovar}
		<td align="center" width="112">	
		<a href="#" class="plainLink" id="btn_aprovar"> <img src="imgs/aceitar.png" border="0" width="48" height="48" /> <BR />
		Tornar sub-cole&ccedil;&atilde;o</a>  
		</td>		
		{/if}
        
        {if $podeReprovar}
		<td align="center" width="130">	
		<a href="#" class="plainLink" id="btn_reprovar"> <img src="imgs/application_remove.png" border="0" width="48" height="48" /> <BR />
		Reprovar Postagem(ns)</a>  
		</td>		
		{/if}
        
      -->
        
        {if $podeVisualizar}
		<td align="center" width="130">	
		<a href="#" id="btn_visualizar" class="plainLink"> <img src="imgs/visualizar.png" border="0" width="48" height="48" /> <BR />
		Visualizar cupom</a>  
		</td>		
		{/if}
        
        {if $podeEnviar}
		<td align="center" width="112">
		<a href="#" id="btn_envio" class="plainLink"> <img src="imgs/adicionar.png" border="0" width="48" height="48" /><br />Cadastrar envio </a>
		</td>
		{/if}	
          
	</tr>
	<tr>
		<td align="center"></td>		
	</tr>
	</tbody>
	</table>  	
	<br />	
	<table id="listagemDados" class="scroll" cellpadding="0" cellspacing="0"></table>
<!-- pager definition. class scroll tels that we want to use the same theme as grid -->
	<div id="paginacao" class="scroll" style="text-align:center;"></div>				
	</td>