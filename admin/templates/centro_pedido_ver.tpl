
<td class="mainContentArea">
    <form name="form_marca" method="post" action="index.php?secao=pedido&opcao=subcol_salvar" enctype="multipart/form-data">
        <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
            <tbody>
                <tr>
                    <td colspan="2" class="buttonRow">
                    <input name="voltar" value="" onClick="window.location='index.php?secao=pedido'" class="normalButtonVoltar" type="button">
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="mainHeader">Visualizando pedido {$pedidoProduto[0]} </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                
                  <tr>
                    <td colspan="2">
                    	<div style="width:95%;margin:auto;font-size:14px;">
                            <table cellpadding="0" cellspacing="0" width="65%" style="margin:auto;">
                                <tr>
                                    <td>
                                        <span style="color:#393939">Pedido: <strong>{$pedidoProduto[0]}</strong></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                      <span style="color:#393939">	
                                        Tipo pagamento: <strong>{$pedido.tipo}</strong>
                                      </span>
                                      <br />
                                      <span style="color:#393939;">Status do pedido:</span>  
                                      <strong>{$pedidoProduto[3]}</strong>	
                                      {if $pedidoProduto[2] eq "8"}
                                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                      <span style="color:#393939">	
                                        Data da pronto: <strong>{$pedidoProduto[4]}</strong>
                                      </span>
                                      {/if} 
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span style="color:#393939">Cliente: <strong>{$nomeCliente}</strong></span><br />
                                        <span style="color:#393939">E-mail: <strong>{$emailCliente}</strong></span><br />
                                        <span style="color:#393939">Telefone: <strong>{$telefoneCliente}</strong></span><br />
                                        <span style="color:#393939">Agendado: <strong>{$agendado}</strong></span>
                                    </td>
                                </tr>
                            </table>
                           
                           <br /><br />
                           
                           <div class="tit_pagamento" style="color: #393939;
                            font-size: 16px;
                            font-weight: bold;
                            margin: auto;
                            position: relative;
                            width: 65%;">DADOS DE SUA COMPRA:</div>
                           
                            {foreach from=$produto  item=prod}
                            <table cellpadding="0" cellspacing="0" width="65%" style="margin:auto;">
                                <tr>
                                    <td align="left" valign="middle">
                                    Item {$prod.item}:<br />
                                    <br />
                                    
                                    {if $prod.id_pao != 3}
                                    
                                        Produto: <strong>{$prod.nomePao} - {$prod.nomeProduto}</strong><br />
                                        {if $prod.nomeDoce == ""}
                                        Queijo: <strong>{$prod.nomeQueijo}</strong><br />
                                        Salada: <strong>{$prod.nomeSalada}</strong><br />
                                        Molho: <strong>{$prod.nomeMolho}</strong><br />
                                        Livre: <strong>{$prod.nomeLivre}</strong><br />
                                        Suco:  <strong>{$prod.nomeSuco}</strong><br />
                                        {else}
                                        Doce: <strong>{$prod.nomeDoce}</strong><br />
                                        Suco:  <strong>{$prod.nomeSuco}</strong><br />
                                        {/if}
                                        
                                    {else}
                                    
                                    	Produto: <strong>{$prod.nomePao} - {$prod.nomeProduto}</strong><br />
                                        Carboidratos: <strong>{$prod.carboidratos}</strong><br />
                                        Proteínas: <strong>{$prod.proteinas}</strong><br />
                                        Legumes: <strong>{$prod.legumes}</strong><br />
                                        Verduras: <strong>{$prod.verduras}</strong><br />
                                        Sementes:  <strong>{$prod.sementes}</strong><br />
                                        Frutas:  <strong>{$prod.frutas}</strong><br />
                                        Molhos:  <strong>{$prod.molhos}</strong><br />
                                        Livres:  <strong>{$prod.livres}</strong><br />
                                        Adicionais:  <strong>{$prod.adicionais}</strong><br />
                                        
                                    {/if}
                     
                                    
                                    Valor Produto: <strong>R$ {$prod.ValorItem|replace:".":","}</strong>
                                    <br /><br /><br />
                                    
                                    </td>
                                </tr>
                            </table>
                            {/foreach}   
                            <br />
                            <br />
                            <div class="tit_pagamento" style="color: #393939;
                            font-size: 16px;
                            font-weight: bold;
                            margin: auto;
                            position: relative;
                            width: 65%;">Valor Total Compra: <strong>R$ {$valorTotalCompra|replace:".":","}</strong></div>
                            </div>
                            
                    </td>
                  </tr>                                             
                                
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2" class="buttonRow">
                   <input name="voltar" value="" onClick="window.location='index.php?secao=pedido'" class="normalButtonVoltar" type="button">                   
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</td>
 