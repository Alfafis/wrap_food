	{literal}
	
	<script type="text/javascript">
	 
	jQuery(document).ready(function(){
	
	jQuery("#listagemDados").jqGrid({
		
		{/literal}
		url:'index.php?secao=queijos&opcao=listar&nd='+new Date().getTime(),
		{literal}
				
		datatype: "json",
	
		colNames:['C&oacute;digo', 'Nomes', 'Ativo', 'Data Cadastro'],
		
		colModel:[
			{name:'id',index:'id', width:60},
			{name:'nome',index:'nome', width:250},
			{name:'status',index:'status', width:200},
			{name:'cadastro',index:'cadastro', width:140}
		],
		
		pager: jQuery('#paginacao'),
		
		rowNum:100,
		
		rowList:[100,200,300],
		
		imgpath: 'imgs',
		
		sortname: 'nome',
		
		viewrecords: true,
		
		sortorder: "asc",
		
		multiselect: true, 
		
		caption: "Listagem de queijos"
	});
	
	
	jQuery("#btn_alterar").click( 
		function(){ 
					
			var id = jQuery("#listagemDados").getGridParam('selarrrow'); 

			if(id.length == 1){

				{/literal}			
				window.location = 'index.php?secao=queijos&opcao=alterar&idqueijos='+id;
				{literal}						

			}else{
			
				if(id.length == 0)
					alert("Selecione o queijo que deseja alterar.");
				else 
					alert("Selecione apenas um queijo.");
			}				
		}
	
	); 
	
	
	jQuery("#btn_excluir").click( 
		function(){ 
		
			var listaIdForum; 
			
			listaIdForum = jQuery("#listagemDados").getGridParam('selarrrow'); 
			
			if (listaIdForum != '') { 
				
				if(confirm("Tem certeza que gostaria de excluir este(s) queijo(s)?")){
				
					{/literal}	
					window.location = 'index.php?secao=queijos&opcao=excluir&listaId='+ listaIdForum;
					{literal}	
				} 
				
			} 
			else { 
				alert("Selecione o(s) queijo(s) que deseja excluir.");
			} 						
			
		}
		
	);
	
	
	jQuery("#btn_bloquear").click( 
		function(){ 
		
			var listaIdForum; 
			
			listaIdForum = jQuery("#listagemDados").getGridParam('selarrrow'); 
			
			if (listaIdForum != '') { 
				
				if(confirm("Tem certeza que gostaria de bloquear este(s) depoimento(s)?")){
				
					{/literal}	
					window.location = 'index.php?secao=queijos&opcao=bloquear&listaId='+ listaIdForum;
					{literal}	
				} 
				
			} 
			else { 
				alert("Selecione o(s) depoimento(s) que deseja bloquear.");
			} 						
			
		}
		
	);
	
	/*jQuery("#btn_visualizar").click( 
		function(){ 
		
			var listaIdForum; 
			
			listaIdForum = jQuery("#listagemDados").getGridParam('selarrrow'); 
			
			if (listaIdForum != '') { 
				
					{/literal}	
					    window.open('index.php?secao=queijos&opcao=visualizar&listaId='+ listaIdForum,'page','toolbar=no,location=yes,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=600');  
					//window.location = 'index.php?secao=queijos&opcao=visualizar&listaIdForum='+ listaIdForum;
					{literal}
					
				
			} 
			else { 
				alert("Selecione o cliente que deseja visualizar.");
			} 						
			
		}
	);*/
	
	
	jQuery("#btn_visualizar").click( 
		function(){ 
		
			{/literal}	
				window.open('index.php?secao=queijos&opcao=visualizar','page','toolbar=no,location=yes,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=600');  
			//window.location = 'index.php?secao=queijos&opcao=visualizar&listaIdForum='+ listaIdForum;
			{literal}					
			
		}
	);
	
	jQuery("#btn_aprovar").click( 
		function(){ 
		
			var listaIdForum; 
			
			listaIdForum = jQuery("#listagemDados").getGridParam('selarrrow'); 
			
			if (listaIdForum != '') { 
				
				if(confirm("Tem certeza que gostaria de aprovar este(s) cliente(s)?")){
				
					{/literal}	
					window.location = 'index.php?secao=queijos&opcao=aprovar&listaId='+ listaIdForum;
					{literal}	
				} 
				
			} 
			else { 
				alert("Selecione o(s) cliente(s) que deseja aprovar.");
			} 						
			
		}
		
	);
	
	});

	</script>	
	{/literal}
	<td class="mainContentArea">	
	<br />		
	<table class="searchTable" border="0" cellpadding="0" cellspacing="0" width="">
    <tbody>
	<tr> 	 
		
        {if $podeIncluir}
		<td align="center" width="140">
		<a href="index.php?secao=queijos&opcao=inserir" class="plainLink"> <img src="imgs/adicionar.png" border="0" width="48" height="48" /><br />Cadastrar Queijo </a>
		</td>
		{/if}	
        	
		{if $podeAlterar}
		<td align="center" width="140">
		<a href="#" class="plainLink" id="btn_alterar"> <img src="imgs/alterar.png" border="0" width="48" height="48" /><br />Alterar Queijo</a>
		</td>
		{/if}
        
		{if $podeExcluir}
		<td align="center" width="140">	
		<a href="#" class="plainLink" id="btn_excluir"> <img src="imgs/remover.png" border="0" width="48" height="48" /><br />Excluir Queijo(s)</a>  
		</td>		
		{/if}
        
        {if $podeVisualizar}
		<td align="center" width="130">	
		<a href="#" id="btn_visualizar" class="plainLink"> <img src="imgs/visualizar.png" border="0" width="48" height="48" /> <BR />
		Visualizar Todos Ativos</a>  
		</td>
        {/if}
        
        {if $podeAprovar}
		<td align="center" width="130">	
		<a href="#" class="plainLink" id="btn_aprovar"> <img src="imgs/aceitar.png" border="0" width="48" height="48" /> <BR />
		Aprovar Cliente(s)</a>  
		</td>		
		{/if}	
        
        {if $podeBloquear}
		<td align="center" width="130">	
		<a href="#" class="plainLink" id="btn_bloquear"> <img src="imgs/application_remove.png" border="0" width="48" height="48" /><br />
        Bloquear Revendedor(s)</a>  
		</td>		
		{/if}	
          
	</tr>
	<tr>
		<td align="center"></td>		
	</tr>
	</tbody>
	</table>  	
	<br />	
	<table id="listagemDados" class="scroll" cellpadding="0" cellspacing="0"></table>
<!-- pager definition. class scroll tels that we want to use the same theme as grid -->
	<div id="paginacao" class="scroll" style="text-align:center;"></div>				
	</td>