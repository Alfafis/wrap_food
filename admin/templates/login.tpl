<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<HTML>
<HEAD>
<TITLE>.:: WRAP BRASIL ::.</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1">

<link rel="icon" href="favicon.ico" type="image/x-icon" />

<LINK href="css/main.css" type=text/css rel=stylesheet>

<link rel="stylesheet" href="css/jquery.jdMenu.css" type="text/css" />

<META content="MSHTML 6.00.2900.3059" name=GENERATOR>

{literal}
<script>

	function validarFormulario(){
		
		if(document.form_login.login.value == ""){
			alert("Preencha o campo Login.");
			document.form_login.login.focus();
			return false;
		}
	
		if(document.form_login.senha.value == ""){
			alert("Preencha o campo Senha.");
			document.form_login.senha.focus();
			return false;
		}
				
		document.form_login.submit();
	}
	
	function verificarTeclaDigitada(objEvent){
			
		var code;  
		
		if (navigator.appName == 'Microsoft Internet Explorer') {  
			code = objEvent.keyCode;  
		} else if (navigator.appName == 'Netscape') {  
			code = objEvent.which;   
		}  
			
		if(code == 13){
			submeterFormulario();		
		}
					
	}
	
	function submeterFormulario() { 
			 
		document.form_login.submit();
	}  
</script>
{/literal}


</HEAD>

<BODY onLoad="document.form_login.login.focus()" style="background-color:#EFF4F8">

<TABLE class=headerTable cellSpacing=0 cellPadding=0 border=0 style="border: 1px solid #333;">
<TBODY>
<TR>
	<TD class=headerLeft align="center"><IMG src="imgs/logo.png" style="margin:10px;" width="100px" border=0></TD>
    <TD class=headerRight><IMG height=1 alt="" src="imgs/spacer.gif" 
      width=1> 
	</TD>
</TR>
</TBODY>
</TABLE>

<div style="border: 1px solid blue; ">
	<ul class="jd_menu"> 
		<li class="accessible">
		Login
		</li>  
	</ul>	 	
</div>

<TABLE class=mainContentTable cellSpacing=0 cellPadding=0 border=0 style="border: 1px solid #333;">
<TBODY>
<TR vAlign=top>
	
    <TD class=mainContentArea>
	
	<form name="form_login" id="form_login" method="post" action="index.php?secao=usuario&opcao=logar">
	<TABLE class=loginContent cellSpacing=0 cellPadding=0 border=0>
	<TBODY>
	<TR>
		<TD>
		<TABLE cellSpacing=0 cellPadding=3 border=0>
		<TBODY>
		<TR>
			<TD><SPAN class=plainText>Login:</SPAN></TD>
			<TD><input type="text" name="login" id="login" class="inputBoxLogin" /></TD></TR>
		<TR>
			<TD><SPAN class=plainText>Senha:</SPAN></TD>
			<TD><input type="password" name="senha" class="inputBoxLogin" onKeyDown="verificarTeclaDigitada(event);" /></TD>
		</TR>
		<TR>
			<TD valign="top"><SPAN class=plainText>C&oacute;digo da imagem:</SPAN></TD>
			<TD>			
			<input type="text" name="imagemValidacao" class="inputBoxLogin" onKeyDown="verificarTeclaDigitada(event);" />
			<BR> 
			<img src='gera_imagem_validacao.php' alt="Codigo" align="baseline" style="padding-top:10px" /> 
			</TD>
		</TR>
		</TBODY>
		</TABLE><BR>
        
		<TABLE>
		<TBODY>
		<TR>
			<TD vAlign=top>
			<INPUT class='normalButtonLogin' type='button' onClick="validarFormulario();" value='' name=''> 
            </TD>
		</TR>
		</TBODY>
		</TABLE>
		</TD>
	</TR>
	</TBODY>
	</TABLE>
	</FORM>
	
	</TD>
</TR>
</TBODY>
</TABLE>

<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0 style="border: 1px solid #333;">
<TBODY>

<TR>
	<TD>
    <TABLE class=footerTable cellSpacing=0 cellPadding=0 border=0>
    <TBODY>
    <TR vAlign=top>
    	<TD class=footerRight>
		<div class="footerText" align="center">
	Copyright &copy; 2018 - WRAP BRASIL - Todos os direitos reservados.			<br>
	</div>
		
		</TD>
	</TR>
	</TBODY>
	</TABLE>
    
	</TD>
</TR>
</TBODY>
</TABLE>

</BODY>
</HTML>
