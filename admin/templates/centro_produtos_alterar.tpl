{literal}
<script>
	function validarFormulario()
	{		
	 		
		if(document.form_marca.nome.value == ""){
			alert("Preencha o campo Nome.");
			document.form_marca.nome.focus();
			return false;
		}
		
		if(document.form_marca.preco.value == ""){
			alert("Preencha o campo Preço.");
			document.form_marca.preco.focus();
			return false;
		}
		
		if(document.form_marca.categoria.value == ""){
			alert("Selecione o Tipo.");
			document.form_marca.categoria.focus();
			return false;
		}
			
					
		document.form_marca.submit();
	}
	 
</script>
{/literal}
<td class="mainContentArea">
    <form name="form_marca" method="post" action="index.php?secao=produtos&opcao=salvarAlteracao" enctype="multipart/form-data">
    <input type="hidden" name="id" id="id_produto" value="{$produtos.id}" />
        <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
            <tbody>
                <tr>
                    <td colspan="2" class="buttonRow">
                    <input name="save" value="" onclick="validarFormulario();" class="normalButtonSalvar" type="button">
                    <input name="cancel" value="" onClick="window.location='index.php?secao=produtos'" class="normalButtonCancelar" type="button">
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="mainHeader">Altera&ccedil;&atilde;o de produtos </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top" width="70%">
                        <table border="0" cellpadding="2" cellspacing="0" width="100%">
                            <tbody>	
                            	<tr>
                                    <td class="labelCell" align="right">Nome:</td>
                                    <td class="contentCell"><input type="text" name="nome" size="50" value="{$produtos.nome}" class="inputBox" id="nome"></td>
                                </tr>
                                                             
                                <tr>
                                    <td class="labelCell" align="right">Pre&ccedil;o:</td>
                                    <td class="contentCell">R$ <input type="text" name="preco" size="40" value="{$produtos.preco}" class="inputBox" id="preco"  onKeyPress="formataNumero(event);" onKeyDown="formataMoeda(this,17,event);" style="width:100px;"></td>
                                </tr>
                                
                                <tr>
                                    <td class="labelCell" align="right">Tipo</td>
                                    <td class="contentCell" align="left">
                                    <select name="categoria" class="categoria" >
                                        <option value="Item" {if $produtos.categoria == "Item"}selected="selected"{/if}>Item Salgado</option>
                                        <option value="Item Doce" {if $produtos.categoria == "Item Doce"}selected="selected"{/if}>Item Doce</option>
                                        <option value="Salada" {if $produtos.categoria == "Salada"}selected="selected"{/if}>Salada</option>
                                        <option value="Suco" {if $produtos.categoria == "Suco"}selected="selected"{/if}>Suco</option>
                                    </select>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="labelCell" align="right">Tempo para preparo:</td>
                                    <td class="contentCell"><input type="text" name="tempo" size="50" value="{$produtos.tempo}" class="inputBox" id="tempo"></td>
                                </tr>
                                
                                
                                <tr>
                                    <td class="labelCell" align="right">Foto Principal:</td>
                                    <td class="contentCell">
                                    {if $produtos.foto}
                                    <img src="../fotos/{$produtos.foto}" width="100px" style="border:solid 1px #CCCCCC;" />
                                    <br />
                                    {/if}
                                    <input type="file" name="foto" size="50" value="" class="inputBox">
                                    <br />Dimens&otilde;es 800px X 400px.
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="labelCell" align="right">Descri&ccedil;&atilde;o:</td>
                                    <td class="contentCell"> 
                                    <textarea name="descricao" id="texto" >{$produtos.descricao}</textarea>
                                    <br /><br />
                                    </td>
                                </tr>	
                                
                                <tr>
                                    <td class="labelCell" align="right">Ativo</td>
                                    <td class="contentCell" align="left">
                                    <select name="ativo" class="ativo" >
                                    	<option value="1" {if $produtos.ativo == 1}selected="selected"{/if}>Sim</option>
                                        <option value="0" {if $produtos.ativo == 0}selected="selected"{/if}>N&atilde;o</option>
                                    </select>
                                    </td>
                                </tr>
                                
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2" class="buttonRow">
                    <input name="save" value="" onclick="validarFormulario();" class="normalButtonSalvar" type="button">
                    <input name="cancel" value="" onClick="window.location='index.php?secao=produtos'" class="normalButtonCancelar" type="button">                     
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</td>
<script>
	carregarTamanho();
	document.getElementById('nome').focus();
</script>
 