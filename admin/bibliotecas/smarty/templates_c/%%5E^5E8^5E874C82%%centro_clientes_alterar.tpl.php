<?php /* Smarty version 2.6.18, created on 2018-05-22 09:24:04
         compiled from centro_clientes_alterar.tpl */ ?>
<?php echo '
<script>
	function validarFormulario()
	{		 		
	
		if(document.form_cliente.nome.value == ""){
			alert("Digite o Nome!");
			document.form_cliente.nome.focus();
			return false;
		}
		
		
		if(document.form_cliente.senha.value == ""){
			alert("Preecha a senha!");
			document.form_cliente.senha.focus();
			return false;
		}
		
			
		document.form_cliente.submit();
		
	}
	 
</script>
'; ?>

<td class="mainContentArea">
    <form name="form_cliente" method="post" action="index.php?secao=clientes&opcao=salvarAlteracao" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $this->_tpl_vars['cliente']['id']; ?>
" />
        <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
            <tbody>
                <tr>
                    <td colspan="2" class="buttonRow">
                    <input name="save" value="" onclick="validarFormulario();" class="normalButtonSalvar" type="button">
                    <input name="cancel" value="" onClick="window.location='index.php?secao=clientes'" class="normalButtonCancelar" type="button">
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="mainHeader">Altera&ccedil;&atilde;o de Cliente </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top" width="70%">
                        <table border="0" cellpadding="2" cellspacing="0" width="100%">
                            <tbody>	
                            
                            	<tr>
                                    <td class="labelCell" align="right">Nome/Empresa(*):</td>
                                    <td class="contentCell"><input type="text" name="nome" size="50" value="<?php echo $this->_tpl_vars['cliente']['nome']; ?>
" class="inputBox" id="nome"></td>
                                </tr>
                                
                                <tr>
                                    <td class="labelCell" align="right">E-mail(*):</td>
                                    <td class="contentCell"><input type="text" name="email" size="50" value="<?php echo $this->_tpl_vars['cliente']['email']; ?>
" class="inputBox" id="email" disabled="disabled"></td>
                                </tr>
                                
                                <tr>
                                    <td class="labelCell" align="right">CPF/CNPJ:</td>
                                    <td class="contentCell"><input type="text" name="cpf" size="50" value="<?php echo $this->_tpl_vars['cliente']['cpf']; ?>
" class="inputBox" id="cpf"></td>
                                </tr>
                                
                                <tr>
                                    <td class="labelCell" align="right">Telefone:</td>
                                    <td class="contentCell"><input type="text" name="telefone" size="50" value="<?php echo $this->_tpl_vars['cliente']['telefone']; ?>
" class="inputBox" id="telefone" placeholder="(xx) xxxxx-xxxx" maxlength="15" onkeypress="mascara(this, mascaraTelefone)"></td>
                                </tr>
                                
                                <tr>
                                    <td class="labelCell" align="right">Sexo:</td>
                                    <td class="contentCell">
                                    <select name="sexo" class="inputBox">
                                        <option value="masculino" <?php if ($this->_tpl_vars['cliente']['sexo'] == 'masculino'): ?>selected="selected"<?php endif; ?>>Masculino</option>
                                    	<option value="feminino" <?php if ($this->_tpl_vars['cliente']['sexo'] == 'feminino'): ?>selected="selected"<?php endif; ?>>Feminino</option>
                                    </select>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="labelCell" align="right">Data de Nascimento:</td>
                                    <td class="contentCell"><input type="text" name="nascimento" size="50" value="<?php echo $this->_tpl_vars['cliente']['nascimento']; ?>
" class="inputBox" id="nascimento" placeholder="xx/xx/xxxx" maxlength="10" onkeypress="mascara(this, mascaraData)"></td>
                                </tr>
                                
                                <tr>
                                    <td class="labelCell" align="right">Senha(*):</td>
                                    <td class="contentCell"><input type="text" name="senha" size="50" value="<?php echo $this->_tpl_vars['cliente']['senha']; ?>
" class="inputBox" id="senha"></td>
                                </tr>
                                
                                <tr>
                                    <td class="labelCell" align="right">CEP:</td>
                                    <td class="contentCell"><input type="text" name="cep" size="50" value="<?php echo $this->_tpl_vars['cliente']['cep']; ?>
" class="inputBox" id="cep" maxlength="9" onkeypress="return mascaraCep(event,this,'#####-###');"></td>
                                </tr>
                                
                                <tr>
                                    <td class="labelCell" align="right">Endereço Completo:</td>
                                    <td class="contentCell"><input type="text" name="endereco" size="50" value="<?php echo $this->_tpl_vars['cliente']['endereco']; ?>
" class="inputBox" id="endereco"></td>
                                </tr>
                                
                                <tr>
                                    <td class="labelCell" align="right">Número:</td>
                                    <td class="contentCell"><input type="text" name="numero" size="50" value="<?php echo $this->_tpl_vars['cliente']['numero']; ?>
" class="inputBox" id="numero"></td>
                                </tr>
                                
                                <tr>
                                    <td class="labelCell" align="right">Complemento:</td>
                                    <td class="contentCell"><input type="text" name="complemento" size="50" value="<?php echo $this->_tpl_vars['cliente']['complemento']; ?>
" class="inputBox" id="complemento"></td>
                                </tr>
                                
                                <tr>
                                    <td class="labelCell" align="right">Bairro:</td>
                                    <td class="contentCell"><input type="text" name="bairro" size="50" value="<?php echo $this->_tpl_vars['cliente']['bairro']; ?>
" class="inputBox" id="bairro"></td>
                                </tr>
                                
                                <tr>
                                    <td class="labelCell" align="right">Cidade:</td>
                                    <td class="contentCell"><input type="text" name="cidade" size="50" value="<?php echo $this->_tpl_vars['cliente']['cidade']; ?>
" class="inputBox" id="cidade"></td>
                                </tr>
                                
                                <tr>
                                    <td class="labelCell" align="right">UF:</td>
                                    <td class="contentCell"><input type="text" name="estado" size="50" value="<?php echo $this->_tpl_vars['cliente']['estado']; ?>
" class="inputBox" id="estado"></td>
                                </tr>
                                
                                <tr>
                                    <td class="labelCell" align="right">Revendedor:</td>
                                    <td class="contentCell">
                                    <select name="revendedor" class="inputBox">
                                        <option value="0" <?php if ($this->_tpl_vars['cliente']['revendedor'] == 0): ?>selected="selected"<?php endif; ?>>Não</option>
                                        <option value="1" <?php if ($this->_tpl_vars['cliente']['revendedor'] == 1): ?>selected="selected"<?php endif; ?>>Sim</option>
                                    </select>
                                    </td>
                                </tr>
                                
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2" class="buttonRow">
                    <input name="save" value="" onclick="validarFormulario();" class="normalButtonSalvar" type="button">
                    <input name="cancel" value="" onClick="window.location='index.php?secao=cliente'" class="normalButtonCancelar" type="button">                     
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</td>
<script>
	document.getElementById('nome').focus();
</script>
 