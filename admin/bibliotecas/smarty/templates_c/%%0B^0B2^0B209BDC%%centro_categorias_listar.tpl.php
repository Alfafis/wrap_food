<?php /* Smarty version 2.6.18, created on 2017-09-11 08:24:49
         compiled from centro_categorias_listar.tpl */ ?>
	<?php echo '
	
	<script type="text/javascript">
	 
	jQuery(document).ready(function(){
	
	jQuery("#listagemDados").jqGrid({
		
		'; ?>

		url:'index.php?secao=categorias&opcao=listar&nd='+new Date().getTime(),
		<?php echo '
				
		datatype: "json",
	
		colNames:[\'C&oacute;digo\', \'Nome\'],
		
		colModel:[
			{name:\'id\',index:\'id\', width:60},
			{name:\'nome\',index:\'nome\', width:250}
		],
		
		pager: jQuery(\'#paginacao\'),
		
		rowNum:100,
		
		rowList:[100,200,300],
		
		imgpath: \'imgs\',
		
		sortname: \'nome\',
		
		viewrecords: true,
		
		sortorder: "asc",
		
		multiselect: true, 
		
		caption: "Listagem de categorias"
	});
	
	jQuery("#btn_alterar").click( 
		function(){ 
					
			var id = jQuery("#listagemDados").getGridParam(\'selarrrow\'); 

			if(id.length == 1){

				'; ?>
			
				window.location = 'index.php?secao=categorias&opcao=alterar&idCat='+ id;
				<?php echo '						

			}else{
			
				if(id.length == 0)
					alert("Selecione o categoria que deseja alterar.");
				else 
					alert("Selecione apenas uma categoria.");
			}				
		}
	
	); 
	
	jQuery("#btn_excluir").click( 
		function(){ 
		
			var listaIdForum; 
			
			listaIdForum = jQuery("#listagemDados").getGridParam(\'selarrrow\'); 
			
			if (listaIdForum != \'\') { 
				
				if(confirm("Tem certeza que gostaria de excluir este(s) iten(s)?")){
				
					'; ?>
	
					window.location = 'index.php?secao=categorias&opcao=excluir&listaIdCat='+ listaIdForum;
					<?php echo '	
				} 
				
			} 
			else { 
				alert("Selecione o(s) categoria(s) que deseja excluir.");
			} 						
			
		}
		
	);
	
	
	});

	</script>	
	'; ?>

	<td class="mainContentArea">	
	<br />		
	<table class="searchTable" border="0" cellpadding="0" cellspacing="0" width="">
    <tbody>
	<tr> 	 
		<?php if ($this->_tpl_vars['podeIncluir']): ?>
		<td align="center" width="112">
		<a href="index.php?secao=categorias&opcao=inserir" class="plainLink"> <img src="imgs/adicionar.png" border="0" width="48" height="48" /><br />Cadastrar categoria </a>
		</td>
		<?php endif; ?>	
		<?php if ($this->_tpl_vars['podeAlterar']): ?>
		<td align="center" width="112">
		<a href="#" class="plainLink" id="btn_alterar"> <img src="imgs/alterar.png" border="0" width="48" height="48" /><br />Alterar categoria</a>
		</td>
		<?php endif; ?>
		<?php if ($this->_tpl_vars['podeExcluir']): ?>
		<td align="center" width="112">	
		<a href="#" class="plainLink" id="btn_excluir"> <img src="imgs/remover.png" border="0" width="48" height="48" /><br />Excluir categoria(s)</a>  
		</td>		
		<?php endif; ?>
	</tr>
	<tr>
		<td align="center"></td>		
	</tr>
	</tbody>
	</table>  	
	<br />	
	<table id="listagemDados" class="scroll" cellpadding="0" cellspacing="0"></table>
<!-- pager definition. class scroll tels that we want to use the same theme as grid -->
	<div id="paginacao" class="scroll" style="text-align:center;"></div>				
	</td>