<?php /* Smarty version 2.6.18, created on 2017-09-22 11:18:05
         compiled from centro_chamadas_listar.tpl */ ?>
	<?php echo '
	
	<script type="text/javascript">
	 
	jQuery(document).ready(function(){
	
	jQuery("#listagemDados").jqGrid({
		
		'; ?>

		url:'index.php?secao=chamadas&opcao=listar&nd='+new Date().getTime(),
		<?php echo '
				
		datatype: "json",
	
		colNames:[\'C&oacute;digo\', \'T&iacute;tulo\', \'Data Cadastro\'],
		
		colModel:[
			{name:\'id\',index:\'id\', width:60},
			{name:\'nome\',index:\'nome\', width:380},
			{name:\'data_cadastro\',index:\'data_cadastro\', width:140}
		],
		
		pager: jQuery(\'#paginacao\'),
		
		rowNum:100,
		
		rowList:[100,200,300],
		
		imgpath: \'imgs\',
		
		sortname: \'data_cadastro\',
		
		viewrecords: true,
		
		sortorder: "desc",
		
		multiselect: true, 
		
		caption: "Listagem de Banners"
	});
	
	jQuery("#btn_alterar").click( 
		function(){ 
					
			var id = jQuery("#listagemDados").getGridParam(\'selarrrow\'); 

			if(id.length == 1){

				'; ?>
			
				window.location = 'index.php?secao=chamadas&opcao=alterar&idChamada='+ id;
				<?php echo '						

			}else{
			
				if(id.length == 0)
					alert("Selecione o chamadas que deseja alterar.");
				else 
					alert("Selecione apenas um chamadas.");
			}				
		}
	
	); 
	
	jQuery("#btn_excluir").click( 
		function(){ 
		
			var listaIdForum; 
			
			listaIdForum = jQuery("#listagemDados").getGridParam(\'selarrrow\'); 
			
			if (listaIdForum != \'\') { 
				
				if(confirm("Tem certeza que gostaria de excluir este(s) iten(s)?")){
				
					'; ?>
	
					window.location = 'index.php?secao=chamadas&opcao=excluir&listaIdBanner='+ listaIdForum;
					<?php echo '	
				} 
				
			} 
			else { 
				alert("Selecione o(s) chamadas(s) que deseja excluir.");
			} 						
			
		}
		
	);
	
	jQuery("#btn_aprovar").click( 
		function(){ 
		
			var listaIdForum; 
			
			listaIdForum = jQuery("#listagemDados").getGridParam(\'selarrrow\'); 
			
			if (listaIdForum != \'\') { 
				
				if(confirm("Tem certeza que gostaria de aprovar este(s) iten(s)?")){
				
					'; ?>
	
					window.location = 'index.php?secao=chamadas&opcao=aprovar&listaIdForum='+ listaIdForum;
					<?php echo '	
				} 
				
			} 
			else { 
				alert("Selecione a(s) chamadas(s) que deseja aprovar.");
			} 						
			
		}
	);
	
	jQuery("#btn_visualizar").click( 
		function(){ 
		
			var listaIdForum; 
			
			listaIdForum = jQuery("#listagemDados").getGridParam(\'selarrrow\'); 
			
			if (listaIdForum != \'\') { 
				
					'; ?>
	
					    //window.open('index.php?secao=chamadas&opcao=visualizar&listaIdForum='+ listaIdForum,'page','toolbar=no,location=yes,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=600');  
					window.location = 'index.php?secao=benner&opcao=visualizar&listaIdForum='+ listaIdForum;
					<?php echo '
					
				
			} 
			else { 
				alert("Selecione a(s) postagem(s) que deseja visualizar.");
			} 						
			
		}
	);
	
	});

	</script>	
	'; ?>

	<td class="mainContentArea">	
	<br />		
	<table class="searchTable" border="0" cellpadding="0" cellspacing="0" width="">
    <tbody>
	<tr> 	 
		<?php if ($this->_tpl_vars['podeIncluir']): ?>
		<td align="center" width="130">
		<a href="index.php?secao=chamadas&opcao=inserir" class="plainLink"> <img src="imgs/adicionar.png" border="0" width="48" height="48" /><br />Cadastrar Chamada </a>
		</td>
		<?php endif; ?>	
        	
		<?php if ($this->_tpl_vars['podeAlterar']): ?>
		<td align="center" width="130">
		<a href="#" class="plainLink" id="btn_alterar"> <img src="imgs/alterar.png" border="0" width="48" height="48" /><br />Alterar Chamada</a>
		</td>
		<?php endif; ?>

		<?php if ($this->_tpl_vars['podeExcluir']): ?>
		<td align="center" width="130">	
		<a href="#" class="plainLink" id="btn_excluir"> <img src="imgs/remover.png" border="0" width="48" height="48" /><br />Excluir Chamada(s)</a>  
		</td>		
		<?php endif; ?>
        <!--
        <?php if ($this->_tpl_vars['podeAprovar']): ?>
		<td align="center" width="112">	
		<a href="#" class="plainLink" id="btn_aprovar"> <img src="imgs/aceitar.png" border="0" width="48" height="48" /> <BR />
		Aprovar Postagem(ns)</a>  
		</td>		
		<?php endif; ?>
        
        <?php if ($this->_tpl_vars['podeReprovar']): ?>
		<td align="center" width="130">	
		<a href="#" class="plainLink" id="btn_reprovar"> <img src="imgs/application_remove.png" border="0" width="48" height="48" /> <BR />
		Reprovar Postagem(ns)</a>  
		</td>		
		<?php endif; ?>
        
      
        
        <?php if ($this->_tpl_vars['podeVisualizar']): ?>
		<td align="center" width="130">	
		<a href="#" id="btn_visualizar" class="plainLink"> <img src="imgs/visualizar.png" border="0" width="48" height="48" /> <BR />
		Visualizar Postagem(ns)</a>  
		</td>		
		<?php endif; ?>
          -->
	</tr>
	<tr>
		<td align="center"></td>		
	</tr>
	</tbody>
	</table>  	
	<br />	
	<table id="listagemDados" class="scroll" cellpadding="0" cellspacing="0"></table>
<!-- pager definition. class scroll tels that we want to use the same theme as grid -->
	<div id="paginacao" class="scroll" style="text-align:center;"></div>				
	</td>