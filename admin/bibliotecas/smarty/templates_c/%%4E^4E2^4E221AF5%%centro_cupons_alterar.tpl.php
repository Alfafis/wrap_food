<?php /* Smarty version 2.6.18, created on 2017-09-06 12:37:56
         compiled from centro_cupons_alterar.tpl */ ?>
<?php echo '
<script>
	function validarFormulario()
	{		 		
		if(document.form_marca.titulo.value == ""){
			alert("Preencha o campo Título.");
			document.form_marca.nome.focus();
			return false;
		}
					
		document.form_marca.submit();
	}
	 
</script>
'; ?>

<td class="mainContentArea">
    <form name="form_marca" method="post" action="index.php?secao=cupons&opcao=salvarAlteracao" enctype="multipart/form-data">
    <input type="hidden" name="idCupom" id="idCupom" value="<?php echo $this->_tpl_vars['cupom']['id']; ?>
" />
        <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
            <tbody>
                <tr>
                    <td colspan="2" class="buttonRow">
                    <input name="save" value="" onclick="validarFormulario();" class="normalButtonSalvar" type="button">
                    <input name="cancel" value="" onClick="window.location='index.php?secao=cupons'" class="normalButtonCancelar" type="button">
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="mainHeader">Altera&ccedil;&atilde;o de Cupom </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top" width="70%">
                        <table border="0" cellpadding="2" cellspacing="0" width="100%">
                            <tbody>	
                            	<tr>
                                    <td class="labelCell" align="right">Título:</td>
                                    <td class="contentCell"><input type="text" name="titulo" size="50" value="<?php echo $this->_tpl_vars['cupom']['titulo']; ?>
" class="inputBox" id="titulo"></td>
                                </tr>
                                <tr>
                                    <td class="labelCell" align="right">Código:</td>
                                    <td class="contentCell"><input type="text" name="codigo" size="50" value="<?php echo $this->_tpl_vars['cupom']['codigo']; ?>
" class="inputBox" id="codigo" disabled="disabled"></td>
                                </tr>
                                
                                <tr>
                                    <td class="labelCell" align="right">Desconto:</td>
                                    <td class="contentCell">R$ <input type="text" name="desconto" size="40" value="<?php echo $this->_tpl_vars['cupom']['desconto']; ?>
" class="inputBox" id="desconto"  onKeyPress="formataNumero(event);" onKeyDown="formataMoeda(this,17,event);" style="width:100px;" disabled="disabled"></td>
                                </tr>
                                
                                <tr>
                                    <td class="labelCell" align="right">Porcentagem:</td>
                                    <td class="contentCell">
                                    <input type="checkbox" name="porcento" value="1" <?php if ($this->_tpl_vars['cupom']['porcento'] == 1): ?>checked="checked"<?php endif; ?> /> Sim
                                    <br /><span style="font-size:10px;color:#999">Ao selecionar a porcentagem será considerado apenas as o valor decimal do desconto. Ex: R$<span style="color:#0C3">50</span>,35.</span>
                                    </td>
                                </tr>
                                
                               <tr>
                                    <td class="labelCell" align="right">Ativo:</td>
                                    <td class="contentCell">
                                    <select name="ativo" class="inputBox">
                                        <option value="0" <?php if ($this->_tpl_vars['cupom']['ativo'] == 0): ?>selected="selected"<?php endif; ?>>Não</option>
                                        <option value="1" <?php if ($this->_tpl_vars['cupom']['ativo'] == 1): ?>selected="selected"<?php endif; ?>>Sim</option>
                                    </select>
                                    </td>
                                </tr>	
                                
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2" class="buttonRow">
                    <input name="save" value="" onclick="validarFormulario();" class="normalButtonSalvar" type="button">
                    <input name="cancel" value="" onClick="window.location='index.php?secao=cupons'" class="normalButtonCancelar" type="button">                     
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</td>
<script>
	carregarTamanho();
	document.getElementById('titulo').focus();
</script>
 