<?php /* Smarty version 2.6.18, created on 2018-03-12 15:41:15
         compiled from centro_pedido_listar.tpl */ ?>
	<?php echo '
	
	<script type="text/javascript">
	 
	jQuery(document).ready(function(){
	
	jQuery("#listagemDados").jqGrid({
		
		'; ?>

		url:'index.php?secao=pedido&opcao=listar&nd='+new Date().getTime(),
		<?php echo '
				
		datatype: "json",
	
		colNames:[\'C&oacute;digo\', \'Cliente\', \'Agendado\', \'Data Cadastro\', \'Status Pedido\', \'Valor Compra\'],
		
		colModel:[
			{name:\'id\',index:\'id\', width:60},
			{name:\'cliente\',index:\'cliente\', width:200},
			{name:\'agendado\',index:\'agendado\', width:100},
			{name:\'data_cadastro\',index:\'data_cadastro\', width:150},
			{name:\'status\',index:\'status\', width:200},
			{name:\'valorTotal\',index:\'valorTotal\', width:200}
		],
		
		pager: jQuery(\'#paginacao\'),
		
		rowNum:100,
		
		rowList:[100,200,300],
		
		imgpath: \'imgs\',
		
		sortname: \'id\',
		
		viewrecords: true,
		
		sortorder: "desc",
		
		multiselect: true, 
		
		caption: "Listagem de pedidos"
	});
	
	jQuery("#btn_alterar").click( 
		function(){ 
					
			var id = jQuery("#listagemDados").getGridParam(\'selarrrow\'); 

			if(id.length == 1){

				'; ?>
			
				window.location = 'index.php?secao=pedido&opcao=alterar&idForum='+ id;
				<?php echo '						

			}else{
			
				if(id.length == 0)
					alert("Selecione o pedido que deseja alterar.");
				else 
					alert("Selecione apenas uma pedido.");
			}				
		}
	
	); 
	
	jQuery("#btn_excluir").click( 
		function(){ 
		
			var listaIdForum; 
			
			listaIdForum = jQuery("#listagemDados").getGridParam(\'selarrrow\'); 
			
			if (listaIdForum != \'\') { 
				
				if(confirm("Tem certeza que gostaria de excluir este(s) iten(s)?")){
				
					'; ?>
	
					window.location = 'index.php?secao=pedido&opcao=excluir&listaIdForum='+ listaIdForum;
					<?php echo '	
				} 
				
			} 
			else { 
				alert("Selecione o(s) pedido(es) que deseja excluir.");
			} 						
			
		}
		
	);
	
	jQuery("#btn_aprovar").click( 
		function(){ 
					
			var id = jQuery("#listagemDados").getGridParam(\'selarrrow\'); 

			if(id.length == 1){

				'; ?>
			
				window.location = 'index.php?secao=pedido&opcao=subcol&idForum='+ id;
				<?php echo '						

			}else{
			
				if(id.length == 0)
					alert("Selecione o pedido que deseja tornar sub-pedido.");
				else 
					alert("Selecione apenas uma pedido.");
			}				
		}
	
	); 
	
	
	jQuery("#btn_visualizar").click( 
		function(){ 
		
			var listaIdForum; 
			
			id = jQuery("#listagemDados").getGridParam(\'selarrrow\'); 
			
			if(id.length == 1){

				'; ?>
			
				window.location = 'index.php?secao=pedido&opcao=visualizar&idForum='+ id;
				<?php echo '						

			}else{
			
				if(id.length == 0)
					alert("Selecione o pedido que deseja visualizar.");
				else 
					alert("Selecione apenas uma pedido.");
			}	
			
			
		}
	);
	
	jQuery("#btn_envio").click( 
		function(){ 
		
			var listaIdForum; 
			
			id = jQuery("#listagemDados").getGridParam(\'selarrrow\'); 
			
			if(id.length == 1){

				'; ?>
			
				window.location = 'index.php?secao=pedido&opcao=marcar_envio_salvar&idPedido='+ id;
				<?php echo '						

			}else{
			
				if(id.length == 0)
					alert("Selecione o pedido que deseja avisar que esta pronto.");
				else 
					alert("Selecione apenas uma pedido.");
			}	
			
			
		}
	);
	
	
	jQuery("#btn_todos_pedidos").click( 
		function(){ 
			
			var largura = screen.width;
			var altura = screen.height;
			
			'; ?>
	
				window.open('index.php?secao=pedido&opcao=tela_pedidos','Tela Pedidos','width='+largura+',height='+altura+',top=0,left=0,screenX=0,screenY=0,status=no,scrollbars=yes,toolbar=no,resizable=no,maximized=yes,menubar=no,location=no');
				//window.open('index.php?secao=pedido&opcao=tela_pedidos','page','toolbar=no,location=yes,status=no,menubar=no,scrollbars=no,resizable=no,width=100%,height=100%');  
			<?php echo '
			
		}
	);
	
	});
	
	
	</script>	
	'; ?>

	<td class="mainContentArea">	
	<br />		
	<table class="searchTable" border="0" cellpadding="0" cellspacing="0" width="">
    <tbody>
	<tr> 	 
		
		<?php if ($this->_tpl_vars['podeAlterar']): ?>
		<td align="center" width="112">
		<a href="#" class="plainLink" id="btn_alterar"> <img src="imgs/alterar.png" border="0" width="48" height="48" /><br />Alterar cole&ccedil;&atilde;o</a>
		</td>
		<?php endif; ?>
		<?php if ($this->_tpl_vars['podeExcluir']): ?>
		<td align="center" width="112">	
		<a href="#" class="plainLink" id="btn_excluir"> <img src="imgs/remover.png" border="0" width="48" height="48" /><br />Excluir cole&ccedil;&atilde;o(s)</a>  
		</td>		
		<?php endif; ?>
        
        <?php if ($this->_tpl_vars['podeAprovar']): ?>
		<td align="center" width="112">	
		<a href="#" class="plainLink" id="btn_aprovar"> <img src="imgs/aceitar.png" border="0" width="48" height="48" /> <BR />
		Tornar sub-cole&ccedil;&atilde;o</a>  
		</td>		
		<?php endif; ?>
        <!--
        <?php if ($this->_tpl_vars['podeReprovar']): ?>
		<td align="center" width="130">	
		<a href="#" class="plainLink" id="btn_reprovar"> <img src="imgs/application_remove.png" border="0" width="48" height="48" /> <BR />
		Reprovar Postagem(ns)</a>  
		</td>		
		<?php endif; ?>
        
      -->
        
        <?php if ($this->_tpl_vars['podeVisualizar']): ?>
		<td align="center" width="130">	
		<a href="#" id="btn_visualizar" class="plainLink"> <img src="imgs/visualizar.png" border="0" width="48" height="48" /> <BR />
		Visualizar Pedido</a>  
		</td>		
		<?php endif; ?>
        
        <?php if ($this->_tpl_vars['podeEnviar']): ?>
		<td align="center" width="112">
		<a href="#" id="btn_envio" class="plainLink"> <img src="imgs/adicionar.png" border="0" width="48" height="48" /><br />Avisar que est&aacute; pronto </a>
		</td>
        
        <td align="center" width="112">
		<a href="#" id="btn_todos_pedidos" class="plainLink"> <img src="imgs/transferir.png" border="0" width="48" height="48" /><br />Tela Pedidos</a>
		</td>
		<?php endif; ?>	
          
	</tr>
	<tr>
		<td align="center"></td>		
	</tr>
	</tbody>
	</table>  	
	<br />	
	<table id="listagemDados" class="scroll" cellpadding="0" cellspacing="0"></table>
<!-- pager definition. class scroll tels that we want to use the same theme as grid -->
	<div id="paginacao" class="scroll" style="text-align:center;"></div>				
	</td>