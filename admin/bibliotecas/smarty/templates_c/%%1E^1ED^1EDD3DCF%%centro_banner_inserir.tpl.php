<?php /* Smarty version 2.6.18, created on 2018-05-17 18:37:07
         compiled from centro_banner_inserir.tpl */ ?>
<?php echo '
<script>
	function validarFormulario()
	{		 		
		if(document.form_marca.nome.value == ""){
			alert("Preencha o campo Nome.");
			document.form_marca.nome.focus();
			return false;
		}
		
		if(document.form_marca.arquivo.value == ""){
			alert("Preencha o campo arquivo.");
			document.form_marca.arquivo.focus();
			return false;
		} 	
			
		document.form_marca.submit();
	}
	 
</script>
'; ?>

<td class="mainContentArea">
    <form name="form_marca" method="post" action="index.php?secao=banner&opcao=salvar" enctype="multipart/form-data">
        <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
            <tbody>
                <tr>
                    <td colspan="2" class="buttonRow">
                    <input name="save" value="" onclick="validarFormulario();" class="normalButtonSalvar" type="button">
                    <input name="cancel" value="" onClick="window.location='index.php?secao=banner'" class="normalButtonCancelar" type="button">
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="mainHeader">Cadastro de Banner </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top" width="70%">
                        <table border="0" cellpadding="2" cellspacing="0" width="100%">
                            <tbody>	
                            	
                                <tr>
                                    <td class="labelCell" align="right">T&iacute;tulo:</td>
                                    <td class="contentCell"><input type="text" name="nome" size="50" value="" class="inputBox" id="nome"></td>
                                </tr>
                                
                                <tr>
                                    <td class="labelCell" align="right">Posi&ccedil;&atilde;o:</td>
                                    <td class="contentCell"><select name="posicao" class="inputBox" id="posicao">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    </select></td>
                                </tr>
                                <tr>
                                    <td class="labelCell" align="right">Arquivo:</td>
                                    <td class="contentCell">
                                    <input type="file" name="arquivo" size="50" value="" class="inputBox" id="arquivo">
                                    <br />Dimens&otilde;es:<br /> 
                                    800px X 400px<br />
                                    </td>
                                </tr>
                                
                                
                                
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2" class="buttonRow">
                    <input name="save" value="" onclick="validarFormulario();" class="normalButtonSalvar" type="button">
                    <input name="cancel" value="" onClick="window.location='index.php?secao=banner'" class="normalButtonCancelar" type="button">                     
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</td>
<script>
	document.getElementById('nome').focus();
</script>
 