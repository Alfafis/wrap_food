<?php /* Smarty version 2.6.18, created on 2017-10-16 23:56:31
         compiled from centro_cupons_inserir.tpl */ ?>
<?php echo '
<script>
	function validarFormulario()
	{		 		
				
		if(document.form_marca.titulo.value == ""){
			alert("Preencha o campo Título.");
			document.form_marca.titulo.focus();
			return false;
		}
		
		
		if(document.form_marca.codigo.value == ""){
			alert("Preencha o Código.");
			document.form_marca.codigo.focus();
			return false;
		}
		
		if(document.form_marca.desconto.value == ""){
			alert("Preencha o Desconto.");
			document.form_marca.desconto.focus();
			return false;
		}
		
		
		document.form_marca.submit();
	}
	 
</script>
'; ?>

<td class="mainContentArea">
    <form name="form_marca" method="post" action="index.php?secao=cupons&opcao=salvar" enctype="multipart/form-data">
        <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
            <tbody>
                <tr>
                    <td colspan="2" class="buttonRow">
                    <input name="save" value="" onclick="validarFormulario();" class="normalButtonSalvar" type="button">
                    <input name="cancel" value="" onClick="window.location='index.php?secao=cupons'" class="normalButtonCancelar" type="button">
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="mainHeader">Cadastro de Cupom</td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top" width="70%">
                        <table border="0" cellpadding="2" cellspacing="0" width="100%">
                            <tbody>	
                            	<tr>
                                    <td class="labelCell" align="right">Título:</td>
                                    <td class="contentCell"><input type="text" name="titulo" size="50" value="" class="inputBox" id="titulo"></td>
                                </tr>
                                <tr>
                                    <td class="labelCell" align="right">Código:</td>
                                    <td class="contentCell"><input type="text" name="codigo" size="50" value="" class="inputBox" id="codigo"></td>
                                </tr>
                                
                                <tr>
                                    <td class="labelCell" align="right">Desconto:</td>
                                    <td class="contentCell">R$ <input type="text" name="desconto" size="40" value="" class="inputBox" id="desconto" onKeyPress="formataNumero(event);" onKeyDown="formataMoeda(this,17,event);" style="width:100px;"></td>
                                </tr>
                                
                                <tr>
                                    <td class="labelCell" align="right">Porcentagem:</td>
                                    <td class="contentCell">
                                    <input type="checkbox" name="porcento" value="1" /> Sim
                                    <br /><span style="font-size:10px;color:#999">Ao selecionar a porcentagem será considerado apenas as o valor decimal do desconto. Ex: R$<span style="color:#0C3">50</span>,35.</span>
                                    </td>
                                </tr>
                                
                                
                                <!--<tr>
                                    <td class="labelCell" align="right">Ativo:</td>
                                    <td class="contentCell">
                                    <select name="ativo" class="inputBox">
                                        <option value="0">Não</option>
                                        <option value="1">Sim</option>
                                    </select>
                                    </td>
                                </tr>-->
                                
                                
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2" class="buttonRow">
                    <input name="save" value="" onclick="validarFormulario();" class="normalButtonSalvar" type="button">
                    <input name="cancel" value="" onClick="window.location='index.php?secao=cupons'" class="normalButtonCancelar" type="button">                     
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</td>
<script>
	document.getElementById('nome').focus();
</script>
 