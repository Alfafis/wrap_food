<?php /* Smarty version 2.6.18, created on 2017-09-06 10:57:53
         compiled from centro_cupons_listar.tpl */ ?>
	<?php echo '
	
	<script type="text/javascript">
	 
	jQuery(document).ready(function(){
	
	jQuery("#listagemDados").jqGrid({
		
		'; ?>

		url:'index.php?secao=cupons&opcao=listar&nd='+new Date().getTime(),
		<?php echo '
				
		datatype: "json",
	
		colNames:[\'C&oacute;digo\', \'T&iacute;tulo\', \'C&oacute;digo Promocional\', \'Desconto\', \'Porcentagem\', \'Ativo\', \'Data Cadastro\'],
		
		colModel:[
			{name:\'id\',index:\'id\', width:60},
			{name:\'titulo\',index:\'titulo\', width:200},
			{name:\'codigo\',index:\'codigo\', width:150},
			{name:\'desconto\',index:\'desconto\', width:150},
			{name:\'porcento\',index:\'porcento\', width:100},
			{name:\'ativo\',index:\'ativo\', width:100},
			{name:\'data_cadastro\',index:\'data_cadastro\', width:140}
		],
		
		pager: jQuery(\'#paginacao\'),
		
		rowNum:100,
		
		rowList:[100,200,300],
		
		imgpath: \'imgs\',
		
		sortname: \'id\',
		
		viewrecords: true,
		
		sortorder: "desc",
		
		multiselect: true, 
		
		caption: "Listagem de Cupons"
	});
	
	jQuery("#btn_alterar").click( 
		function(){ 
					
			var id = jQuery("#listagemDados").getGridParam(\'selarrrow\'); 

			if(id.length == 1){

				'; ?>
			
				window.location = 'index.php?secao=cupons&opcao=alterar&idCupom='+ id;
				<?php echo '						

			}else{
			
				if(id.length == 0)
					alert("Selecione o cupom que deseja alterar.");
				else 
					alert("Selecione apenas um cupom.");
			}				
		}
	
	); 
	
	jQuery("#btn_excluir").click( 
		function(){ 
		
			var listaIdForum; 
			
			listaIdForum = jQuery("#listagemDados").getGridParam(\'selarrrow\'); 
			
			if (listaIdForum != \'\') { 
				
				if(confirm("Tem certeza que gostaria de excluir este(s) iten(s)?")){
				
					'; ?>
	
					window.location = 'index.php?secao=cupons&opcao=excluir&listaIdForum='+ listaIdForum;
					<?php echo '	
				} 
				
			} 
			else { 
				alert("Selecione o(s) cupons(s) que deseja excluir.");
			} 						
			
		}
		
	);
	
	jQuery("#btn_aprovar").click( 
		function(){ 
		
			var listaIdForum; 
			
			listaIdForum = jQuery("#listagemDados").getGridParam(\'selarrrow\'); 
			
			if (listaIdForum != \'\') { 
				
				if(confirm("Tem certeza que gostaria de aprovar este(s) iten(s)?")){
				
					'; ?>
	
					window.location = 'index.php?secao=cupons&opcao=aprovar&listaIdForum='+ listaIdForum;
					<?php echo '	
				} 
				
			} 
			else { 
				alert("Selecione a(s) cupons(s) que deseja aprovar.");
			} 						
			
		}
	);
	
	jQuery("#btn_visualizar").click( 
		function(){ 
		
			var listaIdForum; 
			
			listaIdForum = jQuery("#listagemDados").getGridParam(\'selarrrow\'); 
			
			if (listaIdForum != \'\') { 
				
					'; ?>
	
					    window.open('index.php?secao=cupons&opcao=visualizar&idCupom='+ listaIdForum,'page','toolbar=no,location=yes,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=600');  
					//window.location = 'index.php?secao=cupons&opcao=visualizar&listaIdForum='+ listaIdForum;
					<?php echo '
					
				
			} 
			else { 
				alert("Selecione o cupom que deseja visualizar.");
			} 						
			
		}
	);
	
	});
	
	var nome = ""; 
	var cod = ""; 
	
	function pesquisar(){ 
	
		nome = jQuery("#nome").val(); 
		cod = jQuery("#cod").val();
		 
		jQuery("#listagemDados").setGridParam({url:\'index.php?secao=cupons&opcao=listar&nome=\'+nome+\'&cod=\'+cod+\'&nd=\'+new Date().getTime(),page:1}).trigger("reloadGrid"); 

	} 

	</script>	
	'; ?>

	<td class="mainContentArea">	
	<br />		
	<table class="searchTable" border="0" cellpadding="0" cellspacing="0" width="">
    <tbody>
	<tr> 	 
		<?php if ($this->_tpl_vars['podeIncluir']): ?>
		<td align="center" width="112">
		<a href="index.php?secao=cupons&opcao=inserir" class="plainLink"> <img src="imgs/adicionar.png" border="0" width="48" height="48" /><br />Cadastrar cupom </a>
		</td>
		<?php endif; ?>	
		<?php if ($this->_tpl_vars['podeAlterar']): ?>
		<td align="center" width="112">
		<a href="#" class="plainLink" id="btn_alterar"> <img src="imgs/alterar.png" border="0" width="48" height="48" /><br />Alterar cupom</a>
		</td>
		<?php endif; ?>
		<?php if ($this->_tpl_vars['podeExcluir']): ?>
		<td align="center" width="112">	
		<a href="#" class="plainLink" id="btn_excluir"> <img src="imgs/remover.png" border="0" width="48" height="48" /><br />Excluir cupom(ns)</a>  
		</td>		
		<?php endif; ?>
        
        <?php if ($this->_tpl_vars['podeAprovar']): ?>
		<td align="center" width="112">	
		<a href="#" class="plainLink" id="btn_aprovar"> <img src="imgs/aceitar.png" border="0" width="48" height="48" /> <BR />
		Aprovar produto(s)</a>  
		</td>		
		<?php endif; ?>
        
        <?php if ($this->_tpl_vars['podeReprovar']): ?>
		<td align="center" width="130">	
		<a href="#" class="plainLink" id="btn_reprovar"> <img src="imgs/application_remove.png" border="0" width="48" height="48" /> <BR />
		Reprovar produto(s)</a>  
		</td>		
		<?php endif; ?>
        
        <?php if ($this->_tpl_vars['podeVisualizar']): ?>
		<td align="center" width="130">	
		<a href="#" id="btn_visualizar" class="plainLink"> <img src="imgs/visualizar.png" border="0" width="48" height="48" /> <BR />
		Visualizar utilizados</a>  
		</td>		
		<?php endif; ?>
          
	</tr>
	<tr>
		<td align="center"></td>		
	</tr>
	</tbody>
	</table>  	
	<br />	
	 
	<!--<div style="padding-left:20px">	
		<div style="font-size:12px">Pesquisar por:</div> 
		
		<div>  		 
		Nome<br />  
		<input name="nome" size="40" value="<?php echo $this->_tpl_vars['nome']; ?>
" class="inputBox" type="text" id="nome" onKeyPress="return pesquisarUsandoTeclaEnter(event)">
		</div> 
        <div>  		 
		REF<br />  
		<input name="cod" size="40" value="<?php echo $this->_tpl_vars['cod']; ?>
" class="inputBox" type="text" id="cod" onKeyPress="formataNumero(event); pesquisarUsandoTeclaEnter(event);" onclick="pesquisarUsandoTeclaEnter(this.value)">
		<button onclick="pesquisar()" id="submitButton" style="margin-left:10px;">Pesquisar</button> 
		</div> 
		
		
	</div>	 
	
	<BR />-->
    
	<table id="listagemDados" class="scroll" cellpadding="0" cellspacing="0"></table>
<!-- pager definition. class scroll tels that we want to use the same theme as grid -->
	<div id="paginacao" class="scroll" style="text-align:center;"></div>				
	</td>