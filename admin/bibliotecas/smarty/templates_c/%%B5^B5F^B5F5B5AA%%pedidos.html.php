<?php /* Smarty version 2.6.18, created on 2018-04-11 14:40:54
         compiled from pedidos.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'replace', 'pedidos.html', 97, false),)), $this); ?>
<meta charset="utf-8">
<meta http-equiv="refresh" content="10">
<div class="tit_pagamento" style="color: #FFFFFF;
font-size: 20px;
font-weight: bold;
margin: auto;
position: relative;
padding:10px 0;
text-align:center;
background-color:#004501;
width: 100%;">QUADRO DE PEDIDOS:</div>

<div style="overflow-x:auto;width:auto;">
<table cellpadding="0" cellspacing="0" style="margin:auto;">
<tr>
<?php $_from = $this->_tpl_vars['pedidoProduto']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['pedido']):
?>
<td valign="top" align="left" style="padding:20px 10px;width:300px;">
<div style="width:300px;font-size:12px;border:solid 1px #666666;float:left;padding:10px;">
<table cellpadding="0" cellspacing="0" width="100%" style="">
    <tr>
        <td style="text-align:center;background-color:#CCC;">
            <span style="color:#000000"><strong>Pedido: <?php echo $this->_tpl_vars['pedido']['id']; ?>
</strong></span>
        </td>
    </tr>
    <tr>
        <td>
          <span style="color:#393939">	
            Tipo pagamento: <strong><?php echo $this->_tpl_vars['pedido']['tipo']; ?>
</strong>
          </span>
          <br />
          <span style="color:#393939;">Status do pedido:</span>  
          <strong><?php echo $this->_tpl_vars['pedido']['descricao']; ?>
</strong>	
          <?php if ($this->_tpl_vars['pedido']['data_envio'] != ""): ?>
          <br />
          <span style="color:#393939">	
            Data da pronto: <strong><?php echo $this->_tpl_vars['pedido']['data_envio']; ?>
</strong>
          </span>
          <?php endif; ?> 
        </td>
    </tr>
    <tr>
        <td>
            <span style="color:#393939">Cliente: <strong><?php echo $this->_tpl_vars['pedido']['nomeCliente']; ?>
</strong></span><br />
            <span style="color:#393939">E-mail: <strong><?php echo $this->_tpl_vars['pedido']['emailCliente']; ?>
</strong></span><br />
            <span style="color:#393939">Telefone: <strong><?php echo $this->_tpl_vars['pedido']['telefoneCliente']; ?>
</strong></span><br />
            <span style="color:#393939">Agendado: <strong><?php echo $this->_tpl_vars['pedido']['agendado']; ?>
 horas</strong></span>
        </td>
    </tr>
</table>

<br />

<div class="tit_pagamento" style="color: #393939;
font-size: 14px;
font-weight: bold;
margin: auto;
position: relative;
width: 100%;">DADOS DE SUA COMPRA:</div>

<?php $_from = $this->_tpl_vars['pedido']['produtos']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['prod']):
?>
<table cellpadding="0" cellspacing="0" width="100%" style="margin:auto;">
    <tr>
        <td align="left" valign="middle">
        <br />
        <strong>Item <?php echo $this->_tpl_vars['prod']['item']; ?>
</strong>:<br />
        
        <?php if ($this->_tpl_vars['prod']['id_pao'] != 3): ?>
        
            Produto: <strong><?php echo $this->_tpl_vars['prod']['nomePao']; ?>
 - <?php echo $this->_tpl_vars['prod']['nomeProduto']; ?>
</strong><br />
            <?php if ($this->_tpl_vars['prod']['nomeDoce'] == ""): ?>
            Queijo: <strong><?php echo $this->_tpl_vars['prod']['nomeQueijo']; ?>
</strong><br />
            Salada: <strong><?php echo $this->_tpl_vars['prod']['nomeSalada']; ?>
</strong><br />
            Molho: <strong><?php echo $this->_tpl_vars['prod']['nomeMolho']; ?>
</strong><br />
            Livre: <strong><?php echo $this->_tpl_vars['prod']['nomeLivre']; ?>
</strong><br />
            Suco:  <strong><?php echo $this->_tpl_vars['prod']['nomeSuco']; ?>
</strong><br />
            <?php else: ?>
            Doce: <strong><?php echo $this->_tpl_vars['prod']['nomeDoce']; ?>
</strong><br />
            Suco:  <strong><?php echo $this->_tpl_vars['prod']['nomeSuco']; ?>
</strong><br />
            <?php endif; ?>
            
        <?php else: ?>
        
            Produto: <strong><?php echo $this->_tpl_vars['prod']['nomePao']; ?>
 - <?php echo $this->_tpl_vars['prod']['nomeProduto']; ?>
</strong><br />
            Carboidratos: <strong><?php echo $this->_tpl_vars['prod']['carboidratos']; ?>
</strong><br />
            Proteínas: <strong><?php echo $this->_tpl_vars['prod']['proteinas']; ?>
</strong><br />
            Legumes: <strong><?php echo $this->_tpl_vars['prod']['legumes']; ?>
</strong><br />
            Verduras: <strong><?php echo $this->_tpl_vars['prod']['verduras']; ?>
</strong><br />
            Sementes:  <strong><?php echo $this->_tpl_vars['prod']['sementes']; ?>
</strong><br />
            Frutas:  <strong><?php echo $this->_tpl_vars['prod']['frutas']; ?>
</strong><br />
            Molhos:  <strong><?php echo $this->_tpl_vars['prod']['molhos']; ?>
</strong><br />
            Livres:  <strong><?php echo $this->_tpl_vars['prod']['livres']; ?>
</strong><br />
            Adicionais:  <strong><?php echo $this->_tpl_vars['prod']['adicionais']; ?>
</strong><br />
            
        <?php endif; ?>

        
        Valor Produto: <strong>R$ <?php echo ((is_array($_tmp=$this->_tpl_vars['prod']['ValorItem'])) ? $this->_run_mod_handler('replace', true, $_tmp, ".", ",") : smarty_modifier_replace($_tmp, ".", ",")); ?>
</strong>
        
        </td>
    </tr>
</table>
<?php endforeach; endif; unset($_from); ?>   
<br />
<div style="color: #000000;
font-size: 14px;
font-weight: bold;
margin: auto;
position: relative;
text-align:center;
background-color:#c1c1c1;
width: 100%;">Valor Total Compra: <strong>R$ <?php echo ((is_array($_tmp=$this->_tpl_vars['pedido']['valor_total'])) ? $this->_run_mod_handler('replace', true, $_tmp, ".", ",") : smarty_modifier_replace($_tmp, ".", ",")); ?>
</strong></div>

<div style="color: #FFFFFF;
font-size: 14px;
font-weight: bold;
margin: auto;
position: relative;
text-align:center;
background-color:#0000FF;
margin-top:5px;
cursor:pointer;
padding:5px 0;
width: 100%;" onclick="location.href='index.php?secao=pedido&opcao=marcar_envio_salvar&idPedido=<?php echo $this->_tpl_vars['pedido']['id']; ?>
&pagina=todos'">Avisar que est&aacute; pronto</div>


</div>

</td>
<?php endforeach; endif; unset($_from); ?>
</tr>
</table>
</div>