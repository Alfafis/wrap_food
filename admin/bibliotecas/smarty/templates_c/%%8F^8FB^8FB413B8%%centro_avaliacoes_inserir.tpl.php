<?php /* Smarty version 2.6.18, created on 2017-10-16 05:56:06
         compiled from centro_avaliacoes_inserir.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_options', 'centro_avaliacoes_inserir.tpl', 48, false),)), $this); ?>
<?php echo '
<script>
	function validarFormulario()
	{		 		
				
		if(document.form_cliente.id_cliente.value == ""){
			alert("Selecione o Cliente!");
			return false;
		}
		
		if(document.form_cliente.id_produto.value == ""){
			alert("Selecione o Produto!");
			return false;
		}
		
			
		document.form_cliente.submit();
	}
	 
</script>
'; ?>

<td class="mainContentArea">
    <form name="form_cliente" method="post" action="index.php?secao=avaliacoes&opcao=salvar" enctype="multipart/form-data">
        <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
            <tbody>
                <tr>
                    <td colspan="2" class="buttonRow">
                    <input name="save" value="" onclick="validarFormulario();" class="normalButtonSalvar" type="button">
                    <input name="cancel" value="" onClick="window.location='index.php?secao=avaliacoes'" class="normalButtonCancelar" type="button">
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="mainHeader">Cadastro de Avalia&ccedil;&atilde;o </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top" width="70%">
                        <table border="0" cellpadding="2" cellspacing="0" width="100%">
                            <tbody>	
                            	
                                <tr>
                                    <td class="labelCell" align="right">Cliente:</td>
                                    <td class="contentCell" align="left">
                                    <select name="id_cliente" class="inputBox" >
                                    <option value="">Selecione o cliente</option>
                                    <?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['selectClientes']), $this);?>

                                    </select>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="labelCell" align="right">Produto:</td>
                                    <td class="contentCell" align="left">
                                    <select name="id_produto" class="inputBox" >
                                    <option value="">Selecione o produto</option>
                                    <?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['selectProdutos']), $this);?>

                                    </select>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="labelCell" align="right">Nota</td>
                                    <td class="contentCell">
                                    <select name="nota" class="inputBox">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </select>
                                    </td>
                                </tr>
                                  
                                
                                <tr>
                                    <td class="labelCell" align="right">Avaliação:</td>
                                    <td class="contentCell"> 
                                    <textarea name="avaliacao" id="texto" ></textarea>
                                    <br /><br />
                                    </td>
                                </tr>	
                                
                                <tr>
                                    <td class="labelCell" align="right">Indicaria?</td>
                                    <td class="contentCell">
                                    <select name="indica" class="inputBox">
                                        <option value="0">Não</option>
                                        <option value="1">Sim</option>
                                    </select>
                                    </td>
                                </tr>
                                                              
                                <tr>
                                    <td class="labelCell" align="right">Aprovado:</td>
                                    <td class="contentCell">
                                    <select name="aprovado" class="inputBox">
                                        <option value="0">Não</option>
                                        <option value="1">Sim</option>
                                    </select>
                                    </td>
                                </tr>
                               
                                
                                
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2" class="buttonRow">
                    <input name="save" value="" onclick="validarFormulario();" class="normalButtonSalvar" type="button">
                    <input name="cancel" value="" onClick="window.location='index.php?secao=avaliacoes'" class="normalButtonCancelar" type="button">                     
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</td>
<script>
	document.getElementById('nome').focus();
</script>
 