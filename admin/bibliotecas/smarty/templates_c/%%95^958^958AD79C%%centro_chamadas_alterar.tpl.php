<?php /* Smarty version 2.6.18, created on 2017-09-22 11:18:18
         compiled from centro_chamadas_alterar.tpl */ ?>
<?php echo '
<script>
	function validarFormulario()
	{		 		
		if(document.form_marca.nome.value == ""){
			alert("Preencha o campo Nome.");
			document.form_marca.nome.focus();
			return false;
		}
							
		document.form_marca.submit();
	}
	 
</script>
'; ?>

<td class="mainContentArea">
    <form name="form_marca" method="post" action="index.php?secao=chamadas&opcao=salvarAlteracao" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $this->_tpl_vars['chamadas']['id']; ?>
" />
        <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
            <tbody>
                <tr>
                    <td colspan="2" class="buttonRow">
                    <input name="save" value="" onclick="validarFormulario();" class="normalButtonSalvar" type="button">
                    <input name="cancel" value="" onClick="window.location='index.php?secao=chamadas'" class="normalButtonCancelar" type="button">
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="mainHeader">Altera&ccedil;&atilde;o de Chamada </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top" width="70%">
                        <table border="0" cellpadding="2" cellspacing="0" width="100%">
                            <tbody>	
                            	
                                <tr>
                                    <td class="labelCell" align="right">T&iacute;tulo:</td>
                                    <td class="contentCell"><input type="text" name="nome" size="50" value="<?php echo $this->_tpl_vars['chamadas']['nome']; ?>
" class="inputBox" id="nome"></td>
                                </tr>
                                
                                <tr>
                                    <td class="labelCell" align="right">Link:</td>
                                    <td class="contentCell"><input type="text" name="link" size="50" value="<?php echo $this->_tpl_vars['chamadas']['link']; ?>
" class="inputBox" id="link"></td>
                                </tr>
                                
                                <!--<tr>
                                    <td class="labelCell" align="right">Posi&ccedil;&atilde;o:</td>
                                    <td class="contentCell"><select name="posicao" class="inputBox" id="posicao">
                                    <option value="1" <?php if ($this->_tpl_vars['chamadas']['posicao'] == 1): ?>selected="selected"<?php endif; ?>>1</option>
                                    <option value="2" <?php if ($this->_tpl_vars['chamadas']['posicao'] == 2): ?>selected="selected"<?php endif; ?>>2</option>
                                    <option value="3" <?php if ($this->_tpl_vars['chamadas']['posicao'] == 3): ?>selected="selected"<?php endif; ?>>3</option>
                                    <option value="4" <?php if ($this->_tpl_vars['chamadas']['posicao'] == 4): ?>selected="selected"<?php endif; ?>>4</option>
                                    <option value="5" <?php if ($this->_tpl_vars['chamadas']['posicao'] == 5): ?>selected="selected"<?php endif; ?>>5</option>
                                    <option value="6" <?php if ($this->_tpl_vars['chamadas']['posicao'] == 6): ?>selected="selected"<?php endif; ?>>6</option>
                                    </select></td>
                                </tr>-->
                                
                                <tr>
                                    <td class="labelCell" align="right">Arquivo:</td>
                                    <td class="contentCell">
                                    <?php if ($this->_tpl_vars['chamadas']['arquivo']): ?>
                                    <img src="../banner/<?php echo $this->_tpl_vars['chamadas']['arquivo']; ?>
" width="300px" style="border:solid 1px #CCCCCC;" />
                                    <br />
                                    <?php endif; ?>
                                    <input type="file" name="arquivo" size="50" value="" class="inputBox" id="arquivo">
                                    <br />Dimens&otilde;es:<br /> 
                                    508px X 286px<br />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCell" align="right">Pequena chamada:</td>
                                    <td class="contentCell"> 
                                    <textarea name="texto" id="texto"><?php echo $this->_tpl_vars['chamadas']['texto']; ?>
</textarea>
                                    <br /><br />
                                    </td>
                                </tr>	
                                
                                
                                
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2" class="buttonRow">
                    <input name="save" value="" onclick="validarFormulario();" class="normalButtonSalvar" type="button">
                    <input name="cancel" value="" onClick="window.location='index.php?secao=chamadas'" class="normalButtonCancelar" type="button">                     
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</td>
<script>
	document.getElementById('nome').focus();
</script>
 