<?php /* Smarty version 2.6.18, created on 2017-04-14 07:06:04
         compiled from centro_fornecedores_alterar.tpl */ ?>
<?php echo '
<script>
	function validarFormulario()
	{		 		
	
		if(document.form_cliente.nome.value == ""){
			alert("Digite o Nome!");
			document.form_cliente.nome.focus();
			return false;
		}
				
			
		document.form_cliente.submit();
		
	}
	 
</script>
'; ?>

<td class="mainContentArea">
    <form name="form_cliente" method="post" action="index.php?secao=fornecedores&opcao=salvarAlteracao" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $this->_tpl_vars['fornecedor']['id']; ?>
" />
        <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
            <tbody>
                <tr>
                    <td colspan="2" class="buttonRow">
                    <input name="save" value="" onclick="validarFormulario();" class="normalButtonSalvar" type="button">
                    <input name="cancel" value="" onClick="window.location='index.php?secao=fornecedores'" class="normalButtonCancelar" type="button">
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="mainHeader">Altera&ccedil;&atilde;o de Fornecedor </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top" width="70%">
                        <table border="0" cellpadding="2" cellspacing="0" width="100%">
                            <tbody>	
                            
                            	<tr>
                                    <td class="labelCell" align="right">Nome/Empresa(*):</td>
                                    <td class="contentCell"><input type="text" name="nome" size="50" value="<?php echo $this->_tpl_vars['fornecedor']['nome']; ?>
" class="inputBox" id="nome"></td>
                                </tr>
                                
                                <tr>
                                    <td class="labelCell" align="right">E-mail(*):</td>
                                    <td class="contentCell"><input type="text" name="email" size="50" value="<?php echo $this->_tpl_vars['fornecedor']['email']; ?>
" class="inputBox" id="email" disabled="disabled"></td>
                                </tr>
                                                                
                                <tr>
                                    <td class="labelCell" align="right">Telefone:</td>
                                    <td class="contentCell"><input type="text" name="telefone" size="50" value="<?php echo $this->_tpl_vars['fornecedor']['telefone']; ?>
" class="inputBox" id="telefone" placeholder="(xx) xxxxx-xxxx" maxlength="15" onkeypress="mascara(this, mascaraTelefone)"></td>
                                </tr>
                                
                                <tr>
                                    <td class="labelCell" align="right">CEP:</td>
                                    <td class="contentCell"><input type="text" name="cep" size="50" value="<?php echo $this->_tpl_vars['fornecedor']['cep']; ?>
" class="inputBox" id="cep" maxlength="9" onkeypress="return mascaraCep(event,this,'#####-###');"></td>
                                </tr>
                                
                                <tr>
                                    <td class="labelCell" align="right">Endereço Completo:</td>
                                    <td class="contentCell"><input type="text" name="endereco" size="50" value="<?php echo $this->_tpl_vars['fornecedor']['endereco']; ?>
" class="inputBox" id="endereco"></td>
                                </tr>
                                
                                <tr>
                                    <td class="labelCell" align="right">Número:</td>
                                    <td class="contentCell"><input type="text" name="numero" size="50" value="<?php echo $this->_tpl_vars['fornecedor']['numero']; ?>
" class="inputBox" id="numero"></td>
                                </tr>
                                
                                <tr>
                                    <td class="labelCell" align="right">Complemento:</td>
                                    <td class="contentCell"><input type="text" name="complemento" size="50" value="<?php echo $this->_tpl_vars['fornecedor']['complemento']; ?>
" class="inputBox" id="complemento"></td>
                                </tr>
                                
                                <tr>
                                    <td class="labelCell" align="right">Bairro:</td>
                                    <td class="contentCell"><input type="text" name="bairro" size="50" value="<?php echo $this->_tpl_vars['fornecedor']['bairro']; ?>
" class="inputBox" id="bairro"></td>
                                </tr>
                                
                                <tr>
                                    <td class="labelCell" align="right">Cidade:</td>
                                    <td class="contentCell"><input type="text" name="cidade" size="50" value="<?php echo $this->_tpl_vars['fornecedor']['cidade']; ?>
" class="inputBox" id="cidade"></td>
                                </tr>
                                
                                <tr>
                                    <td class="labelCell" align="right">UF:</td>
                                    <td class="contentCell"><input type="text" name="estado" size="50" value="<?php echo $this->_tpl_vars['fornecedor']['estado']; ?>
" class="inputBox" id="estado"></td>
                                </tr>
                                
                                
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2" class="buttonRow">
                    <input name="save" value="" onclick="validarFormulario();" class="normalButtonSalvar" type="button">
                    <input name="cancel" value="" onClick="window.location='index.php?secao=cliente'" class="normalButtonCancelar" type="button">                     
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</td>
<script>
	document.getElementById('nome').focus();
</script>
 