<?php /* Smarty version 2.6.18, created on 2017-09-06 08:38:26
         compiled from centro_aviso_produto_listar.tpl */ ?>
	<?php echo '
	
	<script type="text/javascript">
	 
	jQuery(document).ready(function(){
	
	jQuery("#listagemDados").jqGrid({
		
		'; ?>

		url:'index.php?secao=aviso_produto&opcao=listar&nd='+new Date().getTime(),
		<?php echo '
				
		datatype: "json",
	
		colNames:[\'C&oacute;digo\', \'Nome\', \'E-mail\', \'Produto\', \'Data Cadastros\'],
		
		colModel:[
			{name:\'id\',index:\'id\', width:60},
			{name:\'nome\',index:\'nome\', width:200},
			{name:\'email\',index:\'email\', width:200},
			{name:\'produto\',index:\'produto\', width:300},
			{name:\'data_cadastro\',index:\'data_cadastro\', width:140}
		],
		
		pager: jQuery(\'#paginacao\'),
		
		rowNum:100,
		
		rowList:[100,200,300],
		
		imgpath: \'imgs\',
		
		sortname: \'data_cadastro\',
		
		viewrecords: true,
		
		sortorder: "desc",
		
		multiselect: true, 
		
		caption: "Listagem de Avisos"
	});
	
	
	jQuery("#btn_excluir").click( 
		function(){ 
		
			var listaIdForum; 
			
			listaIdForum = jQuery("#listagemDados").getGridParam(\'selarrrow\'); 
			
			if (listaIdForum != \'\') { 
				
				if(confirm("Tem certeza que gostaria de excluir este(s) iten(s)?")){
				
					'; ?>
	
					window.location = 'index.php?secao=news&opcao=excluir&listaIdForum='+ listaIdForum;
					<?php echo '	
				} 
				
			} 
			else { 
				alert("Selecione o(s) news(s) que deseja excluir.");
			} 						
			
		}
		
	);
	
	jQuery("#btn_visualizar").click( 
		function(){ 
		
			'; ?>
	
				window.open('index.php?secao=aviso_produto&opcao=visualizar','page','toolbar=no,location=yes,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=600');  
			//window.location = 'index.php?secao=news&opcao=visualizar&listaIdForum='+ listaIdForum;
			<?php echo '			
			
		}
	);
	
	/*jQuery("#btn_visualizar").click( 
		function(){ 
		
			var listaIdForum; 
			
			listaIdForum = jQuery("#listagemDados").getGridParam(\'selarrrow\'); 
			
			if (listaIdForum != \'\') { 
				
					'; ?>
	
					    window.open('index.php?secao=news&opcao=visualizar&listaIdForum='+ listaIdForum,'page','toolbar=no,location=yes,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=600');  
					//window.location = 'index.php?secao=news&opcao=visualizar&listaIdForum='+ listaIdForum;
					<?php echo '
					
				
			} 
			else { 
				alert("Selecione o news que deseja visualizar.");
			} 						
			
		}
	);*/
	
	});

	</script>	
	'; ?>

	<td class="mainContentArea">	
	<br />		
	<table class="searchTable" border="0" cellpadding="0" cellspacing="0" width="">
    <tbody>
	<tr> 	 
		
		<?php if ($this->_tpl_vars['podeExcluir']): ?>
		<td align="center" width="112">	
		<a href="#" class="plainLink" id="btn_excluir"> <img src="imgs/remover.png" border="0" width="48" height="48" /><br />Excluir email(s)</a>  
		</td>		
		<?php endif; ?>
        
        <?php if ($this->_tpl_vars['podeVisualizar']): ?>
		<td align="center" width="130">	
		<a href="#" id="btn_visualizar" class="plainLink"> <img src="imgs/visualizar.png" border="0" width="48" height="48" /> <BR />
		Visualizar lista</a>  
		</td>		
		<?php endif; ?>
			
          
	</tr>
	<tr>
		<td align="center"></td>		
	</tr>
	</tbody>
	</table>  	
	<br />	
	<table id="listagemDados" class="scroll" cellpadding="0" cellspacing="0"></table>
<!-- pager definition. class scroll tels that we want to use the same theme as grid -->
	<div id="paginacao" class="scroll" style="text-align:center;"></div>				
	</td>