<?php /* Smarty version 2.6.18, created on 2017-07-19 14:19:17
         compiled from centro_colecao_inserir.tpl */ ?>
<?php echo '
<script>
	function validarFormulario()
	{		 		
				
		if(document.form_marca.nome.value == ""){
			alert("Preencha o campo Nome.");
			document.form_marca.nome.focus();
			return false;
		} 	
		
		if(document.form_marca.arquivo.value == ""){
			alert("insira uma imagem.");
			document.form_marca.arquivo.focus();
			return false;
		} 	
			
		document.form_marca.submit();
	}
	 
</script>
'; ?>

<td class="mainContentArea">
    <form name="form_marca" method="post" action="index.php?secao=colecao&opcao=salvar" enctype="multipart/form-data">
        <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
            <tbody>
                <tr>
                    <td colspan="2" class="buttonRow">
                    <input name="save" value="" onclick="validarFormulario();" class="normalButtonSalvar" type="button">
                    <input name="cancel" value="" onClick="window.location='index.php?secao=colecao'" class="normalButtonCancelar" type="button">
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="mainHeader">Cadastro de cole&ccedil;&atilde;o </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top" width="70%">
                        <table border="0" cellpadding="2" cellspacing="0" width="100%">
                            <tbody>	
                            	<tr>
                                    <td class="labelCell" align="right">Nome:</td>
                                    <td class="contentCell"><input type="text" name="nome" size="50" value="" class="inputBox" id="nome"></td>
                                </tr>
                                
                                <tr>
                                    <td class="labelCell" align="right">Posi&ccedil;&atilde;o:</td>
                                    <td class="contentCell"><input type="text" name="posicao" size="50" value="" class="inputBox" id="posicao"></td>
                                </tr>
                                
                                <tr>
                                    <td class="labelCell" align="right">P&aacute;gina Principal:</td>
                                    <td class="contentCell">
                                    <select name="principal" class="inputBox">
                                    <option value="0">N&atilde;o</option>
                                    <option value="1">Sim</option>
                                    </select>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="labelCell" align="right">Texto:</td>
                                    <td class="contentCell"> 
                                    <textarea name="texto" id="texto" ></textarea>
                                    <br /><br />
                                    </td>
                                </tr>	
                                
                                <tr>
                                    <td class="labelCell" align="right">Arquivo:</td>
                                    <td class="contentCell">
                                    <input type="file" name="arquivo" size="50" value="" class="inputBox" id="arquivo">
                                    <br />Dimens&otilde;es 520px X 380px.<br />
                                    </td>
                                </tr>
                                
                                
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2" class="buttonRow">
                    <input name="save" value="" onclick="validarFormulario();" class="normalButtonSalvar" type="button">
                    <input name="cancel" value="" onClick="window.location='index.php?secao=colecao'" class="normalButtonCancelar" type="button">                     
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</td>
<script>
	document.getElementById('titulo').focus();
</script>
 