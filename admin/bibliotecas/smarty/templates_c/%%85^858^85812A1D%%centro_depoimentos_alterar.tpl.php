<?php /* Smarty version 2.6.18, created on 2017-03-10 18:37:54
         compiled from centro_depoimentos_alterar.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_options', 'centro_depoimentos_alterar.tpl', 44, false),)), $this); ?>
<?php echo '
<script>
	function validarFormulario()
	{		 		
	
		if(document.form_cliente.id_cliente.value == ""){
			alert("Selecione o Cliente!");
			return false;
		}
					
		document.form_cliente.submit();
		
	}
	 
</script>
'; ?>

<td class="mainContentArea">
    <form name="form_cliente" method="post" action="index.php?secao=depoimentos&opcao=salvarAlteracao" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $this->_tpl_vars['depoimento']['id']; ?>
" />
        <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
            <tbody>
                <tr>
                    <td colspan="2" class="buttonRow">
                    <input name="save" value="" onclick="validarFormulario();" class="normalButtonSalvar" type="button">
                    <input name="cancel" value="" onClick="window.location='index.php?secao=depoimentos'" class="normalButtonCancelar" type="button">
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="mainHeader">Altera&ccedil;&atilde;o de Depoimento </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top" width="70%">
                        <table border="0" cellpadding="2" cellspacing="0" width="100%">
                            <tbody>	
                            
                            	<tr>
                                    <td class="labelCell" align="right">Cliente:</td>
                                    <td class="contentCell" align="left">
                                    <select name="id_cliente" class="inputBox" >
                                    <option value="">Selecione o cliente</option>
                                    <?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['selectClientes'],'selected' => $this->_tpl_vars['depoimento']['id_cliente']), $this);?>

                                    </select>
                                    </td>
                                </tr>
                                                                
                                <tr>
                                    <td class="labelCell" align="right">Aprovado:</td>
                                    <td class="contentCell">
                                    <select name="aprovado" class="inputBox">
                                        <option value="0" <?php if ($this->_tpl_vars['depoimento']['aprovado'] == 0): ?>selected="selected"<?php endif; ?>>Não</option>
                                        <option value="1" <?php if ($this->_tpl_vars['depoimento']['aprovado'] == 1): ?>selected="selected"<?php endif; ?>>Sim</option>
                                    </select>
                                    </td>
                                </tr>
                                
                                 <tr>
                                    <td class="labelCell" align="right">Foto Cliente:</td>
                                    <td class="contentCell">
                                    <?php if ($this->_tpl_vars['depoimento']['foto']): ?>
                                    <img src="../fotos/<?php echo $this->_tpl_vars['depoimento']['foto']; ?>
" width="100px" style="border:solid 1px #CCCCCC;" />
                                    <br />
                                    <?php endif; ?>
                                    <input type="file" name="foto" size="50" value="" class="inputBox" id="foto">
                                    <br />Dimens&otilde;es 150px X 150px.
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="labelCell" align="right">Depoimento:</td>
                                    <td class="contentCell"> 
                                    <textarea name="depoimento" id="texto"><?php echo $this->_tpl_vars['depoimento']['depoimento']; ?>
</textarea>
                                    <br /><br />
                                    </td>
                                </tr>	
                                
                                
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2" class="buttonRow">
                    <input name="save" value="" onclick="validarFormulario();" class="normalButtonSalvar" type="button">
                    <input name="cancel" value="" onClick="window.location='index.php?secao=depoimentos'" class="normalButtonCancelar" type="button">                     
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</td>
<script>
	document.getElementById('nome').focus();
</script>
 