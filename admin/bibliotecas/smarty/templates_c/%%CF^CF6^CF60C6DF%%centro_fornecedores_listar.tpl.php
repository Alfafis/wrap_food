<?php /* Smarty version 2.6.18, created on 2017-09-19 13:58:14
         compiled from centro_fornecedores_listar.tpl */ ?>
	<?php echo '
	
	<script type="text/javascript">
	 
	jQuery(document).ready(function(){
	
	jQuery("#listagemDados").jqGrid({
		
		'; ?>

		url:'index.php?secao=fornecedores&opcao=listar&nd='+new Date().getTime(),
		<?php echo '
				
		datatype: "json",
	
		colNames:[\'C&oacute;digo\', \'Nome\', \'E-mail\', \'Telefone\', \'Cidade\', \'Estado\', \'Data Cadastro\'],
		
		colModel:[
			{name:\'id\',index:\'id\', width:60},
			{name:\'nome\',index:\'nome\', width:250},
			{name:\'email\',index:\'email\', width:200},
			{name:\'telefone\',index:\'telefone\', width:100},
			{name:\'cidade\',index:\'cidade\', width:100},
			{name:\'estado\',index:\'estado\', width:100},
			{name:\'cadastro\',index:\'cadastro\', width:140}
		],
		
		pager: jQuery(\'#paginacao\'),
		
		rowNum:100,
		
		rowList:[100,200,300],
		
		imgpath: \'imgs\',
		
		sortname: \'cadastro\',
		
		viewrecords: true,
		
		sortorder: "asc",
		
		multiselect: true, 
		
		caption: "Listagem de Fornecedores"
	});
	
	
	jQuery("#btn_alterar").click( 
		function(){ 
					
			var id = jQuery("#listagemDados").getGridParam(\'selarrrow\'); 

			if(id.length == 1){

				'; ?>
			
				window.location = 'index.php?secao=fornecedores&opcao=alterar&idForum='+ id;
				<?php echo '						

			}else{
			
				if(id.length == 0)
					alert("Selecione o fornecedor que deseja alterar.");
				else 
					alert("Selecione apenas um fornecedor.");
			}				
		}
	
	); 
	
	
	jQuery("#btn_excluir").click( 
		function(){ 
		
			var listaIdForum; 
			
			listaIdForum = jQuery("#listagemDados").getGridParam(\'selarrrow\'); 
			
			if (listaIdForum != \'\') { 
				
				if(confirm("Tem certeza que gostaria de excluir este(s) fornecedor(es)?")){
				
					'; ?>
	
					window.location = 'index.php?secao=fornecedores&opcao=excluir&listaId='+ listaIdForum;
					<?php echo '	
				} 
				
			} 
			else { 
				alert("Selecione o(s) fornecedor(es) que deseja excluir.");
			} 						
			
		}
		
	);
	
	
	jQuery("#btn_bloquear").click( 
		function(){ 
		
			var listaIdForum; 
			
			listaIdForum = jQuery("#listagemDados").getGridParam(\'selarrrow\'); 
			
			if (listaIdForum != \'\') { 
				
				if(confirm("Tem certeza que gostaria de bloquear este(s) fornecedore(s)?")){
				
					'; ?>
	
					window.location = 'index.php?secao=fornecedores&opcao=bloquear&listaId='+ listaIdForum;
					<?php echo '	
				} 
				
			} 
			else { 
				alert("Selecione o(s) fornecedore(s) que deseja bloquear.");
			} 						
			
		}
		
	);
	
	jQuery("#btn_visualizar").click( 
		function(){ 
		
			var listaIdForum; 
			
			listaIdForum = jQuery("#listagemDados").getGridParam(\'selarrrow\'); 
			
			if (listaIdForum != \'\') { 
				
					'; ?>
	
					    window.open('index.php?secao=fornecedores&opcao=visualizar&listaId='+ listaIdForum,'page','toolbar=no,location=yes,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=600');  
					//window.location = 'index.php?secao=fornecedores&opcao=visualizar&listaIdForum='+ listaIdForum;
					<?php echo '
					
				
			} 
			else { 
				alert("Selecione o fornecedore que deseja visualizar.");
			} 						
			
		}
	);
	
	jQuery("#btn_aprovar").click( 
		function(){ 
		
			var listaIdForum; 
			
			listaIdForum = jQuery("#listagemDados").getGridParam(\'selarrrow\'); 
			
			if (listaIdForum != \'\') { 
				
				if(confirm("Tem certeza que gostaria de aprovar este(s) fornecedore(s)?")){
				
					'; ?>
	
					window.location = 'index.php?secao=fornecedores&opcao=aprovar&listaId='+ listaIdForum;
					<?php echo '	
				} 
				
			} 
			else { 
				alert("Selecione o(s) fornecedore(s) que deseja aprovar.");
			} 						
			
		}
		
	);
	
	});

	</script>	
	'; ?>

	<td class="mainContentArea">	
	<br />		
	<table class="searchTable" border="0" cellpadding="0" cellspacing="0" width="">
    <tbody>
	<tr> 	 
		
        <?php if ($this->_tpl_vars['podeIncluir']): ?>
		<td align="center" width="140">
		<a href="index.php?secao=fornecedores&opcao=inserir" class="plainLink"> <img src="imgs/adicionar.png" border="0" width="48" height="48" /><br />Cadastrar Fornecedor </a>
		</td>
		<?php endif; ?>	
        	
		<?php if ($this->_tpl_vars['podeAlterar']): ?>
		<td align="center" width="140">
		<a href="#" class="plainLink" id="btn_alterar"> <img src="imgs/alterar.png" border="0" width="48" height="48" /><br />Alterar Fornecedor</a>
		</td>
		<?php endif; ?>
        
		<?php if ($this->_tpl_vars['podeExcluir']): ?>
		<td align="center" width="140">	
		<a href="#" class="plainLink" id="btn_excluir"> <img src="imgs/remover.png" border="0" width="48" height="48" /><br />Excluir Fornecedores(s)</a>  
		</td>		
		<?php endif; ?>
        
        <?php if ($this->_tpl_vars['podeVisualizar']): ?>
		<td align="center" width="130">	
		<a href="#" id="btn_visualizar" class="plainLink"> <img src="imgs/visualizar.png" border="0" width="48" height="48" /> <BR />
		Visualizar Fornecedore</a>  
		</td>
        <?php endif; ?>
        
        <?php if ($this->_tpl_vars['podeAprovar']): ?>
		<td align="center" width="130">	
		<a href="#" class="plainLink" id="btn_aprovar"> <img src="imgs/aceitar.png" border="0" width="48" height="48" /> <BR />
		Aprovar Fornecedore(s)</a>  
		</td>		
		<?php endif; ?>	
        
        <?php if ($this->_tpl_vars['podeBloquear']): ?>
		<td align="center" width="130">	
		<a href="#" class="plainLink" id="btn_bloquear"> <img src="imgs/application_remove.png" border="0" width="48" height="48" /><br />
        Bloquear Fornecedore(s)</a>  
		</td>		
		<?php endif; ?>	
          
	</tr>
	<tr>
		<td align="center"></td>		
	</tr>
	</tbody>
	</table>  	
	<br />	
	<table id="listagemDados" class="scroll" cellpadding="0" cellspacing="0"></table>
<!-- pager definition. class scroll tels that we want to use the same theme as grid -->
	<div id="paginacao" class="scroll" style="text-align:center;"></div>				
	</td>