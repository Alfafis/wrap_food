<?php /* Smarty version 2.6.18, created on 2018-02-14 18:36:31
         compiled from centro_molhos_alterar.tpl */ ?>
<?php echo '
<script>
	function validarFormulario()
	{		 		
	
		if(document.form_cliente.nome.value == ""){
			alert("Preencha o campo Nome.");
			document.form_cliente.nome.focus();
			return false;
		}
					
		document.form_cliente.submit();
		
	}
	 
</script>
'; ?>

<td class="mainContentArea">
    <form name="form_cliente" method="post" action="index.php?secao=molhos&opcao=salvarAlteracao" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $this->_tpl_vars['molhos']['id']; ?>
" />
        <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
            <tbody>
                <tr>
                    <td colspan="2" class="buttonRow">
                    <input name="save" value="" onclick="validarFormulario();" class="normalButtonSalvar" type="button">
                    <input name="cancel" value="" onClick="window.location='index.php?secao=molhos'" class="normalButtonCancelar" type="button">
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="mainHeader">Altera&ccedil;&atilde;o de Molho</td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top" width="70%">
                        <table border="0" cellpadding="2" cellspacing="0" width="100%">
                            <tbody>	
                            	
                                <tr>
                                    <td class="labelCell" align="right">Nome:</td>
                                    <td class="contentCell"><input type="text" name="nome" size="50" value="<?php echo $this->_tpl_vars['molhos']['nome']; ?>
" class="inputBox" id="nome"></td>
                                </tr>
                            	                                
                                
                                 <tr>
                                    <td class="labelCell" align="right">Foto:</td>
                                    <td class="contentCell">
                                    <?php if ($this->_tpl_vars['molhos']['foto']): ?>
                                    <img src="../fotos/<?php echo $this->_tpl_vars['molhos']['foto']; ?>
" width="100px" style="border:solid 1px #CCCCCC;" />
                                    <br />
                                    <?php endif; ?>
                                    <input type="file" name="foto" size="50" value="" class="inputBox" id="foto">
                                    <br />Dimens&otilde;es 480px X 240px.
                                    </td>
                                </tr>
                                
                                
                                <tr>
                                    <td class="labelCell" align="right">Ativo:</td>
                                    <td class="contentCell">
                                    <select name="ativo" class="inputBox">
                                        <option value="0" <?php if ($this->_tpl_vars['molhos']['ativo'] == 0): ?>selected="selected"<?php endif; ?>>Não</option>
                                        <option value="1" <?php if ($this->_tpl_vars['molhos']['ativo'] == 1): ?>selected="selected"<?php endif; ?>>Sim</option>
                                    </select>
                                    </td>
                                </tr>
                                
                                
                                
                                
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2" class="buttonRow">
                    <input name="save" value="" onclick="validarFormulario();" class="normalButtonSalvar" type="button">
                    <input name="cancel" value="" onClick="window.location='index.php?secao=molhos'" class="normalButtonCancelar" type="button">                     
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</td>
<script>
	document.getElementById('nome').focus();
</script>
 