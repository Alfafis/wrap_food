<?php /* Smarty version 2.6.18, created on 2017-12-09 19:59:24
         compiled from centro_usuario_listar.tpl */ ?>
	<?php echo '
	
	<script type="text/javascript">
	
	// We use a document ready jquery function.
	jQuery(document).ready(function(){
	
	jQuery("#listagemDados").jqGrid({
		
		'; ?>

		url:'index.php?secao=usuario&opcao=listarDados&nd='+new Date().getTime(),
		<?php echo '
				
		datatype: "json",
	
		colNames:[\'Nome\', \'E-mail\'],
		
		colModel:[
			{name:\'nome\',index:\'nome\', width:300},
			{name:\'email\',index:\'email\', width:300},
		],
		
		pager: jQuery(\'#paginacao\'),
		
		rowNum:20,
		
		rowList:[20,50,100],
		
		imgpath: \'imgs\',
		
		sortname: \'nome\',
		
		viewrecords: true,
		
		sortorder: "asc",
		
		multiselect: true, 
		
		caption: "Listagem de usu�rios"
	});
	
	jQuery("#btn_alterar").click( 
		function(){ 
					
			var id = jQuery("#listagemDados").getGridParam(\'selarrrow\'); 

			if(id.length == 1){

				'; ?>
			
				window.location = 'index.php?secao=usuario&opcao=alterar&idUsuario='+ id;
				<?php echo '						

			}else{
			
				if(id.length == 0)
					alert("Selecione o usu�rio que deseja alterar.");
				else 
					alert("Selecione apenas um usu�rio.");
			}				
		}
	
	); 
	
	jQuery("#btn_excluir").click( 
		function(){ 
		
			var listaIdUsuarioExcluir; 
			
			listaIdUsuarioExcluir = jQuery("#listagemDados").getGridParam(\'selarrrow\'); 
			
			if (listaIdUsuarioExcluir != \'\') { 
				
				if(confirm("Tem certeza que gostaria de excluir estes itens?")){
				
					'; ?>
	
					window.location = 'index.php?secao=usuario&opcao=excluir&listaIdUsuarioExcluir='+ listaIdUsuarioExcluir;
					<?php echo '	
				} 
				
			} 
			else { 
				alert("Selecione os usu�rios que deseja excluir.");
			} 						
			
		}
	
	);
	
	});

	</script>	
	'; ?>

    <td class="mainContentArea">	
	<br />		
	<table class="searchTable" border="0" cellpadding="0" cellspacing="0" width="">
    <tbody>
	<tr> 	 
		<?php if ($this->_tpl_vars['podeIncluir']): ?>
		<td align="center" width="130">
		<a href="index.php?secao=usuario&opcao=inserir" class="plainLink"> <img src="imgs/adicionar.png" border="0" width="48" height="48" /> <BR />Incluir novo usu&aacute;rio</a>
		</td>
		<?php endif; ?>
		
		<?php if ($this->_tpl_vars['podeAlterar']): ?>
		<td align="center" width="112">
		<a href="#" class="plainLink" id="btn_alterar"> <img src="imgs/alterar.png" border="0" width="48" height="48" /> <BR />Alterar usu&aacute;rio</a>
		</td>
		<?php endif; ?>		
		<?php if ($this->_tpl_vars['podeExcluir']): ?>
		<td align="center" width="112">	
		<a href="#" class="plainLink" id="btn_excluir"> <img src="imgs/remover.png" border="0" width="48" height="48" /> <BR />
		Excluir usu&aacute;rio(s)</a>  
		</td>		
		<?php endif; ?>		
	</tr>
	<tr>
		<td align="center"></td>		
	</tr>
	</tbody>
	</table> 	
	<bR />	
	<table id="listagemDados" class="scroll" cellpadding="0" cellspacing="0"></table>

<!-- pager definition. class scroll tels that we want to use the same theme as grid -->
	<div id="paginacao" class="scroll" style="text-align:center;"></div>
				
	</td>