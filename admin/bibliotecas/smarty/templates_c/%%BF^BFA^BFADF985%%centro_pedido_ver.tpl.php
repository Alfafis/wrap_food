<?php /* Smarty version 2.6.18, created on 2018-05-04 18:10:26
         compiled from centro_pedido_ver.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'replace', 'centro_pedido_ver.tpl', 99, false),)), $this); ?>

<td class="mainContentArea">
    <form name="form_marca" method="post" action="index.php?secao=pedido&opcao=subcol_salvar" enctype="multipart/form-data">
        <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
            <tbody>
                <tr>
                    <td colspan="2" class="buttonRow">
                    <input name="voltar" value="" onClick="window.location='index.php?secao=pedido'" class="normalButtonVoltar" type="button">
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="mainHeader">Visualizando pedido <?php echo $this->_tpl_vars['pedidoProduto'][0]; ?>
 </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                
                  <tr>
                    <td colspan="2">
                    	<div style="width:95%;margin:auto;font-size:14px;">
                            <table cellpadding="0" cellspacing="0" width="65%" style="margin:auto;">
                                <tr>
                                    <td>
                                        <span style="color:#393939">Pedido: <strong><?php echo $this->_tpl_vars['pedidoProduto'][0]; ?>
</strong></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                      <span style="color:#393939">	
                                        Tipo pagamento: <strong><?php echo $this->_tpl_vars['pedido']['tipo']; ?>
</strong>
                                      </span>
                                      <br />
                                      <span style="color:#393939;">Status do pedido:</span>  
                                      <strong><?php echo $this->_tpl_vars['pedidoProduto'][3]; ?>
</strong>	
                                      <?php if ($this->_tpl_vars['pedidoProduto'][2] == '8'): ?>
                                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                      <span style="color:#393939">	
                                        Data da pronto: <strong><?php echo $this->_tpl_vars['pedidoProduto'][4]; ?>
</strong>
                                      </span>
                                      <?php endif; ?> 
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span style="color:#393939">Cliente: <strong><?php echo $this->_tpl_vars['nomeCliente']; ?>
</strong></span><br />
                                        <span style="color:#393939">E-mail: <strong><?php echo $this->_tpl_vars['emailCliente']; ?>
</strong></span><br />
                                        <span style="color:#393939">Telefone: <strong><?php echo $this->_tpl_vars['telefoneCliente']; ?>
</strong></span><br />
                                        <span style="color:#393939">Agendado: <strong><?php echo $this->_tpl_vars['agendado']; ?>
</strong></span>
                                    </td>
                                </tr>
                            </table>
                           
                           <br /><br />
                           
                           <div class="tit_pagamento" style="color: #393939;
                            font-size: 16px;
                            font-weight: bold;
                            margin: auto;
                            position: relative;
                            width: 65%;">DADOS DE SUA COMPRA:</div>
                           
                            <?php $_from = $this->_tpl_vars['produto']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['prod']):
?>
                            <table cellpadding="0" cellspacing="0" width="65%" style="margin:auto;">
                                <tr>
                                    <td align="left" valign="middle">
                                    Item <?php echo $this->_tpl_vars['prod']['item']; ?>
:<br />
                                    <br />
                                    
                                    <?php if ($this->_tpl_vars['prod']['id_pao'] != 3): ?>
                                    
                                        Produto: <strong><?php echo $this->_tpl_vars['prod']['nomePao']; ?>
 - <?php echo $this->_tpl_vars['prod']['nomeProduto']; ?>
</strong><br />
                                        <?php if ($this->_tpl_vars['prod']['nomeDoce'] == ""): ?>
                                        Queijo: <strong><?php echo $this->_tpl_vars['prod']['nomeQueijo']; ?>
</strong><br />
                                        Salada: <strong><?php echo $this->_tpl_vars['prod']['nomeSalada']; ?>
</strong><br />
                                        Molho: <strong><?php echo $this->_tpl_vars['prod']['nomeMolho']; ?>
</strong><br />
                                        Livre: <strong><?php echo $this->_tpl_vars['prod']['nomeLivre']; ?>
</strong><br />
                                        Suco:  <strong><?php echo $this->_tpl_vars['prod']['nomeSuco']; ?>
</strong><br />
                                        <?php else: ?>
                                        Doce: <strong><?php echo $this->_tpl_vars['prod']['nomeDoce']; ?>
</strong><br />
                                        Suco:  <strong><?php echo $this->_tpl_vars['prod']['nomeSuco']; ?>
</strong><br />
                                        <?php endif; ?>
                                        
                                    <?php else: ?>
                                    
                                    	Produto: <strong><?php echo $this->_tpl_vars['prod']['nomePao']; ?>
 - <?php echo $this->_tpl_vars['prod']['nomeProduto']; ?>
</strong><br />
                                        Carboidratos: <strong><?php echo $this->_tpl_vars['prod']['carboidratos']; ?>
</strong><br />
                                        Proteínas: <strong><?php echo $this->_tpl_vars['prod']['proteinas']; ?>
</strong><br />
                                        Legumes: <strong><?php echo $this->_tpl_vars['prod']['legumes']; ?>
</strong><br />
                                        Verduras: <strong><?php echo $this->_tpl_vars['prod']['verduras']; ?>
</strong><br />
                                        Sementes:  <strong><?php echo $this->_tpl_vars['prod']['sementes']; ?>
</strong><br />
                                        Frutas:  <strong><?php echo $this->_tpl_vars['prod']['frutas']; ?>
</strong><br />
                                        Molhos:  <strong><?php echo $this->_tpl_vars['prod']['molhos']; ?>
</strong><br />
                                        Livres:  <strong><?php echo $this->_tpl_vars['prod']['livres']; ?>
</strong><br />
                                        Adicionais:  <strong><?php echo $this->_tpl_vars['prod']['adicionais']; ?>
</strong><br />
                                        
                                    <?php endif; ?>
                     
                                    
                                    Valor Produto: <strong>R$ <?php echo ((is_array($_tmp=$this->_tpl_vars['prod']['ValorItem'])) ? $this->_run_mod_handler('replace', true, $_tmp, ".", ",") : smarty_modifier_replace($_tmp, ".", ",")); ?>
</strong>
                                    <br /><br /><br />
                                    
                                    </td>
                                </tr>
                            </table>
                            <?php endforeach; endif; unset($_from); ?>   
                            <br />
                            <br />
                            <div class="tit_pagamento" style="color: #393939;
                            font-size: 16px;
                            font-weight: bold;
                            margin: auto;
                            position: relative;
                            width: 65%;">Valor Total Compra: <strong>R$ <?php echo ((is_array($_tmp=$this->_tpl_vars['valorTotalCompra'])) ? $this->_run_mod_handler('replace', true, $_tmp, ".", ",") : smarty_modifier_replace($_tmp, ".", ",")); ?>
</strong></div>
                            </div>
                            
                    </td>
                  </tr>                                             
                                
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2" class="buttonRow">
                   <input name="voltar" value="" onClick="window.location='index.php?secao=pedido'" class="normalButtonVoltar" type="button">                   
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</td>
 