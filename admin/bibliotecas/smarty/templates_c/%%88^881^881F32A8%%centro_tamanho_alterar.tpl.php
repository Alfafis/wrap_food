<?php /* Smarty version 2.6.18, created on 2017-05-21 18:06:30
         compiled from centro_tamanho_alterar.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_options', 'centro_tamanho_alterar.tpl', 51, false),)), $this); ?>
<?php echo '
<script>
	function validarFormulario()
	{		 		
	
		if(document.form_marca.id_categoria.value == ""){
			alert("Selecione a categoria.");
			document.form_marca.id_categoria.focus();
			return false;
		}
		
		if(document.form_marca.tamanho.value == ""){
			alert("Preencha o campo Tamanho.");
			document.form_marca.tamanho.focus();
			return false;
		}	
			
		document.form_marca.submit();
	}
	 
</script>
'; ?>

<td class="mainContentArea">
    <form name="form_marca" method="post" action="index.php?secao=tamanho&opcao=salvarAlteracao" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $this->_tpl_vars['tamanho']['id']; ?>
" />
        <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
            <tbody>
                <tr>
                    <td colspan="2" class="buttonRow">
                    <input name="save" value="" onclick="validarFormulario();" class="normalButtonSalvar" type="button">
                    <input name="cancel" value="" onClick="window.location='index.php?secao=tamanho'" class="normalButtonCancelar" type="button">
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="mainHeader">Altera&ccedil;&atilde;o de tamanho </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top" width="70%">
                        <table border="0" cellpadding="2" cellspacing="0" width="100%">
                            <tbody>	
                            
                            
                            	<tr>
                                    <td class="labelCell" align="right">Categoria:</td>
                                    <td class="contentCell">
                                    <select name="id_categoria" id="id_categoria" class="inputBox" disabled="disabled">
                                    <option value="">Selecione a categoria</option>
                                    <?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['selectCategoria'],'selected' => $this->_tpl_vars['tamanho']['id_categoria']), $this);?>

                                    </select>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="labelCell" align="right">Tamanho:</td>
                                    <td class="contentCell"><input type="text" name="tamanho" size="50" value="<?php echo $this->_tpl_vars['tamanho']['tamanho']; ?>
" class="inputBox" id="tamanho"></td>
                                </tr>
                               
                                
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2" class="buttonRow">
                    <input name="save" value="" onclick="validarFormulario();" class="normalButtonSalvar" type="button">
                    <input name="cancel" value="" onClick="window.location='index.php?secao=tamanho'" class="normalButtonCancelar" type="button">                     
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</td>
<script>
	document.getElementById('tamanho').focus();
</script>
 