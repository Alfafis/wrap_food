<?php /* Smarty version 2.6.18, created on 2017-09-06 12:41:37
         compiled from centro_avaliacoes_listar.tpl */ ?>
	<?php echo '
	
	<script type="text/javascript">
	 
	jQuery(document).ready(function(){
	
	jQuery("#listagemDados").jqGrid({
		
		'; ?>

		url:'index.php?secao=avaliacoes&opcao=listar&nd='+new Date().getTime(),
		<?php echo '
				
		datatype: "json",
	
		colNames:[\'C&oacute;digo\', \'Cliente\', \'Produto\', \'Nota\', \'Aprovado\', \'Indicaria?\', \'Data Cadastro\'],
		
		colModel:[
			{name:\'id\',index:\'id\', width:60},
			{name:\'cliente\',index:\'cliente\', width:250},
			{name:\'produto\',index:\'produto\', width:250},
			{name:\'nota\',index:\'nota\', width:100},
			{name:\'status\',index:\'status\', width:150},
			{name:\'indica\',index:\'indica\', width:150},
			{name:\'cadastro\',index:\'cadastro\', width:140}
		],
		
		pager: jQuery(\'#paginacao\'),
		
		rowNum:100,
		
		rowList:[100,200,300],
		
		imgpath: \'imgs\',
		
		sortname: \'cadastro\',
		
		viewrecords: true,
		
		sortorder: "desc",
		
		multiselect: true, 
		
		caption: "Listagem de Depoimentos"
	});
	
	
	jQuery("#btn_alterar").click( 
		function(){ 
					
			var id = jQuery("#listagemDados").getGridParam(\'selarrrow\'); 

			if(id.length == 1){

				'; ?>
			
				window.location = 'index.php?secao=avaliacoes&opcao=alterar&idAv='+id;
				<?php echo '						

			}else{
			
				if(id.length == 0)
					alert("Selecione a avaliação que deseja alterar.");
				else 
					alert("Selecione apenas uma avaliação.");
			}				
		}
	
	); 
	
	
	jQuery("#btn_excluir").click( 
		function(){ 
		
			var listaIdForum; 
			
			listaIdForum = jQuery("#listagemDados").getGridParam(\'selarrrow\'); 
			
			if (listaIdForum != \'\') { 
				
				if(confirm("Tem certeza que gostaria de excluir esta(s) avaliação(ões)?")){
				
					'; ?>
	
					window.location = 'index.php?secao=avaliacoes&opcao=excluir&listaId='+ listaIdForum;
					<?php echo '	
				} 
				
			} 
			else { 
				alert("Selecione a(s) avaliação(ões) que deseja excluir.");
			} 						
			
		}
		
	);
	
	
	jQuery("#btn_bloquear").click( 
		function(){ 
		
			var listaIdForum; 
			
			listaIdForum = jQuery("#listagemDados").getGridParam(\'selarrrow\'); 
			
			if (listaIdForum != \'\') { 
				
				if(confirm("Tem certeza que gostaria de bloquear esta(s) avaliação(ões)?")){
				
					'; ?>
	
					window.location = 'index.php?secao=avaliacoes&opcao=bloquear&listaId='+ listaIdForum;
					<?php echo '	
				} 
				
			} 
			else { 
				alert("Selecione a(s) avaliação(ões) que deseja bloquear.");
			} 						
			
		}
		
	);
	
	jQuery("#btn_reprovar").click( 
		function(){ 
		
			var listaIdForum; 
			
			listaIdForum = jQuery("#listagemDados").getGridParam(\'selarrrow\'); 
			
			if (listaIdForum != \'\') { 
				
				if(confirm("Tem certeza que gostaria de reprovar esta(s) avaliação(ões)?")){
				
					'; ?>
	
					window.location = 'index.php?secao=avaliacoes&opcao=reprovar&listaId='+ listaIdForum;
					<?php echo '	
				} 
				
			} 
			else { 
				alert("Selecione a(s) avaliação(ões) que deseja reprovar.");
			} 						
			
		}
		
	);
	
	jQuery("#btn_visualizar").click( 
		function(){ 
		
			var listaIdForum; 
			
			listaIdForum = jQuery("#listagemDados").getGridParam(\'selarrrow\'); 
			
			if (listaIdForum != \'\') { 
				
					'; ?>
	
					    window.open('index.php?secao=avaliacoes&opcao=visualizar&listaId='+ listaIdForum,'page','toolbar=no,location=yes,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=600');  
					//window.location = 'index.php?secao=avaliacoes&opcao=visualizar&listaIdForum='+ listaIdForum;
					<?php echo '
					
				
			} 
			else { 
				alert("Selecione o cliente que deseja visualizar.");
			} 						
			
		}
	);
	
	
	jQuery("#btn_aprovar").click( 
		function(){ 
		
			var listaIdForum; 
			
			listaIdForum = jQuery("#listagemDados").getGridParam(\'selarrrow\'); 
			
			if (listaIdForum != \'\') { 
				
				if(confirm("Tem certeza que gostaria de aprovar esta(s) avaliação(ões)?")){
				
					'; ?>
	
					window.location = 'index.php?secao=avaliacoes&opcao=aprovar&listaId='+ listaIdForum;
					<?php echo '	
				} 
				
			} 
			else { 
				alert("Selecione a(s) avaliação(ões) que deseja aprovar.");
			} 						
			
		}
		
	);
	
	});

	</script>	
	'; ?>

	<td class="mainContentArea">	
	<br />		
	<table class="searchTable" border="0" cellpadding="0" cellspacing="0" width="">
    <tbody>
	<tr> 	 
		
        <?php if ($this->_tpl_vars['podeIncluir']): ?>
		<td align="center" width="140">
		<a href="index.php?secao=avaliacoes&opcao=inserir" class="plainLink"> <img src="imgs/adicionar.png" border="0" width="48" height="48" /><br />Cadastrar Avaliação </a>
		</td>
		<?php endif; ?>	
        	
		<?php if ($this->_tpl_vars['podeAlterar']): ?>
		<td align="center" width="140">
		<a href="#" class="plainLink" id="btn_alterar"> <img src="imgs/alterar.png" border="0" width="48" height="48" /><br />Alterar Avaliação</a>
		</td>
		<?php endif; ?>
        
		<?php if ($this->_tpl_vars['podeExcluir']): ?>
		<td align="center" width="140">	
		<a href="#" class="plainLink" id="btn_excluir"> <img src="imgs/remover.png" border="0" width="48" height="48" /><br />Excluir Avaliação(ões)</a>  
		</td>		
		<?php endif; ?>
        
        <?php if ($this->_tpl_vars['podeVisualizar']): ?>
		<td align="center" width="110">	
		<a href="#" id="btn_visualizar" class="plainLink"> <img src="imgs/visualizar.png" border="0" width="48" height="48" /> <BR />
		Visualizar</a>  
		</td>
        <?php endif; ?>
        
        <?php if ($this->_tpl_vars['podeAprovar']): ?>
		<td align="center" width="140">	
		<a href="#" class="plainLink" id="btn_aprovar"> <img src="imgs/aceitar.png" border="0" width="48" height="48" /> <BR />
		Aprovar Avaliação(ões)</a>  
		</td>		
		<?php endif; ?>
        
        <?php if ($this->_tpl_vars['podeReprovar']): ?>
		<td align="center" width="150">	
		<a href="#" class="plainLink" id="btn_reprovar"> <img src="imgs/application_remove.png" border="0" width="48" height="48" /><br />
        Reprovar Avaliação(õs)</a>  
		</td>		
		<?php endif; ?>		
        
        <?php if ($this->_tpl_vars['podeBloquear']): ?>
		<td align="center" width="130">	
		<a href="#" class="plainLink" id="btn_bloquear"> <img src="imgs/application_remove.png" border="0" width="48" height="48" /><br />
        Bloquear Avaliação(õs)</a>  
		</td>		
		<?php endif; ?>	
          
	</tr>
	<tr>
		<td align="center"></td>		
	</tr>
	</tbody>
	</table>  	
	<br />	
	<table id="listagemDados" class="scroll" cellpadding="0" cellspacing="0"></table>
<!-- pager definition. class scroll tels that we want to use the same theme as grid -->
	<div id="paginacao" class="scroll" style="text-align:center;"></div>				
	</td>