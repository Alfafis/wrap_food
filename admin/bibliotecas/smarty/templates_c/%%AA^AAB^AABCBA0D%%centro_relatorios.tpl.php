<?php /* Smarty version 2.6.18, created on 2018-03-14 14:00:14
         compiled from centro_relatorios.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'replace', 'centro_relatorios.tpl', 90, false),)), $this); ?>
	<?php echo '
	<script src="js/sorttable.js" type="text/javascript"></script>
	<script type="text/javascript">
	 
	jQuery(document).ready(function(){
	
	
	jQuery("#btn_pedidos").click( 
		function(){ 
			window.location = \'index.php?secao=relatorio&opcao=pedidos\';
		}
	
	); 
	
	
	jQuery("#btn_produtos").click( 
		function(){ 
			window.location = \'index.php?secao=relatorio&opcao=produtos\';
		}
	
	); 
	
	
	});
	
	
	</script>	
	'; ?>

	<td class="mainContentArea">	
	<br />		
	<table class="searchTable" border="0" cellpadding="0" cellspacing="0" width="">
    <tbody>
	<tr> 	 
		
		
        <?php if ($this->_tpl_vars['podeVisualizar']): ?>
		<td align="center" width="130">	
		<a href="#" id="btn_pedidos" class="plainLink"> <img src="imgs/visualizar.png" border="0" width="48" height="48" /> <BR />
		Relat&oacute;rio Pedidos</a>  
		</td>
        <td align="center" width="130">	
		<a href="#" id="btn_produtos" class="plainLink"> <img src="imgs/visualizar.png" border="0" width="48" height="48" /> <BR />
		Relat&oacute;rio Produtos</a>  
		</td>		
		<?php endif; ?>
        
          
	</tr>
	<tr>
		<td align="center"></td>		
	</tr>
	</tbody>
	</table>  	
	<br />	
	
    <?php if ($this->_tpl_vars['opcao'] != ""): ?>
    <?php if ($this->_tpl_vars['listaRelatorio']): ?>
    <center>    
    <a href="#" onclick="document.imprimeEXE.submit();"><img src="imgs/excel.jpg" width="100" /><br />Gerar excel</a>
    <BR />
    <?php echo $this->_tpl_vars['botoes_controle_paginas']; ?>

    </center>
    <BR /><BR />
    <?php endif; ?>
        <?php if ($this->_tpl_vars['opcao'] == 'pedidos'): ?>
        <?php if ($this->_tpl_vars['listaRelatorio']): ?>
        <div id="impressao">
         <h1 style="text-align:left;font-size:13px;line-height:normal;"><center>Relatório de Pedidos</h1>
         
         
         <table cellpadding="0" cellspacing="0" class="sortable">
            <tr style="background-color:#CCC;cursor:pointer;">
                <td class="td_c" align="center"><strong>Pedido</strong></td>
                <td class="td_c" align="center"><strong>Cliente</strong></td>
                <td class="td_c" align="center"><strong>Data_Compra</strong></td>
                <td class="td_c" align="center"><strong>Valor_Compra</strong></td>
                <td class="td_c" align="center"><strong>Status</strong></td>
                <td class="td_c" align="center"><strong>Desconto_Cupom</strong></td>
                <td class="td_c" align="center"><strong>Data_Pagamento</strong></td>
                <td class="td_c" align="center"><strong>Valor_Pagamento</strong></td>
                <td class="td_c" align="center"><strong>Tipo_Pagamento</strong></td>
                <td class="td_c" align="center"><strong>Hora_Agendamento</strong></td>
                <td class="td_c" align="center"><strong>Data_Pronto</strong></td>
            </tr>
            <?php $_from = $this->_tpl_vars['listaRelatorio']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['dados']):
?>
            <tr style="background-color:<?php echo $this->_tpl_vars['dados']['cor']; ?>
">
                <td class="td_c" align="center"><?php echo $this->_tpl_vars['dados']['id']; ?>
</td>
                <td class="td_c" align="center"><?php echo $this->_tpl_vars['dados']['cliente']; ?>
</td>
                <td class="td_c" align="center"><?php echo $this->_tpl_vars['dados']['data_compra']; ?>
</td>
                <td class="td_c" align="center">R$ <?php echo ((is_array($_tmp=$this->_tpl_vars['dados']['valorTotal'])) ? $this->_run_mod_handler('replace', true, $_tmp, ".", ",") : smarty_modifier_replace($_tmp, ".", ",")); ?>
</td>
                <td class="td_c" align="center"><?php echo $this->_tpl_vars['dados']['status']; ?>
</td>
                <td class="td_c" align="center"><?php echo $this->_tpl_vars['dados']['desconto']; ?>
%</td>
                <td class="td_c" align="center"><?php echo $this->_tpl_vars['dados']['data_pagamento']; ?>
</td>
                <td class="td_c" align="center">R$ <?php echo ((is_array($_tmp=$this->_tpl_vars['dados']['valor'])) ? $this->_run_mod_handler('replace', true, $_tmp, ".", ",") : smarty_modifier_replace($_tmp, ".", ",")); ?>
</td>
                <td class="td_c" align="center"><?php echo $this->_tpl_vars['dados']['id_tipo_pagamento']; ?>
</td>
                <td class="td_c" align="center"><?php echo $this->_tpl_vars['dados']['agendado']; ?>
</td>
                <td class="td_c" align="center"><?php echo $this->_tpl_vars['dados']['data_pronto']; ?>
</td>
            </tr>    
            <?php endforeach; endif; unset($_from); ?>
            
        </table>
        
        </div> 
        <br /><br />
        <?php else: ?>
        <p style="text-align:center;font-size:12px;">Sem dados a serem exibidos.</p>
        <?php endif; ?>
        
        <?php endif; ?><!-- fim if se for pedidos --> 
        
        
        
        <?php if ($this->_tpl_vars['opcao'] == 'produtos'): ?>
        <?php if ($this->_tpl_vars['listaRelatorio']): ?>
        <div id="impressao">
         <h1 style="text-align:left;font-size:13px;line-height:normal;"><center>Relatório de Produtos</h1>
         
         
         <table cellpadding="0" cellspacing="0" class="sortable">
            <tr style="background-color:#CCC;cursor:pointer;">
                <td class="td_c" align="center"><strong>Tipo</strong></td>
                <td class="td_c" align="center"><strong>Produto</strong></td>
                <td class="td_c" align="center"><strong>Qtde</strong></td>
                <td class="td_c" align="center"><strong>Valor</strong></td>
                <td class="td_c" align="center"><strong>Valor_Adicionais</strong></td>
                
                <td class="td_c" align="center"><strong>Pedido</strong></td>
                <td class="td_c" align="center"><strong>Cliente</strong></td>
                <td class="td_c" align="center"><strong>Data_Compra</strong></td>
                <td class="td_c" align="center"><strong>Valor_Compra</strong></td>
                <td class="td_c" align="center"><strong>Status</strong></td>
                <td class="td_c" align="center"><strong>Desconto_Cupom</strong></td>
                <td class="td_c" align="center"><strong>Data_Pagamento</strong></td>
                <td class="td_c" align="center"><strong>Valor_Pagamento</strong></td>
                <td class="td_c" align="center"><strong>Tipo_Pagamento</strong></td>
                <td class="td_c" align="center"><strong>Hora_Agendamento</strong></td>
                <td class="td_c" align="center"><strong>Data_Pronto</strong></td>
            </tr>
            <?php $_from = $this->_tpl_vars['listaRelatorio']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['dados']):
?>
            <tr style="background-color:<?php echo $this->_tpl_vars['dados']['cor']; ?>
">
            	<td class="td_c" align="center"><?php echo $this->_tpl_vars['dados']['pao']; ?>
</td>
                <td class="td_c" align="center"><?php echo $this->_tpl_vars['dados']['produto']; ?>
</td>
                <td class="td_c" align="center"><?php echo $this->_tpl_vars['dados']['qtde']; ?>
</td>
                <td class="td_c" align="center">R$ <?php echo ((is_array($_tmp=$this->_tpl_vars['dados']['preco_prod'])) ? $this->_run_mod_handler('replace', true, $_tmp, ".", ",") : smarty_modifier_replace($_tmp, ".", ",")); ?>
</td>
                <td class="td_c" align="center">R$ <?php if ($this->_tpl_vars['dados']['id_pao'] != 3): ?><?php echo ((is_array($_tmp=$this->_tpl_vars['dados']['preco_suco'])) ? $this->_run_mod_handler('replace', true, $_tmp, ".", ",") : smarty_modifier_replace($_tmp, ".", ",")); ?>
<?php else: ?><?php echo ((is_array($_tmp=$this->_tpl_vars['dados']['preco_adicionais'])) ? $this->_run_mod_handler('replace', true, $_tmp, ".", ",") : smarty_modifier_replace($_tmp, ".", ",")); ?>
<?php endif; ?></td>
                
                <td class="td_c" align="center"><?php echo $this->_tpl_vars['dados']['pedido']; ?>
</td>
                <td class="td_c" align="center"><?php echo $this->_tpl_vars['dados']['cliente']; ?>
</td>
                <td class="td_c" align="center"><?php echo $this->_tpl_vars['dados']['data_compra']; ?>
</td>
                <td class="td_c" align="center">R$ <?php echo ((is_array($_tmp=$this->_tpl_vars['dados']['valorTotal'])) ? $this->_run_mod_handler('replace', true, $_tmp, ".", ",") : smarty_modifier_replace($_tmp, ".", ",")); ?>
</td>
                <td class="td_c" align="center"><?php echo $this->_tpl_vars['dados']['status']; ?>
</td>
                <td class="td_c" align="center"><?php echo $this->_tpl_vars['dados']['desconto']; ?>
%</td>
                <td class="td_c" align="center"><?php echo $this->_tpl_vars['dados']['data_pagamento']; ?>
</td>
                <td class="td_c" align="center">R$ <?php echo ((is_array($_tmp=$this->_tpl_vars['dados']['valor'])) ? $this->_run_mod_handler('replace', true, $_tmp, ".", ",") : smarty_modifier_replace($_tmp, ".", ",")); ?>
</td>
                <td class="td_c" align="center"><?php echo $this->_tpl_vars['dados']['id_tipo_pagamento']; ?>
</td>
                <td class="td_c" align="center"><?php echo $this->_tpl_vars['dados']['agendado']; ?>
</td>
                <td class="td_c" align="center"><?php echo $this->_tpl_vars['dados']['data_pronto']; ?>
</td>
            </tr>    
            <?php endforeach; endif; unset($_from); ?>
            
        </table>
        
        </div> 
        <br /><br />
        <?php else: ?>
        <p style="text-align:center;font-size:12px;">Sem dados a serem exibidos.</p>
        <?php endif; ?>
        
        <?php endif; ?><!-- fim if se for pedidos --> 
    
    <?php else: ?>
    
    	<p style="text-align:center;font-size:12px;">Escolha um dos relat&oacute;rios.</p>
    
    <?php endif; ?>
      
    <form target="_blank" name="imprimeEXE" action="excel/gerar_excel.php" method="post">
        <input type="hidden" name="conteudoexe">
    </form>
                    
    <?php echo '
    <script language="javascript">
        document.imprimeEXE.conteudoexe.value = document.getElementById(\'impressao\').innerHTML;
    </script>
    '; ?>

      
   		
	</td>