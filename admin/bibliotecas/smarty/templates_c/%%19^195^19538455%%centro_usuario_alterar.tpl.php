<?php /* Smarty version 2.6.18, created on 2017-12-09 20:00:26
         compiled from centro_usuario_alterar.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_checkboxes', 'centro_usuario_alterar.tpl', 106, false),)), $this); ?>
<?php echo '
<script>

	function validarFormulario(){
	
		if(document.form_usuario.nome.value == ""){
			alert("Preencha o campo Nome.");
			document.form_usuario.nome.focus();
			return false;
		}
		 
						
		if(document.form_usuario.email.value != ""){
		
			if(validarEmail(document.form_usuario.email.value) == false){
				alert("Informe um E-mail v�lido.");
				document.form_usuario.email.focus();
				return false;
			}
		}

	
		if(document.form_usuario.login.value == ""){
			alert("Preencha o campo Login.");
			document.form_usuario.login.focus();
			return false;
		}
		
	
		if(document.form_usuario.senha.value == ""){
			alert("Preencha o campo Senha.");
			document.form_usuario.senha.focus();
			return false;
		}			
			
		document.form_usuario.submit();
	}
</script>
'; ?>



<td class="mainContentArea">

<form name="form_usuario" method="post" action="index.php?secao=usuario&opcao=salvarAlteracao" enctype="multipart/form-data">

<input type="hidden" name="idUsuario" value="<?php echo $this->_tpl_vars['usuario'][0]; ?>
" />

<input type="hidden" name="idAssociado" value="<?php echo $this->_tpl_vars['idAssociado']; ?>
" />


<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
<tbody>
<tr>
	<td colspan="2" class="buttonRow">
	<input name="save" value="Salvar" onclick="validarFormulario();" class="normalButton" type="button">
	<input name="cancel" value="Cancelar" onClick="window.location='index.php?secao=usuario&opcao=listar'" class="normalButton" type="button">
	</td>
</tr>

<tr>
    <td colspan="2">&nbsp;</td>
</tr>

<tr>
    <td colspan="2" class="mainHeader">Alterando usu&aacute;rio</td>
</tr>

<tr>
	<td colspan="2">&nbsp;</td>
</tr>

<tr>
    <td valign="top" width="50%">
	<table border="0" cellpadding="2" cellspacing="0" width="100%">
	<tbody>
	<tr>
		<td class="labelCell" align="right" width="15%">Nome:</td>
		<td class="contentCell"><input name="nome" id="nome" size="45" value="<?php echo $this->_tpl_vars['usuario'][1]; ?>
" class="inputBox" type="text"></td>
    </tr> 
	 
	<tr>
		<td class="labelCell" align="right">E-mail:</td>
		<td class="contentCell"><input name="email" size="45" value="<?php echo $this->_tpl_vars['usuario'][2]; ?>
" class="inputBox" type="text"></td>
    </tr> 
	
	<tr>
		<td class="labelCell" align="right">Login:</td>
		<td class="contentCell"><input name="login" size="21" maxlength="20" value="<?php echo $this->_tpl_vars['usuario'][3]; ?>
" class="inputBox" type="text"></td>
    </tr> 
	
	
	<tr>
		<td class="labelCell" align="right">Senha:</td>
		<td class="contentCell"><input name="senha" size="11" maxlength="10" value="<?php echo $this->_tpl_vars['usuario'][4]; ?>
" class="inputBox" type="password"></td>
    </tr> 
	
	<tr>	
		<td>&nbsp;</td>
	</tr>
	
	<tr>
	
		<td class="labelCell" align="right" valign="">Status do pedido que poder&aacute; ver:</td>
		<td class="contentCell"> 
		
		<?php echo smarty_function_html_checkboxes(array('name' => 'listaIdStatusPedido','selected' => $this->_tpl_vars['listaUsuarioHasStatusPedido'],'options' => $this->_tpl_vars['listaStatusPedido'],'separator' => "<br />"), $this);?>

		
		</td>
		
	</tr>  
	
	<tr>	
		<td>&nbsp;</td>
	</tr>
	
	<tr>
		<td class="labelCell" align="right" valign="top">Permiss&otilde;es:</td>
		<td class="contentCell">

		<?php $_from = $this->_tpl_vars['listaModuloUsuario']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['modulo']):
?>
		
		&bull; <?php echo $this->_tpl_vars['modulo'][1]; ?>
<BR />
		  
		<?php echo smarty_function_html_checkboxes(array('name' => 'listaIdModuloHasFuncionalidadeModulo','selected' => $this->_tpl_vars['listaUsuarioHasModuloHasFuncionalidadeModulo'],'options' => $this->_tpl_vars['modulo'][2],'separator' => "<br />"), $this);?>

		
		<BR />

		<?php endforeach; endif; unset($_from); ?>
		
		</td>
    </tr> 
	
	</tbody>
	</table>
    </td>
	
    <td align="left" valign="top" width="50%">&nbsp;</td>
</tr>
<tr>
    <td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" class="buttonRow">
	<input name="save" value="Salvar" onclick="validarFormulario();" class="normalButton" type="button">
	<input name="cancel" value="Cancelar" onClick="window.location='index.php?secao=usuario&opcao=listar'" class="normalButton" type="button">
	</td>
</tr>
</tbody>
</table>

</form>
</td>


<script>
 
document.getElementById('nome').focus();

</script>