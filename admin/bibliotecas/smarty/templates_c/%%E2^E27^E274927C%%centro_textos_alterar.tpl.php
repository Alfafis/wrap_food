<?php /* Smarty version 2.6.18, created on 2017-09-06 10:26:28
         compiled from centro_textos_alterar.tpl */ ?>
<?php echo '
<script>
	function validarFormulario()
	{		 
			
		document.form_marca.submit();
	}	 
</script>
'; ?>

<td class="mainContentArea">
    <form name="form_marca" method="post" action="index.php?secao=textos&opcao=salvarAlteracao" enctype="multipart/form-data">
        <input type="hidden" name="idTextos" value="<?php echo $this->_tpl_vars['textos']['id']; ?>
" id="idTextos" />
        <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
            <tbody>
                <tr>
                    <td colspan="2" class="buttonRow">
                    <input name="save" value="" onclick="validarFormulario();" class="normalButtonSalvar" type="button">
                    <input name="cancel" value="" onClick="window.location='index.php?secao=textos&opcao=alterar'" class="normalButtonCancelar" type="button">
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="mainHeader">Altera&ccedil;&atilde;o dos textos do site</td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top" width="70%">
                        <table border="0" cellpadding="2" cellspacing="0" width="100%">
                            <tbody>	
                            	
                                <tr>
                                    <td class="labelCell" align="center"></td>
                                    <td class="labelCell" style="color:#1B60E0;font-size:14px;"><br /><br />TODAS AS PÁGINAS</td>
                                </tr>	
                            		
                                <tr>
                                    <td class="labelCell" align="right">CNPJ:</td>
                                    <td class="contentCell"> 
                                    <input type="text" name="cnpj" id="cnpj" value="<?php echo $this->_tpl_vars['textos']['cnpj']; ?>
" style="width:400px;" />
                                    </td>
                                </tr>	
                                <tr>
                                    <td class="labelCell" align="right">Endereço:</td>
                                    <td class="contentCell"> 
                                    <input type="text" name="endereco" id="endereco" value="<?php echo $this->_tpl_vars['textos']['endereco']; ?>
" style="width:400px;" />
                                    </td>
                                </tr>	
                                <tr>
                                    <td class="labelCell" align="right">Telefone:</td>
                                    <td class="contentCell"> 
                                    <input type="text" name="telefone" id="telefone" value="<?php echo $this->_tpl_vars['textos']['telefone']; ?>
" style="width:400px;" />
                                    </td>
                                </tr>
                                	
                                <tr>
                                    <td class="labelCell" align="right">Whatsapp:</td>
                                    <td class="contentCell">
                                    <input type="text" name="zap" id="zap" value="<?php echo $this->_tpl_vars['textos']['zap']; ?>
" style="width:400px;" />
                                    </td>
                                </tr>
                                	
                                <tr>
                                    <td class="labelCell" align="right">Funcionamento:</td>
                                    <td class="contentCell">
                                    <input type="text" name="funcionamento" id="funcionamento" value="<?php echo $this->_tpl_vars['textos']['funcionamento']; ?>
" style="width:400px;" />
                                    </td>
                                </tr>
                                
                                
                                <tr>
                                    <td class="labelCell" align="center"></td>
                                    <td class="labelCell" style="color:#1B60E0;font-size:14px;"><br /><br />SOBRE NÓS</td>
                                </tr>
                                	
                                <tr>
                                    <td class="labelCell" align="right">SOBRE A VICENZI JOIAS:</td>
                                    <td class="contentCell"> 
                                    <textarea name="sobre" id="sobre" ><?php echo $this->_tpl_vars['textos']['sobre']; ?>
</textarea>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="labelCell" align="right">NOSSA MISSÃO E NOSSOS VALORES:</td>
                                    <td class="contentCell"> 
                                    <textarea name="missao_valores" id="missao_valores" ><?php echo $this->_tpl_vars['textos']['missao_valores']; ?>
</textarea>
                                    </td>
                                </tr>
                                
                                
                                
                                
                                <tr>
                                    <td class="labelCell" align="center"></td>
                                    <td class="labelCell" style="color:#1B60E0;font-size:14px;"><br /><br />COMPRE COM SEGURANÇA</td>
                                </tr>
                                	
                                <tr>
                                    <td class="labelCell" align="right">COMO COMPRAR:</td>
                                    <td class="contentCell"> 
                                    <textarea name="como_comprar" id="como_comprar" ><?php echo $this->_tpl_vars['textos']['como_comprar']; ?>
</textarea>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="labelCell" align="right">POLÍTICA DE PRIVACIDADE E SEGURANÇA:</td>
                                    <td class="contentCell"> 
                                    <textarea name="politica" id="politica" ><?php echo $this->_tpl_vars['textos']['politica']; ?>
</textarea>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="labelCell" align="right">TROCAS E DEVOLUÇÕES:</td>
                                    <td class="contentCell"> 
                                    <textarea name="trocas" id="trocas" ><?php echo $this->_tpl_vars['textos']['trocas']; ?>
</textarea>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="labelCell" align="right">ATENDIMENTO:</td>
                                    <td class="contentCell"> 
                                    <textarea name="atendimento" id="atendimento" ><?php echo $this->_tpl_vars['textos']['atendimento']; ?>
</textarea>
                                    </td>
                                </tr>
                                
                                
                                
                                
                                <tr>
                                    <td class="labelCell" align="center"></td>
                                    <td class="labelCell" style="color:#1B60E0;font-size:14px;"><br /><br />ENVIO E PAGAMENTO</td>
                                </tr>
                                	
                                <tr>
                                    <td class="labelCell" align="right">FORMAS DE PAGAMENTO:</td>
                                    <td class="contentCell"> 
                                    <textarea name="formas_pag" id="formas_pag" ><?php echo $this->_tpl_vars['textos']['formas_pag']; ?>
</textarea>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="labelCell" align="right">FORMAS DE ENVIO E CONDIÇÕES DE ENTREGA:</td>
                                    <td class="contentCell"> 
                                    <textarea name="formas_envio" id="formas_envio" ><?php echo $this->_tpl_vars['textos']['formas_envio']; ?>
</textarea>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="labelCell" align="right">POLÍTICA DE ENTREGA:</td>
                                    <td class="contentCell"> 
                                    <textarea name="politica_entrega" id="politica_entrega" ><?php echo $this->_tpl_vars['textos']['politica_entrega']; ?>
</textarea>
                                    </td>
                                </tr>
                                
                                
                                
                                
                                <tr>
                                    <td class="labelCell" align="center"></td>
                                    <td class="labelCell" style="color:#1B60E0;font-size:14px;"><br /><br />DICAS E DÚVIDAS</td>
                                </tr>
                                	
                                <tr>
                                    <td class="labelCell" align="right">FAQ:</td>
                                    <td class="contentCell"> 
                                    <textarea name="faq" id="faq" ><?php echo $this->_tpl_vars['textos']['faq']; ?>
</textarea>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="labelCell" align="right">CUIDADOS:</td>
                                    <td class="contentCell"> 
                                    <textarea name="cuidados" id="cuidados" ><?php echo $this->_tpl_vars['textos']['cuidados']; ?>
</textarea>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="labelCell" align="right">GARANTIA:</td>
                                    <td class="contentCell"> 
                                    <textarea name="garantia" id="garantia" ><?php echo $this->_tpl_vars['textos']['garantia']; ?>
</textarea>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="labelCell" align="right">PROMOÇÕES:</td>
                                    <td class="contentCell"> 
                                    <textarea name="promocoes" id="promocoes" ><?php echo $this->_tpl_vars['textos']['promocoes']; ?>
</textarea>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="labelCell" align="right">DICAS:</td>
                                    <td class="contentCell"> 
                                    <textarea name="dicas" id="dicas" ><?php echo $this->_tpl_vars['textos']['dicas']; ?>
</textarea>
                                    </td>
                                </tr>
                                
                                
                                
                                
                                <tr>
                                    <td class="labelCell" align="center"></td>
                                    <td class="labelCell" style="color:#1B60E0;font-size:14px;"><br /><br />REVENDER</td>
                                </tr>
                                	
                                <tr>
                                    <td class="labelCell" align="right">REVENDER:</td>
                                    <td class="contentCell"> 
                                    <textarea name="revenda" id="revenda" ><?php echo $this->_tpl_vars['textos']['revenda']; ?>
</textarea>
                                    </td>
                                </tr>
                                                                
                                
                                
                                <tr>
                                    <td class="labelCell" align="center"></td>
                                    <td class="labelCell" style="color:#1B60E0;font-size:14px;"><br /><br /><!--ASSINATURA MENSAL E -->CARTÃO FIDELIDADE</td>
                                </tr>
                                	
                                <!--<tr>
                                    <td class="labelCell" align="right">ASSINATURA MENSAL:</td>
                                    <td class="contentCell"> 
                                    <textarea name="assinatura" id="assinatura" ><?php echo $this->_tpl_vars['textos']['assinatura']; ?>
</textarea>
                                    </td>
                                </tr>-->
                                
                                <tr>
                                    <td class="labelCell" align="right">CARTÃO FIDELIDADE:</td>
                                    <td class="contentCell"> 
                                    <textarea name="cartao" id="cartao" ><?php echo $this->_tpl_vars['textos']['cartao']; ?>
</textarea>
                                    </td>
                                </tr>
                                
                            </tbody>
                        </table>
                    </td>	
                    <td align="left" valign="top" width="50%">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2" class="buttonRow">
                    <input name="save" value="" onclick="validarFormulario();" class="normalButtonSalvar" type="button">
                    <input name="cancel" value="" onClick="window.location='index.php?secao=textos&opcao=alterar'" class="normalButtonCancelar" type="button">
                     
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</td>
<script>
	document.getElementById('nome').focus();
</script>
 