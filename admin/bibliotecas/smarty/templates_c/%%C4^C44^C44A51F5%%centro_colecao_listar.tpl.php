<?php /* Smarty version 2.6.18, created on 2017-09-21 15:19:57
         compiled from centro_colecao_listar.tpl */ ?>
	<?php echo '
	
	<script type="text/javascript">
	 
	jQuery(document).ready(function(){
	
	jQuery("#listagemDados").jqGrid({
		
		'; ?>

		url:'index.php?secao=colecao&opcao=listar&nd='+new Date().getTime(),
		<?php echo '
				
		datatype: "json",
	
		colNames:[\'C&oacute;digo\', \'Nome\', \'PG Principal\', \'Posi&ccedil;&atilde;o\', \'Data Cadastro\'],
		
		colModel:[
			{name:\'id\',index:\'id\', width:60},
			{name:\'nome\',index:\'nome\', width:250},
			{name:\'principal\',index:\'principal\', width:100},
			{name:\'posicao\',index:\'posicao\', width:150},
			{name:\'data_cadastro\',index:\'data_cadastro\', width:140}
		],
		
		pager: jQuery(\'#paginacao\'),
		
		rowNum:100,
		
		rowList:[100,200,300],
		
		imgpath: \'imgs\',
		
		sortname: \'nome\',
		
		viewrecords: true,
		
		sortorder: "asc",
		
		multiselect: true, 
		
		caption: "Listagem de cole&ccedil;&otilde;es"
	});
	
	jQuery("#btn_alterar").click( 
		function(){ 
					
			var id = jQuery("#listagemDados").getGridParam(\'selarrrow\'); 

			if(id.length == 1){

				'; ?>
			
				window.location = 'index.php?secao=colecao&opcao=alterar&idForum='+ id;
				<?php echo '						

			}else{
			
				if(id.length == 0)
					alert("Selecione o colecao que deseja alterar.");
				else 
					alert("Selecione apenas uma colecao.");
			}				
		}
	
	); 
	
	jQuery("#btn_excluir").click( 
		function(){ 
		
			var listaIdForum; 
			
			listaIdForum = jQuery("#listagemDados").getGridParam(\'selarrrow\'); 
			
			if (listaIdForum != \'\') { 
				
				if(confirm("Tem certeza que gostaria de excluir este(s) iten(s)?")){
				
					'; ?>
	
					window.location = 'index.php?secao=colecao&opcao=excluir&listaId='+ listaIdForum;
					<?php echo '	
				} 
				
			} 
			else { 
				alert("Selecione o(s) colecao(es) que deseja excluir.");
			} 						
			
		}
		
	);
	
	jQuery("#btn_aprovar").click( 
		function(){ 
					
			var id = jQuery("#listagemDados").getGridParam(\'selarrrow\'); 

			if(id.length == 1){

				'; ?>
			
				window.location = 'index.php?secao=colecao&opcao=subcol&idForum='+ id;
				<?php echo '						

			}else{
			
				if(id.length == 0)
					alert("Selecione o colecao que deseja tornar sub-colecao.");
				else 
					alert("Selecione apenas uma colecao.");
			}				
		}
	
	); 
	
	/*
	jQuery("#btn_aprovar").click( 
		function(){ 
		
			var listaIdForum; 
			
			listaIdForum = jQuery("#listagemDados").getGridParam(\'selarrrow\'); 
			
			if (listaIdForum != \'\') { 
				
				if(confirm("Tem certeza que gostaria de aprovar este(s) iten(s)?")){
				
					'; ?>
	
					window.location = 'index.php?secao=colecao&opcao=aprovar&listaIdForum='+ listaIdForum;
					<?php echo '	
				} 
				
			} 
			else { 
				alert("Selecione a(s) colecao(s) que deseja aprovar.");
			} 						
			
		}
	);*/
	
	jQuery("#btn_visualizar").click( 
		function(){ 
		
			var listaIdForum; 
			
			listaIdForum = jQuery("#listagemDados").getGridParam(\'selarrrow\'); 
			
			if (listaIdForum != \'\') { 
				
					'; ?>
	
					    //window.open('index.php?secao=colecao&opcao=visualizar&listaIdForum='+ listaIdForum,'page','toolbar=no,location=yes,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=600');  
					window.location = 'index.php?secao=benner&opcao=visualizar&listaIdForum='+ listaIdForum;
					<?php echo '
					
				
			} 
			else { 
				alert("Selecione a(s) postagem(s) que deseja visualizar.");
			} 						
			
		}
	);
	
	});

	</script>	
	'; ?>

	<td class="mainContentArea">	
	<br />		
	<table class="searchTable" border="0" cellpadding="0" cellspacing="0" width="">
    <tbody>
	<tr> 	 
		<?php if ($this->_tpl_vars['podeIncluir']): ?>
		<td align="center" width="112">
		<a href="index.php?secao=colecao&opcao=inserir" class="plainLink"> <img src="imgs/adicionar.png" border="0" width="48" height="48" /><br />Cadastrar cole&ccedil;&atilde;o </a>
		</td>
		<?php endif; ?>	
		<?php if ($this->_tpl_vars['podeAlterar']): ?>
		<td align="center" width="112">
		<a href="#" class="plainLink" id="btn_alterar"> <img src="imgs/alterar.png" border="0" width="48" height="48" /><br />Alterar cole&ccedil;&atilde;o</a>
		</td>
		<?php endif; ?>
		<?php if ($this->_tpl_vars['podeExcluir']): ?>
		<td align="center" width="112">	
		<a href="#" class="plainLink" id="btn_excluir"> <img src="imgs/remover.png" border="0" width="48" height="48" /><br />Excluir cole&ccedil;&atilde;o(s)</a>  
		</td>		
		<?php endif; ?>
        
        <?php if ($this->_tpl_vars['podeAprovar']): ?>
		<td align="center" width="112">	
		<a href="#" class="plainLink" id="btn_aprovar"> <img src="imgs/aceitar.png" border="0" width="48" height="48" /> <BR />
		Tornar sub-cole&ccedil;&atilde;o</a>  
		</td>		
		<?php endif; ?>
        <!--
        <?php if ($this->_tpl_vars['podeReprovar']): ?>
		<td align="center" width="130">	
		<a href="#" class="plainLink" id="btn_reprovar"> <img src="imgs/application_remove.png" border="0" width="48" height="48" /> <BR />
		Reprovar Postagem(ns)</a>  
		</td>		
		<?php endif; ?>
        
      
        
        <?php if ($this->_tpl_vars['podeVisualizar']): ?>
		<td align="center" width="130">	
		<a href="#" id="btn_visualizar" class="plainLink"> <img src="imgs/visualizar.png" border="0" width="48" height="48" /> <BR />
		Visualizar Postagem(ns)</a>  
		</td>		
		<?php endif; ?>
          -->
	</tr>
	<tr>
		<td align="center"></td>		
	</tr>
	</tbody>
	</table>  	
	<br />	
	<table id="listagemDados" class="scroll" cellpadding="0" cellspacing="0"></table>
<!-- pager definition. class scroll tels that we want to use the same theme as grid -->
	<div id="paginacao" class="scroll" style="text-align:center;"></div>				
	</td>