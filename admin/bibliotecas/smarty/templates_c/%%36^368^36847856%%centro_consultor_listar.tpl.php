<?php /* Smarty version 2.6.18, created on 2017-03-03 17:39:13
         compiled from centro_consultor_listar.tpl */ ?>
	<?php echo '
	
	<script type="text/javascript">
	 
	jQuery(document).ready(function(){
	
	jQuery("#listagemDados").jqGrid({
		
		'; ?>

		url:'index.php?secao=consultor&opcao=listar&nd='+new Date().getTime(),
		<?php echo '
				
		datatype: "json",
	
		colNames:[\'C&oacute;digo\', \'Nome\', \'E-mail\', \'Telefone\', \'Cidade\', \'Estado\', \'Data Cadastro\', \'Status\'],
		
		colModel:[
			{name:\'id\',index:\'id\', width:60},
			{name:\'nome\',index:\'nome\', width:250},
			{name:\'email\',index:\'email\', width:200},
			{name:\'telefone\',index:\'telefone\', width:100},
			{name:\'cidade\',index:\'cidade\', width:100},
			{name:\'estado\',index:\'estado\', width:100},
			{name:\'data_envio\',index:\'data_envio\', width:140},
			{name:\'ativo\',index:\'ativo\', width:140}
		],
		
		pager: jQuery(\'#paginacao\'),
		
		rowNum:100,
		
		rowList:[100,200,300],
		
		imgpath: \'imgs\',
		
		sortname: \'data_envio\',
		
		viewrecords: true,
		
		sortorder: "asc",
		
		multiselect: true, 
		
		caption: "Listagem de consultores"
	});
	
	
	jQuery("#btn_excluir").click( 
		function(){ 
		
			var listaIdForum; 
			
			listaIdForum = jQuery("#listagemDados").getGridParam(\'selarrrow\'); 
			
			if (listaIdForum != \'\') { 
				
				if(confirm("Tem certeza que gostaria de excluir este(s) iten(s)?")){
				
					'; ?>
	
					window.location = 'index.php?secao=consultor&opcao=excluir&listaIdForum='+ listaIdForum;
					<?php echo '	
				} 
				
			} 
			else { 
				alert("Selecione o(s) consultor(s) que deseja excluir.");
			} 						
			
		}
		
	);
	
	
	jQuery("#btn_bloquear").click( 
		function(){ 
		
			var listaIdForum; 
			
			listaIdForum = jQuery("#listagemDados").getGridParam(\'selarrrow\'); 
			
			if (listaIdForum != \'\') { 
				
				if(confirm("Tem certeza que gostaria de bloquear este(s) consultor(es)?")){
				
					'; ?>
	
					window.location = 'index.php?secao=consultor&opcao=bloquear&listaIdForum='+ listaIdForum;
					<?php echo '	
				} 
				
			} 
			else { 
				alert("Selecione o(s) consultor(s) que deseja bloquear.");
			} 						
			
		}
		
	);
	
	jQuery("#btn_visualizar").click( 
		function(){ 
		
			var listaIdForum; 
			
			listaIdForum = jQuery("#listagemDados").getGridParam(\'selarrrow\'); 
			
			if (listaIdForum != \'\') { 
				
					'; ?>
	
					    window.open('index.php?secao=consultor&opcao=visualizar&listaIdForum='+ listaIdForum,'page','toolbar=no,location=yes,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=600');  
					//window.location = 'index.php?secao=consultor&opcao=visualizar&listaIdForum='+ listaIdForum;
					<?php echo '
					
				
			} 
			else { 
				alert("Selecione o consultor que deseja visualizar.");
			} 						
			
		}
	);
	
	});

	</script>	
	'; ?>

	<td class="mainContentArea">	
	<br />		
	<table class="searchTable" border="0" cellpadding="0" cellspacing="0" width="">
    <tbody>
	<tr> 	 
		
		<?php if ($this->_tpl_vars['podeExcluir']): ?>
		<td align="center" width="112">	
		<a href="#" class="plainLink" id="btn_excluir"> <img src="imgs/remover.png" border="0" width="48" height="48" /><br />Excluir consultor(es)</a>  
		</td>		
		<?php endif; ?>
        
		<td align="center" width="130">	
		<a href="#" id="btn_visualizar" class="plainLink"> <img src="imgs/visualizar.png" border="0" width="48" height="48" /> <BR />
		Visualizar consultor</a>  
		</td>	
        
        <?php if ($this->_tpl_vars['podeBloquear']): ?>
		<td align="center" width="112">	
		<a href="#" class="plainLink" id="btn_bloquear"> <img src="imgs/application_remove.png" border="0" width="48" height="48" /><br />
        Bloquear consultor(es)</a>  
		</td>		
		<?php endif; ?>	
          
	</tr>
	<tr>
		<td align="center"></td>		
	</tr>
	</tbody>
	</table>  	
	<br />	
	<table id="listagemDados" class="scroll" cellpadding="0" cellspacing="0"></table>
<!-- pager definition. class scroll tels that we want to use the same theme as grid -->
	<div id="paginacao" class="scroll" style="text-align:center;"></div>				
	</td>