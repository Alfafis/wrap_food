<?php

$opcao = $_GET['opcao'];


switch($opcao){

    default :
 
		$podeIncluir = false;
		$podeAlterar = false;
		$podeExcluir = false; 
		$podeAprovar = false;
		$podeReprovar = false;
		$podeVisualizar = false;
		$podeBloquear = false;

		$sql = 'SELECT fm.id FROM usuario_has_modulo_has_funcionalidade_modulo fhmhfm, modulo_has_funcionalidade_modulo mhfm, funcionalidade_modulo fm WHERE fhmhfm.id_modulo_has_funcionalidade_modulo = mhfm.id AND mhfm.id_funcionalidade_modulo = fm.id AND mhfm.id_modulo = 13 AND id_usuario = '.$_SESSION['usuarioLogado'][0];

		$resultadoQuery=$db->Execute($sql);
 
		echo $db->ErrorMsg();

		while(!$resultadoQuery->EOF){
			
			if($resultadoQuery->fields[0] == '1')
				$podeIncluir = true;

			if($resultadoQuery->fields[0] == '2')
				$podeAlterar = true;

			if($resultadoQuery->fields[0] == '3')
				$podeExcluir = true; 
			
			if($resultadoQuery->fields[0] == '4')
				$podeAprovar = true; 
			
			if($resultadoQuery->fields[0] == '5')
				$podeReprovar = true; 
				
			if($resultadoQuery->fields[0] == '6')
				$podeVisualizar = true; 
				
			if($resultadoQuery->fields[0] == '7')
				$podeBloquear = true; 
				
			$resultadoQuery->MoveNext();

		} 

 
 
		$smarty->assign('podeIncluir', $podeIncluir);
		$smarty->assign('podeAlterar', $podeAlterar);
		$smarty->assign('podeExcluir', $podeExcluir); 
 		$smarty->assign('podeAprovar', $podeAprovar); 
		$smarty->assign('podeReprovar', $podeReprovar);  
		$smarty->assign('podeVisualizar', $podeVisualizar);  
		$smarty->assign('podeBloquear', $podeBloquear);

		$smarty->assign('templateCentro', 'centro_livres_listar.tpl');

		break;


	##LISTAR PLANO
	case 'listar' : 
		$paginaAtual = $_GET['page']; 
		$numeroMaximoResultadosPagina = $_GET['rows']; 
		$colunaOrdenacao = $_GET['sidx']; 
		$tipoOrdenacao = $_GET['sord']; 
		if(!$colunaOrdenacao) 
			$colunaOrdenacao = 1; 

		//saber o total de registro da tabela que ser�o pego os dados
		$sqlPaginacao = 'SELECT count(*) as total FROM livres d where deletado = 0';
		$resultadoQuery=$db->Execute($sqlPaginacao);
		$totalResultados = $resultadoQuery->fields['total'];

		//c�lculo do total de p�ginas para a query 
		if( $totalResultados > 0 ) 
		{ 
			$totalPaginas = ceil($totalResultados/$numeroMaximoResultadosPagina); 
		} 
		else 
		{ 
			$totalPaginas = 0; 
		} 

		//definir a p�gina atual = a total p�gina
		if ($paginaAtual > $totalPaginas) 
			$paginaAtual = $totalPaginas; 

		//calcular a posi��o inicial da query 
		$start = $numeroMaximoResultadosPagina * $paginaAtual - $numeroMaximoResultadosPagina; 
		if($start < 0) 
			$start = 0; 
		//construindo um JSON 
		$responce->page = $paginaAtual; 
		$responce->total = $totalPaginas; 
		$responce->records = $totalResultados; 
		$i=0; 

		//query para listagem dos dados 
		$sql = 'SELECT d.id, d.nome, IF(d.ativo = 0,"N�o","Sim") as status, DATE_FORMAT(data_cadastro,"%d/%m/%Y %H:%i:%s") AS cadastro
		FROM livres d
		WHERE d.deletado = 0
		ORDER BY '.$colunaOrdenacao.' '.$tipoOrdenacao.' LIMIT '.$start.', '.$numeroMaximoResultadosPagina;

		$resultadoQuery=$db->Execute($sql);
		echo $db->ErrorMsg();
		$totalResultado = $resultadoQuery->RecordCount();
		while(!$resultadoQuery->EOF)
		{
			$responce->rows[$i]['id'] = $resultadoQuery->fields[0]; 
			$responce->rows[$i]['cell'] = array(utf8_encode($resultadoQuery->fields[0]), utf8_encode($resultadoQuery->fields[1]), utf8_encode($resultadoQuery->fields[2]), utf8_encode($resultadoQuery->fields[3])); 
			$responce->rows[$i]['background'] = "";			
			$i++; 
			unset($background); 
			$resultadoQuery->MoveNext();
		}

		// retorna os dados formatados para a chamada javascript 
		echo json_encode($responce); 	
		$db->Disconnect();
		exit;
		break;
		

	##INSERIR NOTICIA##
	case 'inserir' :		
		$smarty->assign('templateCentro', 'centro_livres_inserir.tpl');
		break;

	##INSERIR NOTICIA - SALVAR##
    case 'salvar' :		
			
		$nome = limpaTexto($_REQUEST['nome']);
		
		$foto = $_FILES['foto']; 
		if (preg_match('/(.*)\.[jpg|jpeg|gif|png]/i', $foto['name'])) {
			$foto['name'] = preg_replace('/[^A-Za-z0-9._]/', '', $foto['name']);
			$nomeArquivoForum = date('dmyhi').$foto['name'];
		}
		
				
        $sql ='insert into livres (
		nome, 
		foto,
		data_cadastro
		) values(
		"'.utf8_decode($nome).'", 
		"'.$nomeArquivoForum.'",
		now()
		)'; 

		$resultadoQuery=$db->Execute($sql);

		echo $db->ErrorMsg();		
		if($resultadoQuery)
		{
			if($foto['name']) {
				$dirFotos01 = '../fotos/';				
				copy($foto['tmp_name'], $dirFotos01.$nomeArquivoForum);
			}
			
			echo '
				<SCRIPT LANGUAGE="JavaScript">					
					alert("Dado(s) cadastrado(s) com sucesso.");
					window.location="index.php?secao=livres";					
				</SCRIPT>';
		} 
		else 
		{			
			echo'
				<SCRIPT LANGUAGE="JavaScript">			        
					alert("Erro no cadastrado.");
					history.go(-1);			        
			    </SCRIPT>
			';				
		}
		break;

	##ALTERAR PLANO##
    case 'alterar' : 
		$idlivres = $_GET['idlivres'];
		$sql = 'SELECT * FROM livres WHERE id = '.$idlivres;
		$resultadoQuery=$db->Execute($sql);
		echo $db->ErrorMsg();
		if(!$resultadoQuery->EOF)
		{
			$livres = $resultadoQuery->fields;
			$livres["nome"] = utf8_encode($resultadoQuery->fields["nome"]);
		}
       	$smarty->assign('livres', $livres);
        $smarty->assign('templateCentro', 'centro_livres_alterar.tpl');
		break;

	##ALTERAR PLANO - SALVAR##  
	case 'salvarAlteracao' :
		$id = $_POST['id'];
		
		$ativo = $_POST['ativo'];
		$nome = limpaTexto($_REQUEST['nome']);
		
		$foto = $_FILES['foto']; 
		if (preg_match('/(.*)\.[jpg|jpeg|gif|png]/i', $foto['name'])) {
			$foto['name'] = preg_replace('/[^A-Za-z0-9._]/', '', $foto['name']);
			$nomeArquivoForum = date('dmyhi').$foto['name'];
			$salvarArquivo = ', foto = "'.$nomeArquivoForum.'" ';
		}	

        $sql ='update livres set 
		ativo = "'.$ativo.'", 
		nome = "'.utf8_decode($nome).'" 
		'.$salvarArquivo.'
		where id = '.$id; 

		$resultadoQuery=$db->Execute($sql);
		
		if($resultadoQuery)
		{
			if($foto['name']) {
				$dirFotos01 = '../fotos/';				
				copy($foto['tmp_name'], $dirFotos01.$nomeArquivoForum);
			}
			echo '
				<SCRIPT LANGUAGE="JavaScript">					
					alert("Dado(s) alterados(s) com sucesso.");
					window.location="index.php?secao=livres";					
				</SCRIPT>';
		} 
		else 
		{			
			echo'
				<SCRIPT LANGUAGE="JavaScript">			        
					alert("Erro no cadastrado.");
					history.go(-1);			        
			    </SCRIPT>
			';				
		}
		break;
		
	##EXCLUIR PLANO##
    case 'excluir' :
        $listaIdForum = $_GET['listaId'];
		$sql ='update livres set deletado = 1 WHERE id IN ('.$listaIdForum.')';
		$resultadoQuery=$db->Execute($sql);
		if($resultadoQuery)
		{		
			echo'
				<SCRIPT LANGUAGE="JavaScript">			        
					alert("Dado(s) excluido(s) com sucesso.");
					window.location="index.php?secao=livres";			        
			    </SCRIPT>
			';
		} 
		else 
		{			
			echo'
				<SCRIPT LANGUAGE="JavaScript">			        
					alert("Selecione o(s) cliente(s) que deseja excluir.");
					history.go(-1);			        
			    </SCRIPT>
			';				
		}
		break;
		
	##ALTERAR PLANO##
    case 'visualizar' : 
		$idForum = $_GET['listaId'];
		$sqlDados = 'SELECT * FROM livres WHERE deletado = 0';
		$resultadoQueryDados=$db->Execute($sqlDados);
		while(!$resultadoQueryDados->EOF){
			$listaconsultor[] = $resultadoQueryDados->fields;
			$resultadoQueryDados->MoveNext();
		}

		$smarty->assign('listaconsultor', $listaconsultor);

		
        echo $mensagem = $smarty->fetch("revendedor.html");
		exit;
		//$smarty->assign('templateCentro', 'centro_forum_alterar.tpl');
		break;
		
	##bloquear##  
	case 'aprovar' :
		$listaIdForum = $_GET['listaId'];
		
		$sql = 'UPDATE livres SET ativo = 1 WHERE id IN ('.$listaIdForum.')';
		$resultadoQuery=$db->Execute($sql);
		echo $db->ErrorMsg();		
		if($resultadoQuery)
		{
			echo '
				<SCRIPT LANGUAGE="JavaScript">					
					alert("Consultor(es) bloqueado(s) com sucesso.");
					window.location="index.php?secao=livres";					
				</SCRIPT>';
		} 
		else 
		{			
			echo'
				<SCRIPT LANGUAGE="JavaScript">			        
					alert("Erro no cadastrado.");
					history.go(-1);			        
			    </SCRIPT>
			';				
		}
		break;
		
	##bloquear##  
	case 'bloquear' :
		$listaIdForum = $_GET['listaId'];
		
		$sql = 'UPDATE livres SET deletado = 1 WHERE id IN ('.$listaIdForum.')';
		$resultadoQuery=$db->Execute($sql);
		echo $db->ErrorMsg();		
		if($resultadoQuery)
		{
			echo '
				<SCRIPT LANGUAGE="JavaScript">					
					alert("Consultor(es) bloqueado(s) com sucesso.");
					window.location="index.php?secao=livres";					
				</SCRIPT>';
		} 
		else 
		{			
			echo'
				<SCRIPT LANGUAGE="JavaScript">			        
					alert("Erro no cadastrado.");
					history.go(-1);			        
			    </SCRIPT>
			';				
		}
		break;
}
?>