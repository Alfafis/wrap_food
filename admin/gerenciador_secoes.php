<?php
	error_reporting(E_ALL ^ E_NOTICE);
	error_reporting(0);
	ini_set("display_errors", 0 );


	if($_GET['secao'] != ""){
		$secao = $_GET['secao']; 
	}
	if($_POST['secao'] != ""){
		$secao = $_POST['secao']; 
	}
	switch($secao)
	{
		default :
			$gerenciador_secao = 'gerencia/gerenciador_usuario.php';			
			break;
			
		case 'usuario' :			
			if($_GET['opcao'] != "" && $_GET['opcao'] != "logar")
			include('session.php');	
			$gerenciador_secao = 'gerencia/gerenciador_usuario.php';		   
			break;

		case 'modulo' :
			include('session.php');				
			$gerenciador_secao = 'gerencia/gerenciador_modulo.php';		   
			break;
		
		
		case 'pao' :

			include('session.php');	
			$gerenciador_secao = 'gerencia/gerenciador_pao.php';
		   
			break;
	
			
		case 'produtos' :
	
			include('session.php');	
			$gerenciador_secao = 'gerencia/gerenciador_produtos.php';
		   
			break;
			
		case 'queijos' :
	
			include('session.php');	
			$gerenciador_secao = 'gerencia/gerenciador_queijos.php';
		   
			break;
			
		case 'doces' :
	
			include('session.php');	
			$gerenciador_secao = 'gerencia/gerenciador_doces.php';
		   
			break;
				
		case 'clientes' :
	
			include('session.php');	
			$gerenciador_secao = 'gerencia/gerenciador_clientes.php';
		   
			break;
					
		case 'textos' :
	
			include('session.php');	
			$gerenciador_secao = 'gerencia/gerenciador_textos.php';
		   
			break;
						
		case 'contato' :
	
			include('session.php');	
			$gerenciador_secao = 'gerencia/gerenciador_contato.php';
		   
			break;
			
		case 'banner' :
	
			include('session.php');	
			$gerenciador_secao = 'gerencia/gerenciador_banner.php';
		   
			break;
			
		case 'pedido' :
	
			include('session.php');	
			$gerenciador_secao = 'gerencia/gerenciador_pedido.php';
		   
			break;
			
		case 'molhos' :
	
			include('session.php');	
			$gerenciador_secao = 'gerencia/gerenciador_molhos.php';
		   
			break;
			
		case 'livres' :
	
			include('session.php');	
			$gerenciador_secao = 'gerencia/gerenciador_livres.php';
		   
			break;
			
		case 'news' :
	
			include('session.php');	
			$gerenciador_secao = 'gerencia/gerenciador_news.php';
		   
			break;
			
		case 'saladas' :
	
			include('session.php');	
			$gerenciador_secao = 'gerencia/gerenciador_saladas.php';
		   
			break;
			
		case 'conf' :
	
			include('session.php');	
			$gerenciador_secao = 'gerencia/gerenciador_conf.php';
		   
			break;
			
		case 'cupom' :
	
			include('session.php');	
			$gerenciador_secao = 'gerencia/gerenciador_cupom.php';
		   
			break;
			
		case 'relatorio' :
	
			include('session.php');	
			$gerenciador_secao = 'gerencia/gerenciador_relatorios.php';
		   
			break;
			
		
	} 

	include('../conf/conexao.php');
	include ('./funcoes/funcoes.php');
	include ('./bibliotecas/smarty/SmartySetup.php');
	include("./classes/class.inputfilter_clean.php");
	include('./emails.php');

	$smarty = new SmartySetup();
	include ($gerenciador_secao);

	if($secao == '')
	{
		$templatePaginaPrincipal = 'login.tpl';
	} 
	else 
	{
		$templatePaginaPrincipal = 'principal.tpl';
	}

	#Seleciona os mudulos do Usuario
	$listaIdModulo = 0;
	
	if($_SESSION['usuarioLogado'][0] != "")
	{
		//Seleciona os modulos que o usuario tem permissao
		$sql = 'SELECT DISTINCT m.id FROM usuario_has_modulo_has_funcionalidade_modulo fhmhfm, modulo_has_funcionalidade_modulo mhfm, modulo m WHERE fhmhfm.id_modulo_has_funcionalidade_modulo = mhfm.id AND mhfm.id_modulo = m.id AND id_usuario = '.$_SESSION['usuarioLogado'][0];

		$resultadoQuery=$db->Execute($sql);
		echo $db->ErrorMsg();
		while(!$resultadoQuery->EOF)
		{			
			$listaIdModulo .= ','.$resultadoQuery->fields[0];
			$resultadoQuery->MoveNext();
		}

		$sql = 'SELECT id, nome FROM modulo WHERE id in ('.$listaIdModulo.') AND id_modulo_pai = 0 AND url != "" ORDER BY posicao';
		$resultadoQuery=$db->Execute($sql);
		echo $db->ErrorMsg();
		while(!$resultadoQuery->EOF)
		{
			$listaModulo[] = $resultadoQuery->fields;
			$resultadoQuery->MoveNext();
		}
	}

	$smarty->assign('moduloAtual', $_SESSION['moduloAtual'] );
	$smarty->assign('listaModulo', $listaModulo ); 
	$smarty->assign('usuarioLogado', $_SESSION['usuarioLogado']);
	
	//***************************** monta toda a pagina
/*$sql = 'SELECT * FROM textos WHERE id = 1';
		$resultadoQuery=$db->Execute($sql);
		echo $db->ErrorMsg();
		if(!$resultadoQuery->EOF)
		{
			$textos = $resultadoQuery->fields;
		}
       	$smarty->assign('textos_site', $textos);*/
	
	$smarty->display($templatePaginaPrincipal);
	$db->Disconnect();
	
?>