<?php

session_start();

$cd = $_SESSION['codigoValidacao'];

if (!extension_loaded('gd')) { dl('php_gd2.dll'); };
$img = imagecreate(60,30);

$bg = imagecolorallocate($img, 100, 0, 100);
$text_color = imagecolorallocate($img, 255, 255, 255);
imagestring($img, 135, 8, 6, "$cd", $text_color);

header("Content-type: image/png");

imagepng($img);
imagedestroy($img);

?>