<?php 

	$temposessao = 600000; // valor em segundos 

	// inicia a sess�o 
	session_start(); 

	if(!isset($_SESSION['usuarioLogado'])){
		session_destroy();
		session_unset();
		header("Location: index.php");
	}

	// verifica se h� a vari�vel "sessiontime" 
	if ($_SESSION["sessiontime"]) { 
		// se h� "sessiontime", verifica se ainda � v�lida 
		if ($_SESSION["sessiontime"] < (time() - $temposessao)) { 
			// se n�o for v�lida, libera todas as vari�veis da sess�o 
			session_unset(); 
		} 
	} else { 
		// se n�o h� a vari�vel "sessiontime", 
		// libera todas as vari�veis da sess�o 
		session_unset(); 
	} 

	// session time recebe um novo valor 
	$_SESSION["sessiontime"] = time(); 

	// agora sua sess�o est� pronta para receber novas vari�veis 

?> 
