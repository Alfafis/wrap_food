function abrir_menu(){
	  $("#menu_esq").animate({
		left: "0"
	  }, 500, function() {
		// Animation complete.
	  });
	  
	  $("#centro_site").animate({
		left: "275"
	  }, 500, function() {
		// Animation complete.
	  });
	  
	  $("#fundo_menu").fadeIn(200);
	  $("#fundo_menu").animate({
		opacity: 1
	  }, 500, function() {
		// Animation complete.
	  });
}

function fechar_menu(){
	  $("#menu_esq").animate({
		left: "-275"
	  }, 500, function() {
		// Animation complete.
	  });
	  
	  $("#centro_site").animate({
		left: "0"
	  }, 500, function() {
		// Animation complete.
	  });
	  
	  $("#fundo_menu").fadeOut(500);
	  $("#fundo_menu").animate({
		opacity: 0
	  }, 500, function() {
		// Animation complete.
	  });
}


function abrir_delete(){
	$(".item-content").animate({
		left: "-66"
	  }, 500, function() {
		// Animation complete.
	  });	
}

function fechar_delete(){
	$(".item-content").animate({
		left: "0"
	  }, 500, function() {
		// Animation complete.
	  });	
}



	
function aumentar_quantidade(linha){
	
	aux = "#qtde"+linha;
	var qtde = $(aux).val();
	var novaQtde = parseFloat(qtde)+1;
	$(aux).val(novaQtde);
	aux2 = "#txtqtde"+linha;
	$(aux2).html(novaQtde);
	
	alterarQtdeItem(linha,novaQtde);
}	

function diminuir_quantidade(linha){
	
	aux = "#qtde"+linha;
	var qtde = $(aux).val();
	var novaQtde = parseFloat(qtde)-1;
	//alert(novaQtde);
	if(novaQtde == 0){
		return false;	
	}
	$(aux).val(novaQtde);
	aux2 = "#txtqtde"+linha;
	$(aux2).html(novaQtde);
	
	alterarQtdeItem(linha,novaQtde);
}	



function alterarQtdeItem(contador,qtde)
{	
	
	$.ajax({
		type: 'POST',
		url: '?secao=pedidoProduto&opcao=alterarQtdeItem',
		data: {'cotadorProduto':contador,'qtdeProduto':qtde},
		success: function (result) {
			//alert(result);
			if(result == "sucess"){
				location.reload();			
			}else{
				//alert("Houve um erro, tente novamente mais tarde!");
				
			}
		}
	});
	
}	

function removeItem(contador)
{	
	$.ajax({
		type: 'POST',
		url: '?secao=pedidoProduto&opcao=removerItemProdutoCarrinho',
		data: {'cotadorProduto':contador},
		success: function (result) {
			//alert(result);
			if(result == "sucess"){
				location.reload();			
			}else{
				alert("Houve um erro, tente novamente mais tarde!");
			}
		}
	});
	
}	

function finalizaCarrinho(){
	
	if($("#horario").val() == ""){
		alertas("Digite o horário que irá buscar seu lanche.");
		return false;
	}
	
	document.form_pedido_produto.action = "index.php?secao=pedidoProduto&opcao=definirFormaPagto";
	document.form_pedido_produto.submit();
	
}	

function definir_pagamento(){
	//definindo qual o tipo de pagamento
	$("#fundo_alertas").css("display","block");
	$("#text_dialog").html("Defina qual o método de pagamento!");
	$( "#dialog" ).dialog({
	 buttons: {
        PAGSEGURO: function() {
		  window.location = "index.php?secao=pedidoProduto&opcao=finalizar";
          $( this ).dialog( "close" );
        },
		'DINHEIRO NO LOCAL': function() {
		  $( this ).dialog( "close" );
		  pagamento_dinheiro();
        }		,
		'CARTÃO NO LOCAL': function() {
		  $( this ).dialog( "close" );
		  pagamento_dinheiro();
        }
	  },
	  close: function( event, ui ) {
		$("#fundo_alertas").css("display","none");  
	  }
	});	
}

function pagamento_dinheiro(){
	//definindo qual o tipo de pagamento
	$("#fundo_alertas").css("display","block");
	$("#text_dialog").html("Seu pedido será enviado para a cozinha.");
	$( "#dialog" ).dialog({
	 buttons: {
        OK: function() {
		  window.location = "index.php?secao=pedidoProduto&opcao=finalizarDinheiro";
          $( this ).dialog( "close" );
        }
	  },
	  close: function( event, ui ) {
		$("#fundo_alertas").css("display","none");  
	  }
	});	
}


function alertas(msg){
	$("#fundo_alertas").css("display","block");
	$("#text_dialog").html(msg);
	$( "#dialog" ).dialog({
	 buttons: {
        OK: function() {
          $( this ).dialog( "close" );
        }
	  },
	  close: function( event, ui ) {
		$("#fundo_alertas").css("display","none");  
	  }
	});	
}





















var tempo;

function tempo() {
    var s = 1;
	var m = 0;
	var h = 0;
	tempo = window.setInterval(function() {
		if (s == 60) { m++; s = 0; }
		if (m == 60) { h++; s = 0; m = 0; }
		if (h < 10) hora = "0"+h; else hora = h;
		if (s < 10) segundo = "0"+s; else segundo = s;
		if (m < 10) minuto = "0"+m; else minuto = m;		
		s++;
		horaImprimivel = "Tempo de Leitura<br>"+hora + ":" + minuto + ":" + segundo;
		$('#icone_relogio').tooltip().attr('data-original-title', horaImprimivel);
	},1000);
	$("#carregar_curso").css("display","none");
	$("#borda_paginas").css("opacity","1");
}