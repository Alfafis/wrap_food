



function removerProdutosCarrinho(){

	possuiDadosSelecionados = false;
	 
	for (i=0;i<document.form_pedido_produto.elements.length;i++){
		if(document.form_pedido_produto.elements[i].name == 'listaProdutoCarrinhoExcluir[]'){
			if(document.form_pedido_produto.elements[i].checked)
				possuiDadosSelecionados = true;
		}
	}
			
	if(possuiDadosSelecionados){
		if(confirm("Tem certesa que gostaria de excluir estes itens?")){
			document.form_pedido_produto.action = 'index.php?secao=pedidoProduto&opcao=removerProdutoPedidoProdutoSecao';
			document.form_pedido_produto.submit();
		} 
	}else
		alert("Selecione o(s) registro(s) que deseja excluir.");
			

}


function salvarPedidoProdutoSecao(pagina){
	
		document.form_pedido_produto.action = "index.php?secao=pedidoProduto&opcao=salvarPedidoProdutoSecao&pagina="+ pagina;
	
		document.form_pedido_produto.submit(); 
	}

		
function atualizarValorTotalPedidoProduto(linha,qtde2){
		
		var valorTotalPedidoProduto = 0;
		var preco = 0;
		var qtde = 0;
		var precoTotal = 0;
		
		var qtdeTotal = parseFloat(document.getElementById('qtdeTotal'+linha).value); 
		document.getElementById('listaProdutoCarrinhoQtde'+linha).value = qtde2;
		
		//alert(qtdeTotal);
		
		
		if(document.form_pedido_produto.listaProdutoCarrinhoPrecoVendaVarejo.length){
		
			for (i=0; i < document.form_pedido_produto.listaProdutoCarrinhoPrecoVendaVarejo.length; i++){
		
				preco = document.form_pedido_produto.listaProdutoCarrinhoPrecoVendaVarejo[i].value;
				
				qtde = document.form_pedido_produto.listaProdutoCarrinhoQtde[i].value;
				
				if(qtde2 > qtdeTotal){
					alert("No momento temos apenas "+qtdeTotal+" unidades disponíveis deste produto.");
					//mudar = i+1;
					//alert(linha-1);
					document.form_pedido_produto.listaProdutoCarrinhoQtde[linha-1].value = qtdeTotal;
					//document.getElementById('listaProdutoCarrinhoQtde').value = qtde2;
					//document.getElementById('listaProdutoCarrinhoQtde'+linha).value = qtdeTotal;
					atualizarValorTotalPedidoProduto(linha,qtdeTotal);
					return false;
				}
				
				
				if(qtde != null && qtde != ''){
					
					precoTotal = parseFloat(preco) * parseFloat(qtde);
						
					valorTotalPedidoProduto = parseFloat(valorTotalPedidoProduto) + parseFloat(precoTotal);				
	
				}
				
			}
		
		}else{
			
			if(qtde2 > qtdeTotal){
				alert("No momento temos apenas "+qtdeTotal+" unidades disponíveis deste produto.");
				//mudar = i+1;
				//alert(linha-1);
				//document.form_pedido_produto.listaProdutoCarrinhoQtde[linha-1].value = qtdeTotal;
				document.getElementById('listaProdutoCarrinhoQtde').value = qtdeTotal;
				//document.getElementById('listaProdutoCarrinhoQtde'+linha).value = qtdeTotal;
				atualizarValorTotalPedidoProduto(linha,qtdeTotal);
				return false;
			}
		
			preco = document.form_pedido_produto.listaProdutoCarrinhoPrecoVendaVarejo.value;
				
			qtde = document.form_pedido_produto.listaProdutoCarrinhoQtde.value;
			
			if(qtde != null && qtde != ''){
			
				precoTotal = parseFloat(preco) * parseFloat(qtde);
					
				valorTotalPedidoProduto = parseFloat(valorTotalPedidoProduto) + parseFloat(precoTotal);				
	
			}
		
		}
		
		//somando frete se tiver
		var valor_frete = document.getElementById('valor_frete_selecionado').value;
		var total_com_frete = "";
		if(valor_frete != ""){
			total_com_frete = valorTotalPedidoProduto+parseFloat(valor_frete);
			document.getElementById('valorTotalPedidoFrete').value = total_com_frete;
			total_com_frete = formataValorParaMoeda(total_com_frete);
			document.getElementById('valor_total_frete').innerHTML = "R$ "+ total_com_frete;
		}else{
			document.getElementById('valorTotalPedidoFrete').value = valorTotalPedidoProduto;
			total_com_frete = formataValorParaMoeda(valorTotalPedidoProduto);
			document.getElementById('valor_total_frete').innerHTML = "R$ "+ total_com_frete;
		}
		
		document.getElementById('valorTotalPedido').value = valorTotalPedidoProduto;
		valorTotalPedidoProduto = formataValorParaMoeda(valorTotalPedidoProduto);
		document.getElementById('valor_total').innerHTML = "R$ "+ valorTotalPedidoProduto;
		
		
		
		//alert(linha);		
		document.getElementById('qtde'+linha).value = qtde2;
		//alert(document.getElementById('qtde'+linha).value);
		precoTotalItem = parseFloat(document.getElementById('valorIndividual'+linha).value) * parseFloat(document.getElementById('qtde'+linha).value);
		document.getElementById('valor_total').innerHTML = "R$ "+ valorTotalPedidoProduto;
		document.getElementById('valorItem'+linha).value = precoTotalItem;
		document.getElementById('listaProdutoValorItensCompleto'+linha).value = precoTotalItem;
		document.getElementById('valor_total_item'+linha).innerHTML = "R$ "+ formataValorParaMoeda(precoTotalItem);
		
	}
	
function aumentaQtde(linha){
	
	if(document.form_pedido_produto.listaProdutoQtd.length){
		var qtde = document.form_pedido_produto.listaProdutoQtd[linha-1].value; 
		var novaQtde = parseFloat(qtde)+1;
		document.form_pedido_produto.listaProdutoQtd[linha-1].value = novaQtde;
		aux = "#qtde"+linha;
		$(aux).html(novaQtde);
	}else{
		var qtde = document.getElementById('listaProdutoCarrinhoQtde').value; 
		var novaQtde = parseFloat(qtde)+1;
		document.getElementById('listaProdutoCarrinhoQtde').value = novaQtde;
	}
	//atualizarValorTotalPedidoProduto(linha,novaQtde);
	//verificando se tem o total solicitado
	
	//alterarQtdeItem(linha,novaQtde,qtdeTotal);
}	

function diminuiQtde(linha){
	
	if(document.form_pedido_produto.listaProdutoCarrinhoPrecoVendaVarejo.length){
		var qtde = document.form_pedido_produto.listaProdutoCarrinhoQtde[linha-1].value; 
		if(qtde == 1){
			return false;	
		}
		var novaQtde = parseFloat(qtde)-1;
		document.form_pedido_produto.listaProdutoCarrinhoQtde[linha-1].value = novaQtde;
	}else{
		var qtde = document.getElementById('listaProdutoCarrinhoQtde').value; 
		if(qtde == 1){
			return false;	
		}
		var novaQtde = parseFloat(qtde)-1;
		document.getElementById('listaProdutoCarrinhoQtde').value = novaQtde;
	}
	//atualizarValorTotalPedidoProduto(linha,novaQtde);	
	alterarQtdeItem(linha,novaQtde,0);
}	
	
function finalizaCarrinho(){
		v_total = document.getElementById('valorTotalPedido').value;
		//alert(v_total);
		//if(parseFloat(v_total) >= parseFloat(1500)){
		if(parseFloat(v_total) >= parseFloat(0)){
			document.form_pedido_produto.action = "index.php?secao=pedidoProduto&opcao=definirEnderecoEntrega";
			document.form_pedido_produto.submit();
		}else{
			alert("Sua compra deve ser maior que R$ 1500,00 para ser finalizada!");
		}
	}	
	
	

function calcularFrete()
{		 		
	
	document.getElementById("frete_ja_selecionado").style.display = "none";
	var cep_code = $("#frete_carrinho").val();
	
	if(cep_code == ""){
		alert("Digite o CEP!");
		$("#frete_carrinho").focus()
		return false;
	}

	if(cep_code == "35610-000"){
		document.getElementById("tabela_fretes").style.display = "none";
		var valor_carrinho = $("#valorTotalPedido").val();
		document.getElementById("valor_total_frete").innerHTML = "R$ "+formataValorParaMoeda(valor_carrinho);
		$("#valorTotalPedidoFrete").val(valor_carrinho);
		document.getElementById("frete_gratis_dores").style.display = "block";
		return false;
	}else{
		document.getElementById("frete_gratis_dores").style.display = "none";
	}
	
	var estado = "";
	var cidade = "";
	
	if( cep_code.length <= 0 ) return;
	$.get("http://apps.widenet.com.br/busca-cep/api/cep.json", { code: cep_code },
	 function(result){
		if( result.status!=1 ){
		   alert(result.message || "Houve um erro desconhecido");
		   return;
		}
		estado = result.state;
		cidade = result.city;
		//alert(estado);
		document.getElementById("tabela_fretes").style.display = "block";
		document.getElementById("destino_frete").innerHTML = cidade+" - "+estado;
	 });
		 
	
    var res = cep_code.split("-");
	var cep_limpo = res[0]+res[1];
	
	var estoque = $("#em_estoque").val();
	
	$.ajax({
		type: 'POST',
		url: '?secao=pedidoProduto&opcao=consultaCep',
		data: {'cep':cep_limpo,'estoque':estoque},
		success: function (result) {
			//alert(result);
			var dados = result.split("_");
			$("#valor_sedex_item").val(dados[0]);
			$("#valor_pac_item").val(dados[2]);

			document.getElementById("valor_sedex").innerHTML = dados[0];
			document.getElementById("prazo_sedex").innerHTML = dados[1];
			document.getElementById("valor_pac").innerHTML = dados[2];
			document.getElementById("prazo_pac").innerHTML = dados[3];
			
			if(result == "sucess"){
							
			}else{
				//alert("Houve um erro, tente novamente mais tarde!");
				
			}
		}
	});
	
}	
	
	
function selecionaFrete(tipo){
	
	$("#tipo_frete_selecionado").val(tipo);
	var frete_selecionado = "";
	if(tipo == 1){
		$("#valor_frete_selecionado").val(document.getElementById("valor_sedex_item").value.replace(",","."));
		var frete_selecionado = document.getElementById("valor_sedex_item").value.replace(",",".");
	}
	if(tipo == 2){
		$("#valor_frete_selecionado").val(document.getElementById("valor_pac_item").value.replace(",","."));
		var frete_selecionado = document.getElementById("valor_pac_item").value.replace(",",".");
	}
	
	
	
	//var valor_carrinho = $("#valorTotalPedidoFrete").val();
	var valor_carrinho = $("#valorTotalPedido").val();
	
	//verificando se o presente esta selecionado
	var valor_presente = $("#valor_presente").val();
	if($("#para_presente").is(':checked')){
		var valor_total = parseFloat(valor_carrinho)+parseFloat(frete_selecionado)+parseFloat(valor_presente);
	}else{
		var valor_total = parseFloat(valor_carrinho)+parseFloat(frete_selecionado);
	}
	document.getElementById("valor_total_frete").innerHTML = "R$ "+formataValorParaMoeda(valor_total);
	$("#valorTotalPedidoFrete").val(valor_total);	
}	
	





function alterarQtdeItem(contador,qtde,qtdeTotal)
{	
	if(qtdeTotal != 0){
		if(qtde>qtdeTotal){
			alert("Temos apenas "+qtdeTotal+" deste item!");
			qtde = qtdeTotal;
		}	
	}
	
	$.ajax({
		type: 'POST',
		url: '?secao=pedidoProduto&opcao=alterarQtdeItem',
		data: {'cotadorProduto':contador,'qtdeProduto':qtde,'qtdeTotal':qtdeTotal},
		success: function (result) {
			//alert(result);
			if(result == "sucess"){
				location.reload();			
			}else{
				//alert("Houve um erro, tente novamente mais tarde!");
				
			}
		}
	});
	
}	


function removeItem(contador)
{	
	$.ajax({
		type: 'POST',
		url: '?secao=pedidoProduto&opcao=removerItemProdutoCarrinho',
		data: {'cotadorProduto':contador},
		success: function (result) {
			//alert(result);
			if(result == "sucess"){
				location.reload();			
			}else{
				alert("Houve um erro, tente novamente mais tarde!");
			}
		}
	});
	
}	
	

function validarCupom()
{	
	var cupom = $("#cupom_carrinho").val();
	if(cupom == ""){
		alert("Digite o cupom!");
		return false;
	}
	
	$.ajax({
		type: 'POST',
		url: '?secao=pedidoProduto&opcao=validarCupom',
		data: {'cupom':cupom, 'valorCompra':$("#valorTotalPedido").val()},
		success: function (result) {
			//alert(result);
			if(result == "maior"){
				alert("O valor de sua compra é menor que o desconto do cupom.");	
				return false;
			}
			
			if(result == "sim"){
				document.getElementById("validar_cupom").innerHTML = "Cupom validado com sucesso!";
				location.reload();			
			}else{
				//alert("Houve um erro, tente novamente mais tarde!");
				document.getElementById("validar_cupom").innerHTML = "<span>Cupom não encontrado!</span>";
			}
		}
	});
	
}	


function removerCupom()
{	
	
	$.ajax({
		type: 'POST',
		url: '?secao=pedidoProduto&opcao=removerCupom',
		data: {'cupom':"sim"},
		success: function (result) {
			//alert(result);
			if(result == "sim"){
				document.getElementById("validar_cupom").innerHTML = "Cupom removido com sucesso!";
				location.reload();			
			}else{
				//alert("Houve um erro, tente novamente mais tarde!");
				document.getElementById("validar_cupom").innerHTML = "<span>Cupom não encontrado!</span>";
			}
		}
	});
	
}	

	
	