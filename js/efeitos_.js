/*************efeitos pg principal****************************/
$(document).ready(function(){
			
			
  $('#slide_colecoes').bxSlider({
	slideWidth: 331,
	minSlides: 1,
	maxSlides: 3,
	moveSlides: 1,
	slideMargin: 30,
	nextSelector: '#seta_nav_dir',
	  prevSelector: '#seta_nav_esq',
	  nextText: '<img src="img/seta_dir.png">',
	  prevText: '<img src="img/seta_esq.png">'
  });
    
  $('#slide_lancamentos').bxSlider({
	slideWidth: 260,
	minSlides: 1,
	maxSlides: 4,
	moveSlides: 1,
	slideMargin: 30,
	nextSelector: '#seta_nav_dirl',
	  prevSelector: '#seta_nav_esql',
	  nextText: '<img src="img/seta_dir.png">',
	  prevText: '<img src="img/seta_esq.png">'
  });
  
  $('#slide_promocoes').bxSlider({
	slideWidth: 260,
	minSlides: 1,
	maxSlides: 4,
	moveSlides: 1,
	slideMargin: 30,
	nextSelector: '#seta_nav_dirp',
	  prevSelector: '#seta_nav_esqp',
	  nextText: '<img src="img/seta_dir.png">',
	  prevText: '<img src="img/seta_esq.png">'
  });
  
  $('#slide_depoimentos').bxSlider({
	slideWidth: 510,
	minSlides: 1,
	maxSlides: 2,
	moveSlides: 1,
	slideMargin: 30,
	nextSelector: '#seta_nav_dir',
	  prevSelector: '#seta_nav_esq',
	  nextText: '<img src="img/seta_dir.png">',
	  prevText: '<img src="img/seta_esq.png">'
  });
  
  
  $('.vote label i.fa').on('click mouseover',function(){
	// remove classe ativa de todas as estrelas
	$('.vote label i.fa').removeClass('active');
	// pegar o valor do input da estrela clicada
	var val = $(this).prev('input').val();
	//percorrer todas as estrelas
	$('.vote label i.fa').each(function(){
		/* checar de o valor clicado é menor ou igual do input atual
		*  se sim, adicionar classe active
		*/
		var $input = $(this).prev('input');
		if($input.val() <= val){
			$(this).addClass('active');
		}
	});
	//$("#voto").html(val); // somente para teste
	});
	//Ao sair da div vote
	$('.vote').mouseleave(function(){
		//pegar o valor clicado
		var val = $(this).find('input:checked').val();
		//se nenhum foi clicado remover classe de todos
		if(val == undefined ){
			$('.vote label i.fa').removeClass('active');
		} else { 
			//percorrer todas as estrelas
			$('.vote label i.fa').each(function(){
				/* Testar o input atual do laço com o valor clicado
				*  se maior, remover classe, senão adicionar classe
				*/
				var $input = $(this).prev('input');
				if($input.val() > val){
					$(this).removeClass('active');
				} else {
					$(this).addClass('active');
				}
			});
		}
		//$("#voto").html(val); // somente para teste
	});
	
	/************efeitos modais***********/
	
	//modal medidas
	$('a[href="#lista_tamanhos"]').click(function(event) {
      event.preventDefault();
      $("#lista_tamanhos").modal({
        fadeDuration: 250
      });
    });
	
	$('a[href="#lista_tamanhos_gargantilhas"]').click(function(event) {
      event.preventDefault();
      $("#lista_tamanhos_gargantilhas").modal({
        fadeDuration: 250
      });
    });
	
				   
});
		
		
/********************************************************************/

function abrir_chamadas(chamada,total){
	
	for(i=0;i<=total;i++){
		aux2 = "#it_chamada_"+i;
		$(aux2).css("display","none");
	}
	
	aux = "#it_chamada_"+chamada;
	$(aux).fadeIn(400);
	
	var proximo = chamada+1;
	if(proximo>total){
		proximo = 0;
	}
	
	setTimeout("abrir_chamadas("+proximo+","+total+")",5000);
	
}

function abrir_galeria(id,total,img){
	
	for(i=1;i<=total;i++){
		aux2 = "#min_"+i;
		$(aux2).removeClass("galeria_ativa");
	}
	
	document.getElementById("img_galeria").src = "fotos/"+img;
	$('#img_galeria').attr("data-zoom-image", "fotos_grande/"+img); 
	//document.getElementById("img_galeria_l").href = "fotos_grande/"+img;
	
	aux_ = "#min_"+id;
	$(aux_).addClass("galeria_ativa");
	
}

function muda_campo(){
	$('.caixa_pesquisar').toggleClass('caixa_pesquisar-active');
}


function abrir_bloco(bloco,total){
	
	for(i=1;i<=total;i++){
		aux2 = "bloco"+i;
		document.getElementById(aux2).style.display = "none";
		aux2_ = "#btnbloco_"+i;
		$(aux2_).removeClass("botoes_sec_ativo");
		
	}
	
	aux = "#bloco"+bloco;
	$(aux).fadeIn(200);
	
	aux_ = "#btnbloco_"+bloco;
	$(aux_).addClass("botoes_sec_ativo");
		
}






function menu() {
	$('.nav-produtos-toggle').click(function() {
		if($(".nav-produtos").hasClass("side-fechado")) {
			$('.nav-produtos').animate({
				left: "0px",
			}, 100, function() {
				$(".nav-produtos").removeClass("side-fechado");
			});
		}
		else {
			$('.nav-produtos').animate({
				left: "-240px",
			}, 100, function() {
				$(".nav-produtos").addClass("side-fechado");
			});
		}
	});
}

//Menu Sidebar
$(window).resize(function() {
	var tamanhoJanela = $(window).width();
	$(".nav-produtos-toggle").remove();
	
	if (tamanhoJanela < 800) { 
		$('.nav-produtos').css('left', '-240px').addClass('side-fechado');
		$('.nav-produtos').append( "<div class='nav-produtos-toggle'><img src='img/logo_produtos.png' > FILTRAR</div>" );
	} else {
		$('.nav-produtos').css('left', '0px').addClass('side-fechado');
	}
	
	menu();
});

$(document).ready(function() {
	var tamanhoJanela = $(window).width();
	$(".nav-produtos-toggle").remove();
	
	if (tamanhoJanela < 800) { 
		$('.nav-produtos').css('left', '-240px').addClass('side-fechado');
		$('.nav-produtos').append( "<div class='nav-produtos-toggle'><img src='img/logo_produtos.png' > FILTRAR</div>" );
	} else {
		$('.nav-produtos').css('left', '0px').addClass('side-fechado');
	}
	
	menu();
});


jQuery(function($){
   $("#cep").change(function(){
      var cep_code = $(this).val();
      if( cep_code.length <= 0 ) return;
      $.get("http://apps.widenet.com.br/busca-cep/api/cep.json", { code: cep_code },
         function(result){
            if( result.status!=1 ){
               alert(result.message || "Houve um erro desconhecido");
               return;
            }
            $("input#cep").val( result.code );
            $("input#estado").val( result.state );
            $("input#cidade").val( result.city );
            $("input#bairro").val( result.district );
            $("input#endereco").val( result.address );
            $("input#estado").val( result.state );
            $("input#numero").focus();
         });
   });
});


function consultarCep(){
	window.open("http://www.correios.com.br/servicos/dnec/index.do", "Nova" ,"height = 400, width = 780, scrollbars = yes, toolbar = no");
}


jQuery(document).ready(function($) { 
    $(".scroll").click(function(event){    
        event.preventDefault();
        $('html,body').animate({scrollTop:$(this.hash).offset().top}, 800);
   });
   
   //abrir_carrinho_index
    $("#meuCarrinho").click(function(event){    
       abrir_carrinho();
   });
   
});


function validarCompra(tipo)
{		 	
	var tamanho = "";
	var id_prod = "";	
	if(tipo == 1){
		tamanho = $("#tamanho_selecionado_detalhes").val();
		id_prod = $("#id_prod_selecionado_detalhes").val();
	}else{
		tamanho = $("#tamanho_selecionado").val();
		id_prod = $("#id_prod_selecionado").val();	
	}
	statusCarrinho(1);
		
	if(tamanho == ""){
		alert("Selecione o tamanho do seu produto.");
		//document.compra_produto.tamanho.focus();
		return false;
	}
	
	inicia_load();
	
	$.ajax({
		type: 'POST',
		url: '?secao=pedidoProduto&opcao=salvarPedidoProdutoSecao',
		data: {'id_prod':id_prod, 'tamanho':tamanho},
		success: function (result) {
			//alert(result);
			if(result == "sucess"){
				
				fecha_load();
				//alert("Seu produto foi adicionado ao carrinho com sucesso!");
				location.reload();				
			}else{
				alert("Houve um erro, tente novamente mais tarde!");
				fecha_load();
				
			}
		}
	});
	
}
	 

//salva avaliacao
function salvaAv(){
	var nota = $("input[name='fb']:checked").val();
	var avaliacao = $("#avaliacao").val();
	var recomenda = $("input[name='recomenda']:checked").val();

	if(nota == undefined){
		alert("Selecione a sua nota!");
		return false;
	}
	
	if(avaliacao == ""){
		alert("Digite a sua avaliação!");
		$("#avaliacao").focus();
		return false;
	}
	
	if(recomenda == undefined){
		alert("Selecione se você indicaria!");
		return false;
	}
	
	inicia_load();

	$.ajax({
		type: 'POST',
		url: '?secao=loja&opcao=salvarAV',
		data: {'nota':nota, 'avaliacao':avaliacao, 'recomenda':recomenda, 'id_user':$("#id_user").val(), 'id_prod':$("#id_prod").val()},
		success: function (result) {
			if(result == "sucess"){
				alert("Sua avaliação foi salva com sucesso!");
				fecha_load()
				
				for (i=0;i<document.form_avaliacao.elements.length;i++)
					  if(document.form_avaliacao.elements[i].type == "radio")
						 document.form_avaliacao.elements[i].checked=0 
				
				$("#avaliacao").val("");
			}else{
				alert("Houve um erro, tente novamente mais tarde!");
				$("#load").css("display","none");
				for (i=0;i<document.form_avaliacao.elements.length;i++)
					  if(document.form_avaliacao.elements[i].type == "radio")
						 document.form_avaliacao.elements[i].checked=0 
				$("#avaliacao").val("");
			}
		}
	});
}


	 

//salva avaliacao
function validarPedidoAviso(){
	var id_prod = $("#id_prod_selecionado_detalhes").val();
	var tamanho = $("#tamanho_selecionado_detalhes").val();
	var email = $("#email_aviso").val();
	var nome = $("#nome_aviso").val();

	if(nome == ""){
		alert("Digite o Nome!");
		$("#nome_aviso").focus();
		return false;
	}
	
	if(email == ""){
		alert("Digite o E-mail!");
		$("#email_aviso").focus();
		return false;
	}else{
		if(validarEmailAviso(email) == false){
			alert("Informe um E-mail válido.");
			$("#email_aviso").val("");
			$("#email_aviso").focus();
			return false;
		}
	}
	
	inicia_load();

	$.ajax({
		type: 'POST',
		url: '?secao=loja&opcao=salvarAviso',
		data: {'id_prod':id_prod, 'tamanho':tamanho, 'email':email, 'nome':nome},
		success: function (result) {
			if(result == "sucess"){
				alert("Você será avisado(a) assim que o produto estiver em estoque!");
				fecha_load();
			}else{
				alert("Houve um erro, tente novamente mais tarde!");
				$("#load").css("display","none");
			}
		}
	});
}




jQuery(function($){
   $("#cep_definir_endereco").change(function(){
      var cep_code = $(this).val();
      if( cep_code.length <= 0 ) return;
      $.get("http://apps.widenet.com.br/busca-cep/api/cep.json", { code: cep_code },
         function(result){
            if( result.status!=1 ){
               alert(result.message || "Houve um erro desconhecido");
               return;
            }
            $("input#cep").val( result.code );
            $("input#estado").val( result.state );
            $("input#cidade").val( result.city );
            $("input#bairro").val( result.district );
            $("input#endereco").val( result.address );
            $("input#estado").val( result.state );
            $("input#numero").focus();
			
			if($("#tipo_frete_selecionado").val() != 4){
				calcularFreteEntrega();
			}
			
         });
   });
});	
	

function calcularFreteEntrega()
{		 		
	
	var cep_code = $("#cep_definir_endereco").val();
	
	if(cep_code == ""){
		alert("Digite o CEP!");
		$("#cep_definir_endereco").focus()
		return false;
	}
	
	if($("#tipo_frete_selecionado").val() != 4){
		if(cep_code == "35610-000"){
			document.getElementById("tabela_fretes").style.display = "none";
			var valor_carrinho = $("#valorTotalPedidoFrete").val();
			document.getElementById("valor_total_frete").innerHTML = "R$ "+formataValorParaMoeda(valor_carrinho);
			//$("#valorTotalPedidoFrete").val(valor_carrinho);
			document.getElementById("frete_gratis_dores").style.display = "block";
			$("#valor_frete_selecionado").val("0.00");
			$("#tipo_frete_selecionado").val(3);
			return false;
		}else{
			document.getElementById("tabela_fretes").style.display = "block";
			document.getElementById("frete_gratis_dores").style.display = "none";
			$("#valor_frete_selecionado").val("");
			$("#tipo_frete_selecionado").val("");
		}
	}
		
	var estado = "";
	var cidade = "";
	
	if( cep_code.length <= 0 ) return;
	$.get("http://apps.widenet.com.br/busca-cep/api/cep.json", { code: cep_code },
	 function(result){
		if( result.status!=1 ){
		   alert(result.message || "Houve um erro desconhecido");
		   return;
		}
		estado = result.state;
		cidade = result.city;
		//alert(estado);
		document.getElementById("tabela_fretes").style.display = "block";
		//document.getElementById("destino_frete").innerHTML = cidade+" - "+estado;
	 });
	
	if($("#tipo_frete_selecionado").val() != 4){	 
	
		var res = cep_code.split("-");
		var cep_limpo = res[0]+res[1];
		
		var estoque = $("#em_estoque").val();
		
		$.ajax({
			type: 'POST',
			url: '?secao=pedidoProduto&opcao=consultaCep',
			data: {'cep':cep_limpo,'estoque':estoque},
			success: function (result) {
				//alert(result);
				var dados = result.split("_");
				$("#valor_sedex_item").val(dados[0]);
				$("#valor_pac_item").val(dados[2]);
	
				document.getElementById("valor_sedex").innerHTML = dados[0];
				document.getElementById("prazo_sedex").innerHTML = dados[1];
				document.getElementById("valor_pac").innerHTML = dados[2];
				document.getElementById("prazo_pac").innerHTML = dados[3];
				
				if(result == "sucess"){
								
				}else{
					//alert("Houve um erro, tente novamente mais tarde!");
					
				}
			}
		});
		
	}
	
}	

function calcularFreteEntregaInicial(){		 		
	
	var cep_code = $("#cep_definir_endereco_inicial").val();
	
	if(cep_code == ""){
		return false;
	}
	
	if($("#tipo_frete_selecionado").val() == 4){
		return false;
	}
	
	if(cep_code == "35610-000"){
		document.getElementById("tabela_fretes").style.display = "none";
		var valor_carrinho = $("#valorTotalPedidoFrete").val();
		document.getElementById("valor_total_frete").innerHTML = "R$ "+formataValorParaMoeda(valor_carrinho);
		//$("#valorTotalPedidoFrete").val(valor_carrinho);
		document.getElementById("frete_gratis_dores").style.display = "block";
		$("#valor_frete_selecionado").val("0.00");
		$("#tipo_frete_selecionado").val(3);
		return false;
	}else{
		document.getElementById("tabela_fretes").style.display = "block";
		document.getElementById("frete_gratis_dores").style.display = "none";
		$("#valor_frete_selecionado").val("");
		$("#tipo_frete_selecionado").val("");
	}
		
	var estado = "";
	var cidade = "";
	
    var res = cep_code.split("-");
	var cep_limpo = res[0]+res[1];
	
	var estoque = $("#em_estoque").val();
	
	$.ajax({
		type: 'POST',
		url: '?secao=pedidoProduto&opcao=consultaCep',
		data: {'cep':cep_limpo,'estoque':estoque},
		success: function (result) {
			//alert(result);
			document.getElementById("tabela_fretes").style.display = "block";
			var dados = result.split("_");
			$("#valor_sedex_item").val(dados[0]);
			$("#valor_pac_item").val(dados[2]);

			document.getElementById("valor_sedex").innerHTML = dados[0];
			document.getElementById("prazo_sedex").innerHTML = dados[1];
			document.getElementById("valor_pac").innerHTML = dados[2];
			document.getElementById("prazo_pac").innerHTML = dados[3];
			
			if(result == "sucess"){
							
			}else{
				//alert("Houve um erro, tente novamente mais tarde!");
				
			}
		}
	});
	
}
	
	
function selecionaFreteEntrega(tipo){
	
	
	$("#tipo_frete_selecionado").val(tipo);
	var frete_selecionado = "";
	if(tipo == 1){
		$("#valor_frete_selecionado").val(document.getElementById("valor_sedex_item").value.replace(",","."));
		var frete_selecionado = document.getElementById("valor_sedex_item").value.replace(",",".");
	}
	if(tipo == 2){
		$("#valor_frete_selecionado").val(document.getElementById("valor_pac_item").value.replace(",","."));
		var frete_selecionado = document.getElementById("valor_pac_item").value.replace(",",".");
	}
	
	//alert($("#valorTotalPedido").val().replace(".","").replace(".",""));
	if($("#valorTotalPedido").val().replace(".","").replace(".","") >= 100000){
		var valor_carrinho = $("#valorTotalPedido").val().replace(",","").replace(".","");
	}else{
		var valor_carrinho = $("#valorTotalPedido").val();
	}

	//pegando valor presente se tiver
	var valor_presente = $("#valor_presente").val();
	if(valor_presente != ""){
		var valor_total = parseFloat(valor_carrinho)+parseFloat(frete_selecionado)+parseFloat(valor_presente);
	}else{
		var valor_total = parseFloat(valor_carrinho)+parseFloat(frete_selecionado);	
	}
	
	var valor_final_formatado = formataValorParaMoeda(valor_total);
	$("#valorTotalPedidoFrete").val(valor_final_formatado.replace(",","."));
	document.getElementById("valor_total_frete").innerHTML = "R$ "+valor_final_formatado;	
}	
	

function inicia_load(){
	document.getElementById("fundo_load").style.display = "block";	
	document.getElementById("load_site").style.display = "block";	
}


function fecha_load(){
	document.getElementById("fundo_load").style.display = "none";	
	document.getElementById("load_site").style.display = "none";	
}

function abreProdSeleciona(){
	document.getElementById("fundo_load").style.display = "block";	
	document.getElementById("box_produto_seleciona").style.display = "block";	
}

function fechaProdSeleciona(){
	document.getElementById("fundo_load").style.display = "none";	
	document.getElementById("box_produto_seleciona").style.display = "none";	
}


function abrir_carrinho(){
	document.getElementById("fundo_load").style.display = "block";	
	document.getElementById("carrinho_flutuante").style.display = "block";
	statusCarrinho(1);
}

function fecha_carrinho(){
	document.getElementById("fundo_load").style.display = "none";	
	document.getElementById("carrinho_flutuante").style.display = "none";
	statusCarrinho(0);
}

function statusCarrinho(status){

	$.ajax({
		type: 'POST',
		url: '?secao=pedidoProduto&opcao=mudaStatusCarrinho',
		data: {'status':status},
		success: function (result) {
			//alert(result);
		}
	});
	
}


function iniciaCompra(produto,tamanhos)
{		 		
	
	var id_prod = produto;
	var tamanho = tamanhos;
	var foto = "";
	var result = "";
	
	$.ajax({
		type: 'POST',
		url: '?secao=loja&opcao=pegarDadosProdutos',
		data: {'id_prod':id_prod},
		success: function (result) {
			
			if(result == "erro"){
				alert("Houve um erro, tente novamente mais tarde!");
				return false;
			}
			
			abreProdSeleciona();
			
			var dados = result.split(" --- ");
			document.getElementById("img_prod_adicionar").src = dados[0];
			document.getElementById("texto_prod_adicionar_titulo").innerHTML = dados[1];
			document.getElementById("texto_prod_adicionar_preco").innerHTML = dados[2];
			document.getElementById("tamanhos_produto_seleciona").innerHTML = dados[4];
			$("#id_prod_selecionado").val(id_prod);
						
			
			if(tamanho == 1){
				$(".itens_produto_comprar2").css("display","none");
				$("#fechar_prod_seleciona").css("display","none");
				$("#tamanhos_produto_seleciona_tamanhos").css("display","none");
				setTimeout("validarCompra()", 2000)
			}
			
			if(tamanho > 1){
				$("#tamanhos_produto_seleciona_tamanhos").css("display","block");

			}
			
			
		}
	});
	
}



function calcularFreteDetalhes()
{		 		

	var cep_code = $("#frete_carrinho").val();
	
	if(cep_code == ""){
		alert("Digite o CEP!");
		$("#frete_carrinho").focus()
		return false;
	}
	
	if(cep_code == "35610-000"){
		document.getElementById("tabela_fretes").style.display = "none";
		document.getElementById("frete_gratis_dores").style.display = "block";
		return false;
	}else{
		document.getElementById("frete_gratis_dores").style.display = "none";
	}

	
	var estado = "";
	var cidade = "";
	
	if( cep_code.length <= 0 ) return;
	$.get("http://apps.widenet.com.br/busca-cep/api/cep.json", { code: cep_code },
	 function(result){
		if( result.status!=1 ){
		   alert(result.message || "CEP não encontrado!");
		   //return;
		}
		estado = result.state;
		cidade = result.city;
		//alert(estado);
		document.getElementById("tabela_fretes").style.display = "block";
		document.getElementById("destino_frete").innerHTML = cidade+" - "+estado;
	 });
		 
	
    var res = cep_code.split("-");
	var cep_limpo = res[0]+res[1];
	
	var estoque = $("#em_estoque").val();

	$.ajax({
		type: 'POST',
		url: '?secao=pedidoProduto&opcao=consultaCep',
		data: {'cep':cep_limpo,'estoque':estoque},
		success: function (result) {
			//alert(result);
			var dados = result.split("_");
			$("#valor_sedex_item").val(dados[0]);
			$("#valor_pac_item").val(dados[2]);

			document.getElementById("valor_sedex").innerHTML = dados[0];
			document.getElementById("prazo_sedex").innerHTML = dados[1];
			document.getElementById("valor_pac").innerHTML = dados[2];
			document.getElementById("prazo_pac").innerHTML = dados[3];
			
			if(result == "sucess"){
							
			}else{
				//alert("Houve um erro, tente novamente mais tarde!");
				
			}
		}
	});
	
}	
	
function abre_msg_presente(){
	var estado = document.getElementById("digite_mensagem").style.display;	
	
	var valor_presente = document.getElementById('valor_presente').value;
	var valorTotalPedidoFrete = document.getElementById('valorTotalPedidoFrete').value;
	var valorTotal = 0;
	
	if(estado == "none"){
		document.getElementById("digite_mensagem").style.display = "block";	
		document.getElementById("aparece_presente").style.display = "block";
		valorTotal = parseFloat(valorTotalPedidoFrete) + parseFloat(valor_presente);
	}else{
		document.getElementById("digite_mensagem").style.display = "none";	
		document.getElementById("aparece_presente").style.display = "none";
		valorTotal = parseFloat(valorTotalPedidoFrete) - parseFloat(valor_presente);
	}
	document.getElementById('valorTotalPedidoFrete').value = valorTotal;
	valorTotal = formataValorParaMoeda(valorTotal);
	document.getElementById('valor_total_frete').innerHTML = "R$ "+ valorTotal;
}




	 