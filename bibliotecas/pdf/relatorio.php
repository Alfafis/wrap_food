<?php

include('../conf/conecta_local.php');
session_start();
$camp = $_GET['idcampanha'];

include ('../funcoes/funcao.php');

 include ("../bibliotecas/jpgraph/src/jpgraph.php");
 
 include ("../bibliotecas/jpgraph/src/jpgraph_bar.php");

include("fpdf.php");

$pdf=new FPDF();

$pdf->Open();//cria um novo documento

$pdf->AddPage();//adiciona uma p�gina

//retorna dados dos contatos de acordo com o grupo escolhido(de acordo com a conta)
$sql = "SELECT c.nome, c.data_envio, c.email_resposta, e.nome, e.idefolder, c.assunto_email

		FROM campanha c, efolder e 
		
		where e.idefolder = c.efolder_idefolder  and c.idcampanha = $camp and 
		
			  e.conta_idconta = ".$_SESSION['conta_idconta'];

$rs=$db->Execute($sql);//executa a query que retorna os dados do(s) contato(s) 

$nomcamp = $rs -> fields[0];
		
$sql_qtde = "SELECT  ch.contato_idcontato,   g.nome

			FROM campanha_has_contato ch, contato c, grupo g
			
			where ch.campanha_idcampanha = $camp
			
				  and c.idcontato = ch.contato_idcontato
				  
				  and c.grupo_idgrupo = g.idgrupo
				  
				  and c.idcontato = ch.contato_idcontato";

$rq=$db->Execute($sql_qtde);//executa a query que retorna os dados do(s) contato(s) 
			
$enviados = $rq -> RecordCount();

// retorna a qtde pedidos de remo��o
$sql_remocao = "SELECT solicitou_remocao

			FROM campanha_has_contato
			
			where campanha_idcampanha = $camp
			
				  and solicitou_remocao=1"; // 1 ==> booleano 0 ou 1

$rr=$db->Execute($sql_remocao);

// retorna a qtde de vezes que o link foi acessado
$sql_link = "SELECT link_acessado

			FROM campanha_has_contato
			
			where campanha_idcampanha = $camp
			
				  and link_acessado = 1";// 1 ==> booleano 0 ou 1

$rl=$db->Execute($sql_link);


// retorna a qtde de emails retornado
$sql_retornado = "SELECT email_retornado

			FROM campanha_has_contato
			
			where campanha_idcampanha = $camp
			
				    and email_retornado = 1";// 1 ==> booleano 0 ou 1

$re=$db->Execute($sql_retornado);


// retorna a qtde de emails retornado
$sql_lido = "SELECT email_lido

			FROM campanha_has_contato
			
			where campanha_idcampanha = $camp
			
				  and email_lido = 1";// 1 ==> booleano 0 ou 1

$ri=$db->Execute($sql_lido);

$y = array(); 
$y[0] ="Enviados";
$y[1] =" Abertos";
$y[2] =" Retornados";
$y[3] =" Clicados";
//$y[4] =" Mais Clicados";

$x = array(); 
$x[0] = $rq -> RecordCount();;
$x[1] = $ri -> RecordCount();;
$x[2] = $re -> RecordCount();;
$x[3] = $rl -> RecordCount();;
//$x[4] = $rq -> RecordCount();;

for($i = 0; $i < 4; $i++){
	
  //Retornando os dados e armazenado nos arrays.
  $datay[$i] =  $x[$i];   //dados Eixo Y
  
  $datax[$i] =  $y[$i];  //dados Eixo X string que aparece debaixo da barra
	
}

$grupos =$aux= "";

$i = 0;
while (!$rq->EOF){
	
	if(($aux!= $rs -> fields[3])&&($i < $enviados )){

		$grupos .= $rs -> fields[3];

	}
	elseif (($aux!= $rs -> fields[3])&&($i == $enviados )){

		$grupos .= $rs -> fields[3];
		
	}
	$aux = $rs -> fields[3];	
	
	$i++;

	$rq -> MoveNext();
}


$pdf->SetXY(10, 30);

$pdf->SetFont('Helvetica', 'B', 14);

$pdf->Image('../img/logo_relatorio.jpg',15,20,33);

$pdf->SetX(70);
$pdf->Cell(100, 20, 'Relat�rio de envio de  campanha ');

$pdf->SetFont('Helvetica', 'I', 14);
///$pdf->Cell(0, 5, '(http://www.fpdf.org)');
$pdf->ln(); // pula 1 linha
$pdf->SetLineWidth(0.5);
//$pdf->Line(11, 27, 100, 27);//linha horizontal 11=>inicio, 27=>altura, 125=> final, 27=> altura final da linha
$pdf->ln(8);//altura da pr�xima linha a ser escrita
$pdf->SetFont('Courier', '', 12);//tipo, ''(B, I, S), tamanho da fonte
$pdf->SetLineWidth(0.2);//largura da borda

$pdf->SetX(20);
$pdf->Cell(47, 0, 'Campanha: ', 0,0);
$pdf->Cell(40, 0, $rs -> fields[0],0,0);
$pdf->ln(5); // pula 1 linha

$pdf->SetX(20);
$pdf->Cell(47, 0, 'eFolder: ', 0,0);
$pdf->Cell(40, 0, $rs -> fields[3],0,0);
$pdf->ln(5); // pula 1 linha

$pdf->SetX(20);
$pdf->Cell(47, 0, 'Assunto: ');
//$pdf->SetX(80);
$pdf->Cell(40, 0, $rs -> fields[5]);
$pdf->SetX(20);
$pdf->ln(5); // pula 1 linha

$pdf->SetX(20);
$pdf->Cell(47, 0, 'Data de envio: ');
$pdf->Cell(40, 0, data_usuario($rs -> fields[1]));
$pdf->ln(5); // pula 1 linha

$pdf->SetX(20);
$pdf->Cell(47, 0, 'Email de resposta: ');
$pdf->Cell(40, 0, $rs -> fields[2]);
$pdf->ln(5); // pula 1 linha


$pdf->SetX(20);
$pdf->Cell(40, 20, 'Gerenciamento de resultados');
$pdf->ln(15); // pula 1 linha

$pdf->SetX(20);
$pdf->Cell(60, 10, 'QTDE EMAILS LIDO ', 1, 0,'J');
$pdf->Cell(60, 10, $ri -> RecordCount(), 1, 1,'J');

$pdf->SetX(20);
$pdf->Cell(60, 10, 'QTDE ACESSOS AO LINK ', 1, 0,'J');
$pdf->Cell(60, 10, $rl -> RecordCount(), 1, 1,'J');

$pdf->SetX(20);
$pdf->Cell(60, 10, 'QTDE EMAILS ENVIADOS ', 1, 0,'J');
$pdf->Cell(60, 10, $enviados, 1, 1,'J');

$pdf->SetX(20);
$pdf->Cell(60, 10, 'GRUPOS PARTICIPANTES', 1, 0,'J');
$pdf->Cell(60, 10, $grupos, 1, 1,'J');

$pdf->SetX(20);
$pdf->Cell(60, 10, 'PEDIDOS DE REMO��O  ', 1, 0,'J');
$pdf->Cell(60, 10, $rr -> RecordCount(), 1, 1,'J');


$pdf->ln(1); // pula 1 linha
$pdf->SetX(20);
$pdf->Cell(60, 20, 'Desempenho da campanha');
$pdf->ln(20); // pula 1 linha


/******************INICIO DO GRAFICO******************************/
// Configura��o das dimens�es do gr�fico.
  $graph = new Graph(800,700,"auto");
  $graph->img->SetMargin(30,40,30,90);//(margem esquerda, margem direita,,,)
  $graph->SetScale("textlin");
  $graph->SetImgFormat('jpeg',100);
  $graph->SetMarginColor("lightblue");
  $graph->SetShadow();
  
  // Configura��o do titulo do gr�fico.
  $graph->title->Set("Grafico da Campanha");
  $graph->title->SetFont(FF_VERDANA,FS_NORMAL,12);
  $graph->title->SetColor("darkred");
  
  // Configura��o de Font.
  $graph->xaxis->SetFont(FF_VERDANA,FS_NORMAL,10);
  $graph->yaxis->SetFont(FF_VERDANA,FS_NORMAL,10);
  $graph->yscale->ticks->SupressZeroLabel(false);
  
  // Dados do --> Eixo X
  $graph->xaxis->SetTickLabels($datax);
  $graph->xaxis->SetLabelAngle(50);
  
  // Dados do --> Eixo Y
  $bplot = new BarPlot($datay);
  $bplot->SetWidth(0.6);
  
  // Ajuste de cor
  $bplot->SetFillGradient("navy","#EEEEEE",GRAD_LEFT_REFLECTION);
  $bplot->SetColor("white");
  $graph->Add($bplot);
  
  // Cria o gr�fico
 // $graph->Stroke();

 $campanha = strtolower($nomcamp.$camp);
 $url = "C:\www\webmarketing\img\\".$campanha.".jpg";
 $graph->Stroke($url);


 
 $img = imagecreatefromjpeg($url);
			
 $largura_foto_atual = imagesx($img);

 $altura_foto_atual = imagesy($img);


//$pdf->Image("../grafico/".$campanha.".jpg",15,200,33);
//$pdf->Image('../img/logo_relatorio.jpg',15,200,33);
$pdf->Image("../img/".$campanha.".jpg",15,165,140);



/******************FIM DO GRAFICO******************************/

$pdf->Output();

?>