<?php

$font_texto = 12;

//Inclui o a classe de cria��o de PDF's
require('fpdf.php');

/********************************************CABE�ALHO*******************************************/
//extens�o da classe fpdf o qual cria um cabe�alho e rodap� em todas a paginas
class PDF extends FPDF{

	//Fun��o para criar o cabe�alho da p�gina
	function Header(){
		$font_cabecalho = 14;
		//$this->Image('../img/logo.jpg',5,6,46,15,'jpg','');
		//Seleciona fonte: Arial bold 12
		$this->SetFont('Arial','B',$font_cabecalho);
		//Move para a direita
		$this->SetXY(55,15);
		//T�tulo do relat�rio
		//$this->Cell(50,7,'XXXXXXXXXX',0,0,'C');
		$this->Cell(100,7,'T�tulo XXXXXXXXXXXXXXXXXXXX',0,0,'C');
		$this->Cell(45,7,$this->Data() ,0,0,'R');
	//	$this->Cell(60,7,$this->Hora() ,0,0,'R');
		$this->SetXY(103,30);
		$this->Cell(10,7,'SUBT�TULO XXXXXXXXXXXXXXX',0,0,'C');
		$this->Cell(25,7,'',0,0,'0');
		$this->Cell(20,7,'',0,0,'0');
		//Quebra de linha
		$this->Ln(10);
	}
/**************************************************************************************************/

/********************************************RODAP�************************************************/
//Fun��o para criar o rodap� da p�gina
	function Footer(){
		$font_rodape = 8;
		//Rodap� a 5 cm da margem esquerda e a 8 cm da base (parte inferior da p�gina)
		$this->SetXY(10,-20);
		//Seleciona fonte: Arial Italic 8
		$this->SetFont('Arial','I',$font_rodape);
		//linha de rodap� 10=>inicio da linha, 190 => altura da linha, 280 => final da linha
		$this->Line(8, 280, 200, 280);
		//Rodap� esquerdo 
		$this->Cell(100,10,'DIGITE AQUI O RODAPE PARA APARECER EM TODAS AS P�GINAS.',0,0,'P');
		//Rodap� direito (numero da p�gina)
		$this->Cell(90,10,'P�g: '.$this->PageNo().'/{nb}',0,0,'R');
	}
}//fecha extens�o da classe FPDF
/**************************************************************************************************/

//Instanciando a classe pertencente
//Cria novo arquivo PDF na orienta��o, unidade de medida e tamanho da folha indicada (210x297)
$pdf = new PDF($orientation='P',$unit='mm',$format='A4'); // P - orienta��o "retrato"; L - orienta��o "paisagem"

$pdf->AliasNbPages(); // define um Alias para o numero de p�ginas

$pdf->AddPage(); // Adiciona p�gina

   //Seleciona fonte: Times 8
  $pdf->SetFont('Times','',$font_texto);

  //->Posiciona a tabela a 20 cm da margem esquerda
  $pdf->SetX(20);

  //-> Exibe cabe�alho da tabela
  $pdf->Cell(125, 5, 'Nome', 1, 0,'T'); //200 =>largura da celula, =>altura da celula, nome => Text da celula, 			continuando=>  // 1 => borda com 0 sem borda, 0 => indica onde a posi��o corrente deve ficar
  $pdf->Cell(25, 5, 'CPF/CNPJ', 1, 0,'L'); //cpf
  $pdf->Cell(23, 5, 'RG', 1, 0,'L'); //rg
  $pdf->Ln(5);// quebra de linha		  
  
  //->Posiciona a tabela a 20 cm da margem esquerda
  $pdf->SetX(20);
  $pdf->Cell(125, 5, 'TESTE', 1, 0,'L'); //nome
  $pdf->Cell(25, 5, '123.456.789.01'  , 1, 0,'L'); //cpf / cnpj 
  $pdf->Cell(23, 5, '123456789', 1, 0,'L'); //rg
  $pdf->Ln(5);// quebra de linha		  


	$savedate = date('d-m-Y');
	$acao = 'exibir';
	
	if ($acao=="exibir"){
		$pdf->Output();// exibe o arquivo pdf tempor�rio no browser
	}
	elseif ($acao=="salvar"){
		$pdf->Output($name="ARQ PDF $savedate.pdf",$dest='D'); // exibe caixa de di�logo para salvar o pdf
		header("Location: sua_p�gina.php"); // redireciona ao menu principal
	}
?>

