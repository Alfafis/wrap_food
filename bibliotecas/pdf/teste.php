<?php
include('phplot.php');
$graph = new PHPlot(300,250);      //cria um gr�fico com tamanho 300x250 pixels

$graph->SetTitle("Title\n\rSubtitle");
$graph->SetXTitle('X data');
$graph->SetYTitle('Y data');

//Dados para gerar o gr�fico
$example_data = array(
                array('a',3),
                array('b',5),
                array('c',7),
                array('d',8),
                array('e',2),
                array('f',6),
                array('g',7)
);

$graph->SetDataValues($example_data);
$graph->DrawGraph();
?> 