<?php /* Smarty version 2.6.18, created on 2018-03-26 16:44:50
         compiled from login.tpl */ ?>
<?php echo '
<script type="text/javascript">
function validarFormularioLogin(){	
			
	if(document.form_login.email.value == ""){
		alert("Preencha o campo E-mail.");
		document.form_login.email.focus();
		return false;
	}else{
		if(validarEmail(document.form_login.email.value) == false){
			alert("Informe um E-mail válido.");
			document.form_login.email.focus();
			return false;
		}
	}

	if(document.form_login.senha.value == ""){
		alert("Preencha o campo Senha.");
		document.form_login.senha.focus();
		return false;
	}

	document.form_login.submit();
	
}

</script>
'; ?>
 
<div id="container_swip">
    <br /><br />
	
    <div class="login-bg scroll-content ionic-scroll" style="position:relative;"><div class="scroll" style="transform: translate3d(0px, 0px, 0px) scale(1);">

    <div class="login-content" style="padding:0 20px;">

      <!-- Logo -->
       <div class="padding text-center">
          <img class="profile-picture circle" menu-close="" src="img/logo.png" style="width:30%;margin:5% 0 auto;">
      </div>

      <!-- Login form -->
      <div class="list">
		
        <form action="index.php?secao=usuarioSite&opcao=efetuarLogin" method="post" name="form_login" class="form_contato">
            <input type="hidden" name="paginaRedirecionar" value="<?php echo $this->_tpl_vars['paginaRedirecionar']; ?>
" />
        
            <label class="item item-input item-stacked-label">
              <span class="input-label" aria-label="email" id="_label-14">E-mail</span>
              <input type="text" name="email">
            </label>
            
    
            <label class="item item-input item-stacked-label">
              <span class="input-label" aria-label="senha" id="_label-15">Senha</span>
              <input type="password" name="senha">
            </label>
		</form>

      </div>
      <a href="?secao=user&opcao=esqueceu_senha"><p class="text-right light">Esqueceu a senha?</p></a>

      <div class="padding-top">
        <button class="button button-block button-assertive" ui-sref="home" onclick="validarFormularioLogin()">
          LOGAR
        </button>
      </div>


      <!-- Other links -->
      <div class="text-center">
        <a class="light" href="?secao=user&opcao=cadastrar">Novo aqui? Cadastre-se</a>
      </div>
		<br /><br /><br />
    </div>

  </div><div class="scroll-bar scroll-bar-v"><div class="scroll-bar-indicator scroll-bar-fade-out" style="transform: translate3d(0px, 0px, 0px) scaleY(1); height: 0px;"></div></div></div>
    
    
</div>
