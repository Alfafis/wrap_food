<?php /* Smarty version 2.6.18, created on 2018-04-11 13:58:54
         compiled from centro_pedido_produto_pagamento.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'replace', 'centro_pedido_produto_pagamento.tpl', 48, false),)), $this); ?>
<?php echo '
<style type="text/css">
.ui-dialog .ui-dialog-buttonpane{
text-align:center;	
}
.ui-dialog .ui-dialog-buttonpane button{
float:none;
text-align:center;	
}
</style>
'; ?>

<div id="container_swip">
	<br /><br /><br />
  	
    <form id="form_pedido_produto" method="post" action="" name="form_pedido_produto">
    
    <?php if (count ( $this->_tpl_vars['listaProdutoCarrinho'] ) > 0): ?>
    <center>ESTES SÃO OS ITENS QUE VOCÊ SELECIONOU PARA COMPRA, CONFIRA ABAIXO:</center>
    <br />
    <div class="row">
    
        <div class="col button button-assertive" style="margin:1%;line-height:normal;" onClick="location.href='index.php?secao=pedidoProduto&opcao=listarPedidoProdutoSecao'"> 
            <span class="text-sm">ALTERAR PEDIDO</span> 
        </div>
        <br />
        <div class="col button button-assertive" style="margin:1%;line-height:normal;" onClick="location.href='index.php?secao=produtos'"> 
            <span class="text-sm">CONTINUAR COMPRANDO</span> 
        </div>
    </div>
    <?php endif; ?>
    
    <?php if (count ( $this->_tpl_vars['listaProdutoCarrinho'] ) > 0): ?>
    <div class="list">
    
    <?php $_from = $this->_tpl_vars['listaProdutoCarrinho']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['produtoCarrinho']):
?>
    
    <input type='hidden'name='listaProdutoQtd[]' id="qtde<?php echo $this->_tpl_vars['produtoCarrinho']['contadorItem']; ?>
" value='<?php echo $this->_tpl_vars['produtoCarrinho']['qtde']; ?>
'>
    <input type='hidden' name='listaProdutoContador[]' id="cont<?php echo $this->_tpl_vars['produtoCarrinho']['contadorItem']; ?>
" value='<?php echo $this->_tpl_vars['produtoCarrinho']['contadorItem']; ?>
'>
    
    <div class="item-thumbnail-left card item item-complex item-right-editable">
    
        <div class="item-content" id="aparecer_delete">
        
            <img src="fotos/<?php echo $this->_tpl_vars['produtoCarrinho']['fotoProduto']; ?>
">
            <h2 class="ng-binding"><?php echo $this->_tpl_vars['produtoCarrinho']['nomeProduto']; ?>
</h2>
            
            <div>
            <h2 class="price pull-left assertive ng-binding" style="line-height:normal;">R$ <?php echo ((is_array($_tmp=$this->_tpl_vars['produtoCarrinho']['precoProduto'])) ? $this->_run_mod_handler('replace', true, $_tmp, ".", ",") : smarty_modifier_replace($_tmp, ".", ",")); ?>
 
            <span class="dark ng-binding">x <span id="txtqtde<?php echo $this->_tpl_vars['produtoCarrinho']['contadorItem']; ?>
"><?php echo $this->_tpl_vars['produtoCarrinho']['qtde']; ?>
</span></span><br />
            <?php if ($this->_tpl_vars['produtoCarrinho']['idPao'] != 3): ?>
                <span style="font-size:11px;color:#333;line-height:normal;">Pão: <?php echo $this->_tpl_vars['produtoCarrinho']['nomePao']; ?>
</span><br />
                <span style="font-size:11px;color:#333;line-height:normal;">
                <?php if ($this->_tpl_vars['produtoCarrinho']['nomeDoce'] == ""): ?>
                Queijo: <?php if ($this->_tpl_vars['produtoCarrinho']['nomeQueijo'] != ""): ?><?php echo $this->_tpl_vars['produtoCarrinho']['nomeQueijo']; ?>
<?php endif; ?>
                <?php else: ?>
                Doce: <?php if ($this->_tpl_vars['produtoCarrinho']['nomeDoce'] != ""): ?><?php echo $this->_tpl_vars['produtoCarrinho']['nomeDoce']; ?>
<?php endif; ?>
                <?php endif; ?>
                <!--<?php if ($this->_tpl_vars['produtoCarrinho']['nomeSalada'] != ""): ?>, <?php echo $this->_tpl_vars['produtoCarrinho']['nomeSalada']; ?>
<?php endif; ?>
                <?php if ($this->_tpl_vars['produtoCarrinho']['nomeMolho'] != ""): ?>, <?php echo $this->_tpl_vars['produtoCarrinho']['nomeMolho']; ?>
<?php endif; ?>
                <?php if ($this->_tpl_vars['produtoCarrinho']['nomeLivre'] != ""): ?>, <?php echo $this->_tpl_vars['produtoCarrinho']['nomeLivre']; ?>
<?php endif; ?>-->
                </span>
                <?php if ($this->_tpl_vars['produtoCarrinho']['nomeSuco'] != ""): ?><br /><span style="font-size:11px;color:#333;line-height:normal;">Suco: <?php echo $this->_tpl_vars['produtoCarrinho']['nomeSuco']; ?>
 - R$ <?php echo ((is_array($_tmp=$this->_tpl_vars['produtoCarrinho']['precoSuco'])) ? $this->_run_mod_handler('replace', true, $_tmp, ".", ",") : smarty_modifier_replace($_tmp, ".", ",")); ?>
</span><?php endif; ?>
            <?php else: ?>
                <span style="font-size:11px;color:#333;line-height:normal;">
                <strong>Adicionais:</strong><br /><?php echo $this->_tpl_vars['produtoCarrinho']['nomeSuco']; ?>

                <br /><strong>Total Adicionais:</strong> R$ <?php echo ((is_array($_tmp=$this->_tpl_vars['produtoCarrinho']['precoSuco'])) ? $this->_run_mod_handler('replace', true, $_tmp, ".", ",") : smarty_modifier_replace($_tmp, ".", ",")); ?>
</span>
            <?php endif; ?>
            </h2>
                        
            </div>
        
        </div>
        
    
    </div>
    <?php endforeach; endif; unset($_from); ?>
    
    <?php if ($this->_tpl_vars['desconto_cupons'] == 'sim'): ?>
    <div class="item-thumbnail-left card item item-complex item-right-editable">
    
        <div class="item-content" id="aparecer_delete">
        
            <img src="img/logo.png">
            <h2 class="ng-binding">Desconto de <?php echo $this->_tpl_vars['desconto']; ?>
%</h2>
            
            <div>
            <h2 class="price pull-left assertive ng-binding" style="line-height:normal;">
            	<span style="font-size:11px;color:#333;line-height:normal;">Desconto por ter completado <?php echo $this->_tpl_vars['ticket']; ?>
 cupons.</span><br />
            	R$ <?php echo ((is_array($_tmp=$this->_tpl_vars['descontoTotal'])) ? $this->_run_mod_handler('replace', true, $_tmp, ".", ",") : smarty_modifier_replace($_tmp, ".", ",")); ?>
 
            </h2>
                        
            </div>
        
        </div>
        
    
    </div>
    <?php endif; ?>
    
    <p style="text-align:center;">Horário agendado para buscar: <strong><?php echo $this->_tpl_vars['horario']; ?>
h</strong></p>
    
    </div>
    <?php endif; ?>
    </form>
    <center><img src="img/pagamentos.gif" title="Este site aceita pagamentos com Visa, MasterCard, Diners, American Express, Hipercard, Aura, Bradesco, Itaú, Unibanco, Banco do Brasil, Banco Real, saldo em conta PagSeguro e boleto." border="0" width="80%"></center>
  <br /><br /><br />
  <div class="clearfix"></div>    
    
  <ion-footer-bar class="bar-assertive bar bar-footer" onClick="definir_pagamento()">
    <div class="button">FINALIZAR COMPRA</div>
    <div class="title" style="left: 111px; right: 111px;"></div>
    <div class="button text-2x ng-binding">
    R$ <?php if ($this->_tpl_vars['valorTotalPedido']): ?><?php echo ((is_array($_tmp=$this->_tpl_vars['valorTotalPedido'])) ? $this->_run_mod_handler('replace', true, $_tmp, ".", ",") : smarty_modifier_replace($_tmp, ".", ",")); ?>
<?php else: ?>0,00<?php endif; ?>
    </div>
  </ion-footer-bar>
  
</div>
