<?php /* Smarty version 2.6.18, created on 2018-03-14 06:45:36
         compiled from produto_detalhes_salada.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'replace', 'produto_detalhes_salada.tpl', 127, false),)), $this); ?>
<?php echo '
<script type="text/javascript">
function verifica_carb(id,qtd){
	
	if($(".carb:checked").length > qtd){
		$(id).prop("checked", false);	
	}
	
}

function verifica_prot(id,qtd){
	
	if($(".prot:checked").length > qtd){
		$(id).prop("checked", false);	
	}
	
}

function verifica_legumes(id,qtd){
	
	if($(".legumes:checked").length > qtd){
		$(id).prop("checked", false);	
	}
	
}

function verifica_verduras(id,qtd){
	
	if($(".verduras:checked").length > qtd){
		$(id).prop("checked", false);	
	}
	
}

function verifica_sementes(id,qtd){
	
	if($(".sementes:checked").length > qtd){
		$(id).prop("checked", false);	
	}
	
}

function verifica_frutas(id,qtd){
	
	if($(".frutas:checked").length > qtd){
		$(id).prop("checked", false);	
	}
	
}

function verifica_molhos(id,qtd){
	
	if($(".molhos:checked").length > qtd){
		$(id).prop("checked", false);	
	}
	
}


function valida_compra(){

	if($(".carb:checked").length == 0){
		//alert("Selecione pelo menos 1 Carboidrato.");
		alertas("Selecione pelo menos 1 Carboidrato.");
		return false;
	}
	
	if($(".prot:checked").length == 0){
		//alert("Selecione pelo menos 1 Proteína.");
		alertas("Selecione pelo menos 1 Proteína.");
		return false;	
	}
	
	if($(".legumes:checked").length == 0){
		//alert("Selecione pelo menos 1 Legume.");
		alertas("Selecione pelo menos 1 Legume.");
		return false;	
	}
	
	if($(".verduras:checked").length == 0){
		//alert("Selecione pelo menos 1 Verdura.");
		alertas("Selecione pelo menos 1 Verdura.");
		return false;	
	}
	
	if($(".sementes:checked").length == 0){
		//alert("Selecione pelo menos 1 Semente.");
		alertas("Selecione pelo menos 1 Semente.");
		return false;	
	}
	
	if($(".frutas:checked").length == 0){
		//alert("Selecione pelo menos 1 Fruta.");
		alertas("Selecione pelo menos 1 Fruta.");
		return false;	
	}
	
	if($(".molhos:checked").length == 0){
		//alert("Selecione pelo menos 1 Molho.");
		alertas("Selecione pelo menos 1 Molho.");
		return false;	
	}
	
	document.compra_produto.submit();	
}


function alertas(msg){
	$("#fundo_alertas").css("display","block");
	$("#text_dialog").html(msg);
	$( "#dialog" ).dialog({
	  close: function( event, ui ) {
		$("#fundo_alertas").css("display","none");  
	  }
	});	
}


</script>
'; ?>


<div id="container_swip" style="overflow:hidden;"> 
  <br />
  <br />
  <div class="list no-padding">
    <div class="item item-menu" ng-repeat="category in categories"> <img src="fotos/<?php echo $this->_tpl_vars['produto']['foto']; ?>
" alt=""/>
      <div class="overlay"> <span class="pull-left light ng-binding"> <span style="font-size:14px;color:#FFF;"><?php echo $this->_tpl_vars['produto']['descricao']; ?>
</span> </span> <span class="pull-right light ng-binding"> R$ <?php echo ((is_array($_tmp=$this->_tpl_vars['produto']['preco'])) ? $this->_run_mod_handler('replace', true, $_tmp, ".", ",") : smarty_modifier_replace($_tmp, ".", ",")); ?>
 </span> </div>
    </div>
  </div>
  
  	<p class="text-center">
	Produto escolhido: <strong><?php echo $this->_tpl_vars['paoEscolhido']; ?>
</strong>
	<br />Tempo de preparo: <strong><?php echo $this->_tpl_vars['produto']['tempo']; ?>
</strong>
	</p>
   
  <form name="compra_produto" action="?secao=pedidoProduto&opcao=salvarPedidoProdutoSecao" method="post">
  
  <input type="hidden" name="idPao" id="idPao" value="<?php echo $this->_tpl_vars['idPao']; ?>
" />
  <input type="hidden" name="idProd" id="idProd" value="<?php echo $this->_tpl_vars['idProd']; ?>
" />
  	
            
  <div class="disable-user-behavior">
    <div class="list">
    
      <!----------------------------------carboidratos--------------------------------------->
      
      <div class="item-divider item"> Carboidratos (<?php echo $this->_tpl_vars['carboidratos']; ?>
 <?php if ($this->_tpl_vars['carboidratos'] == 1): ?>opção<?php else: ?>opções<?php endif; ?>) </div>
		
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" name="carboidratos[]" class="carb" id="carb1" value="Batata Cozida" onclick="verifica_carb('#carb1',<?php echo $this->_tpl_vars['carboidratos']; ?>
)">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding">Batata Cozida</span></div>
      </label>
      
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" name="carboidratos[]" class="carb" id="carb2" value="Batata Doce" onclick="verifica_carb('#carb2',<?php echo $this->_tpl_vars['carboidratos']; ?>
)">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding">Batata Doce</span></div>
      </label>
      
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" name="carboidratos[]" class="carb" id="carb3" value="Grão de Bico" onclick="verifica_carb('#carb3',<?php echo $this->_tpl_vars['carboidratos']; ?>
)">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding">Grão de Bico</span></div>
      </label>
      
      <!----------------------------------Proteínas--------------------------------------->
      
      <div class="item-divider item"> Proteínas (<?php echo $this->_tpl_vars['proteinas']; ?>
 <?php if ($this->_tpl_vars['proteinas'] == 1): ?>opção<?php else: ?>opções<?php endif; ?>) </div>
      
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" name="proteinas[]" class="prot" id="prot1" value="Atum" onclick="verifica_prot('#prot1',<?php echo $this->_tpl_vars['proteinas']; ?>
)">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding">Atum</span></div>
      </label>
      
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" name="proteinas[]" class="prot" id="prot2" value="Frango Desfiado" onclick="verifica_prot('#prot2',<?php echo $this->_tpl_vars['proteinas']; ?>
)">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding">Frango Desfiado</span></div>
      </label>
      
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" name="proteinas[]" class="prot" id="prot3" value="Carne de Sol" onclick="verifica_prot('#prot3',<?php echo $this->_tpl_vars['proteinas']; ?>
)">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding">Carne de Sol</span></div>
      </label>
      
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" name="proteinas[]" class="prot" id="prot4" value="Ricota" onclick="verifica_prot('#prot4',<?php echo $this->_tpl_vars['proteinas']; ?>
)">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding">Ricota</span></div>
      </label>
      
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" name="proteinas[]" class="prot" id="prot5" value="Kani" onclick="verifica_prot('#prot5',<?php echo $this->_tpl_vars['proteinas']; ?>
)">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding">Kani</span></div>
      </label>
      
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" name="proteinas[]" class="prot" id="prot6" value="Ovo de Codorna" onclick="verifica_prot('#prot6',<?php echo $this->_tpl_vars['proteinas']; ?>
)">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding">Ovo de Codorna</span></div>
      </label>
      
      
      
      <!----------------------------------Legumes--------------------------------------->
      
      <div class="item-divider item"> Legumes (<?php echo $this->_tpl_vars['legumes']; ?>
 <?php if ($this->_tpl_vars['legumes'] == 1): ?>opção<?php else: ?>opções<?php endif; ?>) </div>
      
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" name="legumes[]" class="legumes" id="legume1" value="Beterraba" onclick="verifica_legumes('#legume1',<?php echo $this->_tpl_vars['legumes']; ?>
)">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding">Beterraba</span></div>
      </label>
      
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" name="legumes[]" class="legumes" id="legume2" value="Cebola Rocha" onclick="verifica_legumes('#legume2',<?php echo $this->_tpl_vars['legumes']; ?>
)">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding">Cebola Rocha</span></div>
      </label>
      
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" name="legumes[]" class="legumes" id="legume3" value="Cenoura" onclick="verifica_legumes('#legume3',<?php echo $this->_tpl_vars['legumes']; ?>
)">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding">Cenoura</span></div>
      </label>
      
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" name="legumes[]" class="legumes" id="legume4" value="Tomate" onclick="verifica_legumes('#legume4',<?php echo $this->_tpl_vars['legumes']; ?>
)">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding">Tomate</span></div>
      </label>
      
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" name="legumes[]" class="legumes" id="legume5" value="Tomate Cereja" onclick="verifica_legumes('#legume5',<?php echo $this->_tpl_vars['legumes']; ?>
)">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding">Tomate Cereja</span></div>
      </label>
      
      
      
      <!----------------------------------Verduras--------------------------------------->
      
      <div class="item-divider item"> Verduras (<?php echo $this->_tpl_vars['verduras']; ?>
 <?php if ($this->_tpl_vars['verduras'] == 1): ?>opção<?php else: ?>opções<?php endif; ?>) </div>
      
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" name="verduras[]" class="verduras" id="verdura1" value="Alface Americana" onclick="verifica_verduras('#verdura1',<?php echo $this->_tpl_vars['verduras']; ?>
)">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding">Alface Americana</span></div>
      </label>
      
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" name="verduras[]" class="verduras" id="verdura2" value="Brócolis" onclick="verifica_verduras('#verdura2',<?php echo $this->_tpl_vars['verduras']; ?>
)">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding">Brócolis</span></div>
      </label>
      
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" name="verduras[]" class="verduras" id="verdura3" value="Couve-flor" onclick="verifica_verduras('#verdura3',<?php echo $this->_tpl_vars['verduras']; ?>
)">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding">Couve-flor</span></div>
      </label>
      
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" name="verduras[]" class="verduras" id="verdura4" value="Pepino Japonês" onclick="verifica_verduras('#verdura4',<?php echo $this->_tpl_vars['verduras']; ?>
)">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding">Pepino Japonês</span></div>
      </label>
      
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" name="verduras[]" class="verduras" id="verdura5" value="Palmito" onclick="verifica_verduras('#verdura5',<?php echo $this->_tpl_vars['verduras']; ?>
)">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding">Palmito</span></div>
      </label>
      
      
      
      <!----------------------------------Sementes--------------------------------------->
      
      <div class="item-divider item"> Sementes (<?php echo $this->_tpl_vars['sementes']; ?>
 <?php if ($this->_tpl_vars['sementes'] == 1): ?>opção<?php else: ?>opções<?php endif; ?>) </div>
      
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" name="sementes[]" class="sementes" id="semente1" value="Chia" onclick="verifica_sementes('#semente1',<?php echo $this->_tpl_vars['sementes']; ?>
)">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding">Chia</span></div>
      </label>
      
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" name="sementes[]" class="sementes" id="semente2" value="Linhaça" onclick="verifica_sementes('#semente2',<?php echo $this->_tpl_vars['sementes']; ?>
)">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding">Linhaça</span></div>
      </label>
      
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" name="sementes[]" class="sementes" id="semente3" value="Milho" onclick="verifica_sementes('#semente3',<?php echo $this->_tpl_vars['sementes']; ?>
)">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding">Milho</span></div>
      </label>
      
      
      
      <!----------------------------------Frutas--------------------------------------->
      
      <div class="item-divider item"> Frutas (<?php echo $this->_tpl_vars['frutas']; ?>
 <?php if ($this->_tpl_vars['frutas'] == 1): ?>opção<?php else: ?>opções<?php endif; ?>) </div>
      
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" name="frutas[]" class="frutas" id="fruta1" value="Abacaxi" onclick="verifica_frutas('#fruta1',<?php echo $this->_tpl_vars['frutas']; ?>
)">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding">Abacaxi</span></div>
      </label>
      
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" name="frutas[]" class="frutas" id="fruta2" value="Maça" onclick="verifica_frutas('#fruta2',<?php echo $this->_tpl_vars['frutas']; ?>
)">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding">Maça</span></div>
      </label>
      
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" name="frutas[]" class="frutas" id="fruta3" value="Manga" onclick="verifica_frutas('#fruta3',<?php echo $this->_tpl_vars['frutas']; ?>
)">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding">Manga</span></div>
      </label>
      
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" name="frutas[]" class="frutas" id="fruta4" value="Morango" onclick="verifica_frutas('#fruta4',<?php echo $this->_tpl_vars['frutas']; ?>
)">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding">Morango</span></div>
      </label>
      
      
      
      <!----------------------------------Molhos--------------------------------------->
      
      <div class="item-divider item"> Molhos (<?php echo $this->_tpl_vars['molhos']; ?>
 <?php if ($this->_tpl_vars['molhos'] == 1): ?>opção<?php else: ?>opções<?php endif; ?>) </div>
      
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" name="molhos[]" class="molhos" id="molho1" value="Ervas" onclick="verifica_molhos('#molho1',<?php echo $this->_tpl_vars['molhos']; ?>
)">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding">Ervas</span></div>
      </label>
      
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" name="molhos[]" class="molhos" id="molho2" value="Iorgute" onclick="verifica_molhos('#molho2',<?php echo $this->_tpl_vars['molhos']; ?>
)">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding">Iorgute</span></div>
      </label>
      
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" name="molhos[]" class="molhos" id="molho3" value="Iorgute de Limão" onclick="verifica_molhos('#molho3',<?php echo $this->_tpl_vars['molhos']; ?>
)">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding">Iorgute de Limão</span></div>
      </label>
      
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" name="molhos[]" class="molhos" id="molho4" value="Pesto" onclick="verifica_molhos('#molho4',<?php echo $this->_tpl_vars['molhos']; ?>
)">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding">Pesto</span></div>
      </label>
      
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" name="molhos[]" class="molhos" id="molho5" value="Barbecue" onclick="verifica_molhos('#molho5',<?php echo $this->_tpl_vars['molhos']; ?>
)">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding">Barbecue</span></div>
      </label>
      
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" name="molhos[]" class="molhos" id="molho6" value="Parmesão" onclick="verifica_molhos('#molho6',<?php echo $this->_tpl_vars['molhos']; ?>
)">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding">Parmesão</span></div>
      </label>
      
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" name="molhos[]" class="molhos" id="molho7" value="Mostarda e Mel" onclick="verifica_molhos('#molho7',<?php echo $this->_tpl_vars['molhos']; ?>
)">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding">Mostarda e Mel</span></div>
      </label>
      
      
      <!----------------------------------Livre--------------------------------------->
      
      <div class="item-divider item"> Escolha Livre</div>
      
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" name="livres[]" value="Azeite">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding">Azeite</span></div>
      </label>
      
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" name="livres[]" value="Orégano">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding">Orégano</span></div>
      </label>
      
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" name="livres[]" value="Pimenta do Reino">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding">Pimenta do Reino</span></div>
      </label>
      
      
      
      <!----------------------------------Adicionais--------------------------------------->
      
      <div class="item-divider item"> Adicionais - R$ 2,00</div>
      
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" name="adicionais[]" value="Atum">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding">Atum</span></div>
      </label>
      
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" name="adicionais[]" value="Frango Desfiado">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding">Frango Desfiado</span></div>
      </label>
      
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" name="adicionais[]" value="Carne de Sol">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding">Carne de Sol</span></div>
      </label>
      
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" name="adicionais[]" value="Ricota">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding">Ricota</span></div>
      </label>
      
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" name="adicionais[]" value="Kani">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding">Kani</span></div>
      </label>
      
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" name="adicionais[]" value="Ovo de Codorna">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding">Ovo de Codorna</span></div>
      </label>
      
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" name="adicionais[]" value="Penne Int.">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding">Penne Int.</span></div>
      </label>
      
     
    </div>
  </div>
  </form>
  
  
    <div class="col button button-assertive" onclick="valida_compra()"> 
        <i class="text-2x ion-ios-cart-outline"></i> 
        <span class="text-sm">ADICIONAR AO CARRINHO</span> 
    </div>
  
  
</div>
<!-- fim cotainer sw -->