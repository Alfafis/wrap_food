<?php /* Smarty version 2.6.18, created on 2018-03-22 13:08:59
         compiled from empresa.tpl */ ?>
<div id="container_swip">
    <br /><br />
    
    <div class="padding stable-bg">

      <h2 class="dark">WRAP BRASIL</h2>
      <!--<span class="dark">225 Valencia St, San Francisco, CA</span>-->

      <h3>NOSSA HISTÓRIA</h3>

      <p>O Wrap Brasil foi idealizado em novembro de 2014 e fundado pelo empreendedor, Rafael Barros.</p>
      
      <p>Seu sonho era trazer para o Brasil um lanche diferente, algo novo, saudável, que não fosse apenas uma novidade, mas que inspira-se as pessoas à se alimentarem com qualidade. Sua busca então chegou aos EUA, que é um país onde o Wrap, um sanduíche leve e saudável, feito com pão sírio ou folha, já faz parte do cotidiano dos americanos, sendo uma opção diferenciada de fast-food.</p>
      
      <p>Após conhecer o lanche, começou sua busca por diferenciais que poderiam agregar ainda mais o Wrap no Brasil, sendo um país tropical e energético, seu cardápio deveria ser pensado de forma cuidadosa, para encaixar na cultura e no cotidiano dos brasileiros. Após essa busca, Rafael então, de forma empreendedora e com um sonho, investiu suas economias em um <em>food truck</em>, projetado para acolher e realçar seu posicionamento, natural e saudável.</p>
      
      <p>Atualmente, o Wrap Brasil está focado no público universitário, atuando em universidades e realizando eventos externos que estejam alinhados com seu posicionamento. </p>
      
      
      
      <h3>DIFERENCIAIS</h3>
      
      <ul style="list-style:outside;margin-left:20px;">
      	<li>Nossos insumos são de qualidade <em>premium</em>, nossos molhos exclusivos são desenvolvidos por <em>Chef’s</em> de cozinha especializados em nutrição. O pão folha utilizado para fazer o Wrap foi desenvolvido pelo <em>Chef</em> Libanês Shamarandy. O pão folha e o sírio, ambos não possuem gordura trans, nem lactose, tudo isso foi pensado para agregar ainda mais de forma saudável na dieta de nossos clientes.</li>
      	<li>Além do Wrap, que é uma refeição leve, classificada como <em>snacks</em>, que são pequenas refeições entre as principais, existe uma opção de almoço, uma variedade incrível de saladas onde as combinações de ingredientes são feitas pelo próprio cliente. </li>
        <li>Possuímos um cartão fidelidade para aqueles que se preocupam com a qualidade de suas refeições e são fiéis aos nossos produtos, onde a cada 06 refeições ele tem um desconto de 50% na compra de uma nova refeição.</li>
        <li>Nossos Wrap's são embalados em um papel especial, desenvolvido exclusivamente para manter a temperatura do sanduíche e evitar que vaze molho, mantendo o padrão de qualidade do produto.</li>
        <li>Além de estarmos presentes nas redes sociais, Facebook e Instagram, atualmente contamos com um app de compra, que permite o cliente, selecionar sua refeição, realizar o pagamento e agendamento de quando retirar o produto.</li>
      </ul>
      
      
      <h3>NOSSO PILARES</h3>
      
      <p><strong>MISSÃO</strong><br />Inspirar nossos clientes e levar uma alimentação saudável, de qualidade e acessível. </p>
      
      <p><strong>VISÃO</strong><br />Prospectar novos clientes por meio de um conceito novo de alimentação saudável, com um atendimento rápido e tecnológico.</p>
      
      <p><strong>VALORES</strong><br />Compromisso<br />
Ética<br />
Dedicação <br />
Confiabilidade</p>
      

      <h3>Funcionamento</h3>

      <div class="list list-inset">
        <!-- ngRepeat: day in days --><label class="item item-input item-stacked-label" ng-repeat="day in days">
          <span class="input-label ng-binding" aria-label="" id="_label-4">Segunda a sexta</span>
          <span class="data ng-binding">11:00 - 21:30</span>
        </label><!-- end ngRepeat: day in days --><label class="item item-input item-stacked-label" ng-repeat="day in days">
          <span class="input-label ng-binding" aria-label="" id="_label-5">Almoço</span>
          <span class="data ng-binding">11:30 - 15:00</span>
        </label>
      </div>

      <!--<h3>Get in touch</h3>

      <div class="list card">
        <a href="tel:" class="item item-icon-left">
          <i class="icon ion-ios-telephone"></i> Call us
        </a>
        <a class="item item-icon-left">
          <i class="icon ion-email"></i> Send us and Email</a>
        <a href="#" class="item item-icon-left">
          <i class="icon ion-compass"></i> Find us / Get directions</a>
      </div>

      <h4>Get social with us</h4>
      <div class="list card">
        <a class="item item-icon-left">
          <i class="icon ion-social-facebook"></i> Visit us on Facebook
        </a>
        <a class="item item-icon-left">
          <i class="icon ion-social-instagram-outline"></i> Follow us on Instagram
        </a>
        <a class="item item-icon-left">
          <i class="icon ion-social-twitter"></i> Follow us on Twitter
        </a>
        <a class="item item-icon-left"><i class="icon ion-social-pinterest"></i> Follow us on Pinterest
        </a>
      </div>-->

    </div>
    
    
</div>