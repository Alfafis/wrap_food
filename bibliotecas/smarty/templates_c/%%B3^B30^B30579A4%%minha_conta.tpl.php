<?php /* Smarty version 2.6.18, created on 2018-03-13 17:56:00
         compiled from minha_conta.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'replace', 'minha_conta.tpl', 122, false),)), $this); ?>
<?php echo '
<script type="text/javascript">
function validarAlteracao(){
		
	
	if(document.contato.nome.value == ""){
		alert("Digite o Nome!");
		document.contato.nome.focus();
		return false;
	}
	
	if(document.contato.telefone.value == ""){
		alert("Digite o Telefone!");
		document.contato.telefone.focus();
		return false;
	}
	
	
	if(document.contato.email.value == ""){
		alert("Digite o E-mail!");
		document.contato.email.focus();
		return false;
	}else{
		if(validarEmail(document.contato.email.value) == false){
			alert("Informe um E-mail válido.");
			document.contato.email.focus();
			return false;
		}
	}
	
	if(document.contato.senha.value == ""){
		alert("Preecha a senha!");
		document.contato.senha.focus();
		return false;
	}
	

	document.contato.submit();
}

</script>
'; ?>
 

<div id="container_swip">
    <br /><br />
	
    <!-- Cover and profile picture -->
    <div class="profile-cover">
      <div class="row">

        <div class="col-33 text-center">
          <img class="profile-picture circle" src="img/logo.png">
        </div>

        <div class="col-66">
          <h4 class="padding-top no-margin light"><?php echo $this->_tpl_vars['dadosUsuarioSite']['nome']; ?>
</h4>
        </div>

      </div>
    </div>

    <!-- User infomation -->
    <div class="list">

      <!--<div class="item item-toggle">
        <span class="assertive">Notification</span>
        <label class="toggle toggle-assertive">
          <input value="on" type="checkbox">
          <div class="track">
            <div class="handle"></div>
          </div>
        </label>
      </div>-->
	<form name="contato" action="index.php?secao=usuarioSite&opcao=salvarAlteracao" method="post">
      
      <input type="hidden" name="onde_retorno" value="<?php echo $this->_tpl_vars['onde_retorno']; ?>
" />
      
      <label class="item item-input item-stacked-label">
        <span class="input-label assertive" aria-label="nome" id="_label-0">Nome:</span>
        <input value="<?php echo $this->_tpl_vars['dadosUsuarioSite']['nome']; ?>
" type="text" name="nome">
      </label>

      <label class="item item-input item-stacked-label">
        <span class="input-label assertive" aria-label="email" id="_label-1">E-mail:</span>
        <input value="<?php echo $this->_tpl_vars['dadosUsuarioSite']['email']; ?>
" type="text" name="email">
      </label>

      <label class="item item-input item-stacked-label">
        <span class="input-label assertive" aria-label="telefone" id="_label-2">Telefone:</span>
        <input type="text" placeholder="(xx) xxxxx-xxxx" name="telefone" maxlength="15" onkeypress="mascara(this, mascaraTelefone)" value="<?php echo $this->_tpl_vars['dadosUsuarioSite']['telefone']; ?>
">
      </label>

      <label class="item item-input item-stacked-label">
        <span class="input-label assertive" aria-label="senha" id="_label-3">Senha:</span>
        <input value="<?php echo $this->_tpl_vars['dadosUsuarioSite']['senha']; ?>
" type="password" name="senha">
      </label>
      
      
	</form>
    </div>

	<br />
    <center><h4>CARTÃO FIDELIDADE</h4></center>
    
    <div class="fidelidade">
    <center><?php echo $this->_tpl_vars['cupons']; ?>
</center>
    </div>
    
    
    
    <?php if (count ( $this->_tpl_vars['pedidoProduto'] ) > 0): ?>
    <br />
    <center><h4>MINHAS COMPRAS</h4></center>
    
    <?php $_from = $this->_tpl_vars['pedidoProduto']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['pedido']):
?>
    
    <div class="list">
    
    <p style="padding:0 2%;"><strong>Pedido</strong> <?php echo $this->_tpl_vars['pedido']['num']; ?>
</p>
    <p style="padding:0 2%;"><strong>Data Pedido:</strong> <?php echo $this->_tpl_vars['pedido']['data_cadastro']; ?>
</p>
    <p style="padding:0 2%;"><strong>Pedido Status:</strong> <?php echo $this->_tpl_vars['pedido']['descricao']; ?>
</p>
    <p style="padding:0 2%;"><strong>Valor Pedido:</strong> R$ <?php echo ((is_array($_tmp=$this->_tpl_vars['pedido']['valor'])) ? $this->_run_mod_handler('replace', true, $_tmp, ".", ",") : smarty_modifier_replace($_tmp, ".", ",")); ?>
 <?php if ($this->_tpl_vars['pedido']['desconto'] > 0): ?>- <?php echo $this->_tpl_vars['pedido']['desconto']; ?>
% (desconto) = R$ <?php echo ((is_array($_tmp=$this->_tpl_vars['pedido']['valorComDesconto'])) ? $this->_run_mod_handler('replace', true, $_tmp, ".", ",") : smarty_modifier_replace($_tmp, ".", ",")); ?>
<?php endif; ?></p>
    
    
    <?php $_from = $this->_tpl_vars['pedido']['produtos']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['produtoCarrinho']):
?>
    
    <div class="item-thumbnail-left card item item-complex item-right-editable">
    
        <div class="item-content" id="aparecer_delete">
        
            <img src="fotos/<?php echo $this->_tpl_vars['produtoCarrinho']['fotoProduto']; ?>
">
            <h2 class="ng-binding"><?php echo $this->_tpl_vars['produtoCarrinho']['nomeProduto']; ?>
</h2>
            
            <div>
            <h2 class="price pull-left assertive ng-binding" style="line-height:normal;">R$ <?php echo ((is_array($_tmp=$this->_tpl_vars['produtoCarrinho']['precoProduto'])) ? $this->_run_mod_handler('replace', true, $_tmp, ".", ",") : smarty_modifier_replace($_tmp, ".", ",")); ?>
 
            <span class="dark ng-binding">x <span id="txtqtde<?php echo $this->_tpl_vars['produtoCarrinho']['contadorItem']; ?>
"><?php echo $this->_tpl_vars['produtoCarrinho']['qtde']; ?>
</span></span><br />
            <?php if ($this->_tpl_vars['produtoCarrinho']['id_pao'] != 3): ?>
                <span style="font-size:11px;color:#333;line-height:normal;">Pão: <?php echo $this->_tpl_vars['produtoCarrinho']['nomePao']; ?>
</span><br />
                <span style="font-size:11px;color:#333;line-height:normal;">Queijo: 
                <?php if ($this->_tpl_vars['produtoCarrinho']['nomeQueijo'] != ""): ?><?php echo $this->_tpl_vars['produtoCarrinho']['nomeQueijo']; ?>
<?php endif; ?>
                <!--<?php if ($this->_tpl_vars['produtoCarrinho']['nomeSalada'] != ""): ?>, <?php echo $this->_tpl_vars['produtoCarrinho']['nomeSalada']; ?>
<?php endif; ?>
                <?php if ($this->_tpl_vars['produtoCarrinho']['nomeMolho'] != ""): ?>, <?php echo $this->_tpl_vars['produtoCarrinho']['nomeMolho']; ?>
<?php endif; ?>
                <?php if ($this->_tpl_vars['produtoCarrinho']['nomeLivre'] != ""): ?>, <?php echo $this->_tpl_vars['produtoCarrinho']['nomeLivre']; ?>
<?php endif; ?>-->
                </span>
                <?php if ($this->_tpl_vars['produtoCarrinho']['nomeSuco'] != ""): ?><br /><span style="font-size:11px;color:#333;line-height:normal;">Suco: <?php echo $this->_tpl_vars['produtoCarrinho']['nomeSuco']; ?>
 - R$ <?php echo ((is_array($_tmp=$this->_tpl_vars['produtoCarrinho']['precoSuco'])) ? $this->_run_mod_handler('replace', true, $_tmp, ".", ",") : smarty_modifier_replace($_tmp, ".", ",")); ?>
</span><?php endif; ?>
            <?php else: ?>
                <span style="font-size:11px;color:#333;line-height:normal;">
                <strong>Adicionais:</strong><br /><?php echo $this->_tpl_vars['produtoCarrinho']['adicionais']; ?>

                <br /><strong>Total Adicionais:</strong> R$ <?php echo ((is_array($_tmp=$this->_tpl_vars['produtoCarrinho']['precoSuco'])) ? $this->_run_mod_handler('replace', true, $_tmp, ".", ",") : smarty_modifier_replace($_tmp, ".", ",")); ?>
</span>
            <?php endif; ?>
            </h2>
                        
            </div>
        
        </div>
        
    
    </div>
    <?php endforeach; endif; unset($_from); ?>
    
    </div>
    <hr />
    <?php endforeach; endif; unset($_from); ?>
    <?php endif; ?>
    
    
	<div class="bar-assertive bar bar-footer" onclick="validarAlteracao()">
        <div class="row item-button-group">
    
          <div class="col button button-assertive">
            SALVAR
          </div>
        </div>
      </div>
     <br /><br /><br />
</div>
