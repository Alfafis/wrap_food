<?php /* Smarty version 2.6.18, created on 2018-04-11 13:56:59
         compiled from produto_detalhes.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'replace', 'produto_detalhes.tpl', 87, false),)), $this); ?>
<?php echo '
<script type="text/javascript">


function valida_compra_doce(){
	
	if($(".doce:checked").length == 0){
		//alert("Selecione pelo menos 1 Carboidrato.");
		alertas("Selecione pelo menos 1 recheio doce");
		return false;
	}
	
	document.compra_produto.submit();	
}


function valida_compra(){
	
	/*if($(".queijo:checked").length == 0){
		//alert("Selecione pelo menos 1 Carboidrato.");
		alertas("Selecione pelo menos 1 queijo.");
		return false;
	}*/
	
	document.compra_produto.submit();	
}


function verifica_salada(id){
	
	/*if($(".salada:checked").length > 3){
		$(id).prop("checked", false);	
	}*/
	
}

function verifica_molho(id){
	
	if($(".molho:checked").length > 2){
		$(id).prop("checked", false);	
	}
	
}

function desabilita_todos(id){
	if($(".doce:checked").length > 1){
		$(id).prop("checked", false);	
	}
	
	if($(".doce:checked").length > 0){
		$(".queijo").prop("disabled", true);
		$(".queijo").prop("checked", false);	
		$(".salada").prop("disabled", true);
		$(".salada").prop("checked", false);
		$(".molho").prop("disabled", true);
		$(".molho").prop("checked", false);
		$(".livre").prop("disabled", true);
		$(".livre").prop("checked", false);
	}else{
		$(".queijo").prop("disabled", false);
		$(".salada").prop("disabled", false);
		$(".molho").prop("disabled", false);
		$(".livre").prop("disabled", false);	
	}
}

function desabilita_doce(id){
	if($(".queijo:checked").length > 1){
		$(id).prop("checked", false);	
	}
	
	if($(".queijo:checked").length > 0){
		$(".doce").prop("checked", false);
		$(".doce").prop("disabled", true);
	}else{
		$(".doce").prop("disabled", false);
	}
}

</script>
'; ?>

<div id="container_swip" style="overflow:hidden;"> 
  <br />
  <br />
  <div class="list no-padding">
    <div class="item item-menu" ng-repeat="category in categories"> <img src="fotos/<?php echo $this->_tpl_vars['produto']['foto']; ?>
" alt=""/>
      <div class="overlay"> <span class="pull-left light ng-binding"> <span style="font-size:14px;color:#FFF;"><?php echo $this->_tpl_vars['produto']['descricao']; ?>
</span> </span> <span class="pull-right light ng-binding"> R$ <?php echo ((is_array($_tmp=$this->_tpl_vars['produto']['preco'])) ? $this->_run_mod_handler('replace', true, $_tmp, ".", ",") : smarty_modifier_replace($_tmp, ".", ",")); ?>
 </span> </div>
    </div>
  </div>
  
  <p class="text-center">
  Pão escolhido: <strong><?php echo $this->_tpl_vars['paoEscolhido']; ?>
</strong>
  <br />Tempo de preparo: <strong><?php echo $this->_tpl_vars['produto']['tempo']; ?>
</strong>
  </p>
   
  <form name="compra_produto" id="compra_produto" action="?secao=pedidoProduto&opcao=salvarPedidoProdutoSecao" method="post">
  
  <input type="hidden" name="idPao" id="idPao" value="<?php echo $this->_tpl_vars['idPao']; ?>
" />
  <input type="hidden" name="idProd" id="idProd" value="<?php echo $this->_tpl_vars['idProd']; ?>
" />
  	
            
  <div class="disable-user-behavior">
    <div class="list">
      
      <?php if ($this->_tpl_vars['produto']['categoria'] == 'Item Doce'): ?> <!-- se for wrap doce-->
      <div class="item-divider item"> Escolha o recheio doce (1 opção) </div>
		       
      <?php $_from = $this->_tpl_vars['listaDoce']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['doce']):
?>  
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" name="doce" class="doce" id="doce<?php echo $this->_tpl_vars['doce']['id']; ?>
" value="<?php echo $this->_tpl_vars['doce']['id']; ?>
" onclick="desabilita_todos('#doce<?php echo $this->_tpl_vars['doce']['id']; ?>
')">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding"><?php echo $this->_tpl_vars['doce']['nome']; ?>
</span></div>
      </label>
      <?php endforeach; endif; unset($_from); ?>
    
      <?php else: ?><!-- se não for wrap doce -->
      
      <div class="item-divider item"> Escolha o queijo (1 opção) </div>
		
      <?php $_from = $this->_tpl_vars['listaQuijo']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['queijo']):
?>  
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" name="queijo" class="queijo" id="queijo<?php echo $this->_tpl_vars['queijo']['id']; ?>
" value="<?php echo $this->_tpl_vars['queijo']['id']; ?>
" onclick="desabilita_doce('#queijo<?php echo $this->_tpl_vars['queijo']['id']; ?>
')">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding"><?php echo $this->_tpl_vars['queijo']['nome']; ?>
</span></div>
      </label>
      <?php endforeach; endif; unset($_from); ?>
      
      <div class="item-divider item"> Escolha as saladas<!-- (3 opções) --></div>
      
      <?php $_from = $this->_tpl_vars['listaSalada']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['salada']):
?>
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" class="salada" id="salada<?php echo $this->_tpl_vars['salada']['id']; ?>
" name="salada[]" value="<?php echo $this->_tpl_vars['salada']['id']; ?>
" onclick="verifica_salada('#salada<?php echo $this->_tpl_vars['salada']['id']; ?>
')">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding"><?php echo $this->_tpl_vars['salada']['nome']; ?>
</span></div>
      </label>
      <?php endforeach; endif; unset($_from); ?>
      
      <div class="item-divider item"> Escolha os molhos (2 opções) </div>
      
      <?php $_from = $this->_tpl_vars['listaMolho']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['molho']):
?>
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" class="molho" id="molho<?php echo $this->_tpl_vars['molho']['id']; ?>
" name="molho[]" value="<?php echo $this->_tpl_vars['molho']['id']; ?>
" onclick="verifica_molho('#molho<?php echo $this->_tpl_vars['molho']['id']; ?>
')">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding"><?php echo $this->_tpl_vars['molho']['nome']; ?>
</span></div>
      </label>
      <?php endforeach; endif; unset($_from); ?>
      
      
      <div class="item-divider item"> Escolha livre </div>
      
      <?php $_from = $this->_tpl_vars['listaLivre']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['livre']):
?>
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" class="livre" name="livre[]" value="<?php echo $this->_tpl_vars['livre']['id']; ?>
">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding"><?php echo $this->_tpl_vars['livre']['nome']; ?>
</span></div>
      </label>
      <?php endforeach; endif; unset($_from); ?>
      
      <?php endif; ?><!-- fim se for wrap doce -->
      
      <div class="item-divider item"> Escolha o suco </div>
      
      <?php $_from = $this->_tpl_vars['listaSuco']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['suco']):
?>
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" name="suco[]" value="<?php echo $this->_tpl_vars['suco']['id']; ?>
">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding"><?php echo $this->_tpl_vars['suco']['nome']; ?>
 - R$ <?php echo ((is_array($_tmp=$this->_tpl_vars['suco']['preco'])) ? $this->_run_mod_handler('replace', true, $_tmp, ".", ",") : smarty_modifier_replace($_tmp, ".", ",")); ?>
</span></div>
      </label>
      <?php endforeach; endif; unset($_from); ?>
      
      
      <!--<label class="item item-radio ng-valid" ng-model="item.size" ng-repeat="size in item.sizes" value="2">
      <input name="radio-group" class="ng-pristine ng-untouched ng-valid" value="2" type="radio">
      <div class="radio-content">
        <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding">Large: $12.00</span></div>
        <i class="radio-icon disable-pointer-events icon ion-checkmark"></i>
      </div>
      </label>-->
      <!-- end ngRepeat: size in item.sizes --> 
    </div>
  </div>
  </form>
  
  <?php if ($this->_tpl_vars['produto']['categoria'] == 'Item Doce'): ?>
  <div class="row item-button-group">
    <div class="col button button-assertive" onclick="valida_compra_doce()"> 
        <i class="text-2x ion-ios-cart-outline"></i> 
        <span class="text-sm">ADICIONAR AO CARRINHO</span> 
    </div>
  </div>
  <?php else: ?>
  <div class="row item-button-group">
    <div class="col button button-assertive" onclick="valida_compra()"> 
        <i class="text-2x ion-ios-cart-outline"></i> 
        <span class="text-sm">ADICIONAR AO CARRINHO</span> 
    </div>
  </div>
  <?php endif; ?>
  
  
</div>
<!-- fim cotainer sw -->