<?php /* Smarty version 2.6.18, created on 2018-04-17 14:16:12
         compiled from centro_index.tpl */ ?>
<?php if ($this->_tpl_vars['bannerPrincipal']): ?> 
<div class="swiper-container">
    <div class="swiper-wrapper">
      <?php $_from = $this->_tpl_vars['bannerPrincipal']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['banner']):
?>
      <div class="swiper-slide"><img class="full-image" src="banner/<?php echo $this->_tpl_vars['banner']['arquivo']; ?>
" alt="<?php echo $this->_tpl_vars['banner']['nome']; ?>
"/></div>
      <?php endforeach; endif; unset($_from); ?>
    </div>
    <!-- Add Pagination -->
    <div class="swiper-pagination"></div>
</div> 
<?php else: ?>
<br /><br />
<?php endif; ?>


<div id="container_swip">
    <h3 class="text-center">Escolha o Produto</h3>
	
    <?php $_from = $this->_tpl_vars['listaPao']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['pao']):
?>
    <div class="list no-padding">
      <div class="item item-menu" ng-repeat="category in categories" onclick="location.href='?secao=produtos&pao=<?php echo $this->_tpl_vars['pao']['id']; ?>
'">
        <img src="fotos/<?php echo $this->_tpl_vars['pao']['foto']; ?>
" alt=""/>

        <div class="overlay">
          <span class="pull-left light">
            <?php echo $this->_tpl_vars['pao']['nome']; ?>

          </span>
        </div>
      </div>
    </div>
    <?php endforeach; endif; unset($_from); ?>
    
    

</div><!-- fim cotainer sw -->
<?php if ($this->_tpl_vars['ped'] == 'sucesso'): ?>
<?php echo '
<script type="text/javascript">
	alertas("Pedido feito com sucesso!");
</script>
'; ?>

<?php endif; ?>


<?php echo '
<!-- Swiper JS -->
<script src="js/swiper.min.js"></script>

<!-- Initialize Swiper -->
<script>
var swiper = new Swiper(\'.swiper-container\', {
	
  loop: true,
  autoplay: {
    delay: 2000,
  },
  pagination: {
    el: \'.swiper-pagination\',
  },
});
</script>
'; ?>