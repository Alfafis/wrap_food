<?php /* Smarty version 2.6.18, created on 2018-03-13 17:54:43
         compiled from produtos.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'replace', 'produtos.tpl', 22, false),)), $this); ?>

<div id="container_swip">
	<br /><br /><br />
    
    <p class="text-center">
    Pão escolhido: <strong><?php echo $this->_tpl_vars['paoEscolhido']; ?>
</strong>
    </p>
    
    <h3 class="text-center">Escolha o Item</h3>

    <?php $_from = $this->_tpl_vars['listaProdutos']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['prod']):
?>
    <div class="list no-padding" onclick="location.href='?secao=produtos&opcao=detalhes&pao=<?php echo $this->_tpl_vars['idPao']; ?>
&prod=<?php echo $this->_tpl_vars['prod']['id']; ?>
'">
      <div class="item item-menu" ng-repeat="category in categories">
        <img src="fotos/<?php echo $this->_tpl_vars['prod']['foto']; ?>
" alt=""/>

        <div class="overlay">
          <span class="pull-left light ng-binding">
            <?php echo $this->_tpl_vars['prod']['nome']; ?>
<br />
            <span style="font-size:12px;color:#FFF;"><?php echo $this->_tpl_vars['prod']['descricao']; ?>
</span>
          </span>
          <span class="pull-right light ng-binding">
            R$ <?php echo ((is_array($_tmp=$this->_tpl_vars['prod']['preco'])) ? $this->_run_mod_handler('replace', true, $_tmp, ".", ",") : smarty_modifier_replace($_tmp, ".", ",")); ?>

          </span>
        </div>
      </div>
    </div>
    <?php endforeach; endif; unset($_from); ?>
    

</div><!-- fim cotainer sw -->
