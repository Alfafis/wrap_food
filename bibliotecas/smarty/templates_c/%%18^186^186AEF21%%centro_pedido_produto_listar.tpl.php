<?php /* Smarty version 2.6.18, created on 2018-04-11 11:33:53
         compiled from centro_pedido_produto_listar.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'replace', 'centro_pedido_produto_listar.tpl', 55, false),)), $this); ?>
<?php echo '
<style type="text/css">
.ui-dialog .ui-dialog-buttonpane{
text-align:center;	
}
.ui-dialog .ui-dialog-buttonpane button{
float:none;
text-align:center;	
}
.ui-dialog .ui-dialog-buttonpane .ui-dialog-buttonset{
float:none;
text-align:center;	
}
</style>
'; ?>

<div id="container_swip_carrinho">
	<br /><br /><br />
  	
    <form id="form_pedido_produto" method="post" action="" name="form_pedido_produto">
    
    <?php if (count ( $this->_tpl_vars['listaProdutoCarrinho'] ) > 0): ?>
    <center>ESTES SÃO OS ITENS QUE VOCÊ SELECIONOU PARA COMPRA, CONFIRA ABAIXO:</center>
    <?php else: ?>
    <center>Seu carrinho est&aacute; vazio.</center>
    <br />
    <div class="row" onClick="location.href='index.php?secao=produtos'">
        <div class="col button button-assertive"> 
            <span class="text-sm">CONTINUAR COMPRANDO</span> 
        </div>
    </div>
    <?php endif; ?>
    
    <?php if (count ( $this->_tpl_vars['listaProdutoCarrinho'] ) > 0): ?>
    <div class="list">
    
    <?php $_from = $this->_tpl_vars['listaProdutoCarrinho']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['produtoCarrinho']):
?>
    
    <input type='hidden'name='listaProdutoQtd[]' id="qtde<?php echo $this->_tpl_vars['produtoCarrinho']['contadorItem']; ?>
" value='<?php echo $this->_tpl_vars['produtoCarrinho']['qtde']; ?>
'>
    <input type='hidden' name='listaProdutoContador[]' id="cont<?php echo $this->_tpl_vars['produtoCarrinho']['contadorItem']; ?>
" value='<?php echo $this->_tpl_vars['produtoCarrinho']['contadorItem']; ?>
'>
    
    <div class="item-thumbnail-left card item item-complex item-right-editable">
    
        <div class="item-content" id="aparecer_delete">
        	
            
            <img src="fotos/<?php echo $this->_tpl_vars['produtoCarrinho']['fotoProduto']; ?>
">
            <h2 class="ng-binding">
            <?php echo $this->_tpl_vars['produtoCarrinho']['nomeProduto']; ?>

            <a style="font-size:30px;position:absolute;right:0;z-index:30;top:0;" onclick="removeItem(<?php echo $this->_tpl_vars['produtoCarrinho']['contadorItem']; ?>
)">
            	<i class="icon ion-trash-a padding"></i>
            </a>
            </h2>
            
            <div>
            <h2 class="price pull-left assertive ng-binding" style="line-height:normal;max-width:55%">R$ <?php echo ((is_array($_tmp=$this->_tpl_vars['produtoCarrinho']['precoProduto'])) ? $this->_run_mod_handler('replace', true, $_tmp, ".", ",") : smarty_modifier_replace($_tmp, ".", ",")); ?>
 
            <span class="dark ng-binding">x <span id="txtqtde<?php echo $this->_tpl_vars['produtoCarrinho']['contadorItem']; ?>
"><?php echo $this->_tpl_vars['produtoCarrinho']['qtde']; ?>
</span></span><br />
            <?php if ($this->_tpl_vars['produtoCarrinho']['idPao'] != 3): ?>
                <span style="font-size:11px;color:#333;line-height:normal;">Pão: <?php echo $this->_tpl_vars['produtoCarrinho']['nomePao']; ?>
</span><br />
                
                <span style="font-size:11px;color:#333;line-height:normal;">
                <?php if ($this->_tpl_vars['produtoCarrinho']['nomeDoce'] == ""): ?>
                Queijo: <?php echo $this->_tpl_vars['produtoCarrinho']['nomeQueijo']; ?>

                <?php else: ?>
                Doce: <?php echo $this->_tpl_vars['produtoCarrinho']['nomeDoce']; ?>

                <?php endif; ?>
                </span>
                <?php if ($this->_tpl_vars['produtoCarrinho']['nomeSuco'] != ""): ?><br /><span style="font-size:11px;color:#333;line-height:normal;">Suco: <?php echo $this->_tpl_vars['produtoCarrinho']['nomeSuco']; ?>
 - R$ <?php echo ((is_array($_tmp=$this->_tpl_vars['produtoCarrinho']['precoSuco'])) ? $this->_run_mod_handler('replace', true, $_tmp, ".", ",") : smarty_modifier_replace($_tmp, ".", ",")); ?>
</span><?php endif; ?>
            <?php else: ?>
                <span style="font-size:11px;color:#333;line-height:normal;">
                <strong>Adicionais:</strong><br /><?php echo $this->_tpl_vars['produtoCarrinho']['nomeSuco']; ?>

                <br /><strong>Total Adicionais:</strong> R$ <?php echo ((is_array($_tmp=$this->_tpl_vars['produtoCarrinho']['precoSuco'])) ? $this->_run_mod_handler('replace', true, $_tmp, ".", ",") : smarty_modifier_replace($_tmp, ".", ",")); ?>
</span>
            <?php endif; ?>
            </h2>
            
            <div class="pull-right button button-assertive button-card button-qty" style="margin-top:17px;" onclick="aumentar_quantidade(<?php echo $this->_tpl_vars['produtoCarrinho']['contadorItem']; ?>
)"><i class="ion-plus"></i></div>
            <div class="pull-right button button-assertive button-card button-qty" style="margin-top:17px;" onclick="diminuir_quantidade(<?php echo $this->_tpl_vars['produtoCarrinho']['contadorItem']; ?>
)"><i class="ion-minus"></i></div>
            
            </div>
        
        </div>
        
        <div class="item-options">
            <div class="stable-bg button" onclick="removeItem(<?php echo $this->_tpl_vars['produtoCarrinho']['contadorItem']; ?>
)">
            <i class="icon ion-trash-a padding"></i>
            </div>
        </div>
    
    </div>
    <?php endforeach; endif; unset($_from); ?>
    
    <?php if ($this->_tpl_vars['desconto_cupons'] == 'sim'): ?>
    <div class="item-thumbnail-left card item item-complex item-right-editable">
    
        <div class="item-content" id="aparecer_delete">
        
            <img src="img/logo.png">
            <h2 class="ng-binding">Desconto de <?php echo $this->_tpl_vars['desconto']; ?>
%</h2>
            
            <div>
            <h2 class="price pull-left assertive ng-binding" style="line-height:normal;">
            	<span style="font-size:11px;color:#333;line-height:normal;">Desconto por ter completado <?php echo $this->_tpl_vars['ticket']; ?>
 cupons.</span><br />
            	R$ <?php echo ((is_array($_tmp=$this->_tpl_vars['descontoTotal'])) ? $this->_run_mod_handler('replace', true, $_tmp, ".", ",") : smarty_modifier_replace($_tmp, ".", ",")); ?>
 
            </h2>
                        
            </div>
        
        </div>
        
    
    </div>
    <?php endif; ?>
    
    
    <label class="item item-input item-stacked-label">
      <span class="input-label" aria-label="email" id="_label-14">Agendar horário para buscar:</span>
      <input type="text" name="horario" id="horario" placeholder="hh:mm" value="<?php echo $this->_tpl_vars['horario']; ?>
" maxlength="5" onkeypress="mascara(this, mascaraHora)">
    </label>
    
    <br /><br /><br />
    
    </div>
    <?php endif; ?>
    </form>
  
  <div class="clearfix"></div>    
    
  <ion-footer-bar class="bar-assertive bar bar-footer" onclick="finalizaCarrinho()">
    <div class="button">COMPRAR</div>
    <div class="title" style="left: 111px; right: 111px;"></div>
    <div class="button text-2x ng-binding">
    R$ <?php if ($this->_tpl_vars['valorTotalPedidoProduto']): ?><?php echo ((is_array($_tmp=$this->_tpl_vars['valorTotalPedidoProduto'])) ? $this->_run_mod_handler('replace', true, $_tmp, ".", ",") : smarty_modifier_replace($_tmp, ".", ",")); ?>
<?php else: ?>0,00<?php endif; ?>
    </div>
  </ion-footer-bar>
  <div class="clearfix"></div>
</div><!-- fim cotainer sw -->
<!--
<?php echo '
<script type="text/javascript">
$(function () {
    
  $(".item-content").swipe( {
	//Generic swipe handler for all directions
	swipe:function(event, direction, distance, duration, fingerCount, fingerData) {
		if (direction == \'left\') abrir_delete();
		if (direction == \'right\') fechar_delete();  
	},
	//Default is 75px, set to 0 for demo so any distance triggers swipe
	 threshold:0
  });
  
});
</script>
'; ?>
-->