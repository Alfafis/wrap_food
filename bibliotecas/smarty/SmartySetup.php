<?php

// carrega a biblioteca do Smarty
require('./bibliotecas/smarty/libs/Smarty.class.php');

// O arquivo setup.php � uma boa forma para carregar
// arquivos de bibliotecas da aplica��o exigida, e voc� pode fazer
// isso corretamente aqui. Um exemplo:
// require('guestbook/guestbook.lib.php');
class SmartySetup extends Smarty {

    function SmartySetup() {
        
		// Construtor da Classe. Estes automaticamente s�o definidos a cada nova inst�ncia.
        $this->Smarty();

		$this->template_dir = './templates/';
        $this->compile_dir = './bibliotecas/smarty/templates_c';
        $this->config_dir = './bibliotecas/smarty/configs/';
        $this->cache_dir = './bibliotecas/smarty/cache/';

    }

}

?>