<?php


 function monta_produto($idPao,$idProd,$queijo,$salada,$molho,$livre,$suco,$doce) {     
 	include('./conf/conexao.php');
	
	$dados = "";
 	/**********pegando o preco do pao***********/
	$sqlP = 'select nome, foto from pao where id = '.$idPao; 
	$resultadoQueryP = $db->Execute($sqlP);
	$nomePao = "";
	if($resultadoQueryP->RecordCount() > 0){
		$nomePao = $resultadoQueryP->fields["nome"];
	};
	$dados["nomePao"] = $nomePao;
		
	/**********pegando o preco do produto***********/
	$sqlPr = 'select nome, preco, foto from produtos where id = '.$idProd; 
	$resultadoQueryPr = $db->Execute($sqlPr);
	$totalResultadosEncontrados = $resultadoQueryPr->RecordCount();
	$nomeProduto = "";
	$precoProduto = 0;
	$fotoProduto = "";
	if($totalResultadosEncontrados > 0){
		$nomeProduto = $resultadoQueryPr->fields["nome"];
		$precoProduto = $resultadoQueryPr->fields["preco"];
		$fotoProduto = $resultadoQueryPr->fields["foto"];
	};	
	$dados["nomeProduto"] = $nomeProduto;
	$dados["precoProduto"] = number_format($precoProduto,2);
	$dados["fotoProduto"] = $fotoProduto;
	
	if($queijo != ""){
	/**********pegando o nome do queijo***********/
	$sqlP = 'select nome from queijos where id = '.$queijo; 
	$resultadoQueryP = $db->Execute($sqlP);
	$nomeQueijo = "";
	if($resultadoQueryP->RecordCount() > 0){
		$nomeQueijo = $resultadoQueryP->fields["nome"];
	};
	}
	$dados["nomeQueijo"] = $nomeQueijo;
	
	if($doce != ""){
	/**********pegando o nome do doce***********/
	$sqlD = 'select nome from doces where id = '.$doce; 
	$resultadoQueryD = $db->Execute($sqlD);
	$nomeDoce = "";
	if($resultadoQueryD->RecordCount() > 0){
		$nomeDoce = $resultadoQueryD->fields["nome"];
	};
	}
	$dados["nomeDoce"] = $nomeDoce;
					
	/**********verificando se tem salada********/
	$sqlS = 'select nome from saladas where id in ('.$salada.')'; 
	$resultadoQueryS = $db->Execute($sqlS);
	
	$nomeSalada = "";
	$cont = 0;
	while(!$resultadoQueryS->EOF){
		
		if($cont == 0){
			$nomeSalada .= $resultadoQueryS->fields["nome"];
		}else{
			$nomeSalada .= ", ".$resultadoQueryS->fields["nome"];	
		}
		
		$cont++;
		$resultadoQueryS->MoveNext();
	}
	$dados["nomeSalada"] = $nomeSalada;
				
	/**********verificando se tem molho********/
	$sqlS = 'select nome from molhos where id in ('.$molho.')'; 
	$resultadoQueryS = $db->Execute($sqlS);
	
	$nomeMolho = "";
	$cont = 0;
	while(!$resultadoQueryS->EOF){
		
		if($cont == 0){
			$nomeMolho .= $resultadoQueryS->fields["nome"];
		}else{
			$nomeMolho .= ", ".$resultadoQueryS->fields["nome"];	
		}
		
		$cont++;
		$resultadoQueryS->MoveNext();
	}
	$dados["nomeMolho"] = $nomeMolho;
	
	/**********verificando se tem molho********/
	$sqlS = 'select nome from livres where id in ('.$livre.')'; 
	$resultadoQueryS = $db->Execute($sqlS);
	
	$nomeLivre = "";
	$cont = 0;
	while(!$resultadoQueryS->EOF){
		
		if($cont == 0){
			$nomeLivre .= $resultadoQueryS->fields["nome"];
		}else{
			$nomeLivre .= ", ".$resultadoQueryS->fields["nome"];	
		}
		
		$cont++;
		$resultadoQueryS->MoveNext();
	}
	$dados["nomeLivre"] = $nomeLivre;
	
	/**********verificando se tem suco********/
	$sqlS = 'select nome, preco as preco from produtos where id in ('.$suco.')'; 
	$resultadoQueryS = $db->Execute($sqlS);
	
	$nomeSuco = "";
	$precoSuco = 0;
	$cont = 0;
	while(!$resultadoQueryS->EOF){
		
		if($cont == 0){
			$nomeSuco .= $resultadoQueryS->fields["nome"];
		}else{
			$nomeSuco .= ", ".$resultadoQueryS->fields["nome"];	
		}
		$precoSuco = $precoSuco + $resultadoQueryS->fields["preco"];
		
		$cont++;
		$resultadoQueryS->MoveNext();
	}
	$dados["nomeSuco"] = $nomeSuco;
	$dados["precoSuco"] = number_format($precoSuco,2);
	
	//valor total compra
	$dados["valorTotal"] = $precoProduto+$precoSuco;
	
    return $dados;
	
  }
  
  
  
  function monta_salada($idPao,$idProd,$selecionados) {     
 	include('./conf/conexao.php');
	
	$dados = "";
 	/**********pegando o preco do pao***********/
	$sqlP = 'select nome, foto from pao where id = '.$idPao; 
	$resultadoQueryP = $db->Execute($sqlP);
	$nomePao = "";
	if($resultadoQueryP->RecordCount() > 0){
		$nomePao = $resultadoQueryP->fields["nome"];
	};
	$dados["nomePao"] = $nomePao;
		
	/**********pegando o preco do produto***********/
	$sqlPr = 'select nome, preco, foto from produtos where id = '.$idProd; 
	$resultadoQueryPr = $db->Execute($sqlPr);
	$totalResultadosEncontrados = $resultadoQueryPr->RecordCount();
	$nomeProduto = "";
	$precoProduto = 0;
	$fotoProduto = "";
	if($totalResultadosEncontrados > 0){
		$nomeProduto = $resultadoQueryPr->fields["nome"];
		$precoProduto = $resultadoQueryPr->fields["preco"];
		$fotoProduto = $resultadoQueryPr->fields["foto"];
	};	
	$dados["nomeProduto"] = $nomeProduto;
	$dados["precoProduto"] = number_format($precoProduto,2);
	$dados["fotoProduto"] = $fotoProduto;
	
	
	if($selecionados["adicionais"] != ""){
		$adicionais = explode(",",$selecionados["adicionais"]);
		foreach($adicionais as $item){
			$aux .= $item.", ";	
		}
		$adicionais = substr($aux, 0, strlen($aux) - 2);
	}
	$dados["nomeSuco"] = $adicionais;
	$precoSuco = $selecionados["num_adicionais"]*2;
	$dados["precoSuco"] = number_format($precoSuco,2);
	
	//valor total compra
	$dados["valorTotal"] = $precoProduto+$precoSuco;
	
    return $dados;
	
  }
  
  
  
  
  
  
  
  
  


?>