<?php
/*
function formatarTexto($texto, $alternativo = false){

    $retorno = isSet($texto) ? $texto : $alternativo;
    $retorno = htmlspecialchars($retorno, ENT_QUOTES);
        
    return $retorno;
}
*/

 function humanDate($sqlDate) {              //Retorna data no formato D/M/A
    $data = explode('-', $sqlDate);
    return implode('/', array_reverse($data));
  }


   function sqlDate($humanDate) {              //Retorna data no formato A-M-D
    $data = explode('/', $humanDate);
    return implode('-', array_reverse($data));
  }


	function sqlDate2($humanDate) {              //Retorna data no formato A-M-D
    $data = substr($humanDate,0,10);
	$hora = substr($humanDate,11,19);
	$data = explode('/', $data);
	$data = implode('-', array_reverse($data));
	$data = $data.' '.$hora;
    return $data;
  }
  
function pegaMes($mes){
	switch ($mes) {
        case "01":    $mes = "Janeiro";     break;
        case "02":    $mes = "Fevereiro";   break;
        case "03":    $mes = "Mar�o";       break;
        case "04":    $mes = "Abril";       break;
        case "05":    $mes = "Maio";        break;
        case "06":    $mes = "Junho";       break;
        case "07":    $mes = "Julho";       break;
        case "08":    $mes = "Agosto";      break;
        case "09":    $mes = "Setembro";    break;
        case "10":    $mes = "Outubro";     break;
        case "11":    $mes = "Novembro";    break;
        case "12":    $mes = "Dezembro";    break; 
 	}	
	
	return $mes;
}


function converteData($data) {

	if($data != ''){

		$data = explode("/", $data);
		$data = $data[2].'-'.pegaMes($data[1]).'-'.$data[0];
	}

	return $data;
}



function dataAv($data) {

	if($data != ''){

		$data = explode("/", $data);
		$data = "em ".$data[0].' de '.pegaMes($data[1]).' de '.$data[2];
	}

	return $data;
}


// Troca apenas os caracteres ', ", <, > e &

function removeCaracterSpecial($string){

    $from = '��������������������������';
    $to   = 'AAAAEEIOOOUUCaaaaeeiooouuc';

    $string = strtr($string, $from, $to); //subtitui os acentos

    $patterns[0] = '/ /'; 
    
	$replacements[0] = "";
    
	$string = preg_replace($patterns,$replacements,$string); //remover espa�o em branco
    
	$string = preg_replace("/[_\W]/", "", $string); // remover os caracteres especiais

    return $string;

}

function formatarNomeArquivo($nomeArquivo, $substituto = ''){

    $nomeArquivo = preg_replace('/[^A-Za-z0-9_.]/', $substituto, $nomeArquivo);

    return $nomeArquivo;

}

function formatarTexto($texto){

    $testo_formatado = str_replace("<br />", "", $texto);

    $testo_formatado = htmlspecialchars($testo_formatado);

    $testo_formatado = str_replace("'", "&#039;", $testo_formatado);		

    $testo_formatado = nl2br($testo_formatado);

    return $testo_formatado;

}

function redimensionarImagem($filename, $newW, $newH, $imagemFundo = "") {


    preg_match_all('/[A-Za-z0-9._]+\.(jpg|jpeg|gif|png)/i', $filename, $matches);
    $matches = $matches[1][0];

	// Resample
    switch ($matches) {

      default: return false; break; //N�o consegui redimensionar, usando tamanho normal
      
      case "jpg": //caem no mesmo caso...
      case "JPG": //caem no mesmo caso...
      case "jpeg":
        // Get new dimensions
        list($width, $height) = getimagesize($filename);
        $new_width  = $newW;
        $new_height = $newH;
        
        $image_p = imagecreatetruecolor($new_width, $new_height);
        $image = imagecreatefromjpeg($filename);

        imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);

		imagejpeg($image_p, $filename, 100);

		if($imagemFundo != ""){

			$idImagemFundo = ImageCreateFromJpeg($imagemFundo);
			list($larguraImagemFundo, $alturaImagemFundo) = getimagesize($imagemFundo);

			$image = imagecreatefromjpeg($filename);

			$dest_x = ( $larguraImagemFundo / 2 ) - ( $new_width / 2 );
			$dest_y = ( $alturaImagemFundo / 2 ) - ( $new_height / 2);

			ImageCopyMerge($idImagemFundo, $image, $dest_x, $dest_y, 0, 0, $new_width, $new_height, 100);

			imagejpeg($idImagemFundo, $filename, 100);
		}


      break;
      
      case "gif":
        // Get new dimensions
        list($width, $height) = getimagesize($filename);
        $new_width  = $newW;
        $new_height = $newH;
        
        $image_p = imagecreatetruecolor($new_width, $new_height);
        $image = imagecreatefromgif($filename);
        imagecolortransparent($image,imagecolortransparent($image));
        imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
        imagegif($image_p, $filename, 100);
      break;
      
      case "png":
        // Get new dimensions
        list($width, $height) = getimagesize($filename);
        $new_width  = $newW;
        $new_height = $newH;
        
        $image_p = imagecreatetruecolor($new_width, $new_height);
        $image = imagecreatefrompng($filename);
        $trans_color = imagecolortransparent($image);
        imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
        imagepng($image_p, $filename, 100);
      break;
    }
    
    return true; //Consegui redimensionar.
}

function setIf($variable, $if_false = '') {
    return (isSet($variable))? $variable: $if_false;
}

function converteValor($price) {
    $price = preg_replace("/[^0-9\.]/", "", str_replace(',','.',$price));
    if (substr($price,-3,1)=='.') {
        $sents = '.'.substr($price,-2);
        $price = substr($price,0,strlen($price)-3);
    } elseif (substr($price,-2,1)=='.') {
        $sents = '.'.substr($price,-1);
        $price = substr($price,0,strlen($price)-2);
    } else {
        $sents = '.00';
    }
    $price = preg_replace("/[^0-9]/", "", $price);
    return number_format($price.$sents,2,'.','');
}

function verificar_email($email){
	$mail_correcto = 0;
	//verifico umas coisas
	if ((strlen($email) >= 6) && (substr_count($email,"@") == 1) && (substr($email,0,1) != "@") && (substr($email,strlen($email)-1,1) != "@")){
		if ((!strstr($email,"'")) && (!strstr($email,"\"")) && (!strstr($email,"\\")) && (!strstr($email,"\$")) && (!strstr($email," "))) {
			//vejo se tem caracter .
			if (substr_count($email,".")>= 1){
				//obtenho a termina��o do dominio
				$term_dom = substr(strrchr ($email, '.'),1);
				//verifico que a termina��o do dominio seja correcta
				if (strlen($term_dom)>1 && strlen($term_dom)<5 && (!strstr($term_dom,"@")) ){
					//verifico que o de antes do dominio seja correcto
					$antes_dom = substr($email,0,strlen($email) - strlen($term_dom) - 1);
					$caracter_ult = substr($antes_dom,strlen($antes_dom)-1,1);
					if ($caracter_ult != "@" && $caracter_ult != "."){
					   $mail_correcto = 1;
					}
				}
			}
		}
	}

	if ($mail_correcto)
	   return 1;
	else
	   return 0;

}
 
if(!function_exists('str_ireplace')){
  function str_ireplace($search,$replace,$subject){
    $token = chr(1);
    $haystack = strtolower($subject);
    $needle = strtolower($search);
    while (($pos=strpos($haystack,$needle))!==FALSE){
      $subject = substr_replace($subject,$token,$pos,strlen($search));
      $haystack = substr_replace($haystack,$token,$pos,strlen($search));
    }
    $subject = str_replace($token,$replace,$subject);
    return $subject;
  }
}

function delAcentos($frase) {

	$frase = strtolower( ereg_replace("[^a-zA-Z0-9-]", " ", strtr(trim($frase), "���������������������������� ","aaaaeeiooouuncAAAAEEIOOOUUNC-")) );
	
	return $frase;
}

function delAcentosUTF8 ($frase) {

	$frase = strtolower( ereg_replace("[^a-zA-Z0-9-]", " ", strtr(utf8_decode(trim($frase)), "���������������������������� ","aaaaeeiooouuncAAAAEEIOOOUUNC-")) );

	return utf8_encode($frase);
}


function calcular_frete_($cep_origem,
	$cep_destino,
	$peso,
	$valor,
	$tipo_do_frete,
	$altura = 6,
	$largura = 20,
	$comprimento = 20){


	$url = "http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx?";
	$url .= "nCdEmpresa=";
	$url .= "&sDsSenha=";
	$url .= "&sCepOrigem=" . $cep_origem;
	$url .= "&sCepDestino=" . $cep_destino;
	$url .= "&nVlPeso=" . $peso;
	$url .= "&nVlLargura=" . $largura;
	$url .= "&nVlAltura=" . $altura;
	$url .= "&nCdFormato=1";
	$url .= "&nVlComprimento=" . $comprimento;
	$url .= "&sCdMaoProria=n";
	$url .= "&nVlValorDeclarado=" . $valor;
	$url .= "&sCdAvisoRecebimento=n";
	$url .= "&nCdServico=" . $tipo_do_frete;
	$url .= "&nVlDiametro=0";
	$url .= "&StrRetorno=xml";

	//Sedex: 40010
	//Pac: 41106

	$xml = simplexml_load_file($url);

	return $xml->cServico;

}


function calcular_frete($cep_origem,
	$cep_destino,
	$peso,
	$valor,
	$tipo_do_frete,
	$altura = 2,
	$largura = 11,
	$comprimento = 16){
		
	 $data['nCdEmpresa'] = '';
	 $data['sDsSenha'] = '';
	 $data['sCepOrigem'] = $cep_origem;
	 $data['sCepDestino'] = $cep_destino;
	 $data['nVlPeso'] = $peso;
	 $data['nCdFormato'] = '1';
	 $data['nVlComprimento'] = $comprimento;
	 $data['nVlAltura'] = $altura;
	 $data['nVlLargura'] = $largura;
	 $data['nVlDiametro'] = '0';
	 $data['sCdMaoPropria'] = 'n';
	 $data['nVlValorDeclarado'] = $valor;
	 $data['sCdAvisoRecebimento'] = 'n';
	 $data['StrRetorno'] = 'xml';
	 //$data['nCdServico'] = '40010';
	 $data['nCdServico'] = $tipo_do_frete;
	 $data = http_build_query($data);
	
	 $url = 'http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx';
	
	 $curl = curl_init($url . '?' . $data);
	 curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	
	 $result = curl_exec($curl);
	 $xml = simplexml_load_string($result);

	return $xml->cServico;

}

?>