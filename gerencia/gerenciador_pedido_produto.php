<?php

$opcao = $_GET['opcao'];
$smarty->assign('opcao', $opcao);

$smarty->assign('retorno', $_GET["retorno"]);

///calculo de quantos cupons válidos ele tem
$desconto_cupons = "nao";
if($_SESSION['usuarioSite'][0] != ""){
	
	$sqlPed = 'select pp.*
	from pedido pp
	where pp.utilizado = 0 and pp.id_usuario_site = '.$_SESSION['usuarioSite'][0];
	$resultadoQueryPed=$db->Execute($sqlPed);
	$pedidos_aprovados = 0;
	while(!$resultadoQueryPed->EOF){

		if($resultadoQueryPed->fields["id_status_pedido"] == 6 || $resultadoQueryPed->fields["id_status_pedido"] == 8 || $resultadoQueryPed->fields["id_status_pedido"] == 1){
			$pedidos_aprovados++;
		}
					
		$resultadoQueryPed->MoveNext();

	}
	
	if($pedidos_aprovados >= $_SESSION["ticket"]){
		$desconto_cupons = "sim";
	}
	
}

$smarty->assign('desconto_cupons', $desconto_cupons);


if($_REQUEST["horario"] != ""){
	$_SESSION["horario"] = $_REQUEST["horario"];	
}
$smarty->assign('horario', $_SESSION["horario"]);


switch($opcao){

/********************************** NOVA OP��O **************************************************************/

    case 'listarPedidoProdutoSecao' :

		$listaProdutoCarrinho = $_SESSION['listaProdutoCarrinhoSecao'];
		
		//print_r($listaProdutoCarrinho);
				
		$valorTotalPedidoProduto = 0;

		$qtdeProdutosPedido = 0;

		$qtdeTotalProdutos = 0;

		$listaIdsProdutosUtilizados = 0;
		
		//print_r($_SESSION['listaProdutoCarrinhoSecao']);
		//verificando se tem alguma coisa no carrinho
		if(count($_SESSION['listaProdutoCarrinhoSecao']) > 0){
			//verificando se j� atingiu o preco de revenda ou � revendedor logado
				
				$listaProdutoCarrinho = array();
	
				$cont = 0;
				$contadorItem = 0;
				$fora_estoque = 1;
				
	
				foreach ($_SESSION['listaProdutoCarrinhoSecao'] as $item) {
	
					$produtoSelecionado = array();
					
					$contadorItem = $contadorItem+1;
					
					//parametros função de retorno de dados
					if($item["idPao"] != 3){
						//idPao,idProd,queijo,salada,molho,livre,suco
						$dadosProduto = monta_produto($item["idPao"],$item["idProd"],$item["queijo"],$item["salada"],$item["molho"],$item["livre"],$item["suco"],$item["doce"]);
					}else{
						//idPao,idProd,todas seleções
						$dadosProduto = monta_salada($item["idPao"],$item["idProd"],$item["dados_selecionados"]);
					}
					
					
					$produtoSelecionado["idPao"] = $item["idPao"];//0 - id produto
					$produtoSelecionado["nomePao"] = utf8_encode($dadosProduto["nomePao"]);//1 - nome produto
					
					$produtoSelecionado["idProd"] = $item["idProd"];//2 - preco
					$produtoSelecionado["nomeProduto"] = $dadosProduto["nomeProduto"];//3 - preco formatado
					$produtoSelecionado["precoProduto"] = $dadosProduto["precoProduto"];//3 - preco formatado
					$produtoSelecionado["fotoProduto"] = $dadosProduto["fotoProduto"];//4 - foto
					
					$produtoSelecionado["queijo"] = $item["queijo"];//5 - peso
					$produtoSelecionado["nomeQueijo"] = $dadosProduto["nomeQueijo"];//6 - id tamanho (qtde_produto)
					
					$produtoSelecionado["doce"] = $item["doce"];//5 - peso
					$produtoSelecionado["nomeDoce"] = $dadosProduto["nomeDoce"];//6 - id tamanho (qtde_produto)
					
					$produtoSelecionado["salada"] = $item["salada"];//5 - peso
					$produtoSelecionado["nomeSalada"] = $dadosProduto["nomeSalada"];//6 - id tamanho (qtde_produto)
					
					$produtoSelecionado["molho"] = $item["molho"];//5 - peso
					$produtoSelecionado["nomeMolho"] = $dadosProduto["nomeMolho"];//6 - id tamanho (qtde_produto)
					
					$produtoSelecionado["livre"] = $item["livre"];//5 - peso
					$produtoSelecionado["nomeLivre"] = $dadosProduto["nomeLivre"];//6 - id tamanho (qtde_produto)
					
					$produtoSelecionado["suco"] = $item["suco"];//5 - peso
					$produtoSelecionado["nomeSuco"] = $dadosProduto["nomeSuco"];//6 - id tamanho (qtde_produto)
					$produtoSelecionado["precoSuco"] = $dadosProduto["precoSuco"];//6 - id tamanho (qtde_produto)
					
					$produtoSelecionado["qtde"] = $item["qtde"];//7 - qtde selecionada
					$produtoSelecionado["contadorItem"] = $contadorItem;//8 - contador produtos carrinho
					
					$produtoSelecionado["valorTotal"] = $dadosProduto["valorTotal"]*$item["qtde"];
					
					if($item["idPao"] != 3){
						$produtoSelecionado["tipo"] = "pao";
						$produtoSelecionado["dados_selecionados"] = "";
					}else{
						$produtoSelecionado["tipo"] = "salada";
						$produtoSelecionado["dados_selecionados"] = $item["dados_selecionados"];
					}
					
					$valorTotalPedidoProduto += $produtoSelecionado["valorTotal"];	
					
					
					$listaProdutoCarrinho[] = $produtoSelecionado;
	
					$cont++;
				}
	
				$_SESSION['listaProdutoCarrinhoSecao'] = $listaProdutoCarrinho;
			
		}//fim if se tem alguma coisa no carrinho
		
		$_SESSION['valorTotalPedidoProdutoSecao'] = $valorTotalPedidoProduto;
		
		if($desconto_cupons == "sim"){
			$descontoTotal = $valorTotalPedidoProduto*($_SESSION["desconto"]/100);
			$smarty->assign('descontoTotal', number_format($descontoTotal,2));	
			$valorTotalPedidoProduto = $valorTotalPedidoProduto-$descontoTotal;
		}
		
		$smarty->assign('valorTotalPedidoProduto', number_format($valorTotalPedidoProduto,2));
		$smarty->assign('listaProdutoCarrinho', $listaProdutoCarrinho);
	
		
		$smarty->assign('nomePagina', 'MEU CARRINHO');


		$smarty->assign('templateCentro', 'centro_pedido_produto_listar.tpl');
 
		break;

/********************************** NOVA OP��O ************************************************************/

    case 'salvarPedidoProdutoSecao' :
		
		
		$idPao = $_POST['idPao'];
		$idProd = $_POST['idProd'];
		
		
		if($idPao != 3){//se for pão
			
			$doce = $_POST['doce'];
			
			$queijo = $_POST['queijo'];
			
			if(count($_REQUEST["salada"])>0){
				foreach($_REQUEST["salada"] as $item){
					$aux .= $item.",";	
				}
				$salada = substr($aux, 0, strlen($aux) - 1);
			}else{
				$salada = 0;
			}
	
			if(count($_REQUEST["molho"])>0){
				foreach($_REQUEST["molho"] as $item){
					$aux2 .= $item.",";	
				}
				$molho = substr($aux2, 0, strlen($aux2) - 1);
			}else{
				$molho = 0;
			}
	
			if(count($_REQUEST["livre"])>0){
				foreach($_REQUEST["livre"] as $item){
					$aux3 .= $item.",";	
				}
				$livre = substr($aux3, 0, strlen($aux3) - 1);
			}else{
				$livre = 0;
			}
	
			if(count($_REQUEST["suco"])>0){
				foreach($_REQUEST["suco"] as $item){
					$aux4 .= $item.",";	
				}
				$suco = substr($aux4, 0, strlen($aux4) - 1);
			}else{
				$suco = 0;
			}
			
		}else{//se for salada
			$dados_pedido_salada = "";
			
			$aux = "";
			if(count($_REQUEST["carboidratos"])>0){
				foreach($_REQUEST["carboidratos"] as $item){
					$aux .= utf8_decode($item).",";	
				}
				$carboidratos = substr($aux, 0, strlen($aux) - 1);
				$dados_pedido_salada["carboidratos"] = $carboidratos;
			}else{
				$dados_pedido_salada["carboidratos"] = $_REQUEST["carboidratos"];
			}
			
			$aux = "";
			if(count($_REQUEST["proteinas"])>0){
				foreach($_REQUEST["proteinas"] as $item){
					$aux .= utf8_decode($item).",";	
				}
				$proteinas = substr($aux, 0, strlen($aux) - 1);
				$dados_pedido_salada["proteinas"] = $proteinas;
			}else{
				$dados_pedido_salada["proteinas"] = $_REQUEST["proteinas"];
			}
			
			$aux = "";
			if(count($_REQUEST["legumes"])>0){
				foreach($_REQUEST["legumes"] as $item){
					$aux .= utf8_decode($item).",";	
				}
				$legumes = substr($aux, 0, strlen($aux) - 1);
				$dados_pedido_salada["legumes"] = $legumes;
			}else{
				$dados_pedido_salada["legumes"] = $_REQUEST["legumes"];
			}
			
			$aux = "";
			if(count($_REQUEST["verduras"])>0){
				foreach($_REQUEST["verduras"] as $item){
					$aux .= utf8_decode($item).",";	
				}
				$verduras = substr($aux, 0, strlen($aux) - 1);
				$dados_pedido_salada["verduras"] = $verduras;
			}else{
				$dados_pedido_salada["verduras"] = $_REQUEST["verduras"];
			}
			
			$aux = "";
			if(count($_REQUEST["sementes"])>0){
				foreach($_REQUEST["sementes"] as $item){
					$aux .= utf8_decode($item).",";	
				}
				$sementes = substr($aux, 0, strlen($aux) - 1);
				$dados_pedido_salada["sementes"] = $sementes;
			}else{
				$dados_pedido_salada["sementes"] = $_REQUEST["sementes"];
			}
			
			$aux = "";
			if(count($_REQUEST["frutas"])>0){
				foreach($_REQUEST["frutas"] as $item){
					$aux .= utf8_decode($item).",";	
				}
				$frutas = substr($aux, 0, strlen($aux) - 1);
				$dados_pedido_salada["frutas"] = $frutas;
			}else{
				$dados_pedido_salada["frutas"] = $_REQUEST["frutas"];
			}
			
			$aux = "";
			if(count($_REQUEST["molhos"])>0){
				foreach($_REQUEST["molhos"] as $item){
					$aux .= utf8_decode($item).",";	
				}
				$molhos = substr($aux, 0, strlen($aux) - 1);
				$dados_pedido_salada["molhos"] = $molhos;
			}else{
				$dados_pedido_salada["molhos"] = $_REQUEST["molhos"];
			}
			
			$aux = "";
			if(count($_REQUEST["livres"])>0){
				foreach($_REQUEST["livres"] as $item){
					$aux .= utf8_decode($item).",";	
				}
				$livres = substr($aux, 0, strlen($aux) - 1);
				$dados_pedido_salada["livres"] = $livres;
			}else{
				$dados_pedido_salada["livres"] = $_REQUEST["livres"];
			}
			
			$aux = "";
			if(count($_REQUEST["adicionais"])>0){
				foreach($_REQUEST["adicionais"] as $item){
					$aux .= utf8_decode($item).",";	
				}
				$adicionais = substr($aux, 0, strlen($aux) - 1);
				$dados_pedido_salada["adicionais"] = $adicionais;
			}else{
				$dados_pedido_salada["adicionais"] = $_REQUEST["adicionais"];
			}
			
			
			$dados_pedido_salada["num_adicionais"] = count($_REQUEST["adicionais"]);
			
		}
		
		
		$qtde = 1;
				
		/* Inicio: Confere se o produto selecionado j� est� no carrinho e seleciona dados */
		if($idPao != '' && $idProd != ''){

			$listaProdutoCarrinho = $_SESSION['listaProdutoCarrinhoSecao'];
			
			$valorTotalPedidoProduto = 0;
	
			if($_SESSION['valorTotalPedidoProdutoSecao'] != '')
				$valorTotalPedidoProduto = $_SESSION['valorTotalPedidoProdutoSecao'];

			$produtoJaNoCarrinho = false;

			/*if($listaProdutoCarrinho != ''){
				foreach ($listaProdutoCarrinho as $produto) {

					if($idProduto == $produto["id"] && $idTamanho == $produto["idTamanho"])
						$produtoJaNoCarrinho = true; 
				}
			}*/
						
			if(!$produtoJaNoCarrinho){
				
				//parametros função de retorno de dados
				if($idPao != 3){
					//idPao,idProd,queijo,salada,molho,livre,suco
					$dadosProduto = monta_produto($idPao,$idProd,$queijo,$salada,$molho,$livre,$suco,$doce);
				}else{
					//idPao,idProd,todas seleções
					$dadosProduto = monta_salada($idPao,$idProd,$dados_pedido_salada);
				}
				
			
				if($_SESSION['contadorItem'] > 0){
					$contadorItem = $_SESSION['contadorItem'];
				}else{
					$contadorItem = 0;
				}
		
				
				if(count($dadosProduto) != 0){

					if(!$resultadoQuery->EOF){
						
						$produtoSelecionado = array();
						$contadorItem = $contadorItem+1;
						
						$produtoSelecionado["idPao"] = $idPao;//0 - id produto
						$produtoSelecionado["nomePao"] = utf8_encode($dadosProduto["nomePao"]);//1 - nome produto
						
						$produtoSelecionado["idProd"] = $idProd;//2 - preco
						$produtoSelecionado["nomeProduto"] = $dadosProduto["nomeProduto"];//3 - preco formatado
						$produtoSelecionado["precoProduto"] = $dadosProduto["precoProduto"];//3 - preco formatado
						$produtoSelecionado["fotoProduto"] = $dadosProduto["fotoProduto"];//4 - foto
						
						$produtoSelecionado["queijo"] = $queijo;//5 - peso
						$produtoSelecionado["nomeQueijo"] = $dadosProduto["nomeQueijo"];//6 - id tamanho (qtde_produto)
						
						$produtoSelecionado["doce"] = $doce;//5 - peso
						$produtoSelecionado["nomeDoce"] = $dadosProduto["nomeDoce"];//6 - id tamanho (qtde_produto)
						
						$produtoSelecionado["salada"] = $salada;//5 - peso
						$produtoSelecionado["nomeSalada"] = $dadosProduto["nomeSalada"];//6 - id tamanho (qtde_produto)
						
						$produtoSelecionado["molho"] = $molho;//5 - peso
						$produtoSelecionado["nomeMolho"] = $dadosProduto["nomeMolho"];//6 - id tamanho (qtde_produto)
						
						$produtoSelecionado["livre"] = $livre;//5 - peso
						$produtoSelecionado["nomeLivre"] = $dadosProduto["nomeLivre"];//6 - id tamanho (qtde_produto)
						
						$produtoSelecionado["suco"] = $suco;//5 - peso
						$produtoSelecionado["nomeSuco"] = $dadosProduto["nomeSuco"];//6 - id tamanho (qtde_produto)
						$produtoSelecionado["precoSuco"] = $dadosProduto["precoSuco"];//6 - id tamanho (qtde_produto)
						
						$produtoSelecionado["qtde"] = $qtde;//7 - qtde selecionada
						$produtoSelecionado["contadorItem"] = $contadorItem;//8 - contador produtos carrinho
						
						$produtoSelecionado["valorTotal"] = $dadosProduto["valorTotal"];
						
						if($idPao != 3){
							$produtoSelecionado["tipo"] = "pao";
							$produtoSelecionado["dados_selecionados"] = "";
						}else{
							$produtoSelecionado["tipo"] = "almoco";
							$produtoSelecionado["dados_selecionados"] = $dados_pedido_salada;
						}
						
						$valorTotalPedidoProduto += $dadosProduto["valorTotal"];	
						
						$listaProdutoCarrinho[] = $produtoSelecionado;
					}

				}
				
			}
			
		
			$_SESSION['listaProdutoCarrinhoSecao'] = $listaProdutoCarrinho;
			
			$_SESSION['contadorItem'] = $contadorItem;
			
			$_SESSION['valorTotalPedidoProdutoSecao'] = $valorTotalPedidoProduto;
			
			$smarty->assign('valorTotalPedidoProduto', $valorTotalPedidoProduto);
	
			$smarty->assign('listaProdutoCarrinho', $listaProdutoCarrinho);
			
			
			echo '<SCRIPT LANGUAGE="JavaScript">
					window.location="index.php?secao=pedidoProduto&opcao=listarPedidoProdutoSecao";
				</SCRIPT>';
			
		}
		/* Fim: Confere se o produto selecionado j� est� no carrinho e seleciona dados */
		

		break;


/********************************** NOVA OP��O **************************************************************/

    case 'definirFormaPagto' :

		if(!isset($_SESSION['listaProdutoCarrinhoSecao'])){

			echo '<SCRIPT LANGUAGE="JavaScript">
					window.location="index.php";
				</SCRIPT>';
		}
		
		
		if(!isset($_SESSION['usuarioSite'])){
		
			echo '<SCRIPT LANGUAGE="JavaScript">
					window.location="index.php?secao=usuarioSite&opcao=logar&paginaRedirecionar=1";
				</SCRIPT>';

			exit;
		}

		$id_usuario_site = $_SESSION['usuarioSite'][0];


		$listaProdutoCarrinho = $_SESSION['listaProdutoCarrinhoSecao'];
		//print_r($listaProdutoCarrinho);
		$valorTotalPedido = 0;
		$qtdeProdutosPedido = 0;

		if($_SESSION['valorTotalPedidoProdutoSecao'] != '')
			$valorTotalPedido = $_SESSION['valorTotalPedidoProdutoSecao'];


		$listaProdutoCarrinhoAux = array();
		foreach ($listaProdutoCarrinho as $produto) {
			$listaProdutoCarrinhoAux[] = $produto;
			//print_r($listaProdutoCarrinhoAux);
		}
		
		$smarty->assign('listaProdutoCarrinho', $listaProdutoCarrinhoAux);
		
		if($desconto_cupons == "sim"){
			$descontoTotal = $valorTotalPedido*($_SESSION["desconto"]/100);
			$smarty->assign('descontoTotal', number_format($descontoTotal,2));
			$valorTotalPedido = $valorTotalPedido-$descontoTotal;	
		}
		

		$smarty->assign('valorTotalPedido', number_format($valorTotalPedido,2));
		
		$smarty->assign('tituloPagina', 'CONFIRMAR PAGAMENTO');

		$smarty->assign('templateCentro', 'centro_pedido_produto_pagamento.tpl');


		break;


/********************************** NOVA OP��O ***************************************************************/

    case 'finalizar' :
		

		if(!isset($_SESSION['listaProdutoCarrinhoSecao'])){

			echo '<SCRIPT LANGUAGE="JavaScript">
					
					window.location="index.php";
					
				</SCRIPT>';
		}
		
		if($desconto_cupons == "sim"){
			$qtdeDesconto = $_SESSION["desconto"];
			$utilizado = 1;
		}else{
			$utilizado = 0;
			$qtdeDesconto = 0;
		}


		$sql = 'INSERT INTO pedido (id_usuario_site, data_cadastro, id_status_pedido, desconto, utilizado, tipo, agendado) values ('.$_SESSION['usuarioSite'][0].',  now(), 7 ,"'.$qtdeDesconto.'", '.$utilizado.', "Pagseguro", "'.$_SESSION["horario"].'")';
		$db->Execute($sql);
		echo $db->ErrorMsg();
		$idPedidoProduto = $db->insert_id();


		$listaProdutoCarrinhoSecao = $_SESSION['listaProdutoCarrinhoSecao'];
		
		//print_r($listaProdutoCarrinhoSecao);
		
		/*****************verificando e dando baixar no estoque produtos*****************/
		foreach ($listaProdutoCarrinhoSecao as $produto) {
			
			if($produto["idPao"] != 3){
				$sql = 'INSERT INTO pedido_has_produto 
				(id_pedido, qtde, id_pao, id_prod, preco_prod, queijo, doce, salada, molho, livre, suco, preco_suco, valor_total)
				 VALUES 
				('.$idPedidoProduto.', '.$produto["qtde"].', '.$produto["idPao"].', '.$produto["idProd"].', "'.$produto["precoProduto"].'", "'.$produto["queijo"].'", "'.$produto["doce"].'", "'.$produto["salada"].'", "'.$produto["molho"].'", "'.$produto["livre"].'", "'.$produto["suco"].'", "'.$produto["precoSuco"].'", "'.$produto["valorTotal"]*$produto["qtde"].'")';
			}else{
				$sql = 'INSERT INTO pedido_has_produto 
				(id_pedido, qtde, id_pao, id_prod, preco_prod, carboidratos, proteinas, legumes, verduras, sementes, frutas, molhos, livres, adicionais, preco_adicionais, valor_total)
				 VALUES 
				('.$idPedidoProduto.', '.$produto["qtde"].', '.$produto["idPao"].', '.$produto["idProd"].', "'.$produto["precoProduto"].'", 
				"'.$produto["dados_selecionados"]["carboidratos"].'", 
				"'.$produto["dados_selecionados"]["proteinas"].'", 
				"'.$produto["dados_selecionados"]["legumes"].'", 
				"'.$produto["dados_selecionados"]["verduras"].'", 
				"'.$produto["dados_selecionados"]["sementes"].'", 
				"'.$produto["dados_selecionados"]["frutas"].'", 
				"'.$produto["dados_selecionados"]["molhos"].'", 
				"'.$produto["dados_selecionados"]["livres"].'", 
				"'.$produto["dados_selecionados"]["adicionais"].'", 
				"'.$produto["precoSuco"].'", 
				"'.$produto["valorTotal"]*$produto["qtde"].'")';	
			}
			
			$valorTotalPedidoProduto += $produto["valorTotal"]*$produto["qtde"];	
			
			$db->Execute($sql);
			echo $db->ErrorMsg();
			
		}
		

		$html = '<!DOCTYPE html>
		  <html lang="pt-br">
	  	  <HEAD>
		  <meta charset="utf-8">
		  <TITLE></TITLE>
		  <META NAME="Generator" CONTENT="EditPlus">
		  <META NAME="Author" CONTENT="">
		  <META NAME="Keywords" CONTENT="">
		  <META NAME="Description" CONTENT="">
		 </HEAD>

		 <BODY onload="document.form_pag_seguro.submit()">
 
		<form name="form_pag_seguro" action="https://pagseguro.uol.com.br/v2/checkout/payment.html" method="post" />';

		$html .= '<input type="hidden" name="receiverEmail" value="wrapbrasil2@gmail.com" />';
		$html .= '<input type="hidden" name="currency" value="BRL" />';
		$html .= '<input type="hidden" name="reference" value="'.$idPedidoProduto.'" />';
	
		$cont = 1;

		$listaProdutoCarrinhoSecao = $_SESSION['listaProdutoCarrinhoSecao'];
		$totalValorCompras = 0;

		foreach ($listaProdutoCarrinhoSecao as $produto) {
			
			if($produto["idPao"] != 3){
				
				$nomeProduto = "";
				$nomeProduto .= $produto["nomePao"]." - ".$produto["nomeProduto"];
				if($produto["nomeSuco"] != ""){
					$nomeProduto .= " / Suco: Bebidas - ".$produto["precoSuco"];
				}
				
			}else{
				$nomeProduto = "";
				$nomeProduto .= $produto["nomePao"]." - ".$produto["nomeProduto"];
				if($produto["nomeSuco"] != ""){
					$nomeProduto .= " / Adicionais: ".$produto["dados_selecionados"]["num_adicionais"]." adicionais - ".$produto["precoSuco"];
				}
			}
					
			$html .= '<input type="hidden" name="itemId'.$cont.'" value="'.$produto["idProd"].'" />';
			$html .= '<input type="hidden" name="itemDescription'.$cont.'" value="'.utf8_decode($nomeProduto).'" />';
			$html .= '<input type="hidden" name="itemQuantity'.$cont.'" value="'.$produto["qtde"].'" />';
			$html .= '<input type="hidden" name="itemAmount'.$cont.'" value="'.number_format($produto["valorTotal"],2).'" />';
			$html .= '<input type="hidden" name="itemWeight'.$cont.'" value="0" />';
			
			$totalValorCompras = $totalValorCompras+$produto["valorTotal"];
			
			$cont++;
		}
		
		if($desconto_cupons == "sim"){
			
			$html .= '<input type="hidden" name="extraAmount" value="-'.number_format($totalValorCompras*($_SESSION["desconto"]/100),2).'" />';
			$listaProdutos2 .= '<strong>Desconto de '.$_SESSION["desconto"].'% utilizado.</strong><br />';
			
			$sql = 'INSERT INTO cupons (id_cliente, data_cadastro, desconto) values ('.$_SESSION['usuarioSite'][0].',  now(),"'.$qtdeDesconto.'")';
			$db->Execute($sql);
			echo $db->ErrorMsg();
			
			$sqlPed = 'select pp.*
			from pedido pp
			where pp.utilizado = 0 and pp.id_status_pedido in (1,6,8) and pp.id_usuario_site = '.$_SESSION['usuarioSite'][0].'
			order by pp.id asc
			limit '.$_SESSION["ticket"];
			$resultadoQueryPed=$db->Execute($sqlPed);
			while(!$resultadoQueryPed->EOF){
		
				$sql ='update pedido set utilizado = 1 where id_usuario_site = '.$_SESSION['usuarioSite'][0].' and id = '.$resultadoQueryPed->fields["id"]; 
				$db->Execute($sql);
				echo $db->ErrorMsg();
							
				$resultadoQueryPed->MoveNext();
			}
		}
		
		$sql = 'SELECT us.id, nome, telefone, email
			FROM clientes us where us.id = '.$_SESSION['usuarioSite'][0];

		$resultadoQuery = $db->Execute($sql);
		
		echo $db->ErrorMsg();
		if(!$resultadoQuery->EOF){
			
			$telefoneEntrega = $resultadoQuery->fields[2];
			$nomeEntrega = $resultadoQuery->fields[1];
			$emailEntrega = $resultadoQuery->fields[3];

			/*****************para envio do e-mail*******************/
			$listaProdutos2 .= '<strong>Dados do Cliente:</strong><br />';
			$listaProdutos2 .= 'Nome: '.$resultadoQuery->fields[1].'<br />';
			
			$listaProdutos2 .= 'Telefone: '.substr($resultadoQuery->fields[2], 1, 2).' - '.str_replace('-', '', substr($resultadoQuery->fields[2], 5, 9)).'<br />';
			$listaProdutos2 .= 'E-mail: '.$resultadoQuery->fields[3].'<br /><br /><br />';
			
		}
		
			$ddd = substr($telefoneEntrega, 1, 2);			
			$tel = str_replace('-', '', substr($telefoneEntrega, 5, 9));

			$html .= '<input type="hidden" name="senderName" value="'.$nomeEntrega.'" />';
			$html .= '<input type="hidden" name="cliente_pais" value="BRA" />';
			$html .= '<input type="hidden" name="senderAreaCode" value="'.$ddd.'" />';
			$html .= '<input type="hidden" name="senderPhone" value="'.$tel.'" />';
			$html .= '<input type="hidden" name="senderEmail" value="'.$emailEntrega.'" />';
		
		
		$html .= '</form></BODY></HTML>';
		
		/***************************envia e-mail****************************/
		
		/*******monta lista de produtos**************/
		$cont2 = 1;
		$listaProdutos .= '<br /><strong>Produto(s):</strong>';
		
		foreach ($listaProdutoCarrinhoSecao as $produto) {
			
			$listaProdutos .= '<br />Item '.$cont2;
			$listaProdutos .= '<br />Produto: '.utf8_decode($produto["nomePao"])." - ".utf8_decode($produto["nomeProduto"]);
			
			if($produto["idPao"] != 3){
				if($produto["nomeQueijo"] != ""){
					$listaProdutos .= '<br />Queijo: '.$produto["nomeQueijo"];
				}
				if($produto["nomeDoce"] != ""){
					$listaProdutos .= '<br />Doce: '.$produto["nomeDoce"];
				}
				if($produto["nomeSalada"] != ""){
					$listaProdutos .= '<br />Salada: '.$produto["nomeSalada"];
				}
				if($produto["nomeMolho"] != ""){
					$listaProdutos .= '<br />Molho: '.$produto["nomeMolho"];
				}
				if($produto["nomeLivre"] != ""){
					$listaProdutos .= '<br />Livre: '.$produto["nomeLivre"];
				}
				if($produto["nomeSuco"] != ""){
					$listaProdutos .= "<br />Suco: ".$produto["nomeSuco"]." - ".$produto["precoSuco"];
				}
				
			}else{
				if($produto["dados_selecionados"]["carboidratos"] != ""){
					$listaProdutos .= '<br />Carboidratos: '.$produto["dados_selecionados"]["carboidratos"];
				}
				if($produto["dados_selecionados"]["proteinas"] != ""){
					$listaProdutos .= '<br />Prote&iacute;nas: '.$produto["dados_selecionados"]["proteinas"];
				}
				if($produto["dados_selecionados"]["legumes"] != ""){
					$listaProdutos .= '<br />Legumes: '.$produto["dados_selecionados"]["legumes"];
				}
				if($produto["dados_selecionados"]["verduras"] != ""){
					$listaProdutos .= '<br />Verduras: '.$produto["dados_selecionados"]["verduras"];
				}
				if($produto["dados_selecionados"]["sementes"] != ""){
					$listaProdutos .= '<br />Sementes: '.$produto["dados_selecionados"]["sementes"];
				}
				if($produto["dados_selecionados"]["frutas"] != ""){
					$listaProdutos .= '<br />Frutas: '.$produto["dados_selecionados"]["frutas"];
				}
				if($produto["dados_selecionados"]["molhos"] != ""){
					$listaProdutos .= '<br />Molhos: '.$produto["dados_selecionados"]["molhos"];
				}
				if($produto["dados_selecionados"]["livres"] != ""){
					$listaProdutos .= '<br />Livres: '.$produto["dados_selecionados"]["livres"];
				}
				
				if($produto["dados_selecionados"]["adicionais"] != ""){
					$listaProdutos .= "<br />Adicionais: ".$produto["dados_selecionados"]["adicionais"]." - ".$produto["precoSuco"];
				}	
				
			}
			$listaProdutos .= '<br />Quantidade do produto : '.$produto["qtde"];
			$listaProdutos .= '<br />Valor do produto : R$ '.number_format($produto["valorTotal"],2).' por unidade';
			$listaProdutos .= '<br />Agendado para buscar as <b>'.$_SESSION["horario"].'</b> horas.';
			$listaProdutos .= '<br />Tipo pagamento: <b>Pagseguro</b>.';
			$listaProdutos .= '<br /><br />';

			$cont2++;
		}
		
		
		$dataCompra = date("d/m/Y");
		
		$mensagemEmail = '
			<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>WRAP BRASIL</title>
        <style type="text/css">
			/* /\/\/\/\/\/\/\/\/ CLIENT-SPECIFIC STYLES /\/\/\/\/\/\/\/\/ */
			#outlook a{padding:0;} /* Force Outlook to provide a "view in browser" message */
			.ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail to display emails at full width */
			.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} /* Force Hotmail to display normal line spacing */
			body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;} /* Prevent WebKit and Windows mobile changing default text sizes */
			table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;} /* Remove spacing between tables in Outlook 2007 and up */
			img{-ms-interpolation-mode:bicubic;} /* Allow smoother rendering of resized image in Internet Explorer */

			/* /\/\/\/\/\/\/\/\/ RESET STYLES /\/\/\/\/\/\/\/\/ */
			body{margin:0; padding:0;}
			img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
			table{border-collapse:collapse !important;}
			body, #bodyTable, #bodyCell{height:100% !important; margin:0; padding:0; width:100% !important;}

			/* /\/\/\/\/\/\/\/\/ TEMPLATE STYLES /\/\/\/\/\/\/\/\/ */

			/* ========== Page Styles ========== */

			#bodyCell{padding:20px;}
			#templateContainer{width:600px;}
			body, #bodyTable{
				/*@editable*/ background-color:#DEE0E2;
			}
			#bodyCell{
				/*@editable*/ border-top:4px solid #BBBBBB;
			}
			#templateContainer{
				/*@editable*/ border:1px solid #BBBBBB;
			}
			h1{
				/*@editable*/ color:#202020 !important;
				display:block;
				/*@editable*/ font-family:Helvetica;
				/*@editable*/ font-size:26px;
				/*@editable*/ font-style:normal;
				/*@editable*/ font-weight:bold;
				/*@editable*/ line-height:100%;
				/*@editable*/ letter-spacing:normal;
				margin-top:0;
				margin-right:0;
				margin-bottom:10px;
				margin-left:0;
				/*@editable*/ text-align:left;
			}
			h2{
				/*@editable*/ color:#404040 !important;
				display:block;
				/*@editable*/ font-family:Helvetica;
				/*@editable*/ font-size:20px;
				/*@editable*/ font-style:normal;
				/*@editable*/ font-weight:bold;
				/*@editable*/ line-height:100%;
				/*@editable*/ letter-spacing:normal;
				margin-top:0;
				margin-right:0;
				margin-bottom:10px;
				margin-left:0;
				/*@editable*/ text-align:left;
			}
			h3{
				/*@editable*/ color:#606060 !important;
				display:block;
				/*@editable*/ font-family:Helvetica;
				/*@editable*/ font-size:16px;
				/*@editable*/ font-style:italic;
				/*@editable*/ font-weight:normal;
				/*@editable*/ line-height:100%;
				/*@editable*/ letter-spacing:normal;
				margin-top:0;
				margin-right:0;
				margin-bottom:10px;
				margin-left:0;
				/*@editable*/ text-align:left;
			}
			h4{
				/*@editable*/ color:#808080 !important;
				display:block;
				/*@editable*/ font-family:Helvetica;
				/*@editable*/ font-size:14px;
				/*@editable*/ font-style:italic;
				/*@editable*/ font-weight:normal;
				/*@editable*/ line-height:100%;
				/*@editable*/ letter-spacing:normal;
				margin-top:0;
				margin-right:0;
				margin-bottom:10px;
				margin-left:0;
				/*@editable*/ text-align:left;
			}

			#templatePreheader{
				/*@editable*/ background-color:#F4F4F4;
				/*@editable*/ border-bottom:1px solid #CCCCCC;
			}

			.preheaderContent{
				/*@editable*/ color:#808080;
				/*@editable*/ font-family:Helvetica;
				/*@editable*/ font-size:10px;
				/*@editable*/ line-height:125%;
				/*@editable*/ text-align:left;
			}

			.preheaderContent a:link, .preheaderContent a:visited, /* Yahoo! Mail Override */ .preheaderContent a .yshortcuts /* Yahoo! Mail Override */{
				/*@editable*/ color:#606060;
				/*@editable*/ font-weight:normal;
				/*@editable*/ text-decoration:underline;
			}

			#templateHeader{
				/*@editable*/ background-color:#F4F4F4;
				/*@editable*/ border-top:1px solid #FFFFFF;
				/*@editable*/ border-bottom:1px solid #CCCCCC;
			}

			.headerContent{
				/*@editable*/ color:#505050;
				/*@editable*/ font-family:Helvetica;
				/*@editable*/ font-size:20px;
				/*@editable*/ font-weight:bold;
				/*@editable*/ line-height:100%;
				/*@editable*/ padding-top:0;
				/*@editable*/ padding-right:0;
				/*@editable*/ padding-bottom:0;
				/*@editable*/ padding-left:0;
				/*@editable*/ text-align:left;
				/*@editable*/ vertical-align:middle;
			}

			.headerContent a:link, .headerContent a:visited, /* Yahoo! Mail Override */ .headerContent a .yshortcuts /* Yahoo! Mail Override */{
				/*@editable*/ color:#EB4102;
				/*@editable*/ font-weight:normal;
				/*@editable*/ text-decoration:underline;
			}

			#headerImage{
				height:auto;
				max-width:600px;
			}

			#templateBody{
				/*@editable*/ background-color:#F4F4F4;
				/*@editable*/ border-top:1px solid #FFFFFF;
				/*@editable*/ border-bottom:1px solid #CCCCCC;
			}

			.bodyContent{
				/*@editable*/ color:#505050;
				/*@editable*/ font-family:Helvetica;
				/*@editable*/ font-size:14px;
				/*@editable*/ line-height:150%;
				padding-top:20px;
				padding-right:20px;
				padding-bottom:20px;
				padding-left:20px;
				/*@editable*/ text-align:left;
			}

			.bodyContent a:link, .bodyContent a:visited, /* Yahoo! Mail Override */ .bodyContent a .yshortcuts /* Yahoo! Mail Override */{
				/*@editable*/ color:#EB4102;
				/*@editable*/ font-weight:normal;
				/*@editable*/ text-decoration:underline;
			}

			.bodyContent img{
				display:inline;
				height:auto;
				max-width:560px;
			}

			#templateFooter{
				/*@editable*/ background-color:#F4F4F4;
				/*@editable*/ border-top:1px solid #FFFFFF;
			}
			
			.footerContent{
				/*@editable*/ color:#808080;
				/*@editable*/ font-family:Helvetica;
				/*@editable*/ font-size:10px;
				/*@editable*/ line-height:150%;
				padding-top:20px;
				padding-right:20px;
				padding-bottom:20px;
				padding-left:20px;
				/*@editable*/ text-align:left;
			}

			.footerContent a:link, .footerContent a:visited, /* Yahoo! Mail Override */ .footerContent a .yshortcuts, .footerContent a span /* Yahoo! Mail Override */{
				/*@editable*/ color:#606060;
				/*@editable*/ font-weight:normal;
				/*@editable*/ text-decoration:underline;
			}

			/* /\/\/\/\/\/\/\/\/ MOBILE STYLES /\/\/\/\/\/\/\/\/ */

            @media only screen and (max-width: 480px){
				/* /\/\/\/\/\/\/ CLIENT-SPECIFIC MOBILE STYLES /\/\/\/\/\/\/ */
				body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:none !important;} /* Prevent Webkit platforms from changing default text sizes */
                body{width:100% !important; min-width:100% !important;} /* Prevent iOS Mail from adding padding to the body */

				/* /\/\/\/\/\/\/ MOBILE RESET STYLES /\/\/\/\/\/\/ */
				#bodyCell{padding:10px !important;}

				
				#templateContainer{
					max-width:600px !important;
					/*@editable*/ width:100% !important;
				}

				h1{
					/*@editable*/ font-size:24px !important;
					/*@editable*/ line-height:100% !important;
				}

				h2{
					/*@editable*/ font-size:20px !important;
					/*@editable*/ line-height:100% !important;
				}

				h3{
					/*@editable*/ font-size:18px !important;
					/*@editable*/ line-height:100% !important;
				}

				h4{
					/*@editable*/ font-size:16px !important;
					/*@editable*/ line-height:100% !important;
				}

				/* ======== Header Styles ======== */

				#templatePreheader{display:none !important;} /* Hide the template preheader to save space */

				#headerImage{
					height:auto !important;
					/*@editable*/ max-width:600px !important;
					/*@editable*/ width:100% !important;
				}
				.headerContent{

					/*@editable*/ font-size:20px !important;
					/*@editable*/ line-height:125% !important;
				}

				.bodyContent{
					/*@editable*/ font-size:18px !important;
					/*@editable*/ line-height:125% !important;
				}

				.footerContent{
					/*@editable*/ font-size:14px !important;
					/*@editable*/ line-height:115% !important;
				}

				.footerContent a{display:block !important;} /* Place footer social and utility links on their own lines, for easier access */
			}
		</style>
    </head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
    	<center>
        	<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
            	<tr>
                	<td align="center" valign="top" id="bodyCell">
                    	<!-- BEGIN TEMPLATE // -->
                    	<table border="0" cellpadding="0" cellspacing="0" id="templateContainer">
                        	<tr>
                            	<td align="center" valign="top">
                                	<!-- BEGIN HEADER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateHeader">
                                        <tr>
                                            <td valign="top" class="headerContent">
                                            	<img src="http://www.wrapbrasil.com/email/topo.png" style="max-width:600px;" id="headerImage" mc:label="header_image" mc:edit="header_image" mc:allowdesigner mc:allowtext />
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // END HEADER -->
                                </td>
                            </tr>
                        	<tr>
                            	<td align="center" valign="top">
                                	<!-- BEGIN BODY // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateBody">
                                        <tr>
                                            <td valign="top" class="bodyContent" mc:edit="body_content">
												<p style="font-size:12px;color;#484848;">
													Obrigado por comprar nossos produtos!
												</p>
												
												<p>
												Dados da compra - '.$dataCompra.'
												
												<br><br>
												
												'.$listaProdutos.'
												<br />
												'.$listaProdutos2.'
												</p>
												
												<p style="font-size:12px;color;#484848;">
													Att.
													WRAP BRASIL
												</p>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // END BODY -->
                                </td>
                            </tr>
                        	<tr>
                            	<td align="center" valign="top">
                                	<!-- BEGIN FOOTER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateFooter">
                                        <tr>
                                            <td valign="top" class="footerContent" mc:edit="footer_content00">
                                                <a href="https://www.facebook.com/wrapfoodtruck/">Facebook</a>&nbsp;&nbsp;&nbsp;<a href="https://www.instagram.com/wrapbrasil/">Instagram</a>&nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" class="footerContent" style="padding-top:0;" mc:edit="footer_content01">
                                                <em>Copyright &copy; |2018| WRAPBRASIL|, All rights reserved.</em>
                                                <br />
                                                <strong>Nosso endere&ccedil;o &eacute;:</strong>
                                                <br />
                                                <a href="http://www.wrapbrasil.com/">www.wrapbrasil.com</a>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // END FOOTER -->
                                </td>
                            </tr>
                        </table>
                        <!-- // END TEMPLATE -->
                    </td>
                </tr>
            </table>
        </center>
    </body>
</html>
			';
		
		
		  
		//email1
		$destinatarios = "wrapbrasil2@gmail.com";
		//$destinatarios = "jaojao05@gmail.com";
		$destinatarios2 = $emailEntrega;
		//$nomeDestinatario = $nomeGG;
		$Message = $mensagemEmail;
		$assunto = "Compra Wrap Brasil - ".$dataCompra;
		//$anexo = "MANUAL_DE_PRESCRICAO_DO_TREINAMENTO_DE_OCLUSAO_VASCULAR.pdf";
		$enviado = envia_email($destinatarios,$nomeDestinatario,$Message,$assunto,$anexo);
		$enviado = envia_email($destinatarios2,$nomeDestinatario,$Message,$assunto,$anexo);

		
		/***************************fim e-mail*****************************************/		

		
		unset($_SESSION['listaProdutoCarrinhoSecao']);

		unset($_SESSION['valorTotalPedidoProdutoSecao']);
		
		unset($_SESSION['horario']);

		echo $html;

		exit;


		$smarty->assign('idPedidoProduto', $idPedidoProduto);

 

		$smarty->assign('tituloPagina', ' - Pedido concluído');

		$smarty->assign('templateCentro', 'centro_pedido_finalizar.tpl');


		break;



/********************************** NOVA OP��O ***************************************************************/

    case 'finalizarDinheiro' :
		

		if(!isset($_SESSION['listaProdutoCarrinhoSecao'])){

			echo '<SCRIPT LANGUAGE="JavaScript">
					
					window.location="index.php";
					
				</SCRIPT>';
		}
		
		if($desconto_cupons == "sim"){
			$qtdeDesconto = $_SESSION["desconto"];
			$utilizado = 1;
		}else{
			$utilizado = 0;
			$qtdeDesconto = 0;
		}


		$sql = 'INSERT INTO pedido (id_usuario_site, data_cadastro, id_status_pedido, desconto, utilizado, tipo, agendado) values ('.$_SESSION['usuarioSite'][0].',  now(), 7 ,"'.$qtdeDesconto.'", '.$utilizado.', "No local", "'.$_SESSION["horario"].'")';
		$db->Execute($sql);
		echo $db->ErrorMsg();
		$idPedidoProduto = $db->insert_id();


		$listaProdutoCarrinhoSecao = $_SESSION['listaProdutoCarrinhoSecao'];
		
		//print_r($listaProdutoCarrinhoSecao);
		
		/*****************verificando e dando baixar no estoque produtos*****************/
		foreach ($listaProdutoCarrinhoSecao as $produto) {
			
			if($produto["idPao"] != 3){
				$sql = 'INSERT INTO pedido_has_produto 
				(id_pedido, qtde, id_pao, id_prod, preco_prod, queijo, doce, salada, molho, livre, suco, preco_suco, valor_total)
				 VALUES 
				('.$idPedidoProduto.', '.$produto["qtde"].', '.$produto["idPao"].', '.$produto["idProd"].', "'.$produto["precoProduto"].'", "'.$produto["queijo"].'", "'.$produto["doce"].'", "'.$produto["salada"].'", "'.$produto["molho"].'", "'.$produto["livre"].'", "'.$produto["suco"].'", "'.$produto["precoSuco"].'", "'.$produto["valorTotal"]*$produto["qtde"].'")';
			}else{
				$sql = 'INSERT INTO pedido_has_produto 
				(id_pedido, qtde, id_pao, id_prod, preco_prod, carboidratos, proteinas, legumes, verduras, sementes, frutas, molhos, livres, adicionais, preco_adicionais, valor_total)
				 VALUES 
				('.$idPedidoProduto.', '.$produto["qtde"].', '.$produto["idPao"].', '.$produto["idProd"].', "'.$produto["precoProduto"].'", 
				"'.$produto["dados_selecionados"]["carboidratos"].'", 
				"'.$produto["dados_selecionados"]["proteinas"].'", 
				"'.$produto["dados_selecionados"]["legumes"].'", 
				"'.$produto["dados_selecionados"]["verduras"].'", 
				"'.$produto["dados_selecionados"]["sementes"].'", 
				"'.$produto["dados_selecionados"]["frutas"].'", 
				"'.$produto["dados_selecionados"]["molhos"].'", 
				"'.$produto["dados_selecionados"]["livres"].'", 
				"'.$produto["dados_selecionados"]["adicionais"].'", 
				"'.$produto["precoSuco"].'", 
				"'.$produto["valorTotal"]*$produto["qtde"].'")';	
			}
			
			$valorTotalPedidoProduto += $produto["valorTotal"]*$produto["qtde"];	
			
			$db->Execute($sql);
			echo $db->ErrorMsg();
			
		}
		

		$cont = 1;

		$listaProdutoCarrinhoSecao = $_SESSION['listaProdutoCarrinhoSecao'];
		$totalValorCompras = 0;

		foreach ($listaProdutoCarrinhoSecao as $produto) {
			
			if($produto["idPao"] != 3){
				
				$nomeProduto = "";
				$nomeProduto .= $produto["nomePao"]." - ".$produto["nomeProduto"];
				if($produto["nomeSuco"] != ""){
					$nomeProduto .= " / Suco: Bebidas - ".$produto["precoSuco"];
				}
				
			}else{
				$nomeProduto = "";
				$nomeProduto .= $produto["nomePao"]." - ".$produto["nomeProduto"];
				if($produto["nomeSuco"] != ""){
					$nomeProduto .= " / Adicionais: ".$produto["dados_selecionados"]["num_adicionais"]." adicionais - ".$produto["precoSuco"];
				}
			}
						
			$totalValorCompras = $totalValorCompras+$produto["valorTotal"];
			
			$cont++;
		}
		
		if($desconto_cupons == "sim"){
			
			$listaProdutos2 .= '<strong>Desconto de '.$_SESSION["desconto"].'% utilizado.</strong><br />';
			
			$sql = 'INSERT INTO cupons (id_cliente, data_cadastro, desconto) values ('.$_SESSION['usuarioSite'][0].',  now(),"'.$qtdeDesconto.'")';
			$db->Execute($sql);
			echo $db->ErrorMsg();
			
			$sqlPed = 'select pp.*
			from pedido pp
			where pp.utilizado = 0 and pp.id_status_pedido in (1,6,8) and pp.id_usuario_site = '.$_SESSION['usuarioSite'][0].'
			order by pp.id asc
			limit '.$_SESSION["ticket"];
			$resultadoQueryPed=$db->Execute($sqlPed);
			while(!$resultadoQueryPed->EOF){
		
				$sql ='update pedido set utilizado = 1 where id_usuario_site = '.$_SESSION['usuarioSite'][0].' and id = '.$resultadoQueryPed->fields["id"]; 
				$db->Execute($sql);
				echo $db->ErrorMsg();
							
				$resultadoQueryPed->MoveNext();
			}
		}
		
		$sql = 'SELECT us.id, nome, telefone, email
			FROM clientes us where us.id = '.$_SESSION['usuarioSite'][0];

		$resultadoQuery = $db->Execute($sql);
		
		echo $db->ErrorMsg();
		if(!$resultadoQuery->EOF){
			
			$telefoneEntrega = $resultadoQuery->fields[2];
			$nomeEntrega = $resultadoQuery->fields[1];
			$emailEntrega = $resultadoQuery->fields[3];

			/*****************para envio do e-mail*******************/
			$listaProdutos2 .= '<strong>Dados do Cliente:</strong><br />';
			$listaProdutos2 .= 'Nome: '.$resultadoQuery->fields[1].'<br />';
			
			$listaProdutos2 .= 'Telefone: '.substr($resultadoQuery->fields[2], 1, 2).' - '.str_replace('-', '', substr($resultadoQuery->fields[2], 5, 9)).'<br />';
			$listaProdutos2 .= 'E-mail: '.$resultadoQuery->fields[3].'<br /><br /><br />';
			
		}
		
		
		/*******monta lista de produtos**************/
		$cont2 = 1;
		$listaProdutos .= '<br /><strong>Produto(s):</strong>';
		
		foreach ($listaProdutoCarrinhoSecao as $produto) {
			
			$listaProdutos .= '<br />Item '.$cont2;
			$listaProdutos .= '<br />Produto: '.utf8_decode($produto["nomePao"])." - ".utf8_decode($produto["nomeProduto"]);
			
			if($produto["idPao"] != 3){
				if($produto["nomeQueijo"] != ""){
					$listaProdutos .= '<br />Queijo: '.$produto["nomeQueijo"];
				}
				if($produto["nomeDoce"] != ""){
					$listaProdutos .= '<br />Doce: '.$produto["nomeDoce"];
				}
				if($produto["nomeSalada"] != ""){
					$listaProdutos .= '<br />Salada: '.$produto["nomeSalada"];
				}
				if($produto["nomeMolho"] != ""){
					$listaProdutos .= '<br />Molho: '.$produto["nomeMolho"];
				}
				if($produto["nomeLivre"] != ""){
					$listaProdutos .= '<br />Livre: '.$produto["nomeLivre"];
				}
				if($produto["nomeSuco"] != ""){
					$listaProdutos .= "<br />Suco: ".$produto["nomeSuco"]." - ".$produto["precoSuco"];
				}
				
			}else{
				if($produto["dados_selecionados"]["carboidratos"] != ""){
					$listaProdutos .= '<br />Carboidratos: '.$produto["dados_selecionados"]["carboidratos"];
				}
				if($produto["dados_selecionados"]["proteinas"] != ""){
					$listaProdutos .= '<br />Prote&iacute;nas: '.$produto["dados_selecionados"]["proteinas"];
				}
				if($produto["dados_selecionados"]["legumes"] != ""){
					$listaProdutos .= '<br />Legumes: '.$produto["dados_selecionados"]["legumes"];
				}
				if($produto["dados_selecionados"]["verduras"] != ""){
					$listaProdutos .= '<br />Verduras: '.$produto["dados_selecionados"]["verduras"];
				}
				if($produto["dados_selecionados"]["sementes"] != ""){
					$listaProdutos .= '<br />Sementes: '.$produto["dados_selecionados"]["sementes"];
				}
				if($produto["dados_selecionados"]["frutas"] != ""){
					$listaProdutos .= '<br />Frutas: '.$produto["dados_selecionados"]["frutas"];
				}
				if($produto["dados_selecionados"]["molhos"] != ""){
					$listaProdutos .= '<br />Molhos: '.$produto["dados_selecionados"]["molhos"];
				}
				if($produto["dados_selecionados"]["livres"] != ""){
					$listaProdutos .= '<br />Livres: '.$produto["dados_selecionados"]["livres"];
				}
				
				if($produto["dados_selecionados"]["adicionais"] != ""){
					$listaProdutos .= "<br />Adicionais: ".$produto["dados_selecionados"]["adicionais"]." - ".$produto["precoSuco"];
				}	
				
			}
			$listaProdutos .= '<br />Quantidade do produto : '.$produto["qtde"];
			$listaProdutos .= '<br />Valor do produto : R$ '.number_format($produto["valorTotal"],2).' por unidade';
			$listaProdutos .= '<br />Agendado para buscar as <b>'.$_SESSION["horario"].'</b> horas.';
			$listaProdutos .= '<br />Tipo pagamento: <b>Pagamento no local</b>.';
			$listaProdutos .= '<br /><br />';

			$cont2++;
		}
		
		
		$dataCompra = date("d/m/Y");
		
		$mensagemEmail = '
			<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>WRAP BRASIL</title>
        <style type="text/css">
			/* /\/\/\/\/\/\/\/\/ CLIENT-SPECIFIC STYLES /\/\/\/\/\/\/\/\/ */
			#outlook a{padding:0;} /* Force Outlook to provide a "view in browser" message */
			.ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail to display emails at full width */
			.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} /* Force Hotmail to display normal line spacing */
			body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;} /* Prevent WebKit and Windows mobile changing default text sizes */
			table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;} /* Remove spacing between tables in Outlook 2007 and up */
			img{-ms-interpolation-mode:bicubic;} /* Allow smoother rendering of resized image in Internet Explorer */

			/* /\/\/\/\/\/\/\/\/ RESET STYLES /\/\/\/\/\/\/\/\/ */
			body{margin:0; padding:0;}
			img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
			table{border-collapse:collapse !important;}
			body, #bodyTable, #bodyCell{height:100% !important; margin:0; padding:0; width:100% !important;}

			/* /\/\/\/\/\/\/\/\/ TEMPLATE STYLES /\/\/\/\/\/\/\/\/ */

			/* ========== Page Styles ========== */

			#bodyCell{padding:20px;}
			#templateContainer{width:600px;}
			body, #bodyTable{
				/*@editable*/ background-color:#DEE0E2;
			}
			#bodyCell{
				/*@editable*/ border-top:4px solid #BBBBBB;
			}
			#templateContainer{
				/*@editable*/ border:1px solid #BBBBBB;
			}
			h1{
				/*@editable*/ color:#202020 !important;
				display:block;
				/*@editable*/ font-family:Helvetica;
				/*@editable*/ font-size:26px;
				/*@editable*/ font-style:normal;
				/*@editable*/ font-weight:bold;
				/*@editable*/ line-height:100%;
				/*@editable*/ letter-spacing:normal;
				margin-top:0;
				margin-right:0;
				margin-bottom:10px;
				margin-left:0;
				/*@editable*/ text-align:left;
			}
			h2{
				/*@editable*/ color:#404040 !important;
				display:block;
				/*@editable*/ font-family:Helvetica;
				/*@editable*/ font-size:20px;
				/*@editable*/ font-style:normal;
				/*@editable*/ font-weight:bold;
				/*@editable*/ line-height:100%;
				/*@editable*/ letter-spacing:normal;
				margin-top:0;
				margin-right:0;
				margin-bottom:10px;
				margin-left:0;
				/*@editable*/ text-align:left;
			}
			h3{
				/*@editable*/ color:#606060 !important;
				display:block;
				/*@editable*/ font-family:Helvetica;
				/*@editable*/ font-size:16px;
				/*@editable*/ font-style:italic;
				/*@editable*/ font-weight:normal;
				/*@editable*/ line-height:100%;
				/*@editable*/ letter-spacing:normal;
				margin-top:0;
				margin-right:0;
				margin-bottom:10px;
				margin-left:0;
				/*@editable*/ text-align:left;
			}
			h4{
				/*@editable*/ color:#808080 !important;
				display:block;
				/*@editable*/ font-family:Helvetica;
				/*@editable*/ font-size:14px;
				/*@editable*/ font-style:italic;
				/*@editable*/ font-weight:normal;
				/*@editable*/ line-height:100%;
				/*@editable*/ letter-spacing:normal;
				margin-top:0;
				margin-right:0;
				margin-bottom:10px;
				margin-left:0;
				/*@editable*/ text-align:left;
			}

			#templatePreheader{
				/*@editable*/ background-color:#F4F4F4;
				/*@editable*/ border-bottom:1px solid #CCCCCC;
			}

			.preheaderContent{
				/*@editable*/ color:#808080;
				/*@editable*/ font-family:Helvetica;
				/*@editable*/ font-size:10px;
				/*@editable*/ line-height:125%;
				/*@editable*/ text-align:left;
			}

			.preheaderContent a:link, .preheaderContent a:visited, /* Yahoo! Mail Override */ .preheaderContent a .yshortcuts /* Yahoo! Mail Override */{
				/*@editable*/ color:#606060;
				/*@editable*/ font-weight:normal;
				/*@editable*/ text-decoration:underline;
			}

			#templateHeader{
				/*@editable*/ background-color:#F4F4F4;
				/*@editable*/ border-top:1px solid #FFFFFF;
				/*@editable*/ border-bottom:1px solid #CCCCCC;
			}

			.headerContent{
				/*@editable*/ color:#505050;
				/*@editable*/ font-family:Helvetica;
				/*@editable*/ font-size:20px;
				/*@editable*/ font-weight:bold;
				/*@editable*/ line-height:100%;
				/*@editable*/ padding-top:0;
				/*@editable*/ padding-right:0;
				/*@editable*/ padding-bottom:0;
				/*@editable*/ padding-left:0;
				/*@editable*/ text-align:left;
				/*@editable*/ vertical-align:middle;
			}

			.headerContent a:link, .headerContent a:visited, /* Yahoo! Mail Override */ .headerContent a .yshortcuts /* Yahoo! Mail Override */{
				/*@editable*/ color:#EB4102;
				/*@editable*/ font-weight:normal;
				/*@editable*/ text-decoration:underline;
			}

			#headerImage{
				height:auto;
				max-width:600px;
			}

			#templateBody{
				/*@editable*/ background-color:#F4F4F4;
				/*@editable*/ border-top:1px solid #FFFFFF;
				/*@editable*/ border-bottom:1px solid #CCCCCC;
			}

			.bodyContent{
				/*@editable*/ color:#505050;
				/*@editable*/ font-family:Helvetica;
				/*@editable*/ font-size:14px;
				/*@editable*/ line-height:150%;
				padding-top:20px;
				padding-right:20px;
				padding-bottom:20px;
				padding-left:20px;
				/*@editable*/ text-align:left;
			}

			.bodyContent a:link, .bodyContent a:visited, /* Yahoo! Mail Override */ .bodyContent a .yshortcuts /* Yahoo! Mail Override */{
				/*@editable*/ color:#EB4102;
				/*@editable*/ font-weight:normal;
				/*@editable*/ text-decoration:underline;
			}

			.bodyContent img{
				display:inline;
				height:auto;
				max-width:560px;
			}

			#templateFooter{
				/*@editable*/ background-color:#F4F4F4;
				/*@editable*/ border-top:1px solid #FFFFFF;
			}
			
			.footerContent{
				/*@editable*/ color:#808080;
				/*@editable*/ font-family:Helvetica;
				/*@editable*/ font-size:10px;
				/*@editable*/ line-height:150%;
				padding-top:20px;
				padding-right:20px;
				padding-bottom:20px;
				padding-left:20px;
				/*@editable*/ text-align:left;
			}

			.footerContent a:link, .footerContent a:visited, /* Yahoo! Mail Override */ .footerContent a .yshortcuts, .footerContent a span /* Yahoo! Mail Override */{
				/*@editable*/ color:#606060;
				/*@editable*/ font-weight:normal;
				/*@editable*/ text-decoration:underline;
			}

			/* /\/\/\/\/\/\/\/\/ MOBILE STYLES /\/\/\/\/\/\/\/\/ */

            @media only screen and (max-width: 480px){
				/* /\/\/\/\/\/\/ CLIENT-SPECIFIC MOBILE STYLES /\/\/\/\/\/\/ */
				body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:none !important;} /* Prevent Webkit platforms from changing default text sizes */
                body{width:100% !important; min-width:100% !important;} /* Prevent iOS Mail from adding padding to the body */

				/* /\/\/\/\/\/\/ MOBILE RESET STYLES /\/\/\/\/\/\/ */
				#bodyCell{padding:10px !important;}

				
				#templateContainer{
					max-width:600px !important;
					/*@editable*/ width:100% !important;
				}

				h1{
					/*@editable*/ font-size:24px !important;
					/*@editable*/ line-height:100% !important;
				}

				h2{
					/*@editable*/ font-size:20px !important;
					/*@editable*/ line-height:100% !important;
				}

				h3{
					/*@editable*/ font-size:18px !important;
					/*@editable*/ line-height:100% !important;
				}

				h4{
					/*@editable*/ font-size:16px !important;
					/*@editable*/ line-height:100% !important;
				}

				/* ======== Header Styles ======== */

				#templatePreheader{display:none !important;} /* Hide the template preheader to save space */

				#headerImage{
					height:auto !important;
					/*@editable*/ max-width:600px !important;
					/*@editable*/ width:100% !important;
				}
				.headerContent{

					/*@editable*/ font-size:20px !important;
					/*@editable*/ line-height:125% !important;
				}

				.bodyContent{
					/*@editable*/ font-size:18px !important;
					/*@editable*/ line-height:125% !important;
				}

				.footerContent{
					/*@editable*/ font-size:14px !important;
					/*@editable*/ line-height:115% !important;
				}

				.footerContent a{display:block !important;} /* Place footer social and utility links on their own lines, for easier access */
			}
		</style>
    </head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
    	<center>
        	<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
            	<tr>
                	<td align="center" valign="top" id="bodyCell">
                    	<!-- BEGIN TEMPLATE // -->
                    	<table border="0" cellpadding="0" cellspacing="0" id="templateContainer">
                        	<tr>
                            	<td align="center" valign="top">
                                	<!-- BEGIN HEADER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateHeader">
                                        <tr>
                                            <td valign="top" class="headerContent">
                                            	<img src="http://www.wrapbrasil.com/email/topo.png" style="max-width:600px;" id="headerImage" mc:label="header_image" mc:edit="header_image" mc:allowdesigner mc:allowtext />
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // END HEADER -->
                                </td>
                            </tr>
                        	<tr>
                            	<td align="center" valign="top">
                                	<!-- BEGIN BODY // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateBody">
                                        <tr>
                                            <td valign="top" class="bodyContent" mc:edit="body_content">
												<p style="font-size:12px;color;#484848;">
													Obrigado por comprar nossos produtos!
												</p>
												
												<p>
												Dados da compra - '.$dataCompra.'
												
												<br><br>
												
												'.$listaProdutos.'
												<br />
												'.$listaProdutos2.'
												</p>
												
												<p style="font-size:12px;color;#484848;">
													Att.
													WRAP BRASIL
												</p>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // END BODY -->
                                </td>
                            </tr>
                        	<tr>
                            	<td align="center" valign="top">
                                	<!-- BEGIN FOOTER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateFooter">
                                        <tr>
                                            <td valign="top" class="footerContent" mc:edit="footer_content00">
                                                <a href="https://www.facebook.com/wrapfoodtruck/">Facebook</a>&nbsp;&nbsp;&nbsp;<a href="https://www.instagram.com/wrapbrasil/">Instagram</a>&nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" class="footerContent" style="padding-top:0;" mc:edit="footer_content01">
                                                <em>Copyright &copy; |2018| WRAPBRASIL|, All rights reserved.</em>
                                                <br />
                                                <strong>Nosso endere&ccedil;o &eacute;:</strong>
                                                <br />
                                                <a href="http://www.wrapbrasil.com/">www.wrapbrasil.com</a>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // END FOOTER -->
                                </td>
                            </tr>
                        </table>
                        <!-- // END TEMPLATE -->
                    </td>
                </tr>
            </table>
        </center>
    </body>
</html>
			';
		
		
		  
		//email1
		$destinatarios = "wrapbrasil2@gmail.com";
		//$destinatarios = "jaojao05@gmail.com";
		$destinatarios2 = $emailEntrega;
		//$nomeDestinatario = $nomeGG;
		$Message = $mensagemEmail;
		$assunto = "Compra Wrap Brasil - ".$dataCompra;
		//$anexo = "MANUAL_DE_PRESCRICAO_DO_TREINAMENTO_DE_OCLUSAO_VASCULAR.pdf";
		$enviado = envia_email($destinatarios,$nomeDestinatario,$Message,$assunto,$anexo);
		$enviado = envia_email($destinatarios2,$nomeDestinatario,$Message,$assunto,$anexo);

		
		/***************************fim e-mail*****************************************/		

		
		unset($_SESSION['listaProdutoCarrinhoSecao']);

		unset($_SESSION['valorTotalPedidoProdutoSecao']);
		
		unset($_SESSION['horario']);

		echo '<SCRIPT LANGUAGE="JavaScript">
				window.location="index.php?ped=sucesso";
			</SCRIPT>';

		exit;


		$smarty->assign('idPedidoProduto', $idPedidoProduto);

 

		$smarty->assign('tituloPagina', ' - Pedido concluído');

		$smarty->assign('templateCentro', 'centro_pedido_finalizar.tpl');


		break;



/********************************** NOVA OP��O **************************************************************/

    case 'limparPedidoProdutoSecao' :


		unset($_SESSION['listaProdutoCarrinhoSecao']);

		unset($_SESSION['valorTotalPedidoProdutoSecao']);
		
		unset($_SESSION['contadorItem']);
		
		unset($_SESSION['horario']);


		echo '<SCRIPT LANGUAGE="JavaScript">
				
				
				window.location="index.php";
			   
				
			</SCRIPT>';


		break;



/********************************** NOVA OP��O ****************************************************************/

    case 'alterarQtdeItem' :

		$listaProdutoCarrinho = $_SESSION['listaProdutoCarrinhoSecao'];
		
		$cotadorProduto = $_POST['cotadorProduto'];
		
		$qtdeProduto = $_POST['qtdeProduto'];
		
        $listaProdutoCarrinhoAux = array();
		
		foreach ($listaProdutoCarrinho as $produto) {
			if($produto["contadorItem"] == $cotadorProduto){
				$produto["qtde"] = $qtdeProduto;
			}
						
			$listaProdutoCarrinhoAux[] = $produto;	
		}
		$listaProdutoCarrinho = $listaProdutoCarrinhoAux;
		

		$_SESSION['listaProdutoCarrinhoSecao'] = $listaProdutoCarrinho;

		echo "sucess";			
		exit;
		

		break;



/********************************** NOVA OP��O ****************************************************************/

    case 'mudaStatusCarrinho' :

		$status = $_POST['status'];
		
		if($status == 0){
			unset($_SESSION['carrinhoAberto']);
		}
		
		if($status == 1){
			$_SESSION['carrinhoAberto'] = "sim";
		}

		

		echo "sucess";			
		exit;
		

		break;



/********************************** NOVA OP��O ****************************************************************/

    case 'removerProdutoPedidoProdutoSecao' :

		$listaProdutoCarrinho = $_SESSION['listaProdutoCarrinhoSecao'];

        $listaProdutoCarrinhoExcluir = $_POST['listaProdutoCarrinhoExcluir'];


        for ($i=0;$i<count($listaProdutoCarrinhoExcluir);$i++) { 
           
			$idProduto = $listaProdutoCarrinhoExcluir[$i];

			$listaProdutoCarrinhoAux = array();


			foreach ($listaProdutoCarrinho as $produto) {

				if($produto["contadorItem"] != $idProduto){

					$listaProdutoCarrinhoAux[] = $produto;
					 
				}

			}

			$listaProdutoCarrinho = $listaProdutoCarrinhoAux;

		}

		foreach ($listaProdutoCarrinho as $produto) {

			$valorTotalPedidoProduto += ($produto["preco"] * $produto["qtde"]);

		}
		  
		if(count($listaProdutoCarrinho) > 0){

			$_SESSION['listaProdutoCarrinhoSecao'] = $listaProdutoCarrinho;

			//$_SESSION['valorTotalPedidoProdutoSecao'] = $valorTotalPedidoProduto;

		}else{

			unset($_SESSION['listaProdutoCarrinhoSecao']);

			unset($_SESSION['valorTotalPedidoProdutoSecao']);
			
			unset($_SESSION['contadorItem']);
			
			unset($_SESSION['horario']);

		}


		echo '<SCRIPT LANGUAGE="JavaScript">
				
				
				window.location="index.php?secao=pedidoProduto&opcao=listarPedidoProdutoSecao";
			   
				
			</SCRIPT>';

		break;


/********************************** NOVA OP��O ****************************************************************/

    case 'removerItemProdutoCarrinho' :

		$listaProdutoCarrinho = $_SESSION['listaProdutoCarrinhoSecao'];

        $cotadorProduto = $_POST['cotadorProduto'];


      	$listaProdutoCarrinhoAux = array();
		
		$contadorItem = 0;

		foreach ($listaProdutoCarrinho as $produto) {
			
			if($produto["contadorItem"] != $cotadorProduto){
				
				$contadorItem = $contadorItem+1;
				$produto["contadorItem"] = $contadorItem;
				
				$listaProdutoCarrinhoAux[] = $produto;
				 
			}

		}

		$listaProdutoCarrinho = $listaProdutoCarrinhoAux;

		$_SESSION['listaProdutoCarrinhoSecao'] = $listaProdutoCarrinho;


		echo "sucess";
		exit;

		break;



/********************************** NOVA OP��O ****************************************************************/

    case 'listarPedidosUsuario' :
	
	//session_start();
	//print_r($_SESSION['usuarioSite']);exit;
		if(!isset($_SESSION['usuarioSite'])){

			echo '<SCRIPT LANGUAGE="JavaScript">
					window.location="index.php?secao=usuarioSite&opcao=logar&paginaRedirecionar=4";
				</SCRIPT>';

		}

		$paginaAtual = 1;
		
		if($_GET['paginaAtual'] != "")
			$paginaAtual = $_GET['paginaAtual']; 


		$numeroMaximoResultadosPagina = 20; 

		$colunaOrdenacao = 1; 

		$tipoOrdenacao = 'desc'; 
 
		if(!$colunaOrdenacao) 
			$colunaOrdenacao = 1; 

		//saber o total de registro da tabela que ser�o pego os dados
		$sqlPaginacao = 'select count(*) as total FROM pedido pp, status_pedido spp WHERE pp.id_status_pedido = spp.id  AND pp.id_usuario_site = '.$_SESSION['usuarioSite'][0];

		$resultadoQuery=$db->Execute($sqlPaginacao);

		$totalResultados = $resultadoQuery->fields['total'];


		//c�lculo do total de p�ginas para a query 
		if( $totalResultados > 0 ) { 
			$totalPaginas = ceil($totalResultados/$numeroMaximoResultadosPagina); 
		} 
		else { 
			$totalPaginas = 0; 
		} 

		//definir a p�gina atual = a total p�gina
		if ($paginaAtual > $totalPaginas) 
			$paginaAtual = $totalPaginas; 

		//calcular a posi��o inicial da query 
		$start = $numeroMaximoResultadosPagina * $paginaAtual - $numeroMaximoResultadosPagina; 

		if($start < 0) 
			$start = 0; 

 			
		$listaNumeroPagina = array();

		for($i = 1; $i <= $totalPaginas; $i++){

			$listaNumeroPagina[] = $i;
		}


		//query para listagem dos dados 
		$sql = 'SELECT pp.id, date_format(pp.data_cadastro, "%d/%m/%Y"), spp.descricao FROM pedido pp, status_pedido spp WHERE pp.id_status_pedido = spp.id  AND pp.id_usuario_site = '.$_SESSION['usuarioSite'][0].' ORDER BY '.$colunaOrdenacao.' '.$tipoOrdenacao.' LIMIT '.$start.', '.$numeroMaximoResultadosPagina;

		$resultadoQuery=$db->Execute($sql);

		echo $db->ErrorMsg();
 
		while(!$resultadoQuery->EOF){

			$valorTotalPedido = 0.0;


			$idPedidoProduto = $resultadoQuery->fields[0];


			$pedidoProduto = array();

			$pedidoProduto[] = $resultadoQuery->fields[0];
			$pedidoProduto[] = $resultadoQuery->fields[1];
			$pedidoProduto[] = $resultadoQuery->fields[2]; 


			$sql = 'SELECT preco_venda, qtde FROM pedido_has_produto WHERE id_pedido = '.$idPedidoProduto; 

			$resultadoQuery2 = $db->Execute($sql);

			while(!$resultadoQuery2->EOF){

				$valorTotalPedido += $resultadoQuery2->fields[0] * $resultadoQuery2->fields[1];

				$resultadoQuery2->MoveNext();

			}



			$valorTotalPedido += $resultadoQuery->fields[4];

			$pedidoProduto[] = $valorTotalPedido;


			$listaPedidoProduto[] = $pedidoProduto;


			$resultadoQuery->MoveNext();

		}


		$smarty->assign('listaPedidoProduto', $listaPedidoProduto);


		$smarty->assign('paginaAtual', $paginaAtual);     

		$smarty->assign('totalPaginas', $totalPaginas);     

		$smarty->assign('totalResultados', $totalResultados);     

		$smarty->assign('listaNumeroPagina', $listaNumeroPagina);     

		$smarty->assign('numeroMaximoResultadosPagina', $numeroMaximoResultadosPagina);     


		$smarty->assign('tituloPagina', ' - Meus pedidos');

		$smarty->assign('templateCentro', 'meus_pedidos.tpl');
		
		//$smarty->assign('templateCentro', 'centro_pedido_produto_usuario_listar.tpl');


		break;


/********************************** NOVA OP��O ****************************************************************/

    case 'verPedidoUsuario' :

		if(!isset($_SESSION['usuarioSite'])){
			echo '<SCRIPT LANGUAGE="JavaScript">
					window.location="index.php?secao=usuarioSite&opcao=logar&paginaRedirecionar=4";
				</SCRIPT>';
		}

		
		$idPedidoProduto = $_GET['idPedidoProduto'];

		if($idPedidoProduto != ''){

			//seleciona os dados
			$sql = 'select pp.id, date_format(pp.data_cadastro, "%d/%m/%Y"), spp.id, spp.descricao, pp.usar_endereco_atual, date_format(pp.data_envio, "%d/%m/%Y"), pp.codigo_correios, pp.presente, pp.msg_presente,
			pp.tipo_frete, pp.valor_frete, pp.valor_presente
			from pedido pp, status_pedido spp where pp.id_status_pedido = spp.id AND pp.id = '.$idPedidoProduto;

			$resultadoQuery=$db->Execute($sql);

			echo $db->ErrorMsg();

			if(!$resultadoQuery->EOF){

				$idPedido = $resultadoQuery->fields[0];
				$usar_endereco_atual = $resultadoQuery->fields[4];
				
				$presente = $resultadoQuery->fields[7];
				$msg_presente = utf8_encode($resultadoQuery->fields[8]);
				$tipo_frete = $resultadoQuery->fields[9];
				$valor_frete = $resultadoQuery->fields[10];
				$valor_presente = $resultadoQuery->fields[11];

				$pedidoProduto = $resultadoQuery->fields;

			}

			
			$sql = 'SELECT p.id, p.nome, p.cod, p.foto, pphp.preco_venda, pphp.qtde, (select c.nome from cor c where c.id = pphp.id_cor) as cor, (select t.tamanho from tamanho t where t.id = pphp.id_tamanho) as tamanho  
			FROM produtos p, pedido_has_produto pphp 
			WHERE p.id = pphp.id_produto AND pphp.id_pedido = '.$idPedidoProduto.' 
			ORDER BY p.id ASC'; 
			
			
			$resultadoQuery = $db->Execute($sql);

			while(!$resultadoQuery->EOF){

				$produto = array();

				$produto[] = $resultadoQuery->fields[0];
				$produto[] = utf8_encode($resultadoQuery->fields[1]);
				$produto[] = $resultadoQuery->fields[2];
				$produto[] = $resultadoQuery->fields[3];
				
				$produto[] = number_format($resultadoQuery->fields[4],2,',','.');

				$produto[] = $resultadoQuery->fields[5];
				
				$produto[] = utf8_encode($resultadoQuery->fields[6]);
				
				$produto[] = utf8_encode($resultadoQuery->fields[7]);

				$produto[] = number_format(($resultadoQuery->fields[5]*$resultadoQuery->fields[4]),2,',','.');

				$valorTotalPedido += $resultadoQuery->fields[4] * $resultadoQuery->fields[5];

				$listaProduto[] = $produto;

				$resultadoQuery->MoveNext();

			}

			
			$valorTotalPedido += $pedidoProduto[8];
			

			if($usar_endereco_atual == '1'){

				$sql = 'SELECT id, nome, endereco, cidade, estado, bairro, cep, telefone, email, complemento, numero 
			FROM clientes where id = '.$_SESSION['usuarioSite'][0];

				$resultadoQuery = $db->Execute($sql);

				if(!$resultadoQuery->EOF){

					$enderecoEntrega = $resultadoQuery->fields;
					$enderecoEntrega[1] = utf8_encode($resultadoQuery->fields[nome]);
					$enderecoEntrega[2] = utf8_encode($resultadoQuery->fields[endereco]);
					$enderecoEntrega[3] = utf8_encode($resultadoQuery->fields[cidade]);
					$enderecoEntrega[4] = utf8_encode($resultadoQuery->fields[estado]);
					$enderecoEntrega[5] = utf8_encode($resultadoQuery->fields[bairro]);
					$enderecoEntrega[6] = utf8_encode($resultadoQuery->fields[cep]);
				}


			}else{

				$sql = 'SELECT id, nome, logradouro, cidade, estado, bairro, cep, telefone1, email_entrega, numero_logradouro, complemento 
			FROM pedido WHERE id = '.$idPedidoProduto;
			
				$resultadoQuery = $db->Execute($sql);

				if(!$resultadoQuery->EOF){

					$enderecoEntrega = $resultadoQuery->fields;
					$enderecoEntrega[1] = utf8_encode($resultadoQuery->fields[nome]);
					$enderecoEntrega[2] = utf8_encode($resultadoQuery->fields[logradouro]);
					$enderecoEntrega[3] = utf8_encode($resultadoQuery->fields[cidade]);
					$enderecoEntrega[4] = utf8_encode($resultadoQuery->fields[estado]);
					$enderecoEntrega[5] = utf8_encode($resultadoQuery->fields[bairro]);
					$enderecoEntrega[6] = utf8_encode($resultadoQuery->fields[cep]);
				}

			}

		}


		$smarty->assign('pedidoProduto', $pedidoProduto);

		$smarty->assign('enderecoEntrega', $enderecoEntrega);

		$smarty->assign('listaProduto', $listaProduto);

		$smarty->assign('valorTotalPedido',  number_format($valorTotalPedido,2,',','.'));
		
		
		$smarty->assign('presente', $presente);
		$smarty->assign('msg_presente', $msg_presente);
		$smarty->assign('tipo_frete', $tipo_frete);
		$smarty->assign('valor_frete', $valor_frete);
		$smarty->assign('valor_presente', $valor_presente);

		$smarty->assign('tituloPagina', ' - Dados do pedido');

		$smarty->assign('templateCentro', 'centro_pedido_produto_usuario_ver.tpl');


		break;


/********************************** NOVA OP��O ****************************************************************/

  	case 'verValorFrete' :
        
	
		include('bibliotecas/http.inc');

		
		$cepSemFormatacao = $_GET['cep'];

		$cep = $_GET['cep'];

		$cep = str_replace("-", "", $cep);

		$valorTotal = $_GET['valorTotal'];

		$pesoTotal = $_GET['pesoTotal'];


		$paramentrosUrl = '/encomendas/precos/calculo.cfm?resposta=paginaCorreios&servico=40010&cepOrigem=31640350&cepDestino='.$cep.'&peso='.$pesoTotal.'&MaoPropria=N&valorDeclarado='.$valorTotal.'&avisoRecebimento=N';


		
		$http_client = new http( HTTP_V11, false );

		$http_client->host = "www.correios.com.br";


		if ($http_client->get($paramentrosUrl, true) == HTTP_STATUS_OK){

			$htmlRetorno = $http_client->get_response_body();

		}else
			print('Server returned '.$http_client->status);


		$htmlRetorno;

		if($htmlRetorno != ''){

			$stringPesquisaInicial = '<b>Valor do Servi&ccedil;o</b>';

			$posInicial = strpos($htmlRetorno, $stringPesquisaInicial) + strlen($stringPesquisaInicial);
			
			$htmlRetorno = substr($htmlRetorno, $posInicial);

			
			$stringPesquisaInicial = '<b>R$ ';

			$posInicial = strpos($htmlRetorno, $stringPesquisaInicial) + strlen($stringPesquisaInicial);

			$htmlRetorno = substr($htmlRetorno, $posInicial);

			$stringPesquisaFinal = '</b>';

			$posFinal = strpos($htmlRetorno, $stringPesquisaFinal);

			$valorFrete = substr($htmlRetorno, 0, $posFinal);
			
		}


		$smarty->assign('cep', $cepSemFormatacao);

		$smarty->assign('logradouro', $logradouro);

		$smarty->assign('bairro', $bairro);

		$smarty->assign('cidade_uf', $cidade_uf);

		$smarty->assign('prazoEntrega', $prazoEntrega);

		$smarty->assign('valorFrete', $valorFrete);


		$smarty-> display('popup_cep_valor_frete.tpl');


		exit;

		break;



/********************************** NOVA OP��O ****************************************************************/

  	case 'consultaCep' :
        
		$cep = $_REQUEST["cep"];
		
		$val = (calcular_frete('35610000',$cep,'0.200',0,'40010'));
		$val2 = (calcular_frete('35610000',$cep,'0.200',0,'41106'));
		
		//verificando se tem produto em estoque
		$estoque = $_REQUEST["estoque"];
		if($estoque == 0 || $estoque == ""){
			$prazo_entrega = $val->PrazoEntrega+$_SESSION["dias_frete_nestoque"];
			$prazo_entrega2 = $val2->PrazoEntrega+$_SESSION["dias_frete_nestoque"];
		}else{
			$prazo_entrega = $val->PrazoEntrega+$_SESSION["dias_frete_estoque"];
			$prazo_entrega2 = $val2->PrazoEntrega+$_SESSION["dias_frete_estoque"];
		}
		
		$_SESSION["cep_digitado"] = substr($cep, 0, 5)."-".substr($cep, 5);
		
		if($cep == "35610000"){
			if($estoque == 0){
				echo "0,00_5_0,00_5";
			}else{
				echo "0,00_1_0,00_1";
			}
		}else{
			echo $val->Valor."_".$prazo_entrega."_".$val2->Valor."_".$prazo_entrega2;
		}
		

		exit;

		break;


/********************************** NOVA OP��O ****************************************************************/

  	case 'validarCupom' :
        
		$cupom = $_REQUEST["cupom"];
		
		$valorCompra = $_REQUEST["valorCompra"];
		
		$sql = 'SELECT * FROM cupons where ativo = 1 and deletado = 0 and LOWER(codigo) = "'.strtolower($cupom).'"';

		$resultadoQuery = $db->Execute($sql);
		$retorno = "";
		if(!$resultadoQuery->EOF){
			if($resultadoQuery->fields["desconto"] <= $valorCompra){
			$_SESSION["cupom_digitado"] = $cupom;
			$_SESSION["cupom_digitado_valor"] = $resultadoQuery->fields["desconto"];
			$_SESSION["cupom_digitado_id"] = $resultadoQuery->fields["id"];
			$_SESSION["cupom_digitado_porcento"] = $resultadoQuery->fields["porcento"];
			if($_SESSION["cupom_digitado_porcento"] == 1){
				$_SESSION["cupom_digitado_valor"] = number_format($_SESSION["cupom_digitado_valor"],0);
			}
			$retorno = "sim";
			}else{
				$retorno = "maior";
			}
		}else{
			$retorno = "nao";
		}
		
		
		echo $retorno;

		exit;

		break;


/********************************** NOVA OP��O ****************************************************************/

  	case 'removerCupom' :
        
		
		unset($_SESSION["cupom_digitado"]);
		unset($_SESSION["cupom_digitado_valor"]);
		unset($_SESSION["cupom_digitado_id"]);
		unset($_SESSION["cupom_digitado_porcento"]);
		$retorno = "sim";
	
		
		echo $retorno;

		exit;

		break;


/********************************** NOVA OP��O ****************************************************************/

  	case 'atualizar' :
         

		header('Content-Type: text/html; charset=ISO-8859-1');

		define('TOKEN', '4CB8ED86F1C04E49B01A7C46FA88FD47');


	
		$timeout = 20; 
	
		function notificationPost() {
			$postdata = 'Comando=validar&Token='.TOKEN;
			foreach ($_POST as $key => $value) {
				$valued    = $clearStr($value);
				$postdata .= "&$key=$valued";
			}
			return verify($postdata);
		}
		
		function clearStr($str) {
			if (!get_magic_quotes_gpc()) {
				$str = addslashes($str);
			}
			return $str;
		}
	
		function verify($data) {
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, "https://pagseguro.uol.com.br/pagseguro-ws/checkout/NPI.jhtml");
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_HEADER, false);
			curl_setopt($curl, CURLOPT_TIMEOUT, $timeout);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			$result = trim(curl_exec($curl));
			curl_close($curl);
			return $result;
		}


		if (count($_POST) > 0) {
			
			// POST recebido.
			$result = notificationPost();

			$transacaoID = isset($_POST['TransacaoID']) ? $_POST['TransacaoID'] : '';

			if ($result == "VERIFICADO") {
				//O post foi validado pelo PagSeguro.
			
				if($_POST['Referencia'] != ""){


					foreach($_POST as $k=>$v) $valorVariavelLog .= $k.": ".htmlentities($v, ENT_QUOTES)."\n";

					$sql = 'INSERT INTO log_controle_site (variavel, valor) VALUES ("Atualiza��o de pagamentos", "'.$valorVariavelLog.'")';

					$db->Execute($sql);


					$idPedido = $_POST['Referencia'];
					
					//$valor = $_POST['valor'];
					//$valor = substr($valor,0,strlen($valor)-2).'.'.substr($valor,-2;


					$idStatusPedido = $_POST['StatusTransacao'];

					switch($idStatusPedido) {
						case "Aprovado": $idStatusPedido = "1"; break;
						case "Aguardando Pagto": $idStatusPedido = "2"; break;
						case "Completo": $idStatusPedido = "1"; break;
						case "Cancelado": $idStatusPedido = "3"; break;
						case "Em An�lise": $idStatusPedido = "4"; break;
					}


					$idTipoPedido = $_POST['TipoPedido'];

					switch($idTipoPedido) {
						case "Pedido Online": $idTipoPedido = "1"; break;
						case "Boleto": $idTipoPedido = "3"; break;
						case "Cart�o de Cr�dito": $idTipoPedido = "4"; break;
						case "Pedido": $idTipoPedido = "6"; break;
					}


					if($idStatusPedido == '1')
						$parametroQuery .= ', data_pagto = now()';
				

					$sql = 'UPDATE pedido SET id_status_pedido = "'.$idStatusPedido.'", id_tipo_pedido = "'.$idTipoPedido.'", valor = "'.$valor.'" '.$parametroQuery.' WHERE id = '.$idPedido; 
					
					$resultadoQueryUpdate=$db->Execute($sql);

					echo $db->ErrorMsg();


				}else{

					echo '<SCRIPT LANGUAGE="JavaScript">

							window.location="index.php";
						
						  </SCRIPT>';

					exit;

				}
		 
		
			} else if ($result == "FALSO") {
				//O post n�o foi validado pelo PagSeguro.
				echo "Erro: O POST n�o foi validado pelo PagSeguro!";

				exit;

			} else {
				//Erro na integra��o com o PagSeguro.
				echo "Erro: Erro na integra��o com o PagSeguro!";

				exit;
			}
			
		} else {
			// POST n�o recebido, indica que a requisi��o � o retorno do Checkout PagSeguro.
			// No t�rmino do checkout o usu�rio � redirecionado para este bloco.
			
			echo '<SCRIPT LANGUAGE="JavaScript">

					window.location="index.php";
				
				  </SCRIPT>';
			
			exit;
		}

		break;

}

?>