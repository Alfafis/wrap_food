<?php

function calcular_local($cep){
	//$cep = '82800-000'; // o cep!
	// parametros passados pela URL
	$postCorreios = "CEP=".$cep."&Metodo=listaLogradouro&TipoConsulta=cep";
	// url para fazer a requisicao
	$cURL = curl_init("http://www.buscacep.correios.com.br/servicos/dnec/consultaLogradouroAction.do");
	// seta opcoes para fazer a requisicao
	curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($cURL, CURLOPT_HEADER, false);
	curl_setopt($cURL, CURLOPT_POST, true);
	curl_setopt($cURL, CURLOPT_POSTFIELDS, $postCorreios);
	// faz a requisicao e retorna o conteudo do endereco
	$saida = curl_exec($cURL);
	curl_close($cURL);// encerra e retorna os dados
	$saida = utf8_encode($saida); // codifica conteudo para utf-8
	$campoTabela = "";
	// pega apenas o conteudo das tds e transforma em um array
	preg_match_all('@<td (.*?)<\/td>@i', $saida, $campoTabela); 
	// mostra o conteudo!
	/*print "\n".strip_tags($campoTabela[0][0]); // rua
	print "\n".strip_tags($campoTabela[0][1]); // bairro
	print "\n".strip_tags($campoTabela[0][2]); // cidade
	print "\n".strip_tags($campoTabela[0][3]); // estado
	print "\n".strip_tags($campoTabela[0][4]); // cep*/	
	
	return $campoTabela[0][2]." - ".$campoTabela[0][3];
}

function calcular_frete($cep_origem,
	$cep_destino,
	$peso,
	$valor,
	$tipo_do_frete,
	$altura = 6,
	$largura = 20,
	$comprimento = 20){


	$url = "http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx?";
	$url .= "nCdEmpresa=";
	$url .= "&sDsSenha=";
	$url .= "&sCepOrigem=" . $cep_origem;
	$url .= "&sCepDestino=" . $cep_destino;
	$url .= "&nVlPeso=" . $peso;
	$url .= "&nVlLargura=" . $largura;
	$url .= "&nVlAltura=" . $altura;
	$url .= "&nCdFormato=1";
	$url .= "&nVlComprimento=" . $comprimento;
	$url .= "&sCdMaoProria=n";
	$url .= "&nVlValorDeclarado=" . $valor;
	$url .= "&sCdAvisoRecebimento=n";
	$url .= "&nCdServico=" . $tipo_do_frete;
	$url .= "&nVlDiametro=0";
	$url .= "&StrRetorno=xml";

	//Sedex: 40010
	//Pac: 41106

	$xml = simplexml_load_file($url);

	return $xml->cServico;

}

$val = (calcular_frete('30431020',
	'30411052',
	1,
	1000,
	'41106'));

echo "Valor PAC: R$ ".calcular_local("30431-020")."<br>";
echo "Valor PAC: R$ ".calcular_local("30411-052")."<br>";
echo "Valor PAC: R$ ".$val->Valor."<br>";
echo "Valor PAC: R$ ".$val->Valor."<br>";

?>