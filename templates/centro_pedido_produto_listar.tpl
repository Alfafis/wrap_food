{literal}
<style type="text/css">
.ui-dialog .ui-dialog-buttonpane{
text-align:center;	
}
.ui-dialog .ui-dialog-buttonpane button{
float:none;
text-align:center;	
}
.ui-dialog .ui-dialog-buttonpane .ui-dialog-buttonset{
float:none;
text-align:center;	
}
</style>
{/literal}
<div id="container_swip_carrinho">
	<br /><br /><br />
  	
    <form id="form_pedido_produto" method="post" action="" name="form_pedido_produto">
    
    {if count($listaProdutoCarrinho)>0}
    <center>ESTES SÃO OS ITENS QUE VOCÊ SELECIONOU PARA COMPRA, CONFIRA ABAIXO:</center>
    {else}
    <center>Seu carrinho est&aacute; vazio.</center>
    <br />
    <div class="row" onClick="location.href='index.php?secao=produtos'">
        <div class="col button button-assertive"> 
            <span class="text-sm">CONTINUAR COMPRANDO</span> 
        </div>
    </div>
    {/if}
    
    {if count($listaProdutoCarrinho)>0}
    <div class="list">
    
    {foreach from=$listaProdutoCarrinho  item=produtoCarrinho}
    
    <input type='hidden'name='listaProdutoQtd[]' id="qtde{$produtoCarrinho.contadorItem}" value='{$produtoCarrinho.qtde}'>
    <input type='hidden' name='listaProdutoContador[]' id="cont{$produtoCarrinho.contadorItem}" value='{$produtoCarrinho.contadorItem}'>
    
    <div class="item-thumbnail-left card item item-complex item-right-editable">
    
        <div class="item-content" id="aparecer_delete">
        	
            
            <img src="fotos/{$produtoCarrinho.fotoProduto}">
            <h2 class="ng-binding">
            {$produtoCarrinho.nomeProduto}
            <a style="font-size:30px;position:absolute;right:0;z-index:30;top:0;" onclick="removeItem({$produtoCarrinho.contadorItem})">
            	<i class="icon ion-trash-a padding"></i>
            </a>
            </h2>
            
            <div>
            <h2 class="price pull-left assertive ng-binding" style="line-height:normal;max-width:55%">R$ {$produtoCarrinho.precoProduto|replace:".":","} 
            <span class="dark ng-binding">x <span id="txtqtde{$produtoCarrinho.contadorItem}">{$produtoCarrinho.qtde}</span></span><br />
            {if $produtoCarrinho.idPao != 3}
                <span style="font-size:11px;color:#333;line-height:normal;">Pão: {$produtoCarrinho.nomePao}</span><br />
                
                <span style="font-size:11px;color:#333;line-height:normal;">
                {if $produtoCarrinho.nomeDoce == ""}
                Queijo: {$produtoCarrinho.nomeQueijo}
                {else}
                Doce: {$produtoCarrinho.nomeDoce}
                {/if}
                </span>
                {if $produtoCarrinho.nomeSuco != ""}<br /><span style="font-size:11px;color:#333;line-height:normal;">Suco: {$produtoCarrinho.nomeSuco} - R$ {$produtoCarrinho.precoSuco|replace:".":","}</span>{/if}
            {else}
                <span style="font-size:11px;color:#333;line-height:normal;">
                <strong>Adicionais:</strong><br />{$produtoCarrinho.nomeSuco}
                <br /><strong>Total Adicionais:</strong> R$ {$produtoCarrinho.precoSuco|replace:".":","}</span>
            {/if}
            </h2>
            
            <div class="pull-right button button-assertive button-card button-qty" style="margin-top:17px;" onclick="aumentar_quantidade({$produtoCarrinho.contadorItem})"><i class="ion-plus"></i></div>
            <div class="pull-right button button-assertive button-card button-qty" style="margin-top:17px;" onclick="diminuir_quantidade({$produtoCarrinho.contadorItem})"><i class="ion-minus"></i></div>
            
            </div>
        
        </div>
        
        <div class="item-options">
            <div class="stable-bg button" onclick="removeItem({$produtoCarrinho.contadorItem})">
            <i class="icon ion-trash-a padding"></i>
            </div>
        </div>
    
    </div>
    {/foreach}
    
    {if $desconto_cupons == "sim"}
    <div class="item-thumbnail-left card item item-complex item-right-editable">
    
        <div class="item-content" id="aparecer_delete">
        
            <img src="img/logo.png">
            <h2 class="ng-binding">Desconto de {$desconto}%</h2>
            
            <div>
            <h2 class="price pull-left assertive ng-binding" style="line-height:normal;">
            	<span style="font-size:11px;color:#333;line-height:normal;">Desconto por ter completado {$ticket} cupons.</span><br />
            	R$ {$descontoTotal|replace:".":","} 
            </h2>
                        
            </div>
        
        </div>
        
    
    </div>
    {/if}
    
    
    <label class="item item-input item-stacked-label">
      <span class="input-label" aria-label="email" id="_label-14">Agendar horário para buscar:</span>
      <input type="text" name="horario" id="horario" placeholder="hh:mm" value="{$horario}" maxlength="5" onkeypress="mascara(this, mascaraHora)">
    </label>
    
    <br /><br /><br />
    
    </div>
    {/if}
    </form>
  
  <div class="clearfix"></div>    
    
  <ion-footer-bar class="bar-assertive bar bar-footer" onclick="finalizaCarrinho()">
    <div class="button">COMPRAR</div>
    <div class="title" style="left: 111px; right: 111px;"></div>
    <div class="button text-2x ng-binding">
    R$ {if $valorTotalPedidoProduto}{$valorTotalPedidoProduto|replace:".":","}{else}0,00{/if}
    </div>
  </ion-footer-bar>
  <div class="clearfix"></div>
</div><!-- fim cotainer sw -->
<!--
{literal}
<script type="text/javascript">
$(function () {
    
  $(".item-content").swipe( {
	//Generic swipe handler for all directions
	swipe:function(event, direction, distance, duration, fingerCount, fingerData) {
		if (direction == 'left') abrir_delete();
		if (direction == 'right') fechar_delete();  
	},
	//Default is 75px, set to 0 for demo so any distance triggers swipe
	 threshold:0
  });
  
});
</script>
{/literal}-->
