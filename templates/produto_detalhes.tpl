{literal}
<script type="text/javascript">


function valida_compra_doce(){
	
	if($(".doce:checked").length == 0){
		//alert("Selecione pelo menos 1 Carboidrato.");
		alertas("Selecione pelo menos 1 recheio doce");
		return false;
	}
	
	document.compra_produto.submit();	
}


function valida_compra(){
	
	/*if($(".queijo:checked").length == 0){
		//alert("Selecione pelo menos 1 Carboidrato.");
		alertas("Selecione pelo menos 1 queijo.");
		return false;
	}*/
	
	document.compra_produto.submit();	
}


function verifica_salada(id){
	
	/*if($(".salada:checked").length > 3){
		$(id).prop("checked", false);	
	}*/
	
}

function verifica_molho(id){
	
	if($(".molho:checked").length > 2){
		$(id).prop("checked", false);	
	}
	
}

function desabilita_todos(id){
	if($(".doce:checked").length > 1){
		$(id).prop("checked", false);	
	}
	
	if($(".doce:checked").length > 0){
		$(".queijo").prop("disabled", true);
		$(".queijo").prop("checked", false);	
		$(".salada").prop("disabled", true);
		$(".salada").prop("checked", false);
		$(".molho").prop("disabled", true);
		$(".molho").prop("checked", false);
		$(".livre").prop("disabled", true);
		$(".livre").prop("checked", false);
	}else{
		$(".queijo").prop("disabled", false);
		$(".salada").prop("disabled", false);
		$(".molho").prop("disabled", false);
		$(".livre").prop("disabled", false);	
	}
}

function desabilita_doce(id){
	if($(".queijo:checked").length > 1){
		$(id).prop("checked", false);	
	}
	
	if($(".queijo:checked").length > 0){
		$(".doce").prop("checked", false);
		$(".doce").prop("disabled", true);
	}else{
		$(".doce").prop("disabled", false);
	}
}

</script>
{/literal}
<div id="container_swip" style="overflow:hidden;"> 
  <br />
  <br />
  <div class="list no-padding">
    <div class="item item-menu" ng-repeat="category in categories"> <img src="fotos/{$produto.foto}" alt=""/>
      <div class="overlay"> <span class="pull-left light ng-binding"> <span style="font-size:14px;color:#FFF;">{$produto.descricao}</span> </span> <span class="pull-right light ng-binding"> R$ {$produto.preco|replace:".":","} </span> </div>
    </div>
  </div>
  
  <p class="text-center">
  Pão escolhido: <strong>{$paoEscolhido}</strong>
  <br />Tempo de preparo: <strong>{$produto.tempo}</strong>
  </p>
   
  <form name="compra_produto" id="compra_produto" action="?secao=pedidoProduto&opcao=salvarPedidoProdutoSecao" method="post">
  
  <input type="hidden" name="idPao" id="idPao" value="{$idPao}" />
  <input type="hidden" name="idProd" id="idProd" value="{$idProd}" />
  	
            
  <div class="disable-user-behavior">
    <div class="list">
      
      {if $produto.categoria == "Item Doce"} <!-- se for wrap doce-->
      <div class="item-divider item"> Escolha o recheio doce (1 opção) </div>
		       
      {foreach from=$listaDoce item=doce}  
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" name="doce" class="doce" id="doce{$doce.id}" value="{$doce.id}" onclick="desabilita_todos('#doce{$doce.id}')">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding">{$doce.nome}</span></div>
      </label>
      {/foreach}
    
      {else}<!-- se não for wrap doce -->
      
      <div class="item-divider item"> Escolha o queijo (1 opção) </div>
		
      {foreach from=$listaQuijo item=queijo}  
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" name="queijo" class="queijo" id="queijo{$queijo.id}" value="{$queijo.id}" onclick="desabilita_doce('#queijo{$queijo.id}')">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding">{$queijo.nome}</span></div>
      </label>
      {/foreach}
      
      <div class="item-divider item"> Escolha as saladas<!-- (3 opções) --></div>
      
      {foreach from=$listaSalada item=salada}
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" class="salada" id="salada{$salada.id}" name="salada[]" value="{$salada.id}" onclick="verifica_salada('#salada{$salada.id}')">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding">{$salada.nome}</span></div>
      </label>
      {/foreach}
      
      <div class="item-divider item"> Escolha os molhos (2 opções) </div>
      
      {foreach from=$listaMolho item=molho}
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" class="molho" id="molho{$molho.id}" name="molho[]" value="{$molho.id}" onclick="verifica_molho('#molho{$molho.id}')">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding">{$molho.nome}</span></div>
      </label>
      {/foreach}
      
      
      <div class="item-divider item"> Escolha livre </div>
      
      {foreach from=$listaLivre item=livre}
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" class="livre" name="livre[]" value="{$livre.id}">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding">{$livre.nome}</span></div>
      </label>
      {/foreach}
      
      {/if}<!-- fim se for wrap doce -->
      
      <div class="item-divider item"> Escolha o suco </div>
      
      {foreach from=$listaSuco item=suco}
      <label class="item-checkbox-right checkbox-assertive item item-checkbox">
      <div class="checkbox checkbox-input-hidden disable-pointer-events checkbox-circle">
        <input type="checkbox" name="suco[]" value="{$suco.id}">
        <i class="checkbox-icon"></i>
      </div>
      <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding">{$suco.nome} - R$ {$suco.preco|replace:".":","}</span></div>
      </label>
      {/foreach}
      
      
      <!--<label class="item item-radio ng-valid" ng-model="item.size" ng-repeat="size in item.sizes" value="2">
      <input name="radio-group" class="ng-pristine ng-untouched ng-valid" value="2" type="radio">
      <div class="radio-content">
        <div class="item-content disable-pointer-events" ng-transclude=""><span class="ng-binding">Large: $12.00</span></div>
        <i class="radio-icon disable-pointer-events icon ion-checkmark"></i>
      </div>
      </label>-->
      <!-- end ngRepeat: size in item.sizes --> 
    </div>
  </div>
  </form>
  
  {if $produto.categoria == "Item Doce"}
  <div class="row item-button-group">
    <div class="col button button-assertive" onclick="valida_compra_doce()"> 
        <i class="text-2x ion-ios-cart-outline"></i> 
        <span class="text-sm">ADICIONAR AO CARRINHO</span> 
    </div>
  </div>
  {else}
  <div class="row item-button-group">
    <div class="col button button-assertive" onclick="valida_compra()"> 
        <i class="text-2x ion-ios-cart-outline"></i> 
        <span class="text-sm">ADICIONAR AO CARRINHO</span> 
    </div>
  </div>
  {/if}
  
  
</div>
<!-- fim cotainer sw -->
