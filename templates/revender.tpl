{literal}
<script type="text/javascript" src="https://d335luupugsy2.cloudfront.net/js/integration/stable/rd-js-integration.min.js"></script>  
<script type="text/javascript">
    var meus_campos = {
        'nome': 'nome',
        'email': 'email',
        'telefone': 'telefone'
     };
    options = { fieldMapping: meus_campos };
    RdIntegration.integrate('d6707cfe2bafeab06e37276ff65c513a', 'Formulário Revenda', options);  
</script>
<script type="text/javascript">
function validarFormulario(){
		
	
		if(document.revenda.nome.value == ""){
			alert("Digite o Nome!");
			document.revenda.nome.focus();
			return false;
		}
		
		if(document.revenda.email.value == ""){
			alert("Digite o E-mail!");
			document.revenda.email.focus();
			return false;
		}else{
			if(validarEmail(document.revenda.email.value) == false){
					alert("Informe um E-mail válido.");
					document.revenda.email.focus();
					return false;
				}
			}
		
		if(document.revenda.telefone.value == ""){
			alert("Digite o Telefone!");
			document.revenda.telefone.focus();
			return false;
		}
		
 
		document.revenda.submit();
	}
	
	</script>
   {/literal} 
   

            <!--Inicio parte baixo banner-->
            {include file=$links_principal}
            <!--Fim parte baixo banner-->
      
                        
            <div class="principal_revender">
            	
                <div class="row">
                    <div class="col-md-7">					
                        <div class="titulo_secoes" style="text-align:left;"><img src="img/marcador_tit.png" > REVENDER</div>
                        {$textos.revenda}
                    </div>
                    <div class="col-md-5">					
                        <div class="borda_img_revender"><img src="img/img_revender.jpg"></div>
                    </div>
                </div>
                
                <div class="titulo_secoes" style="margin-top:80px;"><img src="img/marcador_tit.png" > CONFIRA AS VANTAGENS DE SE TORNAR UM REVENDEDOR VICENZI</div>
                
                <div class="row">
                    <div class="col-md-12">					
                    	<div class="quadro_revender_meio">
                        	<div class="caixa_diamantes">
                            	<img src="img/diamante.png"> Coleções Exclusivas
                            </div>
                            <div class="caixa_diamantes">
                            	<img src="img/diamante.png"> Novidades Semanais
                            </div>
                            <div class="caixa_diamantes">
                            	<img src="img/diamante.png"> Promoções e Descontos
                            </div>
                            <div class="caixa_diamantes">
                            	<img src="img/diamante.png"> Suporte Exclusivo
                            </div>
                            <div class="caixa_diamantes">
                            	<img src="img/diamante.png"> Variedade de Produtos
                            </div>
                            <div class="caixa_diamantes">
                            	<img src="img/diamante.png"> Material Gráfico para Divulgação
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            
                        
            <div class="box_revender">
            	<div class="titulo_contato"><img src="img/marcador_tit.png" > CADASTRE-SE E SAIBA MAIS COMO SE TORNAR UM REVENDEDOR</div>
                <div class="row">
                    <div class="col-md-12" align="center">					
                        
                        	<div class="quadro_contato_meio">
                            	
                                <form name="revenda" class="formulario_contato" action="index.php?secao=contato&opcao=revenda" method="post">
                                	<input type="hidden" name="env" value="222" />
                                    <p>Nome: (*)</p>
                                    <input type="text" class="form-control campos_contato" name="nome" id="nome">
                                  
                                    <p><em>E-mail</em>: (*)</p>
                                    <input type="text" class="form-control campos_contato" name="email" id="email">
                                  
                                    <p>Telefone: (*)</p>
                                    <input type="text" class="form-control campos_contato" name="telefone" id="telefone" placeholder="(xx) xxxxx-xxxx" name="telefone" maxlength="15" onkeypress="mascara(this, mascaraTelefone)">
                                  
                                  <div class="clearfix"></div>
                                  <br><br>
                                  <div onclick="validarFormulario()" style="cursor:pointer;"><img src="img/bt_torna_revendendor.jpg" width="100%" /></div>
                                </form>
                            </div>
                        
                    </div>
                </div>
            </div>