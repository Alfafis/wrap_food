<div class="conteudo-interno">
    		
	    		<div id="borda-transparente-pagina">
	    			
		    		<div class="conteudo-pagina">
                    
                    <div class="loja-catalogos-destaques">
		    				
		    				<div class="menu-nav-interno">Home / Meus pedidos</div>
		    				
                            <!--Inicio rodape-->
                            {include file=$link_carrinho}
                            <!--Fim rodape-->
		    				
		    			</div>
                    
			 <br  /><br  /><br  /><br  />
			   <div style="width:95%;margin:auto;">
               
				{if $listaPedidoProduto neq ""}
				<div class="box_cadastrar" style="height:auto;text-align:center;padding:10px 0">
			      <span class="text_endereco" style="font-size:12px;">
					 Aten&ccedil;&atilde;o: atualizamos esta p&aacute;gina todos os dias &agrave;s: 10h00, 14h00, 17h00 e 22h00.<br />
Portanto, acompanhe o andamento do seu pedido, consultando-a ap&oacute;s estes hor&aacute;rios. 
				  </span>
			    </div>
				<br />
				<table id="tabela_meus_pedidos">
				  <tr>
					<td width="150px" style="vertical-align:bottom;text-align:center"><span class="text_tab_pag">N&deg; do pedido</span></td>
					<td width="110px" style="vertical-align:bottom;text-align:center"><span class="text_tab_pag">Data do pedido</span></td>
					<td width="130px" style="vertical-align:bottom;text-align:center"><span class="text_tab_pag">Total</span></td> 
					<td width="160px" style="vertical-align:bottom;text-align:center"><span class="text_tab_pag">Status:</span></td>
                    <td></td>
				  </tr>
				  <tr>
				    <td colspan="5"><div style="border-top:solid 1px #BAB3D6;"></div></td>
				  </tr>
				  
				  {assign var="corLinha" value="EBECEE"}	
					
				  	{foreach from=$listaPedidoProduto  item=pedidoProduto }
					
				  <tr bgcolor="#{$corLinha}">
                      <td width="150px" style="vertical-align:bottom;text-align:center">{$pedidoProduto[0]}</td>
                        <td width="110px" style="vertical-align:bottom;text-align:center">{$pedidoProduto[1]}</td>
                        <td width="130px" style="vertical-align:bottom;text-align:center">
                        {assign var="valorTotalPedidoProduto" value=$pedidoProduto[3]|string_format:"%.2f"}								
						{assign var="valorTotalPedidoProduto" value=$valorTotalPedidoProduto|replace:".":","}
                        R$ {$valorTotalPedidoProduto}</td> 
                        <td width="160px" style="vertical-align:bottom;text-align:center">{$pedidoProduto[2]}</td>
                        <td id="link_ver_pedido"><a href="index.php?secao=pedidoProduto&opcao=verPedidoUsuario&idPedidoProduto={$pedidoProduto[0]}">Ver pedido</a></td>
                   </tr>
				   
				  <tr>
				    <td colspan="5"><div style="border-top:solid 1px #BAB3D6;"></div></td>
				  </tr>
				  
				  {if $corLinha eq "EBECEE"}
					
					{assign var="corLinha" value="d6d6d6"}
					
				 {else}
				
					{assign var="corLinha" value="EBECEE"}
				
				 {/if}
				
				
				 {/foreach}
				
				</table>
				<br />
				<br />
				<center>
				
					  {if $totalResultados neq "0"}
				  		<div class="paginacao">
						{if $paginaAtual neq "1"}
						
						{assign var="numeroPagina" value="`$paginaAtual-1`"}
											 
						<a href="index.php?secao=pedidoProduto&opcao=listarPedidosUsuario&paginaAtual={$numeroPagina}"> << Anterior </a>
						
						{/if}	
								
								
						{if $totalResultados gt $numeroMaximoResultadosPagina}	
							
						{foreach from=$listaNumeroPagina item=numeroPagina}
						<a href="index.php?secao=pedidoProduto&opcao=listarPedidosUsuario&paginaAtual={$numeroPagina}">{$numeroPagina}</a>
						{/foreach}
						
						{/if}
						
						
						{if $paginaAtual neq $totalPaginas}
						
						{assign var="numeroPagina" value="`$paginaAtual+1`"}
							
						<a href="index.php?secao=pedidoProduto&opcao=listarPedidosUsuario&paginaAtual={$numeroPagina}">Pr&oacute;xima >> </a>
						{/if}
						
					  </div> 
					  {/if}
					  
				</center>
				{else}
				<div class="box_cadastrar" style="margin-top:5px;padding-left:10px;height:42px;padding-bottom:5px; width:537px;;" >
			      <span class="text_endereco" style="font-size:15px;">
					  <br />
					  <center><B>N&atilde;o existe nenhum pedido cadastrado.</B></center>
				  </span>
			    </div>
				{/if}
				
			  <br />
				<div style="float:right;margin-right:10px;"><a href="index.php?secao=usuarioSite"><img src="img/bt_voltar.jpg" alt="" /></a></div>
			  </div>
              
              
              
              
              <!--Inicio rodape-->
            {include file=$rodape}
            <!--Fim rodape-->
              </div>
              </div>
              </div>