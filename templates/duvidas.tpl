<div class="box_secundarias">
    <div class="titulo_secoes"><img src="img/marcador_tit.png" > DICAS E DÚVIDAS</div>
    
    <div class="row">
        <div class="col-md-12">					
            
            <div id="borda_secundarias">
                <div class="caixa_botoes_sec">
                    <div class="botoes_sec {if $opcao== 'faq'}botoes_sec_ativo{/if}" id="btnbloco_1" onClick="abrir_bloco(1,5)">FAQ</div>
                    <div class="botoes_sec {if $opcao== 'cuidados'}botoes_sec_ativo{/if}" id="btnbloco_2" onClick="abrir_bloco(2,5)">CUIDADOS</div>
                    <div class="botoes_sec {if $opcao== 'garantia'}botoes_sec_ativo{/if}" id="btnbloco_3" onClick="abrir_bloco(3,5)">GARANTIA</div>
                    <div class="botoes_sec {if $opcao== 'promocoes'}botoes_sec_ativo{/if}" id="btnbloco_4" onClick="abrir_bloco(4,5)">PROMOÇÕES</div>
                    <div class="botoes_sec {if $opcao== 'dicas'}botoes_sec_ativo{/if}" id="btnbloco_5" onClick="abrir_bloco(5,5)">DICAS</div>
                </div>
                <div class="caixa_conteudo_sec">
                    <div id="bloco1" {if $opcao== 'faq'}style="display:block;"{else}style="display:none;"{/if}>
                        <div class="texto_conteudo_sec">
                            {$textos.faq}
                        </div>
                    </div>
                    <div id="bloco2" {if $opcao== 'cuidados'}style="display:block;"{else}style="display:none;"{/if}>
                        <div class="texto_conteudo_sec">
                            {$textos.cuidados}
                        </div>
                    </div>
                    <div id="bloco3" {if $opcao== 'garantia'}style="display:block;"{else}style="display:none;"{/if}>
                        <div class="texto_conteudo_sec">
                            {$textos.garantia}
                        </div>
                    </div>
                    <div id="bloco4" {if $opcao== 'promocoes'}style="display:block;"{else}style="display:none;"{/if}>
                        <div class="texto_conteudo_sec">
                            {$textos.promocoes}
                        </div>
                    </div>
                    
                    <div id="bloco5" {if $opcao== 'dicas'}style="display:block;"{else}style="display:none;"{/if}>
                        <div class="texto_conteudo_sec">
                            {$textos.dicas}
                        </div>
                    </div>
                    
                </div>
            </div>
            
        </div>
    </div>
    
</div>