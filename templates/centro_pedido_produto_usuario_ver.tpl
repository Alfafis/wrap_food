<div class="principal_revender">
            	
    <div class="row">
    
        <div class="col-md-12">					
            <div style="width:95%;margin:auto;margin-top:40px;">
               
               	<table cellpadding="0" cellspacing="0" width="80%" style="margin:auto;">
                	<tr>
                    	<td>
                        	<div class="box_ver_detalhes_pedido" >
                                <span class="text_endereco" style="font-size:18px; text-align:center;">
                                	<br />
                                    <center>
                                    <span style="color:#393939">N&uacute;mero do seu pedido:<br />  
                                    <span style="font-size:25px;"><strong>{$pedidoProduto[0]}</strong></span>
                                    </span>
                                    </center>
                                </span>
                            </div>
                        </td>
                        <td>
                        	<div class="box_ver_detalhes_pedido" style="padding-left:15px;">
                              <span class="text_endereco" style="font-size:12px;">
                                  <span style="color:#393939;">Status do pedido:</span>  
                                  
                                    <span class="" style="font-size:14px;">
                                    <strong>{$pedidoProduto[3]}</strong>	
                                    </span>
                                  
                                  <br /><br />
                                  <span style="color:#393939">	
                                    {if $pedidoProduto[2] eq "8"}
                                                     
                                    Data de envio: <strong>{$pedidoProduto[5]}</strong><BR />
                                    C&oacute;digo dos Correios:  <strong>{$pedidoProduto[6]}</strong><BR />
                                    <a href="#" onclick='window.open("http://websro.correios.com.br/sro_bin/txect01$.QueryList?P_LINGUA=001&P_TIPO=001&P_COD_UNI={$pedidoProduto[6]}", "Nova" , "width=550, height=400, scrollbars=yes, resizable=no, toolbar=no");'>Acompanhe seu pedido</a>
                                    
                                    {else}
                                    
                                    {if $pedidoProduto[2] neq "2"}
                                    Aten&ccedil;&atilde;o: atualizamos esta p&aacute;gina todos os dias &agrave;s: 10h00, 14h00, 17h00 e 22h00.<br />
            Portanto, acompanhe o andamento do seu pedido, consultando-a ap&oacute;s estes hor&aacute;rios. 
                                    {/if}
                                                                    
                                    {/if} 
                                    
                                    </span>
                              </span>
                            </div>
                        </td>
                    </tr>
                </table>
               
               <br /><br />
               
               <span class="tit_pagamento">DADOS DE SUA COMPRA:</span>
				<br /><br />
                <div class="tit_itens_carrinho esconde_cel">
                    <div class="row">
                        <div class="col-md-6" align="left">Produto</div>
                        <div class="col-md-2" align="center">Qtd.</div>
                        <div class="col-md-2" align="center">Pre&ccedil;o p/ Item</div>
                        <div class="col-md-2" align="center">Valor Total</div>
                    </div>
                </div>
                    
                    
                {foreach from=$listaProduto  item=produtoCarrinho }
                <div class="itens_carrinho">
                
                 <div class="row">
                    <div class="col-md-6" align="left" style="min-width:250px;text-transform:uppercase;">
                        <img src="fotos/{$produtoCarrinho[3]}" width="50" height="50" alt="{$produtoCarrinho[2]} - {$produtoCarrinho[1]}" title="{$produtoCarrinho[2]} - {$produtoCarrinho[1]}" style="border:solid 1px #999999;margin-right:10px;float:left;" />
                         <div class="tit_prod_carrinho">{$produtoCarrinho[2]} - {$produtoCarrinho[1]}<br /> Tamanho: {$produtoCarrinho[7]}</div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-md-2 col-sm-2" align="center">
                        <div class="alinha_qtd_cel">
                        {$produtoCarrinho[5]}
                        </div>
                    </div>
                    <div class="col-md-2" align="center">
                        <div class="alinha_preco_item_cel">
                        <div class="texto_carrinho">
                           R$ {$produtoCarrinho[4]}
                        </div>
                       
                        </div>
                    </div>
                    <div class="col-md-2" align="center">
                        <div class="alinha_preco_itens_cel">
                        <div class="texto_carrinho" id="valor_total_item{$produtoCarrinho.contadorItem}">
                        	R$ {$produtoCarrinho[8]}
                        </div>
                        </div>
                    </div>
                </div>
                
            </div>
            {/foreach}
				
                <div id="box_baixo_itens_carrinho">
                     <div class="tit_itens_carrinho" style="width:240px;position:absolute;right:120px;top:0px;">
                        Valor Total Compra:
                    </div>
                    
                    <div class="texto_carrinho2" id="valor_total" style="position:absolute;left:819px;top:20px;">
                        R$ {$valorTotalPedido}
                        <input type="hidden" name="valorTotalPedido" id="valorTotalPedido" value='{$valorTotalPedidoProduto2|replace:",":"."}' />
                    </div>
                    
                </div>
				
              	{if $presente == "sim"}
                  <strong>Valor do presente:</strong> <span style="font-family:lator;"><br />R$ {$valor_presente|replace:".":","}</span>
                  <br />
                  <br />
                    <strong>Mensagem do presente:</strong> <span style="font-family:lator;"><br />{$msg_presente}</span>
                {/if}  
                  
			  <br /> <br /> <br /> <br />
			  <center><span class="tit_endereco">ENDERE&Ccedil;O PARA ENTREGA</span></center>
			  
			  <div class="box_ver_detalhes_pedido" style="text-align:center;width:320px;height:110px;text-transform:uppercase;">
			    <span class="text_endereco" style="font-size:14px;">
				  <strong>{$enderecoEntrega[1]}</strong><br /> 
                  {$enderecoEntrega[2]} - {$enderecoEntrega[10]}{if $enderecoEntrega[9] != ""} - {$enderecoEntrega[9]}{/if}<br />
				{$enderecoEntrega[5]} - {$enderecoEntrega[3]} - {$enderecoEntrega[4]} - BR<br />
				CEP: {$enderecoEntrega[6]}<br />
                {if $tipo_frete == 4}
                <strong>Frete grátis.</strong></strong>
                 {/if}
                 {if $tipo_frete == 3}
                 <strong>Dores do Indaiá - Frete grátis.</strong></strong>
                 {/if}
                 {if $tipo_frete == 1 ||  $tipo_frete == 2}
                 {if $valor_frete != ""}Frete escolhido: <strong>{if $tipo_frete == 1}Sedex{else}PAC{/if} - R$ {$valor_frete|replace:".":","}{/if}</strong>
                 {/if}
				</span>
			    </div>
				<br />
                <center>
				<div style="margin-right:10px;"><a href="index.php?secao=pedidoProduto&opcao=listarPedidosUsuario"><img src="img/bt_voltar.jpg" alt="VOLTAR" /></a></div>
                </center>
				</div>
        </div>
        
    </div>
    
  
    
</div>
