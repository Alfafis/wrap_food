{literal}
<script src="js/funcoes_c.js"></script>
{/literal}
<div id="carrinho_flutuante">
	<div id="carrinho_flutuante_tit">CARRINHO DE COMPRAS</div>
    <div id="carrinho_flutuante_fechar" onClick="fecha_carrinho()"><img src="img/close.png"></div>
    
    <div id="carrinho_flutuante_itens">
    
        <form id="form_pedido_produto" method="post" action="" name="form_pedido_produto">
            
           {if $listaProdutoCarrinho neq ""}
            {else}
            <div class="titulo_secoes_carrinho"><span>Seu carrinho est&aacute; vazio.</span></div>
           {/if}
           <div id="box_carrinho">
                
                {if $listaProdutoCarrinho neq ""}
                
                {foreach from=$listaProdutoCarrinho  item=produtoCarrinho }
                <div class="itens_carrinho" style="padding:10px 0">
                
                    <input type='hidden' name='listaProdutoCarrinhoId[]' value='{$produtoCarrinho.id}'>
                    <input type='hidden' name='listaProdutoCarrinhoNome[]' value='{$produtoCarrinho.nome}'>
                    <input type='hidden' name='listaProdutoCarrinhoPrecoVendaVarejo[]' id="listaProdutoCarrinhoPrecoVendaVarejo" value='{$produtoCarrinho.preco}'>
                    <input type='hidden' name='listaProdutoCarrinhoPrecoVendaVarejoFormatado[]' value='{$produtoCarrinho.valor}'>
                    <input type='hidden' name='listaProdutoCarrinhoArquivoFoto[]' value='{$produtoCarrinho.foto}'>
                    <input type='hidden' name='listaProdutoCarrinhoPeso[]' value='{$produtoCarrinho.peso}'>
                    <input type='hidden' name='listaProdutoTamanho[]' value='{$produtoCarrinho.idTamanho}'>
                    <input type='hidden' name='listaProdutoQtd[]' value='{$produtoCarrinho.qtde}'>
                    <input type='hidden' name='listaProdutoContador[]' value='{$produtoCarrinho.contadorItem}'>
                    <input type='hidden' name='listaProdutoComDesconto[]' value='{$produtoCarrinho.produtoComDesconto}'>
                    <input type='hidden' name='listaProdutoValorNormal[]' value='{$produtoCarrinho.precoNormal}'>
                    
                     <div class="row">
                        <div class="col-md-12" align="left" style="min-width:250px">
                            <img src="img/lixeira.png" class="check_excluir_carrinho_flutuante" title="Excluir item." onclick="removeItem({$produtoCarrinho.contadorItem})" />
                            <img src="fotos/{$produtoCarrinho.foto}" width="50" height="50" alt="{$produtoCarrinho.nome}" title="{$produtoCarrinho.nome}" style="border:solid 1px #999999;margin-right:10px;float:left;" />
                            <div class="tit_prod_carrinho"><span style="text-transform:uppercase">{$produtoCarrinho.nome}</span><br />Tamanho: {$produtoCarrinho.tamanho}</div>
                            <div class="texto_carrinho" style="color:#C30;">
                                R$ {$produtoCarrinho.valor}
                            </div>
                            <div class="carrinho_flutuante_itens_qtde">
                            	<span onclick="diminuiQtde({$produtoCarrinho.contadorItem})" class="menos_carrinho"><img src="img/menos.png" /></span>
                            <input type="text" name="listaProdutoCarrinhoQtde[]" id="listaProdutoCarrinhoQtde" onKeyPress="formataNumero(event);" onblur="alterarQtdeItem({$produtoCarrinho.contadorItem},this.value,{$produtoCarrinho.qtdeDisponivel});" class="form_carrinho" value="{$produtoCarrinho.qtde}" size="2" style="text-align:center;"/><span onclick="aumentaQtde({$produtoCarrinho.contadorItem},{$produtoCarrinho.qtdeDisponivel})" class="mais_carrinho"><img src="img/mais.png" /></span>
                            </div>
                            
                        </div>
                        
                    </div>
                    
                </div>
                {/foreach}
                {/if}
                
                
           </div>
    	</form>
            
    </div>
    
    <div class="clearfix"></div>
    {if $listaProdutoCarrinho neq ""}
    <div class="carrinho_flutuante_total">
    {if $tamanho_carrinho > 1}Itens{else}Item{/if}: <strong>{if $tamanho_carrinho}{$tamanho_carrinho}{else}0{/if}</strong>
    <div>Total: <strong>{if $valor_carrinho}{$valor_carrinho}{else}0,00{/if}</strong></div>
    </div>
    
    <div class="carrinho_flutuante_finalizar" onclick="location.href='?secao=pedidoProduto&opcao=listarPedidoProdutoSecao'">FINALIZAR COMPRA</div>
    
    {/if}
    
</div>