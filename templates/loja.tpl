{literal}
<script type="text/javascript">
$('#imagem-lancamentos span').cycle({ 
fx:     'fade', 
speed:  750, 
timeout: 8000, 
next:   '#seta_dir', 
prev:   '#seta_esq' 
});

$('.imagem-catalogos-destaques span').cycle({ 
fx:     'fade', 
speed:  750, 
timeout: 8000, 
next:   '#seta_dir2', 
prev:   '#seta_esq2' 
});

</script>
{/literal}
<div class="conteudo-interno">
    		
	    		<div id="borda-transparente-pagina">
	    			
		    		<div class="conteudo-pagina">
		    			
		    			<div class="loja-catalogos-destaques">
		    				
		    				{if !$bannerProdutos}                            
                            {if $bannerLancamentos}
                            <div class="titulo-interno">Lançamentos</div><br />
                            <div id="imagem-lancamentos">
                            {assign var=count_banners value=$bannerLancamentos|@count}
                            {if $count_banners > 1}
                            <div id="seta_esq" style="top:135px;"></div>
                            <div id="seta_dir" style="top:135px;"></div>
                            {/if}
                            <span>
                            {foreach from=$bannerLancamentos item=bLancamentos}
                            <img src="banner/{$bLancamentos.arquivo}" width="937px" />
                            {/foreach}
                            </span>
                            </div>
                            {/if}
                            {/if}
                            
                            {if $bannerProdutos}
                            <div class="titulo-interno" {if $bannerLancamentos}style="margin-top:20px;"{/if}>{$nomeColecao}</div><br />
                            <div class="imagem-catalogos-destaques">
                            {assign var=count_banners2 value=$bannerProdutos|@count}
                            {if $count_banners2 > 1}
                            <div id="seta_esq2" style="top:135px;"></div>
                            <div id="seta_dir2" style="top:135px;"></div>
                            {/if}
                            <span>
                            {foreach from=$bannerProdutos item=bProdutos}
                            <img src="banner/{$bProdutos.arquivo}" width="937px" />
                            {/foreach}
                            </span>
                            </div>
                            {/if}
		    				
		    				<!--Inicio rodape-->
                            {include file=$link_carrinho}
                            <!--Fim rodape-->
                            
		    			</div>
		    			
		    			<div class="loja-destaques-topo">
		    				
		    				<div class="loja-lista-categorias">
		    					
		    					<div class="titulo-interno">Comprar por: <br />Cole&ccedil;&atilde;o</div>
		    					<form action="index.php" method="get" name="form_ordenacao">
                                
                                	<input type="hidden" name="secao" value="loja">
                                    
                                    <input type="hidden" name="numeroMaximoResultadosPagina" value="{$numeroMaximoResultadosPagina}">
                                                                        
                                    <input type="hidden" name="paginaAtual" value="{$paginaAtual}"/>
                                    <input type="hidden" name="campoOrdenacao" value="{$campoOrdenacao}"/>
                                
		    					{foreach from=$selectColecao item=listaCol}
                                    <input type="checkbox" name="listaColecao[]" value="{$listaCol.id}" 
                                    {foreach from=$selectColecaoSelecionado item=listaColSel}
                                    {if $listaCol.id == $listaColSel}checked="checked"{/if} 
                                    {/foreach}
                                    /> {$listaCol.nome}<br />
                                {/foreach}

                                <div class="titulo-interno" style="margin-top:30px;">Categorias</div>
                                
                                    {foreach from=$selectCategoria item=listaCat}
                                        <input type="checkbox" name="listaCategorias[]" value="{$listaCat.id}" 
                                        {foreach from=$selectCategoriaSelecionado item=listaCatSel}
                                        {if $listaCat.id == $listaCatSel}checked="checked"{/if} 
                                        {/foreach}
                                        /> {$listaCat.nome}<br />
                                    {/foreach}
                                    
                                 
                                 
                                 
                                 <div class="titulo-interno" style="margin-top:30px;">Limite Pre&ccedil;o</div>
		    					 <input type="radio" name="limitePreco" value="1" {if $limitePreco == 1}checked="checked"{/if} /> At&eacute; R$ 10,00<br />
                                 <input type="radio" name="limitePreco" value="2" {if $limitePreco == 2}checked="checked"{/if} /> At&eacute; R$ 20,00<br />
                                 <input type="radio" name="limitePreco" value="3" {if $limitePreco == 3}checked="checked"{/if} /> At&eacute; R$ 30,00<br />
                                 <input type="radio" name="limitePreco" value="4" {if $limitePreco == 4}checked="checked"{/if} /> At&eacute; R$ 40,00<br />
                                 <input type="radio" name="limitePreco" value="5" {if $limitePreco == 5}checked="checked"{/if} /> At&eacute; R$ 50,00<br />
                                 <input type="radio" name="limitePreco" value="6" {if $limitePreco == 6}checked="checked"{/if} /> Acima R$ 50,00
                                 <br /><br />
                                 <div class="bt_carrinho" onClick="document.form_ordenacao.submit();">Filtrar</div>
                                 
                                 </form>
                                 
		    				</div>
		    				
		    				<div class="loja-produtos-destaque">
                            	
                                {if $listaProduto}
		    					<div class="box_filtrar" style="margin-bottom:20px;">
                                    <form action="index.php" method="get" name="form_paginacao">
                                                    
                                    <input type="hidden" name="secao" value="loja">
                                    
                                    <input type="hidden" name="campoOrdenacao" value="{$campoOrdenacao}"/>
                                    
                                    <input type="hidden" name="limitePreco" value="{$limitePreco}"/>
                                    {foreach from=$selectColecaoSelecionado item=listaColSel}
                                    <input type="hidden" name="listaColecao[]" value="{$listaColSel}"/>
                                    {/foreach}
                                    
                                    {foreach from=$selectCategoriaSelecionado item=listaCatSel}
                                    <input type="hidden" name="listaCategorias[]" value="{$listaCatSel}"/>
                                    {/foreach}
                                     
                                    Orderar por: 
                                    <select class="form_select" style="width:160px;" name="campoOrdenacao">
                                    <option value="1" {if $campoOrdenacao eq "1"}selected="selected"{/if}>Preço: Ord. crescente</option>
                                    <option value="2" {if $campoOrdenacao eq "2"}selected="selected"{/if}>Preço: Ord. decrescente</option>
                                    <option value="3" {if $campoOrdenacao eq "3"}selected="selected"{/if}>Nome A-Z</option>
                                    <option value="4" {if $campoOrdenacao eq "4"}selected="selected"{/if}>Nome Z-A</option>
                                    <option value="5" {if $campoOrdenacao eq "5"}selected="selected"{/if}>Mais recente</option>
                                    <option value="6" {if $campoOrdenacao eq "6"}selected="selected"{/if}>Destaques</option>
                                    </select>
                                          
                                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                                      
                                                                        
                                    <select name="numeroMaximoResultadosPagina" class="form_select" style="width:110px;">
                                        <option value="15" {if $numeroMaximoResultadosPagina eq "15"}selected="selected"{/if}>15 itens por pág.</option>
                                        <option value="30" {if $numeroMaximoResultadosPagina eq "30"}selected="selected"{/if}>30 itens por pág.</option>
                                        <option value="45" {if $numeroMaximoResultadosPagina eq "45"}selected="selected"{/if}>45 itens por pág.</option>
                                        <option value="60" {if $numeroMaximoResultadosPagina eq "60"}selected="selected"{/if}>60 itens por pág.</option>
                                    </select>
                                    
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    
                                    pág. <input type="text" name="paginaAtual" value="{$paginaAtual}" style="width:15px;    height: 15px;" /> de {$totalPaginas}
                                    <a href="#" onClick="document.form_paginacao.submit();"><img src="img/bt_ok_sec.png" alt="" style="margin:0 3px -4px 3px;" /></a>
                                    
                                    {if ($paginaAtual neq "1") || ($paginaAtual neq $totalPaginas) }
                                    &nbsp;|&nbsp;
                                    {/if}
                                    
                                    {if $paginaAtual neq "1"}
                                        
                                    {assign var="numeroPagina" value="`$paginaAtual-1`"}
                                                         
                                    <a href="index.php?secao=loja&pesquisar={$pesquisar}&colecao={$idColecao}&numeroMaximoResultadosPagina={$numeroMaximoResultadosPagina}&campoOrdenacao={$campoOrdenacao}&paginaAtual={$numeroPagina}&campoOrdenacao={$campoOrdenacao}&limitePreco={$limitePreco}{$urlCat}{$urlCol}"><img src="img/seta_esq.png" alt="Anterior" title="Anterior" style="margin:0 0 -4px 1px;" /></a>
                                    
                                    {/if}	
                                    
                                    {if $paginaAtual neq $totalPaginas}
                                        
                                    {assign var="numeroPagina" value="`$paginaAtual+1`"}
                                        
                                    <a href="index.php?secao=loja&pesquisar={$pesquisar}&colecao={$idColecao}&numeroMaximoResultadosPagina={$numeroMaximoResultadosPagina}&campoOrdenacao={$campoOrdenacao}&paginaAtual={$numeroPagina}&campoOrdenacao={$campoOrdenacao}&limitePreco={$limitePreco}{$urlCat}{$urlCol}"><img src="img/seta_dir.png" alt="Próxima" title="Próxima" style="margin:0 0 -4px 0;" /></a>
                                    {/if} 
                                    
                                    </form>
                                    
                                  </div>
                                  {/if}
                                  
                                {if $listaProduto}
                                {foreach from=$listaProduto item=produtos}
		    					<div class="loja-item">
		    						<div class="imagem-produto" style="width:218px;height:218px;"><a href="?secao=loja&opcao=detalhes&prod={$produtos[0]}"><img src="fotos/{$produtos[5]}" width="218px" height="218px;" /></a></div>
		    						<div class="descricao-item">
		    							<div class="item-nome-produto">{$produtos[1]}</div>
		    							<div class="item-nome-colecao">{$produtos[4]}</div>
		    							<div class="item-nome-codigo">REF: {$produtos[2]}</div>
		    							<div class="item-precos">
		    								<div class="item-preco-total" style="margin:10px">{if $produtos[3] eq "" || $produtos[3] eq "0,00"}A consultar{else}R${$produtos[3]}{/if}</div>
		    								<div class="botao-detalhes"><a href="?secao=loja&opcao=detalhes&prod={$produtos[0]}"><img src="img/botao_detalhe.png" alt="Detalhes" title="Detalhes"></a></div>
		    							</div>
		    						</div>
		    					</div>
                                {/foreach}
                                {else}
                                Sem produtos para serem exibidos.
                                {/if}
		    					
                                
                                {if $listaProduto}
                                
                                <div class="box_filtrar2">
                                   
                                    Exibindo a pagina <strong>{$paginaAtual}</strong> de um total de <strong>{$totalPaginas}</strong> paginas.<br />
                                    
                                    
                                    
                                    {if $paginaAtual neq "1"}
                                        
                                    {assign var="numeroPagina" value="`$paginaAtual-1`"}
                                                         
                                    <a href="index.php?secao=loja&pesquisar={$pesquisar}&colecao={$idColecao}&numeroMaximoResultadosPagina={$numeroMaximoResultadosPagina}&campoOrdenacao={$campoOrdenacao}&paginaAtual={$numeroPagina}&campoOrdenacao={$campoOrdenacao}&limitePreco={$limitePreco}{$urlCat}{$urlCol}"><img src="img/seta_esq.png" alt="Anterior" title="Anterior" style="margin:0 0 -4px 1px;" /></a>
                                    
                                    {/if}	
                                    
                                    <span class="paginacao">
                                    {foreach from=$listaNumeroPagina item=num}
                                    	<a href="index.php?secao=loja&pesquisar={$pesquisar}&colecao={$idColecao}&numeroMaximoResultadosPagina={$numeroMaximoResultadosPagina}&campoOrdenacao={$campoOrdenacao}&paginaAtual={$num}&campoOrdenacao={$campoOrdenacao}&limitePreco={$limitePreco}{$urlCat}{$urlCol}" {if $num == $paginaAtual}style="color:#999;"{/if}>{$num}</a>
                                    {/foreach}
                                    </span>
                                    
                                    {if $paginaAtual neq $totalPaginas}
                                        
                                    {assign var="numeroPagina" value="`$paginaAtual+1`"}
                                        
                                    <a href="index.php?secao=loja&pesquisar={$pesquisar}&colecao={$idColecao}&numeroMaximoResultadosPagina={$numeroMaximoResultadosPagina}&campoOrdenacao={$campoOrdenacao}&paginaAtual={$numeroPagina}&campoOrdenacao={$campoOrdenacao}&limitePreco={$limitePreco}{$urlCat}{$urlCol}"><img src="img/seta_dir.png" alt="Próxima" title="Próxima" style="margin:0 0 -4px 0;" /></a>
                                    {/if} 
                                    
                                  </div>
		    					{/if}

                                
		    					
		    				</div>
		    				
		    			</div>
		    			
		    			
		    			
			    		
			    		<!--Inicio rodape-->
                        {include file=$rodape}
                        <!--Fim rodape-->
			    		
		    		</div>
	    		
	    		</div>
    		
    		</div>