{literal}
<script type="text/javascript">
function validarAlteracao(){
		
	
	if(document.contato.nome.value == ""){
		alert("Digite o Nome!");
		document.contato.nome.focus();
		return false;
	}
	
	if(document.contato.telefone.value == ""){
		alert("Digite o Telefone!");
		document.contato.telefone.focus();
		return false;
	}
	
	
	if(document.contato.email.value == ""){
		alert("Digite o E-mail!");
		document.contato.email.focus();
		return false;
	}else{
		if(validarEmail(document.contato.email.value) == false){
			alert("Informe um E-mail válido.");
			document.contato.email.focus();
			return false;
		}
	}
	
	if(document.contato.senha.value == ""){
		alert("Preecha a senha!");
		document.contato.senha.focus();
		return false;
	}
	

	document.contato.submit();
}

</script>
{/literal} 

<div id="container_swip">
    <br /><br />
	
    <!-- Cover and profile picture -->
    <div class="profile-cover">
      <div class="row">

        <div class="col-33 text-center">
          <img class="profile-picture circle" src="img/logo.png">
        </div>

        <div class="col-66">
          <h4 class="padding-top no-margin light">{$dadosUsuarioSite.nome}</h4>
        </div>

      </div>
    </div>

    <!-- User infomation -->
    <div class="list">

      <!--<div class="item item-toggle">
        <span class="assertive">Notification</span>
        <label class="toggle toggle-assertive">
          <input value="on" type="checkbox">
          <div class="track">
            <div class="handle"></div>
          </div>
        </label>
      </div>-->
	<form name="contato" action="index.php?secao=usuarioSite&opcao=salvarAlteracao" method="post">
      
      <input type="hidden" name="onde_retorno" value="{$onde_retorno}" />
      
      <label class="item item-input item-stacked-label">
        <span class="input-label assertive" aria-label="nome" id="_label-0">Nome:</span>
        <input value="{$dadosUsuarioSite.nome}" type="text" name="nome">
      </label>

      <label class="item item-input item-stacked-label">
        <span class="input-label assertive" aria-label="email" id="_label-1">E-mail:</span>
        <input value="{$dadosUsuarioSite.email}" type="text" name="email">
      </label>

      <label class="item item-input item-stacked-label">
        <span class="input-label assertive" aria-label="telefone" id="_label-2">Telefone:</span>
        <input type="text" placeholder="(xx) xxxxx-xxxx" name="telefone" maxlength="15" onkeypress="mascara(this, mascaraTelefone)" value="{$dadosUsuarioSite.telefone}">
      </label>

      <label class="item item-input item-stacked-label">
        <span class="input-label assertive" aria-label="senha" id="_label-3">Senha:</span>
        <input value="{$dadosUsuarioSite.senha}" type="password" name="senha">
      </label>
      
      
	</form>
    </div>

	<br />
    <center><h4>CARTÃO FIDELIDADE</h4></center>
    
    <div class="fidelidade">
    <center>{$cupons}</center>
    </div>
    
    
    
    {if count($pedidoProduto)>0}
    <br />
    <center><h4>MINHAS COMPRAS</h4></center>
    
    {foreach from=$pedidoProduto  item=pedido}
    
    <div class="list">
    
    <p style="padding:0 2%;"><strong>Pedido</strong> {$pedido.num}</p>
    <p style="padding:0 2%;"><strong>Data Pedido:</strong> {$pedido.data_cadastro}</p>
    <p style="padding:0 2%;"><strong>Pedido Status:</strong> {$pedido.descricao}</p>
    <p style="padding:0 2%;"><strong>Valor Pedido:</strong> R$ {$pedido.valor|replace:".":","} {if $pedido.desconto > 0}- {$pedido.desconto}% (desconto) = R$ {$pedido.valorComDesconto|replace:".":","}{/if}</p>
    
    
    {foreach from=$pedido.produtos  item=produtoCarrinho}
    
    <div class="item-thumbnail-left card item item-complex item-right-editable">
    
        <div class="item-content" id="aparecer_delete">
        
            <img src="fotos/{$produtoCarrinho.fotoProduto}">
            <h2 class="ng-binding">{$produtoCarrinho.nomeProduto}</h2>
            
            <div>
            <h2 class="price pull-left assertive ng-binding" style="line-height:normal;">R$ {$produtoCarrinho.precoProduto|replace:".":","} 
            <span class="dark ng-binding">x <span id="txtqtde{$produtoCarrinho.contadorItem}">{$produtoCarrinho.qtde}</span></span><br />
            {if $produtoCarrinho.id_pao != 3}
                <span style="font-size:11px;color:#333;line-height:normal;">Pão: {$produtoCarrinho.nomePao}</span><br />
                <span style="font-size:11px;color:#333;line-height:normal;">Queijo: 
                {if $produtoCarrinho.nomeQueijo != ""}{$produtoCarrinho.nomeQueijo}{/if}
                <!--{if $produtoCarrinho.nomeSalada != ""}, {$produtoCarrinho.nomeSalada}{/if}
                {if $produtoCarrinho.nomeMolho != ""}, {$produtoCarrinho.nomeMolho}{/if}
                {if $produtoCarrinho.nomeLivre != ""}, {$produtoCarrinho.nomeLivre}{/if}-->
                </span>
                {if $produtoCarrinho.nomeSuco != ""}<br /><span style="font-size:11px;color:#333;line-height:normal;">Suco: {$produtoCarrinho.nomeSuco} - R$ {$produtoCarrinho.precoSuco|replace:".":","}</span>{/if}
            {else}
                <span style="font-size:11px;color:#333;line-height:normal;">
                <strong>Adicionais:</strong><br />{$produtoCarrinho.adicionais}
                <br /><strong>Total Adicionais:</strong> R$ {$produtoCarrinho.precoSuco|replace:".":","}</span>
            {/if}
            </h2>
                        
            </div>
        
        </div>
        
    
    </div>
    {/foreach}
    
    </div>
    <hr />
    {/foreach}
    {/if}
    
    
	<div class="bar-assertive bar bar-footer" onclick="validarAlteracao()">
        <div class="row item-button-group">
    
          <div class="col button button-assertive">
            SALVAR
          </div>
        </div>
      </div>
     <br /><br /><br />
</div>

