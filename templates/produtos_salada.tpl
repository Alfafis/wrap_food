
<div id="container_swip">
	<br /><br /><br />
    
    <p class="text-center">Produto escolhido: <strong>{$paoEscolhido}</strong></p>
    
    <h3 class="text-center">Escolha o {$paoEscolhido}</h3>

    {foreach from=$listaProdutos item=prod}
    <div class="list no-padding" onclick="location.href='?secao=produtos&opcao=detalhes&pao={$idPao}&prod={$prod.id}'">
      <div class="item item-menu" ng-repeat="category in categories">
        <img src="fotos/{$prod.foto}" alt=""/>

        <div class="overlay">
          <span class="pull-left light ng-binding">
            {$prod.nome}<br />
            <span style="font-size:12px;color:#FFF;">{$prod.descricao}</span>
          </span>
          <span class="pull-right light ng-binding">
            R$ {$prod.preco|replace:".":","}
          </span>
        </div>
      </div>
    </div>
    {/foreach}
    

</div><!-- fim cotainer sw -->

