{literal}
<script type="text/javascript" src="https://d335luupugsy2.cloudfront.net/js/integration/stable/rd-js-integration.min.js"></script>  
<script type="text/javascript">
    var meus_campos = {
        'email': 'email'
     };
    options = { fieldMapping: meus_campos };
    RdIntegration.integrate('d6707cfe2bafeab06e37276ff65c513a', 'Formulário News', options);  
</script>
<script type="text/javascript">
function validarNews(){
		
   if(document.news.email.value == ""){
		alert("Digite o E-mail!");
		document.news.email.focus();
		return false;
	}else{
		if(validarEmail(document.news.email.value) == false){
				alert("Informe um E-mail válido.");
				document.news.email.focus();
				return false;
			}
		}
	
	document.news.submit();
}

</script>
{/literal} 
    <!--parte news-->
  <div class="box_news">
    <div class="row">
        <div class="col-md-3">					
            <img src="img/img_news.png" style="margin-top:10px; margin-left:50px;" />
        </div>
        <div class="col-md-5">					
            <div class="box_news_texto">
                <h1><span>ASSINE</span> NOSSA <span>NEWSLETTER</span> E RECEBA NOVIDADES E PROMOÇÕES DA <span>VICENZI!</span></h1>
                <p>Enviaremos por e-mail informações e ofertas exclusivas de nosso site.</p>
            </div>
        </div>
        <div class="col-md-4" align="center">
        	<form name="news" action="index.php?secao=contato&opcao=news" class="form-inline" style="margin-top:45px;" method="post">	
              <input type="hidden" name="env" value="111" />				
              <div class="form-group">
                <label for="email"><img src="img/carta_news.png" class="imagem_carta" /></label>
                <input type="text" name="email" class="form-control form_news" id="email" placeholder="Digite seu E-mail">
              </div>
              <div class="btn bt_enviar_news" onClick="validarNews()"></div>
            </form>
        </div>
    </div>
  </div> 
  
  