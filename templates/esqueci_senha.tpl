{literal}
<script type="text/javascript">

	/*******************logar********************/
	function validarFormularioLogin(){	
			
		if(document.form_login.email.value == "Digite o seu e-mail de cadastro"){
			alert("Preencha o campo E-mail.");
			document.form_login.email.focus();
			return false;
		}else{
			
			if(validarEmail(document.form_login.email.value) == false){
				alert("Informe um E-mail válido.");
				document.form_login.email.focus();
				return false;
			}
		}
 
 
		document.form_login.submit();
		
	}
	
	
	</script>
   {/literal} 
  <div id="container_swip">
    <br /><br />
	
    <div class="login-bg scroll-content ionic-scroll" style="position:relative;"><div class="scroll" style="transform: translate3d(0px, 0px, 0px) scale(1);">

    <div class="login-content">

		
      <!-- Logo -->
      <div class="padding text-center">
          <img class="profile-picture circle" menu-close="" src="img/logo.png" style="width:30%;margin:5% 0 auto;">
      </div>

      <!-- Login form -->
      <div class="list" style="width:80%;margin:5px auto;">
		
        <form action="index.php?secao=user&opcao=esqueceu_senha_enviar" method="post" name="form_login">
        
            <input type="hidden" name="paginaRedirecionar" value="{$paginaRedirecionar}" />
        
            <label class="item item-input item-stacked-label">
              <span class="input-label" aria-label="email" id="_label-14">Email</span>
              <input type="text" name="email">
            </label>
    
		</form>

      </div>


      <div class="padding-top" style="width:80%;margin:5px auto;">
        <button class="button button-block button-assertive" ui-sref="home" onclick="validarFormularioLogin()">
          RECUPERAR
        </button>
      </div>


		<br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
    </div>

  </div><div class="scroll-bar scroll-bar-v"><div class="scroll-bar-indicator scroll-bar-fade-out" style="transform: translate3d(0px, 0px, 0px) scaleY(1); height: 0px;"></div></div></div>
    
    
</div>

