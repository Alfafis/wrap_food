<div class="box_secundarias">
    <div class="titulo_secoes"><img src="img/marcador_tit.png" > ENVIO E PAGAMENTO</div>
    
    <div class="row">
        <div class="col-md-12">					
            
            <div id="borda_secundarias">
                <div class="caixa_botoes_sec">
                    <div class="botoes_sec {if $opcao== 'pagamento'}botoes_sec_ativo{/if}" id="btnbloco_1" onClick="abrir_bloco(1,3)">FORMAS DE PAGAMENTO</div>
                    <div class="botoes_sec duas_linhas {if $opcao== 'envio'}botoes_sec_ativo{/if}" id="btnbloco_2" onClick="abrir_bloco(2,3)">FORMAS DE ENVIO<br>E CONDIÇÕES DE ENTREGA</div>
                    <div class="botoes_sec {if $opcao== 'entrega'}botoes_sec_ativo{/if}" id="btnbloco_3" onClick="abrir_bloco(3,3)">POLÍTICA DE ENTREGA</div>
                </div>
                <div class="caixa_conteudo_sec">
                    <div id="bloco1" {if $opcao== 'pagamento'}style="display:block;"{else}style="display:none;"{/if}>
                        <div class="texto_conteudo_sec">
                            {$textos.formas_pag}
                        </div>
                    </div>
                    <div id="bloco2" {if $opcao== 'envio'}style="display:block;"{else}style="display:none;"{/if}>
                        <div class="texto_conteudo_sec">
                            {$textos.formas_envio}
                        </div>
                    </div>
                    <div id="bloco3" {if $opcao== 'entrega'}style="display:block;"{else}style="display:none;"{/if}>
                        <div class="texto_conteudo_sec">
                            {$textos.politica_entrega}
                        </div>
                    </div>
                    
                </div>
            </div>
            
        </div>
    </div>
    
</div>
