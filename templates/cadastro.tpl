{literal}
<script type="text/javascript">
function validarFormulario(){
		

	if(document.contato.nome.value == ""){
		alert("Digite o Nome!");
		document.contato.nome.focus();
		return false;
	}
	
	
	if(document.contato.email.value == ""){
		alert("Digite o E-mail!");
		document.contato.email.focus();
		return false;
	}else{
		if(validarEmail(document.contato.email.value) == false){
				alert("Informe um E-mail válido.");
				document.contato.email.focus();
				return false;
			}/*else{
				if(document.contato.email.value != document.contato.c_email.value){
					alert("As confirmação de e-mail esta diferente do email digitado!");
					document.contato.email.focus();
					return false;
				}			
			}*/
		}
	
	if(document.contato.telefone.value == ""){
		alert("Digite o Telefone!");
		document.contato.telefone.focus();
		return false;
	}
	
	
	if(document.contato.senha.value == ""){
		alert("Preecha a senha!");
		document.contato.senha.focus();
		return false;
	}
	
	
	document.contato.submit();
}

</script>
{/literal} 
<div id="container_swip">
    <br /><br />
	
    <div class="login-bg scroll-content ionic-scroll" style="position:relative;"><div class="scroll" style="transform: translate3d(0px, 0px, 0px) scale(1);">

    <div class="login-content" style="padding:0 20px;">

      <!-- Logo -->
      <div class="padding text-center">
          <img class="profile-picture circle" menu-close="" src="img/logo.png" style="width:30%;margin:5% 0 auto;">
      </div>

      <!-- Login form -->
      <div class="list">
		
        <form name="contato" action="index.php?secao=user&opcao=salvar" method="post">
        <input type="hidden" name="paginaRedirecionar" value="{$paginaRedirecionar}" />
        
            <label class="item item-input item-stacked-label">
              <span class="input-label" aria-label="nome" id="_label-13">Nome</span>
              <input type="text" name="nome">
            </label>
    
            <label class="item item-input item-stacked-label">
              <span class="input-label" aria-label="email" id="_label-14">Email</span>
              <input type="text" name="email">
            </label>
            
            <label class="item item-input item-stacked-label">
              <span class="input-label" aria-label="telefone" id="_label-14">Telefone</span>
              <input type="text" placeholder="(xx) xxxxx-xxxx" name="telefone" maxlength="15" onkeypress="mascara(this, mascaraTelefone)">
            </label>
    
            <label class="item item-input item-stacked-label">
              <span class="input-label" aria-label="senha" id="_label-15">Senha</span>
              <input type="password" name="senha">
            </label>
		</form>

      </div>

      <div class="padding-top">
        <button class="button button-block button-assertive" ui-sref="home" onclick="validarFormulario()">
          CADASTRAR
        </button>
      </div>


      <!-- Other links -->
      <div class="text-center">
        <a class="light" href="?secao=usuarioSite&opcao=logar&paginaRedirecionar=4">Já tenho cadastro</a>
      </div>
		<br />
    </div>

  </div><div class="scroll-bar scroll-bar-v"><div class="scroll-bar-indicator scroll-bar-fade-out" style="transform: translate3d(0px, 0px, 0px) scaleY(1); height: 0px;"></div></div></div>
    
    
</div>

