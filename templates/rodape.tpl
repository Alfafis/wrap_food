  
          <!--parte rodape-->
          <div class="box_rodape">
            <div class="row">
                
                <div class="col-md-12">					
                    <div class="bloco_texto_rodape">
                    	<div class="blocos_rodape">
                        	<h1><img src="img/marcador_rodape.png"> COMPRE COM CONFIANÇA</h1>
                            <a href="?secao=institucional">Sobre Nós</a><br>
                            <a href="?secao=confianca&opcao=como_comprar">Como Comprar</a><br>
                            <a href="?secao=confianca&opcao=politica">Política de Privacidade e Segurança</a><br>
                            <a href="?secao=confianca&opcao=trocas">Trocas e Devoluções</a><br>
                            <a href="?secao=institucional">Depoimentos</a><br>
                            <a href="?secao=confianca&opcao=atendimento">Atendimento</a><br>
                        </div>
                        <div class="blocos_rodape" style="width:250px;">
                        	<h1><img src="img/marcador_rodape.png"> ENVIO E PAGAMENTO</h1>
                            <a href="?secao=envio_pag&opcao=pagamento">Formas de Pagamento</a><br>
                            <a href="?secao=envio_pag&opcao=envio">Formas de Envio</a><br>
                            <a href="?secao=envio_pag&opcao=entrega">Política de Entrega</a><br>
                        </div>
                        <div class="blocos_rodape">
                        	<h1><img src="img/marcador_rodape.png"> MINHA CONTA</h1>
                            {if $usuarioSite neq ""}
                            {else}
                            <a href="?secao=usuarioSite&opcao=logar&paginaRedirecionar=4">Login</a><br>
                            {/if}
                            <a href="?secao=usuarioSite&opcao=alterar&parte=dados">Dados Pessoais</a><br>
                            <a href="?secao=pedidoProduto&opcao=listarPedidosUsuario">Meus Pedidos</a><br>
                        </div>
                        <div class="blocos_rodape" style="width:200px;">
                        	<h1><img src="img/marcador_rodape.png"> DICAS E DÚVIDAS</h1>
                            <a href="?secao=duvidas&opcao=faq">FAQ</a><br>
                            <a href="?secao=duvidas&opcao=cuidados">Cuidados</a><br>
                            <a href="?secao=duvidas&opcao=garantia">Garantia</a><br>
                            <a href="?secao=duvidas&opcao=promocoes">Promoções</a><br>
                            <a href="?secao=duvidas&opcao=dicas">Dicas</a><br>
                        </div>
                    </div>
                </div>
                
            </div>
            
            <div class="row">
                
                <div class="col-md-12">					
                    <div class="bloco_texto_rodape">
                    	<div class="blocos_rodape" style="width:100%;">
                        	<h1><img src="img/marcador_rodape.png"> MEIOS DE CONTATO:</h1>
                            <div class="itens_contato_rodape">
                                <img src="img/atendimento_rodape.png"> Atendimento
                            </div>
                            <div class="itens_contato_rodape">
                                <img src="img/chat_rodape.png"> <span>Chat</span> - ONLINE
                            </div>
                            <div class="itens_contato_rodape">
                                <img src="img/telefone_rodape.png"> {$textos.telefone}
                            </div>
                            <div class="itens_contato_rodape">
                                <img src="img/zap_rodape.png"> {$textos.zap}
                            </div>
                            <div class="itens_contato_rodape" style="line-height:normal;">
                                <img src="img/horario_rodape.png"> {$textos.funcionamento}
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            
            <div class="row">
                
                <div class="col-md-12">					
                    <div class="bloco_texto_rodape">
                    	<div class="blocos_rodape" style="width:100%;">
                        	<h1><img src="img/marcador_rodape.png"> LOCALIZAÇÃO:</h1>
                            <p>{$textos.endereco}</p>
                        </div>
                    </div>
                </div>
                
            </div>
            
            
          </div>  
          
          
          
          <!--parte baixo rodape-->
          <div class="box_baixo_rodape">
            <div class="row">
                
                <div class="col-md-3" align="center">
                	<a href="index.php"><img src="img/logo_rodape.png"></a>
                    <div class="creditos_rodape">
                    	© Copyright 2017<br>
Todos os Direitos Reservados - Vicenzi<br>
CNPJ: {$textos.cnpj}
                    </div>
                </div>
                <div class="col-md-2 alinhar_centro">					
                    <div class="blocos_rodape" style="margin-bottom:20px;">
                        <h1><img src="img/marcador.png"> REDES SOCIAIS:</h1>
                    </div>
                    
                	<a href="http://facebook.com/vicenzijoias" target="_blank"><img src="img/face_rodape.png"></a>
                    <!--<a href="#"><img src="img/twitter_rodape.png" style="margin-right:8px;"></a>-->
                    <a href="http://instagram.com/vicenzijoias" target="_blank"><img src="img/instagram.png" style="margin-right:8px;"></a>
                    <!--<a href="#"><img src="img/p_rodape.png"></a>-->
                </div>
                <div class="col-md-4" align="center">					
                    <div class="blocos_rodape">
                        <h1><img src="img/marcador.png"> FORMAS DE PAGAMENTO:</h1>
                        <img src="img/img_pagseguro.png" class="img_pagamentos">
                    </div>
                </div>
                <div class="col-md-3 alinhar_centro">					
                    <div class="blocos_rodape">
                        <h1><img src="img/marcador.png"> SEGURANÇA CERTIFICADA:</h1>
                        <img src="img/img_site_seguro.png">
                    </div>
                </div>
                
            </div>
            <a href="http://www.websoftbh.com.br" target="_blank"><img src="img/websoft.png" class="websoft"></a>
            
          </div> 