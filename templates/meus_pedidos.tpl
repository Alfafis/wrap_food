 <div class="box_secundarias">
    <div class="titulo_secoes"><img src="img/marcador_tit.png" > ÁREA DO CLIENTE</div>
    
    <div class="row">
        <div class="col-md-12">					
            
            <div id="borda_secundarias">
                <div class="caixa_botoes_sec">
                    <div class="botoes_sec {if $parte == 'minha_conta'}botoes_sec_ativo{/if}" id="btnbloco_1" onClick="location.href='?secao=usuarioSite&opcao=alterar&parte=minha_conta'">MINHA CONTA</div>
                    <div class="botoes_sec {if $parte == 'dados'}botoes_sec_ativo{/if}" id="btnbloco_2" onClick="location.href='?secao=usuarioSite&opcao=alterar&parte=dados'">DADOS PESSOAIS</div>
                    <div class="botoes_sec botoes_sec_ativo" onclick="location.href='?secao=pedidoProduto&opcao=listarPedidosUsuario'">MEUS PEDIDOS</div>
                </div>
                <div class="caixa_conteudo_sec">
                    
                    <div id="bloco1" style="display:block;">
                        <div class="texto_conteudo_sec">
                           
                           
                           
                           <div style="width:95%;margin:auto;">
               
				{if $listaPedidoProduto neq ""}
				<div class="box_cadastrar" style="height:auto;text-align:center;padding:10px 0">
			      <span class="text_endereco" style="font-size:12px;">
					 Aten&ccedil;&atilde;o: atualizamos esta p&aacute;gina todos os dias &agrave;s: 10h00, 14h00, 17h00 e 22h00.<br />
Portanto, acompanhe o andamento do seu pedido, consultando-a ap&oacute;s estes hor&aacute;rios. 
				  </span>
			    </div>
				<br />
				<table id="tabela_meus_pedidos">
				  <tr>
					<td width="150px" style="vertical-align:bottom;text-align:center"><span class="text_tab_pag">N&deg; do pedido</span></td>
					<td width="110px" style="vertical-align:bottom;text-align:center"><span class="text_tab_pag">Data do pedido</span></td>
					<td width="130px" style="vertical-align:bottom;text-align:center"><span class="text_tab_pag">Total</span></td> 
					<td width="160px" style="vertical-align:bottom;text-align:center"><span class="text_tab_pag">Status:</span></td>
                    <td></td>
				  </tr>
				  <tr>
				    <td colspan="5"><div style="border-top:solid 1px #BAB3D6;"></div></td>
				  </tr>
				  
				  {assign var="corLinha" value="EBECEE"}	
					
				  	{foreach from=$listaPedidoProduto  item=pedidoProduto }
					
				  <tr bgcolor="#{$corLinha}">
                      <td width="150px" style="vertical-align:bottom;text-align:center">{$pedidoProduto[0]}</td>
                        <td width="110px" style="vertical-align:bottom;text-align:center">{$pedidoProduto[1]}</td>
                        <td width="130px" style="vertical-align:bottom;text-align:center">
                        {assign var="valorTotalPedidoProduto" value=$pedidoProduto[3]|string_format:"%.2f"}								
						{assign var="valorTotalPedidoProduto" value=$valorTotalPedidoProduto|replace:".":","}
                        R$ {$valorTotalPedidoProduto}</td> 
                        <td width="160px" style="vertical-align:bottom;text-align:center">{$pedidoProduto[2]}</td>
                        <td id="link_ver_pedido"><a href="index.php?secao=pedidoProduto&opcao=verPedidoUsuario&idPedidoProduto={$pedidoProduto[0]}">Ver pedido</a></td>
                   </tr>
				   
				  <tr>
				    <td colspan="5"><div style="border-top:solid 1px #BAB3D6;"></div></td>
				  </tr>
				  
				  {if $corLinha eq "EBECEE"}
					
					{assign var="corLinha" value="d6d6d6"}
					
				 {else}
				
					{assign var="corLinha" value="EBECEE"}
				
				 {/if}
				
				
				 {/foreach}
				
				</table>
				<br />
				<br />
				<center>
				
					  {if $totalResultados neq "0"}
				  		<div class="paginacao">
						{if $paginaAtual neq "1"}
						
						{assign var="numeroPagina" value="`$paginaAtual-1`"}
											 
						<a href="index.php?secao=pedidoProduto&opcao=listarPedidosUsuario&paginaAtual={$numeroPagina}"> << Anterior </a>
						
						{/if}	
								
								
						{if $totalResultados gt $numeroMaximoResultadosPagina}	
							
						{foreach from=$listaNumeroPagina item=numeroPagina}
						<a href="index.php?secao=pedidoProduto&opcao=listarPedidosUsuario&paginaAtual={$numeroPagina}">{$numeroPagina}</a>
						{/foreach}
						
						{/if}
						
						
						{if $paginaAtual neq $totalPaginas}
						
						{assign var="numeroPagina" value="`$paginaAtual+1`"}
							
						<a href="index.php?secao=pedidoProduto&opcao=listarPedidosUsuario&paginaAtual={$numeroPagina}">Pr&oacute;xima >> </a>
						{/if}
						
					  </div> 
					  {/if}
					  
				</center>
				{else}
				<div class="box_cadastrar" style="margin-top:5px;padding-left:10px;height:42px;padding-bottom:5px; width:537px;;" >
			      <span class="text_endereco" style="font-size:15px;">
					  <br />
					  <center><B>N&atilde;o existe nenhum pedido cadastrado.</B></center>
				  </span>
			    </div>
				{/if}
				
			  <br />
				<div style="float:right;margin-right:10px;"><a href="index.php?secao=usuarioSite"><img src="img/bt_voltar.jpg" alt="" /></a></div>
			  </div>
                           
                           
                           
                        </div>
                    </div>
                    <div id="bloco2" {if $parte== 'dados'}style="display:block;"{else}style="display:none;"{/if}>
                        <div class="texto_conteudo_sec">
                            <h1>ESPAÇO DESTINADO A INSERÇÃO DE TEXTOS E CONTEÚDOS DOS ITENS AO LADO</h1>
                            <p>Lorem Ipsum è un testo segnaposto utilizzato nel settore della tipografia e della stampa. Lorem Ipsum è considerato il testo segnaposto standard sin dal sedicesimo secolo, quando un anonimo tipografo prese una cassetta di caratteri e li assemblò per preparare un testo campione. È sopravvissuto non solo a più di cinque secoli, ma anche al passaggio alla videoimpaginazione, pervenendoci sostanzialmente inalterato. Fu reso popolare, negli anni ’60, con la diffusione dei fogli di caratteri trasferibili “Letraset”, che contenevano passaggi del Lorem Ipsum, e più recentemente da software di impaginazione come Aldus PageMaker, che includeva versioni del Lorem Ipsum.</p>
                            <p>Lorem Ipsum è un testo segnaposto utilizzato nel settore della tipografia e della stampa. Lorem Ipsum è considerato il testo segnaposto standard sin dal sedicesimo secolo, quando un anonimo tipografo prese una cassetta di caratteri e li assemblò per preparare un testo campione. È sopravvissuto non solo a più di cinque secoli, ma anche al passaggio alla videoimpaginazione, pervenendoci sostanzialmente inalterato. Fu reso popolare, negli anni ’60, con la diffusione dei fogli di caratteri trasferibili “Letraset”, che contenevano passaggi del Lorem Ipsum, e più recentemente da software di impaginazione come Aldus PageMaker, che includeva versioni del Lorem Ipsum.</p>
                            <p>Lorem Ipsum è un testo segnaposto utilizzato nel settore della tipografia e della stampa. Lorem Ipsum è considerato il testo segnaposto standard sin dal sedicesimo secolo, quando un anonimo tipografo prese una cassetta di caratteri e li assemblò per preparare un testo campione. È sopravvissuto non solo a più di cinque secoli, ma anche al passaggio alla videoimpaginazione, pervenendoci sostanzialmente inalterato. Fu reso popolare, negli anni ’60, con la diffusione dei fogli di caratteri trasferibili “Letraset”, che contenevano passaggi del Lorem Ipsum, e più recentemente da software di impaginazione come Aldus PageMaker, che includeva versioni del Lorem Ipsum.</p>
                        </div>
                    </div>
                </div>
                    
                </div>
            </div>
            
        </div>
    </div>
    
</div>  