{literal}
<script src="js/funcoes_c.js"></script>
{/literal}

{literal}

<script>

	function validarFormulario(){
	
	
		var campoMarcado = false;
			
	
		if(document.form_pedido_produto.nome.value ==""){
			alert("Preencha o campo Nome.");
			document.form_pedido_produto.nome.focus();
			return false;
		}
					
		if(document.form_pedido_produto.cep.value ==""){
			alert("Preencha o campo CEP.");
			document.form_pedido_produto.cep.focus();
			return false;
		}
		
		if(document.form_pedido_produto.endereco.value == ""){
			alert("Preencha o campo Endere&ccedil;o Completo.");
			document.form_pedido_produto.endereco.focus();
			return false;
		}
		
		if(document.form_pedido_produto.bairro.value ==""){
			alert("Preencha o campo Bairro.");
			document.form_pedido_produto.bairro.focus();
			return false;
		}
		
		if(document.form_pedido_produto.cidade.value ==""){
			alert("Preencha o campo Cidade.");
			document.form_pedido_produto.cidade.focus();
			return false;
		}
		
		if(document.form_pedido_produto.estado.value ==""){
			alert("Preencha o campo Estado.");
			document.form_pedido_produto.estado.focus();
			return false;
		}
	 				
		if(document.form_pedido_produto.telefone.value == ""){
			alert("Preencha o campo Telefone.");
			document.form_pedido_produto.telefone.focus();
			return false;
		}	
		
		if($("#tipo_frete_selecionado").val() == ""){
			alert("Selecione o frete!");
			return false;
		}		
			
		document.form_pedido_produto.submit();
	}
	
	function manterEnderecoAtual(){
		
		if($("#tipo_frete_selecionado").val() == ""){
			alert("Selecione o frete!");
			return false;
		}
		
		document.form_pedido_produto.usar_endereco_atual.value = "1";
	
		document.form_pedido_produto.submit();
	
	}
	
	
	function mudarEnderecoAtual(){
		
		if($("#tipo_frete_selecionado").val() == 4){

			$(".usar_endereco_atual").css("display","none");
			$(".usar_endereco_outro").css("display","block");

		}else{
		
			//verificar se a tabela de fretes esta aberta
			var estado = document.getElementById("tabela_fretes").style.display;
			if(estado == "block"){
				document.getElementById("valor_frete_selecionado1").checked = false;
				document.getElementById("valor_frete_selecionado2").checked = false;
			}
			document.getElementById("tabela_fretes").style.display = "none";
			document.getElementById("frete_gratis_dores").style.display = "none";
			
			$(".usar_endereco_atual").css("display","none");
			$(".usar_endereco_outro").css("display","block");
			$("#valor_frete_selecionado").val("");
			$("#tipo_frete_selecionado").val("");
			
			if($("#valorTotalPedido").val().replace(".","").replace(".","") >= 100000){
				var valor_carrinho = $("#valorTotalPedido").val().replace(",","").replace(".","");
			}else{
				var valor_carrinho = $("#valorTotalPedido").val();
			}
		
			//pegando valor presente se tiver
			var valor_presente = $("#valor_presente").val();
			if(valor_presente != ""){
				var valor_total = parseFloat(valor_carrinho)+parseFloat(valor_presente);
			}else{
				var valor_total = parseFloat(valor_carrinho);	
			}
			//alert(formataValorParaMoeda(valor_total));
			var valor_final_formatado = formataValorParaMoeda(valor_total);
			$("#valorTotalPedidoFrete").val(valor_final_formatado.replace(",","."));
			document.getElementById("valor_total_frete").innerHTML = "R$ "+valor_final_formatado;
			
			if($("#cep_definir_endereco").val() != ""){
				calcularFreteEntrega();	
			}

		}
			
	}
	
	function voltarEnderecoAtual(){
		
		if($("#tipo_frete_selecionado").val() == 4){
			$(".usar_endereco_atual").css("display","block");
			$(".usar_endereco_outro").css("display","none");
		}else{
			
			var estado = document.getElementById("tabela_fretes").style.display;
			if(estado == "block"){
				document.getElementById("valor_frete_selecionado1").checked = false;
				document.getElementById("valor_frete_selecionado2").checked = false;
			}
			document.getElementById("tabela_fretes").style.display = "none";
			document.getElementById("frete_gratis_dores").style.display = "none";
			
			$(".usar_endereco_atual").css("display","block");
			$(".usar_endereco_outro").css("display","none");
			$("#valor_frete_selecionado").val("");
			$("#tipo_frete_selecionado").val("");
			
			if($("#valorTotalPedido").val().replace(".","").replace(".","") >= 100000){
				var valor_carrinho = $("#valorTotalPedido").val().replace(",","").replace(".","");
			}else{
				var valor_carrinho = $("#valorTotalPedido").val();
			}
			
			//pegando valor presente se tiver
			var valor_presente = $("#valor_presente").val();
			if(valor_presente != ""){
				var valor_total = parseFloat(valor_carrinho)+parseFloat(valor_presente);
			}else{
				var valor_total = parseFloat(valor_carrinho);	
			}
			//alert(formataValorParaMoeda(valor_total));
			var valor_final_formatado = formataValorParaMoeda(valor_total);
			$("#valorTotalPedidoFrete").val(valor_final_formatado.replace(",","."));
			document.getElementById("valor_total_frete").innerHTML = "R$ "+valor_final_formatado;
			calcularFreteEntregaInicial();
		
		}
	}

</script>

{/literal}
<div class="box_secundarias">
    <div class="titulo_secoes_carrinho">ENDEREÇO DE ENTREGA</div>
        
    
        <div class="row">
            <div class="col-md-6" align="left">	
            
            
				<div class="itens_prod_comprar2" style="margin-right:15px;" onClick="location.href='index.php?secao=loja'">Continuar comprando</div>
				<div class="itens_prod_comprar2" onClick="location.href='index.php?secao=pedidoProduto&opcao=listarPedidoProdutoSecao'">Alterar pedido</div>
                <div class="clearfix"></div>
				<div class="texto_exp_entrega" style="text-align:justify;">
                ABAIXO APARECEM OS ENDEREÇOS DE ENTREGA CADASTRADOS POR VOCÊ EM NOSSO SISTEMA. 
                PARA ENVIAR SEU PEDIDO PARA O ENDEREÇO ABAIXO CLIQUE EM: “USAR ESTE ENDEREÇO” OU PARA ALTERAR O ENDEREÇO DE ENVIO CLIQUE EM “USAR OUTRO ENDEREÇO”.
                </div>
                
				<div class="texto_exp_entrega2">ENDERE&Ccedil;O DE ENTREGA</div>
                
				<div class="box_endereco_entrega usar_endereco_atual">
				 <b>Meu endere&ccedil;o:</b><br />
				 {$dadosUsuarioSite[1]}<br />
				 {$dadosUsuarioSite[2]}, {$dadosUsuarioSite[8]} {if $dadosUsuarioSite[9] != ""}- {$dadosUsuarioSite[9]}{/if}, {$dadosUsuarioSite[3]}<br />
				 CEP: {$dadosUsuarioSite[4]} - {$dadosUsuarioSite[5]} - {$dadosUsuarioSite[6]}<br />
                 <input type="hidden" id="cep_definir_endereco_inicial" value="{$dadosUsuarioSite[4]}" />
                 
			    </div>
                
				<div class="itens_prod_comprar2 usar_endereco_atual" style="margin-right:15px;" onClick="manterEnderecoAtual();">Usar este endere&ccedil;o</div>
                <!--<div class="itens_prod_comprar2" onClick="location.href='index.php?secao=usuarioSite&opcao=alterar&parte=dados&onde_retorno=1'">Editar</div>-->
                <div class="itens_prod_comprar2 usar_endereco_atual" onClick="mudarEnderecoAtual();">Usar outro endere&ccedil;o</div>
                
                <div class="itens_prod_comprar2 usar_endereco_outro" onClick="voltarEnderecoAtual();" style="display:none;">Usar endere&ccedil;o de cadastro</div>
                
                <div class="clearfix"></div>
             
                <div class="texto_exp_entrega usar_endereco_outro" style="text-align:justify;display:none">
				 DIGITE OS DADOS DO NOVO ENDEREÇO, SELECIONE O FRETE E CLIQUE EM “CONTINUAR”.
                 <br /><strong>ATENÇÃO:</strong><br />
                 OS CAMPOS MARCADOS EM “<strong>NEGRITO</strong>” SÃO DE PREENCHIMENTO OBRIGATÓRIO E ESSENCIAIS PARA O ENVIO DE SEU PEDIDO.
				</div>
                {literal}
                <style type="text/css">
					.partes_cadastro{
						width:100%;	
					}
					.partes_cadastro p{
						margin-top:5px;	
					}
					.campos_cadastro{
					 	width:70%;	
						height:25px;
						line-height:25px;
					}
                </style>
                {/literal}
				
                <input type="hidden" name="em_estoque" id="em_estoque" value="{$fora_estoque}" />
                
				<form action="index.php?secao=pedidoProduto&opcao=definirFormaPagto" method="post" name="form_pedido_produto" class="form_contato">
				 
					<input type="hidden" name="usar_endereco_atual" value="0" />
                    
					<div class="partes_cadastro usar_endereco_outro" style="display:none">
                    	<p>Nome:</p>
                        <input type="text" class="form-control campos_cadastro" name="nome" id="nome" value="{$enderecoEntrega[2]}">
                        
                        <p><strong>CEP:</strong></p>
                        <input type="text" class="form-control campos_cadastro" name="cep" id="cep_definir_endereco" maxlength="9" onkeypress="return mascaraCep(event,this,'#####-###');" value="{$enderecoEntrega[5]}"> <span class="text_endereco" style="font-size:10px; vertical-align:top;">(Ex. 99999-999)&nbsp;&nbsp;<a href="http://www.buscacep.correios.com.br/sistemas/buscacep/default.cfm" target="_blank">N&atilde;o sabe o seu CEP? Consulte aqui</a></span> 
                        
                        <p>Endereço Completo:</p>
                        <input type="text" class="form-control campos_cadastro" name="endereco" id="endereco" value="{$enderecoEntrega[3]}">
                        
                        <p>Número:</p>
                        <input type="text" class="form-control campos_cadastro" name="numero" id="numero" value="{$enderecoEntrega[9]}">
                      
                        <p style="font-family:lator;color:#858585">Complemento:</p>
                        <input type="text" class="form-control campos_cadastro" name="complemento" id="complemento" value="{$enderecoEntrega[10]}">
                        
                        <p>Bairro:</p>
                        <input type="text" class="form-control campos_cadastro" name="bairro" id="bairro" value="{$enderecoEntrega[4]}">
                        
                        <p>Cidade:</p>
                        <input type="text" class="form-control campos_cadastro" name="cidade" id="cidade" value="{$enderecoEntrega[6]}">
                      
                        <p>UF:</p>
                        <input type="text" class="form-control campos_cadastro" name="estado" id="estado" value="{$enderecoEntrega[7]}">
                        
                        <p>Telefone:</p>
                        <input type="text" class="form-control campos_cadastro" name="telefone" onkeypress="mascara(this, mascaraTelefone)" maxlength="15" value="{$enderecoEntrega[8]}">
                                              
                        
                      </div>
                    {if ($revendedor == 0 && $valorTotalPedido|replace:",":"." >= $frete_gratis) || ($revendedor == 1 && $valorTotalPedido|replace:".":"" >= $frete_revenda|replace:".":"")}
                    <input type="hidden" name="valor_frete_selecionado" id="valor_frete_selecionado" value="0.00" />
                    <input type="hidden" name="tipo_frete_selecionado" id="tipo_frete_selecionado" value="4" />
                    {else}  
                    <input type="hidden" name="valor_frete_selecionado" id="valor_frete_selecionado" value="{$valor_frete_selecionado}" />
                    <input type="hidden" name="tipo_frete_selecionado" id="tipo_frete_selecionado" value="{$tipo_frete_selecionado}" />
                    {/if}

                                           
                    <input type="hidden" name="valorTotalPedido" id="valorTotalPedido" value='{$valorTotalPedido|replace:",":"."}' />
                    
                    <input type="hidden" name="valorTotalPedidoFrete" id="valorTotalPedidoFrete" value='{$valorTotalPedidoFrete|replace:",":"."}' />
                    
                    {if $para_presente == "sim"}
                    <input type="hidden" name="valor_presente" id="valor_presente" value='{$valor_presente}' />
                    <input type="hidden" name="para_presente" id="para_presente" value='sim' />
                    {else}
                    <input type="hidden" name="valor_presente" id="valor_presente" value='' />
                    <input type="hidden" name="para_presente" id="para_presente" value='' />
                    {/if}
						
		  	    </form>
				<div class="clearfix"></div>
				<BR>
				<div class="itens_prod_comprar2 usar_endereco_outro" style="float:left;display:none;" onClick="validarFormulario()">Continuar</div>
				<BR><BR><BR>
                
			
            </div>
            
            <div class="col-md-6" align="left">	
            	<br />
                <div class="texto_carrinho">
            	<strong>Escolha o seu frete:</strong>
                </div>
                
                 <table cellpadding="0" cellspacing="0" width="300px" id="tabela_fretes">
                   
                    <tr>
                        <td width="150px" align="left" class="texto_desconto_carrinho" style="text-align:left;">
                            <input type="radio" name="valor_frete" id="valor_frete_selecionado1" onclick="selecionaFreteEntrega(1)" />&nbsp;
                            SEDEX
                        </td>
                        <td width="75px" align="left" class="texto_desconto_carrinho" style="text-align:left;">
                        	R$ <span id="valor_sedex"></span>
                        </td>
                        <td width="75px" align="left" class="texto_desconto_carrinho" style="text-align:left;">
                        	<span id="prazo_sedex"></span> dia(s)
                            <input type="hidden" name="valor_sedex_item" id="valor_sedex_item" />
                        </td>
                    </tr>
                    <tr>
                        <td width="150px" align="left" class="texto_desconto_carrinho" style="text-align:left;">
                            <input type="radio" name="valor_frete" id="valor_frete_selecionado2" onclick="selecionaFreteEntrega(2)" />&nbsp;
                            PAC
                        </td>
                        <td width="75px" align="left" class="texto_desconto_carrinho" style="text-align:left;">
                        	R$ <span id="valor_pac"></span>
                        </td>
                        <td width="75px" align="left" class="texto_desconto_carrinho" style="text-align:left;">
                        	<span id="prazo_pac"></span> dia(s)
                            <input type="hidden" name="valor_pac_item" id="valor_pac_item" />
                        </td>
                    </tr>
                </table>
                
                <span id="frete_gratis_dores" style="display:none;">
                    <div class="texto_carrinho" id="frete_ja_selecionado" style="text-align:left;display:block;"><strong>Dores do Indaiá - Frete grátis.</strong></div>
                <br />
                </span>
                {if ($revendedor == 0 && $valorTotalPedido|replace:",":"." >= $frete_gratis) || ($revendedor == 1 && $valorTotalPedido|replace:".":"" >= $frete_revenda|replace:".":"")}
                	<div class="texto_carrinho" id="frete_ja_selecionado" style="text-align:left;display:block;"><strong>Frete grátis.</strong></div>
                	<br />
                {/if}
                
            	<div class="clearfix"></div>
            	<div class="tit_itens_carrinho_precos" style="margin-top:15px;float:left;text-align:center;width:270px;">
                    Valor Total + Frete:
                </div>
                <div class="clearfix"></div>
                <div class="texto_carrinho_precos" id="valor_total_frete" style="float:left;text-align:center;width:270px;">
                    R$ {$valorTotalPedidoFrete|replace:".":","}
                </div>
                
			
            </div>
           
	</div>

</div>
