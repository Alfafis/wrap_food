<div id="container_swip">
    <br /><br />
    
    <div class="padding stable-bg">

      <h3>Como funciona a fidelidade</h3>

      <p>
      	 Fazendo {$tickets} compras na próxima você terá {$desconto}% de desconto. Essa promoção não é cumulativa com outras.
      </p>
        
       {if $usuarioSite neq ""}
       <div class="item item-icon-left" onClick="location.href='?secao=usuarioSite&opcao=alterar&parte=minha_conta'"> <i class="icon ion-bookmark"></i> Ver Meus Cupons</div>
       {/if}

    </div>
    
    
</div>