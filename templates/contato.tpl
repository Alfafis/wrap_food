{literal}
<script type="text/javascript" src="https://d335luupugsy2.cloudfront.net/js/integration/stable/rd-js-integration.min.js"></script>  
<script type="text/javascript">
    var meus_campos = {
        'nome': 'nome',
        'email': 'email',
        'telefone': 'telefone',
        'assunto': 'Assunto',
        'cidade': 'cidade',
        'estado': 'estado',
        'msg': 'Mensagem'
     };
    options = { fieldMapping: meus_campos };
    RdIntegration.integrate('d6707cfe2bafeab06e37276ff65c513a', 'Formulário Contato', options);  
</script>
<script type="text/javascript">
function validarFormulario(){
		
	
		if(document.contato.nome.value == ""){
			alert("Digite o Nome!");
			document.contato.nome.focus();
			return false;
		}
		
		if(document.contato.email.value == ""){
			alert("Digite o E-mail!");
			document.contato.email.focus();
			return false;
		}else{
			if(validarEmail(document.contato.email.value) == false){
					alert("Informe um E-mail válido.");
					document.contato.email.focus();
					return false;
				}
			}
		
		if(document.contato.msg.value == ""){
			alert("Digite a Mensagem!");
			document.contato.msg.focus();
			return false;
		}
		
 
		document.contato.submit();
	}
	
	</script>
   {/literal} 
   

            <!--Inicio parte baixo banner-->
            {include file=$links_principal}
            <!--Fim parte baixo banner-->
     
                        
            <div class="box_contato">
            	<div class="titulo_contato"><img src="img/marcador_tit.png" > CONTATO</div>
                <div class="row">
                    <div class="col-md-12" align="center">					
                        
                        	<div class="quadro_contato_meio">
                            	<h1>Entre em contato conosco através do formulário abaixo:</h1>
                                
                                <form name="contato" action="index.php?secao=contato&opcao=salvar" method="post">
                                    <p>Nome/Empresa: (*)</p>
                                    <input type="text" class="form-control campos_contato" name="nome" id="nome">
                                  
                                    <p>E-mail: (*)</p>
                                    <input type="text" class="form-control campos_contato" name="email" id="email">
                                  
                                    <p>Telefone:</p>
                                    <input type="text" class="form-control campos_contato" name="telefone" id="telefone" placeholder="(xx) xxxxx-xxxx" name="telefone" maxlength="15" onkeypress="mascara(this, mascaraTelefone)">
                                  
                                    <p>Assunto:</p>
                                    <input type="text" class="form-control campos_contato" name="assunto" id="assunto">
                                  
                                    <p>Cidade:</p>
                                    <input type="text" class="form-control campos_contato" name="cidade" id="cidade">
                                    
                                    <p>Estado:</p>
                                    <input type="text" class="form-control campos_contato" name="estado" id="estado">
                                    
                                    <p>Mensagem:</p>
                                    <textarea class="form-control campos_contato" name="msg" id="msg" style="line-height:normal;height:100px;"></textarea>
                                  
                                  <div class="clearfix"></div>
                                  <br><br>
                                  <a href="#" onclick="validarFormulario()"><img src="img/bt_enviar.jpg" /></a>
                                </form>
                            </div>
                        
                    </div>
                </div>
            </div>   