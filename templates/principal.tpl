<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width">
<meta http-equiv="Content-Security-Policy">
<title></title>
{literal}
<!-- compiled css output -->
<link href="css/ionic.app.css" rel="stylesheet">
<link href="css/estilos.css" rel="stylesheet">
<link rel="stylesheet" href="css/swiper.min.css">
    
<script src="js/jquery.min.js"></script>
<script src="js/jquery.touchSwipe.min.js"></script>

<script src="js/mascaras.js"></script>
<script src="js/efeitos.js"></script>

<link rel="stylesheet" href="css/jquery-ui.css">
<script src="js/jquery-ui.js"></script>

{/literal}
</head>
<body>

	<div id="todo_conteudo">
    	<div id="fundo_menu" onClick="fechar_menu()"></div>
    	<div id="menu_esq" style="overflow-y:auto;">
                  
          <!-- User profile -->
          <div class="text-center"> 
            <img class="profile-picture circle" menu-close="" src="img/logo.png" style="width:30%;margin:5% 0 auto;">
            {if $usuarioSite neq ""}
            <h4>{$nome_usuario}</h4>
            {else}
            <!--<h4>Visitante</h4>-->
            {/if}
          </div>
          
          <!-- Menu -->
          <div class="list" menu-close="">
            <div class="item item-icon-left" onClick="location.href='index.php'"> <i class="icon ion-ios-home-outline"></i> Home </div>
            <div class="item item-icon-left" onClick="location.href='index.php'"> <i class="icon ion-grid"></i> Fazer Pedido </div>
            <div class="item item-icon-left" onClick="location.href='?secao=fidelidade'"> <i class="icon ion-bookmark"></i> Fidelidade </div>
                        
            <div class="item item-icon-left" onClick="location.href='?secao=pedidoProduto&opcao=listarPedidoProdutoSecao'"> <i class="icon ion-ios-cart-outline"></i> Meu carrinho <span class="badge badge-assertive">{if $tamanho_carrinho}{$tamanho_carrinho}{else}0{/if}</span> </div>
           
            <div class="item item-icon-left" onClick="location.href='?secao=usuarioSite&opcao=alterar&parte=minha_conta'"> <i class="icon ion-ios-gear-outline"></i> Meus dados </div>
            
            <div class="item item-icon-left" onClick="location.href='?secao=empresa'"> <i class="icon ion-ios-information-outline"></i> Sobre nós </div>
            {if $usuarioSite neq ""}
            <div class="item item-icon-left" onClick="location.href='?secao=usuarioSite&opcao=sair'"> <i class="icon ion-log-out"></i> Sair </div>
            {else}
            <!--<div class="item item-icon-left" onClick="location.href='?secao=usuarioSite&opcao=cadastrar&paginaRedirecionar=1'"> <i class="icon ion-log-out"></i> Cadastre-se </div>-->
            <div class="item item-icon-left" onClick="location.href='index.php?secao=usuarioSite&opcao=logar&paginaRedirecionar=4'"> <i class="icon ion-log-out"></i> Login </div>
            {/if}
          </div>
          
        </div>
    	
        <div id="centro_site">
        
        	<div class="bar bar-assertive">
            	<button class="button button-icon button-clear ion-navicon" onClick="abrir_menu();"> </button>
                <div class="title title-center header-item">{$nomePagina}</div>
                <button class="button button-icon button-clear ion-android-more-vertical" ui-sref="home"> </button>
            </div>
            <div id="fundo_alertas"></div>
            <div id="dialog" title="Alerta" style="display:none;">
              <p id="text_dialog"></p>
            </div>
            <!--Inicio meio-->
            {include file=$templateCentro}
            <!--Fim meio-->
              
                
        </div>
    </div>
    
    
    {literal}
    <script type="text/javascript">
		/*$(function () {
		  
		  $("#container_swip, #fundo_menu, #menu_esq").swipe( {
			//Generic swipe handler for all directions
			swipe:function(event, direction, distance, duration, fingerCount, fingerData) {
			  	if (direction == 'left') fechar_menu();
    			if (direction == 'right') abrir_menu();  
			},
			//Default is 75px, set to 0 for demo so any distance triggers swipe
			 threshold:0
		  });
		  
		});*/
		
		
		
		$(function(){
						
		  var curDown = false,
			  curYPos = 0,
			  curXPos = 0;
		  $(window).mousemove(function(m){
			if(curDown === true){
			 $(window).scrollTop($(window).scrollTop() + (curYPos - m.pageY)); 
			 $(window).scrollLeft($(window).scrollLeft() + (curXPos - m.pageX));
			}
		  });
		  
		  $(window).mousedown(function(m){
			curDown = true;
			curYPos = m.pageY;
			curXPos = m.pageX;
		  });
		  
		  $(window).mouseup(function(){
			curDown = false;
		  });
		})
    </script>
    {/literal}
    
        
    {if $retorno == "sucesso"}
    {literal}
    <script type="text/javascript">
        alertas("Parabéns. Seu cadastro foi realizado com sucesso!");
    </script>
    {/literal}
    {/if}
    
    
    {if $retorno == "erroemail"}
    {literal}
    <script type="text/javascript">
        alertas("Seu e-mail já esta cadastrado em nossa base! Tente lembrar ou recuperar sua senha.");
    </script>
    {/literal}
    {/if}
    
    
    {if $retorno == "erro"}
    {literal}
    <script type="text/javascript">
        alertas("Erro no cadastrado.");
    </script>
    {/literal}
    {/if}

    
</body>
</html>