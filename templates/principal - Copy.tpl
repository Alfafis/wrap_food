<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width">
    <meta http-equiv="Content-Security-Policy">
    <title></title>

	{literal}
    <!-- compiled css output -->
    <link href="css/ionic.app.css" rel="stylesheet">
	
    <link rel="stylesheet" href="css/font-awesome.min.css">
    
    <!-- Bootstrap core CSS -->
    <link href="css/fontes.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/estilos.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="css/carousel.css" rel="stylesheet">
  
    <!-- google maps javascript -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB16sGmIekuGIvYOfNoW9T44377IU2d2Es&amp;sensor=true"></script>

    <script src="js/mascaras.js"></script>
    
    {/literal}
  </head>
  <body>
  
      <div class="container">
              
              <!--parte cima topo-->
              <div class="cima_topo">
                <div class="row">
                    <div class="col-md-6" align="center">
                        {if $usuarioSite neq ""}
                        <div class="link_cadastro">
                        Seja bem vindo(a), <span style="font-size:14px;"><strong>{$nome_usuario}</strong></span>. <a href="?secao=usuarioSite&opcao=sair" style="color:#CCC;">(sair)</a>
                        </div>
                        {else}
                        <div class="link_login">
                            <img src="img/chave.png" style="vertical-align:baseline;" />&nbsp;<a href="?secao=usuarioSite&opcao=logar&paginaRedirecionar=4">LOGIN</a>
                        </div>
                        <div class="link_cadastro">
                            &nbsp;- Ainda não tem Cadastro? <a href="?secao=user&opcao=cadastrar">Cadastre-se Agora.</a>
                        </div>	
                        {/if}				
                        
                    </div>
                    <div class="col-md-6" align="center">
                        
                        <div class="icons_cima_topo">
                            <img src="img/telefone.png" style="vertical-align:central;" />&nbsp;<strong>{$textos.telefone}</strong>
                        </div>
                        <div class="icons_cima_topo">
                            <img src="img/chat.png" style="vertical-align:central;" />&nbsp;Chat - <a href="#">ONLINE</a>
                        </div>
                        <div class="icons_cima_topo">
                            <img src="img/atendimento.png" style="vertical-align:central;" />&nbsp;<a href="?secao=contato">Atendimento</a>
                        </div>
                    </div>
                </div>
              </div>  
                       
                
                <!--Inicio meio-->
                {include file=$templateCentro}
                <!--Fim meio-->
                 
              
                <!--Inicio rodape-->
                {include file=$rodape}
                <!--Fim rodape-->
              
               
                
            </div>

    <ion-side-menus>

      <!-- Center content -->
      <ion-side-menu-content>
        <ion-nav-bar class="bar-assertive" align-title="center">
          <!-- Back button -->
          <ion-nav-back-button>
            Back
          </ion-nav-back-button>

          <!-- Burger button -->
          <ion-nav-buttons side="left">
            <button class="button button-icon button-clear ion-navicon" menu-toggle="left">
            </button>
          </ion-nav-buttons>

          <!-- Cart button -->
          <ion-nav-buttons side="right">
            <button class="button button-icon button-clear ion-android-more-vertical" ui-sref="home">
            </button>
          </ion-nav-buttons>
        </ion-nav-bar>

        <!-- Main view content -->
        <ion-nav-view animation="slide-left-right"></ion-nav-view>
      </ion-side-menu-content>

      <!-- Left menu -->
      <ion-side-menu side="left">
        <ion-content>

          <!-- User profile -->
          <div class="text-center">
            <a ui-sref="user">
              <!--<img class="profile-picture circle" menu-close="" src="img/people/adam.jpg">-->
              <h4>Adam Lambert</h4>
            </a>
          </div>

          <!-- Menu -->
          <div class="list" menu-close="">
            <div class="item item-icon-left" ui-sref="home">
              <i class="icon ion-ios-home-outline"></i>
              Menu
            </div>

            <div class="item item-icon-left" ui-sref="categories">
              <i class="icon ion-grid"></i>
              Categories
            </div>

            <!--<div class="item item-icon-left" ui-sref="offer">
              <i class="icon ion-ios-pricetag-outline"></i>
              Offers
              <span class="badge badge-assertive">5</span>
            </div> -->

            <div class="item item-icon-left" ui-sref="cart">
              <i class="icon ion-ios-cart-outline"></i>
              My Cart
              <span class="badge badge-assertive">10</span>
            </div>

            <!--<div class="item item-icon-left" ui-sref="last_orders">
              <i class="icon ion-ios-timer-outline"></i>
              Last orders
            </div> -->

            <div class="item item-icon-left" ui-sref="favorite">
              <i class="icon ion-ios-star-outline"></i>
              Favorite
            </div>

            <div class="item item-icon-left" ui-sref="setting">
              <i class="icon ion-ios-gear-outline"></i>
              Settings
            </div>

            <!--<div class="item item-icon-left" ui-sref="news">
              <i class="icon ion-ios-paper-outline"></i>
              News
            </div> -->

            <div class="item item-icon-left" ui-sref="about_us">
              <i class="icon ion-ios-information-outline"></i>
              About us
            </div>
            <!-- 
            <div class="item item-icon-left" ui-sref="chats">
              <i class="icon ion-ios-help-outline"></i>
              Supports
            </div> -->

            <div class="item item-icon-left" ui-sref="login">
              <i class="icon ion-log-out"></i>
              Logout
            </div>
          </div>

        </ion-content>

      </ion-side-menu>

    </ion-side-menus>
    
    {literal}
    
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery.min.js"><\/script>')</script>
    <script src="js/bootstrap.min.js"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="js/vendor/holder.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>
    
    <script src="js/jquery.modal.js" type="text/javascript" charset="utf-8"></script>
  	<link rel="stylesheet" href="css/jquery.modal.css" type="text/css" media="screen" />
    
    <link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css" />
	<script type="text/javascript" src="js/jquery.bxslider.min.js"></script>
    <script type="text/javascript" src="js/efeitos.js"></script>
    
    <script src="js/jquery.cycle.all.js" type="text/javascript" charset="utf-8"></script>
    
   
   
    {/literal}
    
  </body>
</html>






<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <META NAME="GOOGLEBOT" content="INDEX,FOLLOW">
    <META NAME="robots" content="INDEX,FOLLOW">
    <meta name="description" content="">
    <meta name="keywords" content=""> 
    <meta name="author" content="Websoft"> 
    <link rel="icon" href="favicon/favicon.ico">
    <link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
    <link rel="manifest" href="favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    
    <title>VICENZI JOIAS</title>
	
    {literal}
    <link rel="stylesheet" href="css/font-awesome.min.css">
    
    <!-- Bootstrap core CSS -->
    <link href="css/fontes.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/estilos.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="css/carousel.css" rel="stylesheet">
    
    
    
    <script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
	
	  ga('create', 'UA-97448996-1', 'auto');
	  /*var dimensionValue = {/literal}{$produto.id}{literal};
	  ga('set', 'dimension1', dimensionValue);
	  var dimensionValue = {/literal}{$secao}{literal};
	  ga('set', 'dimension2', dimensionValue);
	  var dimensionValue = {/literal}{$produto.preco_venda}{literal};
	  ga('set', 'dimension3', dimensionValue);*/
	  var metricValue = '123';
	  ga('set', 'metric1', metricValue);
	  ga('send', 'pageview');
	
	</script>
    
    <!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-PL6BJR2');</script>
    <!-- End Google Tag Manager -->
    
    <!-- Facebook Pixel Code -->
	<script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '1483008735124441'); // Insert your pixel ID here.
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=1483008735124441&ev=PageView&noscript=1"
    /></noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->

    {/literal}
    
  </head>
<!-- NAVBAR
================================================== -->
  <body>
  <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PL6BJR2"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
  	<div class="geral">

   		<div id="fundo_load"></div>
        <img src="img/load.gif" width="50px" id="load_site">
        
        <div id="box_produto_seleciona">
        	<div id="fechar_prod_seleciona" onClick="fechaProdSeleciona()"><img src="img/close.png"></div>
            <div style="position:relative;min-height:110px">
                <img src="img/img_prod_g1.jpg" width="100" id="img_prod_adicionar" />
                <div id="texto_prod_adicionar">
                <span id="texto_prod_adicionar_titulo"></span>
                <p>por <span id="texto_prod_adicionar_preco"</span></p>
                </div>
            </div>
            <div class="clearfix"></div>
            <input type="hidden" name="id_prod" id="id_prod_selecionado" value="" />
            <div id="tamanhos_produto_seleciona_tamanhos">Tamanhos:</div>
            <div id="tamanhos_produto_seleciona"></div>
            <div class="itens_produto_comprar2" onclick="validarCompra()"></div>
        </div>
        
        {if $secao != "pedidoProduto"}
        <!--Inicio pesquisa-->
        {include file=$carrinho_principal}
        <!--Fim pesquisa-->
        {/if}
        
        
        <div class="container">
          
          <!--parte cima topo-->
          <div class="cima_topo">
            <div class="row">
                <div class="col-md-6" align="center">
                	{if $usuarioSite neq ""}
                    <div class="link_cadastro">
                    Seja bem vindo(a), <span style="font-size:14px;"><strong>{$nome_usuario}</strong></span>. <a href="?secao=usuarioSite&opcao=sair" style="color:#CCC;">(sair)</a>
                    </div>
                    {else}
                	<div class="link_login">
                    	<img src="img/chave.png" style="vertical-align:baseline;" />&nbsp;<a href="?secao=usuarioSite&opcao=logar&paginaRedirecionar=4">LOGIN</a>
                    </div>
                    <div class="link_cadastro">
                    	&nbsp;- Ainda não tem Cadastro? <a href="?secao=user&opcao=cadastrar">Cadastre-se Agora.</a>
                    </div>	
                    {/if}				
                    
                </div>
                <div class="col-md-6" align="center">
                    
                    <div class="icons_cima_topo">
                    	<img src="img/telefone.png" style="vertical-align:central;" />&nbsp;<strong>{$textos.telefone}</strong>
                    </div>
                    <div class="icons_cima_topo">
                    	<img src="img/chat.png" style="vertical-align:central;" />&nbsp;Chat - <a href="#">ONLINE</a>
                    </div>
                    <div class="icons_cima_topo">
                    	<img src="img/atendimento.png" style="vertical-align:central;" />&nbsp;<a href="?secao=contato">Atendimento</a>
                    </div>
                </div>
            </div>
          </div>  
          
          <!--parte topo-->
          <div class="topo">
            <div class="row">
                <div class="col-md-2" align="center">					
                    <a href="index.php"><img src="img/logo_topo.png" alt="VINCENZI JOIAS" class="logo_topo" /></a>
                </div>
                <div class="col-md-7" align="center">					
                    <ul class="lista_topo">
                    	<li><img src="img/marcador.png" style="vertical-align:baseline;">&nbsp;&nbsp;<a href="?secao=revender">QUER SER UM EMPREENDEDOR?</a></li>
                        <li><img src="img/marcador.png" style="vertical-align:baseline;">&nbsp;&nbsp;<a href="?secao=assinaturas&opcao=cartao">CARTÃO FIDELIDADE</a></li>
                        <!--<li><img src="img/marcador.png" />&nbsp;<a href="?secao=assinaturas&opcao=mensal">ASSINATURA MENSAL DE COMPRAS</a></li>-->
                    </ul>
                </div>
                <div class="col-md-3" align="center">
                   <ul class="lista_topo2">
                   {if $usuarioSite neq ""}
                    	<li><a href="?secao=usuarioSite&opcao=alterar&parte=minha_conta">ÁREA DO CLIENTE</a></li>
                        <li><a href="http://localhost/vicenzi/index.php?secao=pedidoProduto&opcao=listarPedidosUsuario">MEUS PEDIDOS</a></li>
                   {else}
                    	<li><a href="?secao=usuarioSite&opcao=logar&paginaRedirecionar=4">ÁREA DO CLIENTE</a></li>
                        <li><a href="?secao=usuarioSite&opcao=logar&paginaRedirecionar=2">MEUS PEDIDOS</a></li>
                   {/if}
                   
                   </ul>
                    {if $secao != "pedidoProduto"}
                    <!--Inicio pesquisa-->
                    {include file=$link_carrinho}
                    <!--Fim pesquisa-->
                    {/if}
                </div>
            </div>
          </div>  
          
          
          <!----parte menu------>
          <div class="navbar-wrapper">
        
                <nav class="navbar navbar-inverse navbar-static-top">
                  <div class="container">
                    <div class="navbar-header">
                      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                      
                    </div>
                    <div id="navbar" class="navbar-collapse collapse">
                      <ul class="nav navbar-nav">
                      	<li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="caret"></span> PRODUTOS</a>
                          <ul class="dropdown-menu">
                          	{foreach from=$menuCategorias item=cat}
                            <li><a href="?secao=loja&listaCategorias[]={$cat.id}">{$cat.nome}</a></li>
                            {/foreach}
                          </ul>
                        </li>
                        <li><a href="?secao=institucional">SOBRE NÓS</a></li>
                        <li><a href="?secao=colecao">COLEÇÃO</a></li>
                        <li><a href="?secao=revender">REVENDER</a></li>
                        <!--<li><a href="#">BLOG</a></li>-->
                        <li><a href="?secao=contato">CONTATO</a></li>
                      </ul>
                      
                    <!--Inicio pesquisa-->
                    {include file=$pesquisa}
                    <!--Fim pesquisa-->
                      
                    </div>
                  </div>
                </nav>
        
            </div>
            
            
            <!--Inicio meio-->
            {include file=$templateCentro}
            <!--Fim meio-->
             
          
          	<!--Inicio rodape-->
            {include file=$rodape}
            <!--Fim rodape-->
          
           
            
        </div><!-- fim content-->
    </div>
  	
      <!-- /END THE FEATURETTES -->

	
    
    

  </body>
</html>