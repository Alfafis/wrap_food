<div class="box_secundarias">
            	<div class="titulo_secoes"><img src="img/marcador_tit.png" > COMPRE COM SEGURANÇA</div>
                
                <div class="row">
                    <div class="col-md-12">					
                        
                        <div id="borda_secundarias">
                            <div class="caixa_botoes_sec">
                                <div class="botoes_sec {if $opcao== 'como_comprar'}botoes_sec_ativo{/if}" id="btnbloco_1" onClick="abrir_bloco(1,4)">COMO COMPRAR</div>
                                <div class="botoes_sec duas_linhas {if $opcao== 'politica'}botoes_sec_ativo{/if}" id="btnbloco_2" onClick="abrir_bloco(2,4)">POLÍTICA DE PRIVACIDADE E SEGURANÇA</div>
                                <div class="botoes_sec {if $opcao== 'trocas'}botoes_sec_ativo{/if}" id="btnbloco_3" onClick="abrir_bloco(3,4)">TROCAS E DEVOLUÇÕES</div>
                                <div class="botoes_sec {if $opcao== 'atendimento'}botoes_sec_ativo{/if}" id="btnbloco_4" onClick="abrir_bloco(4,4)">ATENDIMENTO</div>
                            </div>
                            <div class="caixa_conteudo_sec">
                            	<div id="bloco1" {if $opcao== 'como_comprar'}style="display:block;"{else}style="display:none;"{/if}>
                                    <div class="texto_conteudo_sec">
                                        {$textos.como_comprar}
                                    </div>
                                </div>
                                <div id="bloco2" {if $opcao== 'politica'}style="display:block;"{else}style="display:none;"{/if}>
                                    <div class="texto_conteudo_sec">
                                        {$textos.politica}
                                    </div>
                                </div>
                                <div id="bloco3" {if $opcao== 'trocas'}style="display:block;"{else}style="display:none;"{/if}>
                                    <div class="texto_conteudo_sec">
                                        {$textos.trocas}
                                    </div>
                                </div>
                                <div id="bloco4" {if $opcao== 'atendimento'}style="display:block;"{else}style="display:none;"{/if}>
                                    <div class="texto_conteudo_sec">
                                        {$textos.atendimento}
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        
                    </div>
                </div>
                
            </div>