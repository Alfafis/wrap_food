{literal}
<style type="text/css">
.ui-dialog .ui-dialog-buttonpane{
text-align:center;	
}
.ui-dialog .ui-dialog-buttonpane button{
float:none;
text-align:center;	
}
</style>
{/literal}
<div id="container_swip">
	<br /><br /><br />
  	
    <form id="form_pedido_produto" method="post" action="" name="form_pedido_produto">
    
    {if count($listaProdutoCarrinho)>0}
    <center>ESTES SÃO OS ITENS QUE VOCÊ SELECIONOU PARA COMPRA, CONFIRA ABAIXO:</center>
    <br />
    <div class="row">
    
        <div class="col button button-assertive" style="margin:1%;line-height:normal;" onClick="location.href='index.php?secao=pedidoProduto&opcao=listarPedidoProdutoSecao'"> 
            <span class="text-sm">ALTERAR PEDIDO</span> 
        </div>
        <br />
        <div class="col button button-assertive" style="margin:1%;line-height:normal;" onClick="location.href='index.php?secao=produtos'"> 
            <span class="text-sm">CONTINUAR COMPRANDO</span> 
        </div>
    </div>
    {/if}
    
    {if count($listaProdutoCarrinho)>0}
    <div class="list">
    
    {foreach from=$listaProdutoCarrinho  item=produtoCarrinho}
    
    <input type='hidden'name='listaProdutoQtd[]' id="qtde{$produtoCarrinho.contadorItem}" value='{$produtoCarrinho.qtde}'>
    <input type='hidden' name='listaProdutoContador[]' id="cont{$produtoCarrinho.contadorItem}" value='{$produtoCarrinho.contadorItem}'>
    
    <div class="item-thumbnail-left card item item-complex item-right-editable">
    
        <div class="item-content" id="aparecer_delete">
        
            <img src="fotos/{$produtoCarrinho.fotoProduto}">
            <h2 class="ng-binding">{$produtoCarrinho.nomeProduto}</h2>
            
            <div>
            <h2 class="price pull-left assertive ng-binding" style="line-height:normal;">R$ {$produtoCarrinho.precoProduto|replace:".":","} 
            <span class="dark ng-binding">x <span id="txtqtde{$produtoCarrinho.contadorItem}">{$produtoCarrinho.qtde}</span></span><br />
            {if $produtoCarrinho.idPao != 3}
                <span style="font-size:11px;color:#333;line-height:normal;">Pão: {$produtoCarrinho.nomePao}</span><br />
                <span style="font-size:11px;color:#333;line-height:normal;">
                {if $produtoCarrinho.nomeDoce == ""}
                Queijo: {if $produtoCarrinho.nomeQueijo != ""}{$produtoCarrinho.nomeQueijo}{/if}
                {else}
                Doce: {if $produtoCarrinho.nomeDoce != ""}{$produtoCarrinho.nomeDoce}{/if}
                {/if}
                <!--{if $produtoCarrinho.nomeSalada != ""}, {$produtoCarrinho.nomeSalada}{/if}
                {if $produtoCarrinho.nomeMolho != ""}, {$produtoCarrinho.nomeMolho}{/if}
                {if $produtoCarrinho.nomeLivre != ""}, {$produtoCarrinho.nomeLivre}{/if}-->
                </span>
                {if $produtoCarrinho.nomeSuco != ""}<br /><span style="font-size:11px;color:#333;line-height:normal;">Suco: {$produtoCarrinho.nomeSuco} - R$ {$produtoCarrinho.precoSuco|replace:".":","}</span>{/if}
            {else}
                <span style="font-size:11px;color:#333;line-height:normal;">
                <strong>Adicionais:</strong><br />{$produtoCarrinho.nomeSuco}
                <br /><strong>Total Adicionais:</strong> R$ {$produtoCarrinho.precoSuco|replace:".":","}</span>
            {/if}
            </h2>
                        
            </div>
        
        </div>
        
    
    </div>
    {/foreach}
    
    {if $desconto_cupons == "sim"}
    <div class="item-thumbnail-left card item item-complex item-right-editable">
    
        <div class="item-content" id="aparecer_delete">
        
            <img src="img/logo.png">
            <h2 class="ng-binding">Desconto de {$desconto}%</h2>
            
            <div>
            <h2 class="price pull-left assertive ng-binding" style="line-height:normal;">
            	<span style="font-size:11px;color:#333;line-height:normal;">Desconto por ter completado {$ticket} cupons.</span><br />
            	R$ {$descontoTotal|replace:".":","} 
            </h2>
                        
            </div>
        
        </div>
        
    
    </div>
    {/if}
    
    <p style="text-align:center;">Horário agendado para buscar: <strong>{$horario}h</strong></p>
    
    </div>
    {/if}
    </form>
    <center><img src="img/pagamentos.gif" title="Este site aceita pagamentos com Visa, MasterCard, Diners, American Express, Hipercard, Aura, Bradesco, Itaú, Unibanco, Banco do Brasil, Banco Real, saldo em conta PagSeguro e boleto." border="0" width="80%"></center>
  <br /><br /><br />
  <div class="clearfix"></div>    
    
  <ion-footer-bar class="bar-assertive bar bar-footer" onClick="definir_pagamento()">
    <div class="button">FINALIZAR COMPRA</div>
    <div class="title" style="left: 111px; right: 111px;"></div>
    <div class="button text-2x ng-binding">
    R$ {if $valorTotalPedido}{$valorTotalPedido|replace:".":","}{else}0,00{/if}
    </div>
  </ion-footer-bar>
  
</div>

