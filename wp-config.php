<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wrapb782_WPL5R');

/** MySQL database username */
define('DB_USER', 'wrapb782_WPL5R');

/** MySQL database password */
define('DB_PASSWORD', 'OLihnBi9ZVwxSx3i1');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', '2129e01f619c0242e6d71ca0c4ae6a6ce11a9a42574044c296f78f64e0dbf22c');
define('SECURE_AUTH_KEY', '66823fd4053ce1c4289a52c2bcb9bb48ca32edd1646a82c89c0be0289abe5314');
define('LOGGED_IN_KEY', '8dd8a75a140bcfbc2e152bf15d158af6c36d520bd7524d8fdb40972c4b396f7a');
define('NONCE_KEY', 'dd1fb161476f9236a0f9e717f0031900f14032f7113192a876c8239376b3c761');
define('AUTH_SALT', 'c508ad74ec5dfe73531388f8614289b34f5c4b436cf06a9662bbb4d96334b468');
define('SECURE_AUTH_SALT', '36bcead3bce562159392e02cebd355994806bbf36348330ee80e4daef7b6470d');
define('LOGGED_IN_SALT', '2e3ff8b1cfd18e247e32290aeac213505e3fae1e4841e2b4e1fe18f455e1cab5');
define('NONCE_SALT', '54efcd5bb7926062423545cb60fc94b495b967aab3835d9b771984b8e7f8874f');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = '_L5R_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

define( 'WP_CRON_LOCK_TIMEOUT', 120   ); 
define( 'AUTOSAVE_INTERVAL',    300   );
define( 'WP_POST_REVISIONS',    5     );
define( 'EMPTY_TRASH_DAYS',     7     );
define( 'WP_AUTO_UPDATE_CORE',  true  );


